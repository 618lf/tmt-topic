<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><sitemesh:title/> - Powered By TMT</title>
		<link   href="${ctxStatic}/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/bootstrap/3.3.0/css/bootstrap-theme.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
		<script src="${ctxStatic}/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
		<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
		<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
		<link href="${ctxStatic}/common/common-ui.css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-form/jquery.select2.min.js" type="text/javascript"></script>
		<link href="${ctxStatic}/common/common-3.0.css" rel="stylesheet" />
		<sitemesh:head/>
	</head>
	<body>
	    <sitemesh:body/>
	</body>
</html>
