<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var typesFmt = function(cellvalue, options, rowObject) {
			if(cellvalue==1) { return "访问";}
			return "异常";
		};
		var optionsFmt = function (cellvalue, options, rowObject) {
			return Public.billsOper(cellvalue, options, rowObject);
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/weixin/fans/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!0, 
				rownumbers: !0,//序号
				multiselect:true,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', '粉丝昵称', '所在省', '所在市', '是否关注', '关注日期', '最后活动日期', '未活动时长',' '],
				colModel: [
                    {name:'id', index:'id', width:80,sortable:false,hidden:true},
	                {name:'nickName', index:'nickName', width:120, align:'center', sortable:false},
	                {name:'province', index:'province', width:150,sortable:false},
	                {name:'city', index:'city', width:150,sortable:false},
	                {name:'subscribe', index:'subscribe', width:150,sortable:false},
	                {name:'subscribeDate', index:'subscribeDate', width:150,sortable:false},
	                {name:'lastActiveDate', index:'lastActiveDate', width:150,sortable:false},
	                {name:'idleTime', index:'idleTime', width:150,sortable:false},
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		var delBusinessType = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
				Public.deletex("确定删除选中的业务类型？","${ctx}/platform/businessType/delete",param,function(data){
					if(!!data.success) {
						Public.success('删除业务类型成功');
						Public.doQuery();
					} else {
						Public.error(data.msg);
					}
				});
			} else {
				Public.error("请选择要删除的业务类型!");
			}
		}
		$('#dataGrid').on('click','.edit',function(e){
			window.location.href = "${ctx}/platform/businessType/form?id="+$(this).attr('data-id');
		});
		$('#dataGrid').on('click','.delete',function(e){
			delBusinessType($(this).attr('data-id'));
		});
		$(document).on('click','#addBtn',function(e){
			window.location.href = "${ctx}/platform/businessType/form";
		});
		$(document).on('click','#delBtn',function(e){
			var checkeds = $('#grid').getGridParam('selarrrow');
			delBusinessType(checkeds);
		});
		//导入
		$(document).on('click','#impBtn',function(e){
			Public.openImportWindow($("#impDiv").val(), "#impForm", '${ctx}/platform/businessType/doImport', '导入业务类型');
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
		<div class="top">
		    <form name="queryForm" id="queryForm">
				<div class="fl">
				  <div class="ui-btn-menu">
				      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
				         <strong>点击查询</strong><b></b>
				      </span>
				      <div class="dropdown-menu" style="width: 320px;">
				           <div class="control-group formSep">
								<label class="control-label">粉丝昵称:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="nickName"/>
								</div>
						   </div>
					       <div class="ui-btns"> 
				              <input class="btn btn-primary query" type="button" value="查询"/>
				              <input class="btn reset" type="button" value="重置"/>
				           </div> 
				      </div>
				  </div>
				  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
				</div>
				<div class="fr">
				   <input type="button" class="btn btn-primary" id="delBtn" value="&nbsp;删&nbsp;除&nbsp;">&nbsp;
				</div>
			</form>
		</div>
	</div>
	<div id="dataGrid" class="autoGrid">
		<table id="grid"></table>
		<div id="page"></div>
	</div> 
</div>
<textarea id="impDiv" style="display: none;">
   <div class="row-fluid">
   <form id="impForm" method="post" class="form-horizontal" enctype="multipart/form-data">
      <tags:token/>
      <div class="control-group formSep">
		<label class="control-label">选择文件(*.xls):</label>
		<div class="controls">
		     <input type="file" name="file" class="required selectFile"/>
		</div>
	  </div>
	  <div class="control-group formSep">
		<label class="control-label">选择模版:</label>
		<div class="controls">
		     <select id="templateId" name="templateId">
		         <c:forEach items="${templates}" var="template">
			    	 <option value="${template.id}">${template.name}</option>
			     </c:forEach>
		     </select>
		</div>
	  </div>
   </form>
   </div>
</textarea>
<iframe name="importIframeObj" src="" width="0" height="0" style="display: none;" class="importIframeObj"></iframe>
</body>
</html>