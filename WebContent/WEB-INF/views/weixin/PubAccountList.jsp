<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list3"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.addEvent();
	},
	addEvent : function(){
		//创建索引
		$(document).on('click','#indexBtn',function(e){
			Public.executex('确定测试?','${ctx}/wechat/menu/testMenu',null,function(data){
				if(data.success) {
					Public.success('测试成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
</style>
</head>
<body style="overflow: hidden;" class="white">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
         <div class="top">
               <form name="queryForm" id="queryForm">
                   <div class="fl">
                   </div>
                   <div class="fr">
					   <input type="button" id="addBtn" class="btn btn-primary" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" id="addBtn" class="btn btn-default" value="&nbsp;删&nbsp;除&nbsp;">
					</div>
               </form>
         </div>
    </div>
	<div class="wrapper-content">
	    <c:forEach items="${accounts}" var="account">
		    <div class="row">
		       <div class="col-xs-12">
		          <div class="bs-callout <c:if test="${account.type eq '服务号'}">bs-callout-danger</c:if><c:if test="${not(account.type eq '服务号')}">bs-callout-info</c:if>">
		               <h4>${account.name}&nbsp;&nbsp;<span class="label label-primary">${account.type}</span>&nbsp;&nbsp;<span class="label label-default">${account.authenState}</span></h4>
		               <p>登录邮箱:${account.email}</p>
		               <p>主体信息:${account.principal}</p>
		               <p>地址:${account.address}</p>
		               <p>二维码:${account.qrCode}</p>
		          </div>
		       </div>
		    </div>
	    </c:forEach>
	</div>
</div>
</body>
</html>