<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.addEvent();
	},
	addEvent : function(){
		//创建索引
		$(document).on('click','#indexBtn',function(e){
			Public.executex('确定测试?','${ctx}/wechat/menu/testMenu',null,function(data){
				if(data.success) {
					Public.success('测试成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
}
</style>
</head>
<body style="overflow: hidden;">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
		<div class="top">
		    <form name="queryForm" id="queryForm">
				<div class="fl">
				</div>
				<div class="fr">
				   <input type="button" class="btn btn-success" id="indexBtn" value="&nbsp;测试&nbsp;">&nbsp;
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>