<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
  String errorMsg = null;
  if("com.tmt.base.system.security.CaptchaException".equals(error)) {
	  errorMsg = "验证码错误.";
  } else if("com.tmt.base.system.security.AccountLockException".equals(error)) {
	  errorMsg = "用户被锁定,请联系管理员.";
  } else {
	  errorMsg = "用户不存在.";
  }
%>
<!DOCTYPE html>
<html>
	<head>
		<title>税务公社登录</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
		<script src="${ctxStatic}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
		<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet"/>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
		<link   href="${ctxStatic}/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#loginForm").validate({
					messages: {
						username: {required: "请填写电子邮箱."},
						password: {required: "请填写密码."}
					},
					errorPlacement: function(error, element) {
						$(element).toggleClass('text_error');
					} 
				});
			});
		</script>
		<style type="text/css">
		  .login{
		     position: relative;
		     margin: auto;
		     padding: 8% 0 0;
		     float: none;
		     border-radius: 2px;
		  }
		  #loginForm{
		    background: #fff;
		    padding: 10px 15px 30px 15px;
		  }
		  #loginForm h2{
		     text-align: center;
		     margin-bottom: 40px;
		  }
		  #loginBtn{
		     width: 100%;
		     line-height: 34px;
		     border-radius: 0px;
		  }
		  #username, #password{
		     border-radius: 0px;
		     padding: 20px 12px;
		  }
		  .form-horizontal .form-group{
		     margin-right: 0px;
             margin-left: 0px;  
             margin-bottom: 20px;
		  }
		  .text_error{
		    border-color: red;
		  }
		</style>
	</head>
	<body style="background: #f5f5f5; ">
	    <div class="login col-sm-4">
		    <div id="messageBox"></div>
			<form id="loginForm" class="form-horizontal" action="${ctx}/login" method="post">
			    <h2>税务公社授权登录</h2>
			    <div class="form-group">
				    <div class="input-group">
					  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					  <input type="text" class="form-control required" id="username" name="username" placeholder="Email" value="${username}">
					</div>
			    </div>
			    <div class="form-group">
				    <div class="input-group">
					  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					  <input type="password" class="form-control required" id="password" name="password" placeholder="Password" value="${password}">
					</div>
				</div>
				<div class="form-group">
				    <input id="loginBtn" type="submit" class="btn btn-primary" value="授权登录"/>
				</div>
			</form>
		</div>
	</body>
</html>