<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title><sitemesh:title/> - Powered By TMT</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="renderer" content="webkit">
		<script src="${ctxStatic}/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
		<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet"/>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
		<link   href="${ctxStatic}/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/common/common-mobile.css" rel="stylesheet"/>
		<sitemesh:head/>
	</head>
	<body class="white">
		<div id="header" class="navbar navbar-default navbar-fixed-top" role="navigation">
		    <div class="container-fluid">
			   <ul class="nav pull-right">
				  <li class="active"><a href="#">欢迎：<sys:user/></a></li>
			   </ul>
		    </div>
		</div>
		<div id="main" class="container-fluid" style="margin-top: 50px;">
			<sitemesh:body/>
		</div>
		<div id="footer" class="navbar navbar-default navbar-fixed-bottom" role="navigation">
            <div class="container-fluid">
			   <ul class="nav  footer-items four-items dropup">
			       <li><a href="#"><i class="fa fa-home"></i>首页</a></li>
			       <li><a href="#"><i class="fa fa-briefcase"></i>攻擂</a></li>
			       <li><a href="#"><i class="fa fa-comments"></i>商铺</a></li>
			       <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>我<span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="#"><i class="fa fa-user"></i>我的账户</a></li>
			            <li><a href="#"><i class="fa fa-graduation-cap"></i>我的战绩</a></li>
			            <li><a href="#"><i class="fa fa-comments"></i>我的建议</a></li>
			            <li><a href="#"><i class="fa fa-exclamation-circle"></i>我的错题</a></li>
			          </ul>
			       </li>
			   </ul>
		    </div>
        </div>
	</body>
</html>