<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>404</title>
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script type="text/javascript">
</script>
</head>
<body>
<div class="row-fluid wrapper">
    <div class="bills" style="width: 93%;height: 90%;">
	    <div class="page-header">
			<h3><span class="icon32 icon-color icon-info"></span>&nbsp;页面不存在</h3>
		</div>
		<h3 class="lighter smaller">
		    错误访问URL：${pageContext.errorData.requestURI}
		</h3>
		<hr>
		<div class="space"></div>
		<div class="tc"></div>
	</div>
</div>
</body>
</html>