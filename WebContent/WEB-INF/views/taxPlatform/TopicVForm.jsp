<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ taglib prefix="fnt" uri="/WEB-INF/tld/fns-tax.tld"%>  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>试题--命题</title>
<meta name="decorator" content="topicForm"/>
<script type="text/javascript">
$(function(){
	THISPAGE._init();
	$(document).on('click', '#finishBtn', function(e){
		Public.confirmx("完成初审,进入试题终审阶段,确认完成初审?",function(){
			$('#optionType').val('FINALLY_VERITY');
			$('#inputForm').submit();
		});
	});
});
</script>
</head>
<body>
<div class="page-header">
	<h3>试题<small> &gt;&gt; 初审 </small></h3>
	<div class="page-toolbar no-border">
	  <ul class="nav nav-tabs" id="recent-tab">
			<li><a data-toggle="tab" href="javascript:void(0)" data-type='INIT'>命题</a></li>
			<li class="active"><a data-toggle="tab" href="javascript:void(0)" data-type='INIT_VERITY'>初审</a></li>
			<c:if test="${topic.type == 'FINALLY_VERITY' || topic.type == 'FINALLY'}">
			<li><a data-toggle="tab" href="javascript:void(0)" data-type='FINALLY_VERITY'>终审</a></li>
			</c:if>
	  </ul>
	</div>
</div>
</body>
</html>