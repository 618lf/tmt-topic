<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script src="${ctxModules}/taxPlatform/topic_common.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common-file.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/common-file.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/template-native.js" type="text/javascript" ></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			return Public.billsOper(cellvalue, options, rowObject);
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/platform/topic/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!0, 
				rownumbers: !0,//序号
				multiselect:true,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', '试题编码', '题型', '难度', '来源', '题干', '命题人员', '命题时间', '状态', '税种', '税种明细', ' '],
				colModel: [
                    {name:'id', index:'id', width:80,sortable:false,hidden:true},
	                {name:'code', index:'code', width:120, align:'center', sortable:false},
	                {name:'qtypeStr', index:'qtypeStr', width:100,align:'center',sortable:false},
	                {name:'qlevelStr', index:'qlevelStr', width:80,align:'center',sortable:false},
	                {name:'qfromStr', index:'qfromStr', width:80,align:'center',sortable:false},
	                {name:'content', index:'content', width:150,sortable:false},
	                {name:'createName', index:'createName', width:100,align:'center',sortable:false},
	                {name:'createDate', index:'createDate', width:120,sortable:false},
	                {name:'type', index:'type', width:80,align:'center',sortable:false,formatter:Topic.typesFmt},
	                {name:'categorys', index:'categorys', width:80,sortable:false},
					{name:'categoryItems', index:'categoryItems', width:80,sortable:false},
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:Topic.optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		var delTopic = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var id = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:id});
					});
					param.push({name:'optionType',value:'INIT'});
				} else {
					param.push({name:'idList',value:checkeds});
					param.push({name:'optionType',value:'DEL'});
				}
				Public.deletex("确定删除选中的试题？","${ctx}/platform/topic/delete",param,function(data){
					if(!!data.success) {
						Public.success('删除试题成功');
						Public.doQuery();
					} else {
						Public.error(data.msg);
					}
				});
			} else {
				Public.error("请选择要删除的试题!");
			}
		}
		$('#dataGrid').on('click','.edit',function(e){
			var param  = 'id='+$(this).data('id');
			    param += '&optionType=INIT'; 
        	Public.openOnTab('topic-edit', '试题编制',"${ctx}/platform/topic/form?"+param);
		});
		$('#dataGrid').on('click','.delete',function(e){
			delTopic($(this).attr('data-id'));
		});
		$(document).on('click','#qaAdd',function(e){
			Public.openOnTab('qa-topic-add', '答疑汇编-编制试题',"${ctx}/platform/qa/initTopicMake");
		});
		$(document).on('click','#cusAdd',function(e){
			var param  = 'id=-1&optionType=INIT'
	    	Public.openOnTab('topic-edit', '试题编制',"${ctx}/platform/topic/form?"+param);
		});
		$(document).on('click','#delBtn',function(e){
			var checkeds = $('#grid').getGridParam('selarrrow');
			delTopic(checkeds);
		});
		//导入
		$(document).on('click','#impBtn',function(e){
			Public.openImportWindow($("#impDiv").val(), "#impForm", '${ctx}/platform/topic/doImport', '导入题库');
		});
		
		//格式化显示
		Public.combo('qType');
		Public.combo('qLevel');
		Public.combo('qFrom');
		Public.combo('type');
		//人员
		Public.autoCombo('createId', "${ctx}/system/user/jSonList");
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
		<div class="top">
		    <form name="queryForm" id="queryForm">
		        <input type="hidden" value="INIT" name="optionType" id="optionType"/>
				<div class="fl">
				  <div class="ui-btn-menu">
				      <span class="ui-btn ui-menu-btn" style='vertical-align: middle;'>
				         <strong>更多查询</strong><b></b>
				      </span>
				      <div class="dropdown-menu" style="width: 320px;">
				           <div class="control-group formSep">
								<label class="control-label">题型:</label>
								<div class="controls">
									<select name="qType" id="qType">
									     <option value=""></option>
									     <option value="ONE_CHOICE">单项选择题</option>
									     <option value="MULT_CHOICE">多项选择题</option>
									     <option value="ONE_MORE_CHOICE">不定项选择题</option>
									     <option value="TFNG">判断题</option>
									     <option value="COUNTING">计算题</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">难度:</label>
								<div class="controls">
									<select name="qLevel" id="qLevel">
									     <option value=""></option>
									     <option value="I">容易</option>
									     <option value="II">一般</option>
									     <option value="III">很难</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">来源:</label>
								<div class="controls">
									<select name="qFrom" id="qFrom">
									     <option value=""></option>
									     <option value="QA">答疑汇编</option>
									     <option value="EXAM">考试汇编</option>
									     <option value="CUSTOM">自定义</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">题干:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="name"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">状态:</label>
								<div class="controls">
									<select name="type" id="type">
									     <option value=""></option>
									     <option value="INIT">命题</option>
									     <option value="INIT_VERITY">初审</option>
									     <option value="FINALLY_VERITY">终审</option>
									     <option value="FINALLY">完成</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">税种:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="categoryId" name="categoryId"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">税种明细:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="categoryItemId" name="categoryItemId"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">命题人:</label>
								<div class="controls">
									<input type='text' id='createId' name='createId' maxlength="64" class="required"/>
								</div>
						   </div>
					       <div class="ui-btns"> 
				              <input class="btn btn-primary query" type="button" value="查询"/>
				              <input class="btn reset" type="button" value="重置"/>
				           </div> 
				      </div>
				  </div>
				  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()"/>
				  <!--  
				  <div class="btn-group">
					  <button class="btn">命题</button>
					  <button class="btn">初审</button>
					  <button class="btn">终审</button>
					  <button class="btn">完成</button>
				  </div>-->
				</div>
				<div class="fr">
				   <input type="button" class="btn btn-primary" id="qaAdd" value="&nbsp;答疑汇编编制&nbsp;"/>&nbsp;
				   <input type="button" class="btn btn-primary disabled" value="&nbsp;考试汇编编制&nbsp;" disabled="disabled"/>&nbsp;
				   <input type="button" class="btn btn-success" id="cusAdd" value="&nbsp;自定义&nbsp;"/>&nbsp;
				   <input type="button" class="btn" id="impBtn" value="&nbsp;导入&nbsp;"/>&nbsp;
				   <input type="button" class="btn" id="delBtn" value="&nbsp;删&nbsp;除&nbsp;"/>&nbsp;
				</div>
			</form>
		</div>
	</div>
	<div id="dataGrid" class="autoGrid">
		<table id="grid"></table>
		<div id="page"></div>
	</div> 
</div>
<textarea id="impDiv" style="display: none;">
   <div class="row-fluid">
   <form id="impForm" method="post" class="form-horizontal" enctype="multipart/form-data">
      <tags:token/>
      <div class="control-group formSep">
		<label class="control-label">选择文件(*.xls):</label>
		<div class="controls">
		     <input type="file" name="file" class="required selectFile"/>
		</div>
	  </div>
	  <div class="control-group formSep">
		<label class="control-label">选择模版:</label>
		<div class="controls">
		     <select id="templateId" name="templateId" class='required'>
		         <c:forEach items="${templates}" var="template">
			    	 <option value="${template.id}">${template.name}</option>
			     </c:forEach>
		     </select>
		</div>
	  </div>
   </form>
   </div>
</textarea>
</body>
</html>