<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script src="${ctxStatic}/common/template-native.js" type="text/javascript" ></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.addEvent();
	},
	searchPage : function(pageNo, pageSize){
		$('#resbills').show();
		if( !!pageNo) {
			$('#pageIndex').val(pageNo);
		}
		if( !!pageSize) {
			$('#pageSize').val(pageSize);
		}
		var form = $('#searchForm');
		Public.loading("法规检索中...");
		Public.ajaxSubmit(form, '${ctx}/platform/taxdoc/search', function(){
			return true;
		},function(data){
			Public.loaded();
			var datas = data.data;
			if(datas != null && datas.length != 0) {
				var text = [];
				for(var i = 0, j = datas.length; i < j; i++ ) {
					text.push('<div class="search-item clearfix">');
					text.push('<span class="searchNb">'+(i+1)+'</span>');
					text.push('<div class="search-content">');
					text.push('<h4><a href="javascript:void(0)" class="sepV_a">'+datas[i].docName+'</a></h4>');
					text.push('<p class="sepH_b item_description">'+datas[i].docContent+'</p>');
					text.push('<p class="sepH_a">'+datas[i].docNum+'</p>');
					text.push('<small>'+datas[i].categorys+'</small>');
					text.push('</div></div>');
				}
				$('#searchRes').html(text.join(''));
				$('#paginationT').html(data.param.simplePagination);
				$('#paginationB').html(data.param.simplePagination);
			} else {
				Public.error("检索失败");
			}
		})
	},
	addEvent : function(){
		var that = this;
		$(document).on('click','.search-btn',function(){
			that.searchPage();
		});
		var value = '${query}';
		if(!!value) {
			that.searchPage();
		} else {
			$('#resbills').hide();
		}
	}
};
$(function(){
	THISPAGE._init();
});
//分页
var page = function(pageNo, pageSize){
	THISPAGE.searchPage(pageNo, pageSize);
}
</script>
<style type="text/css">
  p.MsoNormal{
    margin: 10px;
  }
</style>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	       <form class="form-search" action="${ctx}/platform/taxdoc/search" method="post" id="searchForm">
		       <input type="hidden" name="param.pageIndex" id="pageIndex" value="1"/>
		       <input type="hidden" name="param.pageSize" id="pageSize" value="20"/>
		       <div class="input-append">
			    <input type="text" name="query" class="search-query" value="${query}" placeholder="查找法规..."/>
			    <input type="button" class="btn search-btn" value="查找"/>
			   </div>
	       </form>
	   </div>
	   <div class="bills" id="resbills">
	       <div id="paginationT" class="pagination"></div>
	       <div class="search-panel clearfix" id='searchRes'></div>
	       <div id="paginationB" class="pagination"></div>
	   </div>
	</div>
</body>
</html>