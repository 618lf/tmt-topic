<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common-file.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/common-file.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/template-native.js" type="text/javascript" ></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			return Public.billsOper(cellvalue, options, rowObject);
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/platform/taxdoc/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!0,
				rownumbers: !0,//序号
				multiselect:true,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', '标题', '文号', '涉及税种', '效力', '发文日期', '发文机关', ' '],
				colModel: [
                    {name:'id', index:'id', width:80,sortable:false,hidden:true},
	                {name:'docName', index:'docName', width:330,sortable:false},
	                {name:'docNum', index:'docNum', width:150,sortable:false},
	                {name:'categorys', index:'categorys', width:150,sortable:false},
	                {name:'enabled', index:'enabled', width:100,sortable:false},
	                {name:'pubDate', index:'pubDate',align:'center',width:100,sortable:false},
	                {name:'pubOffice', index:'pubOffice', width:150,sortable:false},
					{name:'options', index:'options',align:'center',width:120,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		//编辑
		$('#dataGrid').on('click','.edit',function(e){
			Public.openOnTab('doc-edit', '税收法规详情',"${ctx}/platform/taxdoc/form?id="+$(this).attr('data-id'));
		});
		//获取网页内容
		$(document).on('click','#fetchBtn',function(e){
			var param = [];
			var checkeds = $('#grid').getGridParam('selarrrow');
			if( !!checkeds && checkeds.length ) {
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
			}
			var title = param.length == 0?"全部":"选定";
			Public.executex('确定获取' + title + "法规的内容?",'${ctx}/platform/taxdoc/fetchContent',param,function(data){
				if(data.success) {
					Public.success('获取页面内容成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		});
		
		//创建索引
		$(document).on('click','#indexBtn',function(e){
			var param = [];
			var checkeds = $('#grid').getGridParam('selarrrow');
			if( !!checkeds && checkeds.length ) {
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
			}
			var title = param.length == 0?"全部":"选定";
			Public.executex('确定创建' + title + "法规的检索索引?",'${ctx}/platform/taxdoc/createIndex',param,function(data){
				if(data.success) {
					Public.success('创建索引成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
}
</style>
</head>
<body style="overflow: hidden;">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
		<div class="top">
		    <form name="queryForm" id="queryForm">
				<div class="fl">
				  <div class="ui-btn-menu">
				      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
				         <strong>点击查询</strong><b></b>
				      </span>
				      <div class="dropdown-menu" style="width: 320px;">
				           <div class="control-group formSep">
								<label class="control-label">标题:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="docName" value="${docName}"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">字号:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="docNum" value="${docNum}"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">发文机关:</label>
								<div class="controls">
									<select name="pubOffice" id="pubOffice">
									    <option value=""></option>
									    <option value="总局法规">总局法规</option>
									    <option value="地方法规">地方法规</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">文件类型:</label>
								<div class="controls">
									<select name="docType">
									   <option value=""></option>
									   <optgroup label="法律级次">
									       <option value="法律">法律</option>
									       <option value="行政法规">行政法规</option>
									       <option value="部门规章">部门规章</option>
									       <option value="地方法规">地方法规</option>
									       <option value="地方规章">地方规章</option>
									       <option value="规范性文件">规范性文件</option>
									       <option value="税收协定">税收协定</option>
									   </optgroup>
									   <optgroup label="优惠政策">
									       <option value="就业再就业">就业再就业</option>
									       <option value="高新技术、软件开发、集成电路产业">高新技术、软件开发、集成电路产业</option>
									       <option value="农副产品加工业和农业产业化">农副产品加工业和农业产业化</option>
									       <option value="节能减排和环境保护">节能减排和环境保护</option>
									       <option value="金融保险证券">金融保险证券</option>
									       <option value="文教卫生体育">文教卫生体育</option>
									       <option value="民政福利与灾害减免">民政福利与灾害减免</option>
									       <option value="区域经济发展优惠">区域经济发展优惠</option>
									       <option value="其他税收优惠">其他税收优惠</option>
									   </optgroup>
									   <optgroup label="税种类别">
									       <option value="增值税">增值税</option>
									       <option value="消费税">消费税</option>
									       <option value="营业税">营业税</option>
									       <option value="企业所得税">企业所得税</option>
									       <option value="个人所得税">个人所得税</option>
									       <option value="资源税">资源税</option>
									       <option value="城市维护建设税">城市维护建设税</option>
									       <option value="房产税">房产税</option>
									       <option value="印花税">印花税</option>
									       <option value="城镇土地使用税">城镇土地使用税</option>
									       <option value="土地增值税">土地增值税</option>
									       <option value="车船税">车船税</option>
									       <option value="车辆购置税">车辆购置税</option>
									       <option value="烟叶税">烟叶税</option>
									       <option value="耕地占用税">耕地占用税</option>
									       <option value="契税">契税</option>
									       <option value="进出口税收">进出口税收</option>
									       <option value="有关规费">有关规费</option>
									   </optgroup>
									   <optgroup label="征收管理">
									       <option value="税务登记">税务登记</option>
									       <option value="账簿凭证管理">账簿凭证管理</option>
									       <option value="纳税申报">纳税申报</option>
									       <option value="税款征收">税款征收</option>
									       <option value="增值税专用发票管理">增值税专用发票管理</option>
									       <option value="普通发票管理">普通发票管理</option>
									       <option value="税务检查">税务检查</option>
									       <option value="税务代理">税务代理</option>
									       <option value="其他">其他</option>
									   </optgroup>
									   <optgroup label="适用对象">
									       <option value="个人">个人</option>
									       <option value="企业">企业</option>
									   </optgroup>
									   <optgroup label="法律救济">
									       <option value="税务行政复议">税务行政复议</option>
									       <option value="税务行政诉讼">税务行政诉讼</option>
									       <option value="税务行政赔偿">税务行政赔偿</option>
									   </optgroup>
									   <option value="失效法规">失效法规</option>
									   <option value="涉税法律">涉税法律</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">涉及税种:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="categorys"/>
								</div>
						   </div>
					       <div class="ui-btns"> 
				              <input class="btn btn-primary query" type="button" value="查询"/>
				              <input class="btn reset" type="button" value="重置"/>
				           </div>
				      </div>
				  </div>
				  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
				</div>
				<div class="fr">
				   <input type="button" class="btn btn-primary" id="fetchBtn" value="&nbsp;获取法规内容&nbsp;">&nbsp;
				   <input type="button" class="btn btn-success" id="indexBtn" value="&nbsp;创建索引&nbsp;">&nbsp;
				   <input type="button" class="btn" id="delBtn" value="&nbsp;删&nbsp;除&nbsp;">&nbsp;
				</div>
			</form>
		</div>
	</div>
	<div id="dataGrid" class="autoGrid">
		<table id="grid"></table>
		<div id="page"></div>
	</div> 
</div>
</body>
</html>