<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#inputForm").validate(
			Public.validate({
				//使用 ajax 提交
				submitHandler: function(form){
					var that = this;
					Public.ajaxSubmit(form, '${ctx}/platform/topicRela/save', null, function(data){
						if( !!THISPAGE.callback ) {
							THISPAGE.callback(data,that);
						}
					});
				}
			})
		);
	},
	addEvent : function(){
		Public.combo('applyCity');
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
	},
	submit : function(param, callback){
		this.callback = callback;
		$('#inputForm').submit();
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
 body{
    background-color: #fff;
 }
</style>
</head>
<body>
   <div class="row-fluid wrapper white">
		<form:form id="inputForm" modelAttribute="topicRela" action="${ctx}/platform/topicRela/save" method="post" class="form-horizontal  form-fluid form-min">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="topicId"/>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">税种<font color="red">*</font>:</label>
				<div class="controls">
				    <input type='text' id='categoryId' name='categoryId' maxlength="64" class="required"
				           value='${topicRela.categoryId}' data-name='${topicRela.categoryName}'/>
				</div>
			</div>
			<div class="control-group" style="float: right">
				<label class="control-label">文件号<font color="red">*</font>:</label>
				<div class="controls">
					<form:input path="larDocNum" htmlEscape="false" maxlength="200" class="required" cssStyle="width:290px;"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">税种明细<font color="red">*</font>:</label>
				<div class="controls">
					<input type='text' id='categoryItemId' name='categoryItemId' maxlength="64" class="required"
				           value='${topicRela.categoryItemId}' data-name='${topicRela.categoryItemName}'/>
				</div>
			</div>
			<div class="control-group" style="float: right">
				<label class="control-label">文件名<font color="red">*</font>:</label>
				<div class="controls">
					<form:input path="larDocName" htmlEscape="false" maxlength="200" class="required" cssStyle="width:290px;"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">文件内容:</label>
				<div class="controls">
					<form:textarea path="larDocContent"  maxlength="2000" cssStyle="width:640px;" rows="10" class="required"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">是否全国通用:</label>
				<div class="controls">
					<label class='radio inline'>
				       <form:radiobutton path="applyProvince" value="1" data-form='uniform' class="required"/>&nbsp;是
				    </label>
				    <label class='radio inline'>
				       <form:radiobutton path="applyProvince" value="0" data-form='uniform' class="required"/>&nbsp;否
				   </label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">适用省/市:</label>
				<div class="controls">
					<select id="applyCity" name="applyCity">
					    <option value=""></option>
					    <c:forEach items="${fns:getDictList('TAX_PLATFORM_P_CS')}" var="vo">
						   <option value="${vo.value}" <c:if test="${topicRela.applyCity eq vo.value}"> selected="selected"</c:if>>${vo.value}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			</div>
		</form:form>
   </div>
</body>
</html>