<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="fnt" uri="/WEB-INF/tld/fns-tax.tld"%>  
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><sitemesh:title/> - Powered By LiFeng</title>
		<!-- styles -->
		<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
		<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
		<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
		<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
		<link href="${ctxStatic}/common/common-ui.css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-form/jquery.select2.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-form/jquery.uniform.min.js" type="text/javascript"></script>
		
		<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/common/template-native.js" type="text/javascript" ></script>
		<script src="${ctxModules}/taxPlatform/topic_common.js" type="text/javascript"></script>
		<script src="${ctxModules}/taxPlatform/topic.js" type="text/javascript"></script>
		<link   href="${ctxStatic}/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
		<sitemesh:head/>
		<style type="text/css">
		 #options{ width: 100%;} #options .option-label{ width: 50px;display: inline-block;}
		 #add_option A{ text-decoration: underline;}
		 #add_option A:HOVER{ text-decoration: none;}
		 #add_option A i,#options i{ opacity: .5;}
		 #options .radio input[type="radio"], #options .checkbox input[type="checkbox"] {
		   float: none; margin: 0px 0 0;
		 }
		 #options .text_line{
		   width: 70%;
		 }
		 textarea[disabled], textarea[readonly] {
			cursor: auto;
			background-color: #eeeeee;
		  }
		</style>
	</head>
	<body class="has-scroll-y">
	    <tags:message content="${message}"/>
	    <div class="row-fluid wrapper">
		   <div class="bills" id="firstBills" style='width: 93%;'>
		    <sitemesh:body/>
			<form:form id="inputForm" modelAttribute="topic" action="${ctx}/platform/topic/save" method="post" class="form-horizontal">
				<tags:token/>
				<form:hidden path="id"/>
				<form:hidden path="qId"/>
				<form:hidden path="type"/>
				<form:hidden path="optionType"/>
				<form:hidden path="editFlag"/>
				<form:hidden path="version"/>
				<div id="topicInfo">
				<div class='clearfix'>
				<div class="control-group formSep">
					<label class="control-label">试题编号:</label>
					<div class="controls">
						<form:input path="code" htmlEscape="false" maxlength="50" readonly="true"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">试题来源:</label>
					<div class="controls">
					     <select id="qFrom" name="qFrom" class="required">
					       <option value=""></option>
					       <c:forEach items="${fnt:getQFroms()}" var="type">
					          <option value="${type.key}" <c:if test="${topic.QFrom == type.key}">selected="selected"</c:if>>${type.value}</option>
					       </c:forEach>
					     </select>
					</div>
				</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">试题难度:</label>
					<div class="controls">
					     <select id="qLevel" name="qLevel"  class="required">
					       <option value=""></option>
					       <c:forEach items="${fnt:getQLevels()}" var="type">
					          <option value="${type.key}" <c:if test="${topic.QLevel == type.key}">selected="selected"</c:if>>${type.value}</option>
					       </c:forEach>
					     </select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">预计答题时间（秒）:</label>
					<div class="controls">
						<form:input path="postMinute" htmlEscape="false" maxlength="20" class="required digits"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">试题类型:</label>
					<div class="controls">
					     <select id="qType" name="qType"  class="required">
					       <option value=""></option>
					       <c:forEach items="${fnt:getQTypes()}" var="type">
					          <option value="${type.key}" <c:if test="${topic.QType == type.key}">selected="selected"</c:if>>${type.value}</option>
					       </c:forEach>
					     </select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">题干内容:</label>
					<div class="controls">
						<form:textarea path="content"  maxlength="2000" cssStyle="width:95%;" rows="4" class="required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">答案设置<span id='add_option'>（<a href='javascript:void(0)' class='blue'> <i class="icon-plus"></i>&nbsp;添加选项</a>）</span>:</label>
					<div class="controls" style="position: relative;">
					    <div id="options">
					         <c:if test="${topic.QType == 'ONE_CHOICE'}">
					           <c:forEach items="${topic.options}" var="option">
					             <label class='radio'><span class='option-label'>选项&nbsp;${option.soption}&nbsp;</span><input type='radio' id='${option.soption}_key' name='skey' value='${option.soption}' data-form='uniform'<c:if test="${fns:contains(topic.skey,option.soption)}">checked="checked"</c:if>/>&nbsp;<input type='checkbox' id='${option.soption}_option' name='soption' value='${option.soption}' checked='checked' style='display:none;' readonly='readonly'/>&nbsp;<input type='text' id='${option.soption}_salisa' name='salisa' value='${option.salisa}' class='text_line'/>&nbsp;<i class='icon-remove red'></i></label>
					           </c:forEach>
					         </c:if>
					         <c:if test="${topic.QType == 'MULT_CHOICE' || topic.QType == 'ONE_MORE_CHOICE'}">
					           <c:forEach items="${topic.options}" var="option">
					             <label class='checkbox'><span class='option-label'>选项&nbsp;${option.soption}&nbsp;</span><input type='checkbox' id='${option.soption}_key' name='skey' value='${option.soption}' data-form='uniform' <c:if test="${fns:contains(topic.skey,option.soption)}">checked="checked"</c:if>/>&nbsp;<input type='checkbox' id='${option.soption}_option' name='soption' value='${option.soption}' checked='checked' style='display:none;' readonly='readonly'/>&nbsp;<input type='text' id='${option.soption}_salisa' name='salisa' value='${option.salisa}' class='text_line'/>&nbsp;<i class='icon-remove red'></i></label>
					           </c:forEach>
					         </c:if>
					         <c:if test="${topic.QType == 'TFNG'}">
					             <label class="radio inline"> 
					                   <form:radiobutton path="skey" cssClass="required" value="1"/>&nbsp;是 
					             </label>
			                     <label class="radio inline"> 
			                           <form:radiobutton path="skey" cssClass="required" value="0"/>&nbsp;错
			                     </label>
					         </c:if>
					         <c:if test="${topic.QType == 'COUNTING'}">
					             <form:input path="skey" htmlEscape="false" maxlength="50" cssClass="required number"/>
					         </c:if>
					    </div>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">答案解析:</label>
					<div class="controls">
						<form:textarea path="keyDesc"  maxlength="2000" cssStyle="width:95%;" rows="4" class="required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">业务类型:</label>
					<div class="controls">
					    <tags:treeselect id="businessType" name="businessTypeId" value="${topic.businessTypeId}" labelName="businessTypeName" labelValue="${topic.businessTypeName}"
						      title="业务类型" url="${ctx}/platform/businessType/treeSelect" cssClass="required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">行业:</label>
					<div class="controls">
						<tags:treeselect id="indutry" name="indutryId" value="${topic.indutryId}" labelName="indutryName" labelValue="${topic.indutryName}"
						      title="行业" url="${ctx}/platform/indutry/treeSelect" cssClass="required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">税种关联:</label>
					<div class="controls" style="overflow: auto;">
						<table id="sample-table" class="table sample-table table-striped table-bordered table-hover" style="width:95%;">
							<thead>
								<tr>
									<th class="tc">序号</th>
									<th>涉及税种</th>
									<th>涉及税种明细项</th>
									<th>涉及法规文号</th>
									<th>涉及法规名称</th>
									<th>涉及法规内容</th>
									<th>全国通用</th>
									<th>适用省/市</th>
									<c:if test="${topic.editFlag}">
									  <th class="qa_rela_option"></th>
									</c:if>
								</tr>
							</thead>
							<tbody>
							    <c:if test="${fn:length(topic.relas) == 0}">
								    <tr>
										<td class="index tc"></td>
										<td class="tc">
										    <input type='hidden' name='relas.id' class="id" value="-1"/>
										    <input type='hidden' name='relas.categoryId' class="categoryId"/>
										    <label class="categoryName"></label>
										</td>
										<td>
										    <input type='hidden' name='relas.categoryItemId' class="categoryItemId" value=""/>
										    <label class="categoryItemName"></label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocNum' class="larDocNum" value=""/>
										    <label class="larDocNum"></label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocName' class="larDocName" value=""/>
										    <label class="larDocName"></label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocContent' class="larDocContent" value=""/>
										    <label class="larDocContent"></label>
										</td>
										<td class="tc">
										    <input type='hidden' name='relas.applyProvince' class="applyProvince" value=""/>
										    <label class="applyProvince"></label>
										</td>
										<td>
										    <input type='hidden' name='relas.applyCity' class="applyCity" value=""/>
										    <label class="applyCity"></label>
										</td>
										<c:if test="${topic.editFlag}">
										<td class="tc qa_rela_option">
											<i class="fa fa-pencil edit" data-id='-1' data-qid='${topic.id}'></i>
										    <i class="fa fa-plus add" data-id='-1' data-qid='${topic.id}'></i>
											<i class="fa fa-minus delete" data-id='-1' data-qid='${topic.id}'></i>
										</td>
										</c:if>
									</tr>
							    </c:if>
								<c:forEach items="${topic.relas}" var="vo" varStatus="i">
								    <tr>
										<td class="index tc">${i.index + 1}</td>
										<td>
										    <input type='hidden' name='relas.id' class="id" value="${vo.id}"/>
										    <input type='hidden' name='relas.categoryId' class="categoryId" value="${vo.categoryId}"/>
										    <label class="categoryName">${vo.categoryName}</label>
										</td>
										<td>
										    <input type='hidden' name='relas.categoryItemId' class="categoryItemId" value="${vo.categoryItemId}"/>
										    <label class="categoryItemName">${vo.categoryItemName}</label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocNum' class="larDocNum" value="${vo.larDocNum}"/>
										    <label class="larDocNum">${vo.larDocNum}</label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocName' class="larDocName" value="${vo.larDocName}"/>
										    <label class="larDocName">${vo.larDocName}</label>
										</td>
										<td>
										    <input type='hidden' name='relas.larDocContent' class="larDocContent" value="${vo.larDocContent}"/>
										    <label class="larDocContent">${fns:abbr(vo.larDocContent,30)}</label>
										</td>
										<td class="tc">
										    <input type='hidden' name='relas.applyProvince' class="applyProvince" value="${vo.applyProvince}"/>
										    <label class="applyProvince">
										       <c:if test="${ vo.applyProvince eq '1'}">是</c:if>
										       <c:if test="${ not(vo.applyProvince eq '1')}">否</c:if>
										    </label>
										</td>
										<td>
										    <input type='hidden' name='relas.applyCity' class="applyCity" value="${vo.applyCity}"/>
										    <label class="applyCity">${vo.applyCity}</label>
										</td>
										<c:if test="${topic.editFlag}">
										<td class="tc qa_rela_option">
											<i class="fa fa-pencil edit" data-id='${vo.id}' data-qid='${topic.id}'></i>
											<i class="fa fa-plus add" data-id='${vo.id}' data-qid='${topic.id}'></i>
											<i class="fa fa-minus delete" data-id='${vo.id}' data-qid='${topic.id}'></i>
										</td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">
						 <c:if test="${topic.optionType == 'INIT'}">命题人:</c:if>
					     <c:if test="${topic.optionType == 'INIT_VERITY'}">初审人:</c:if>
					     <c:if test="${topic.optionType == 'FINALLY_VERITY'}">终审人:</c:if>
					</label>
					<div class="controls">
						<form:input path="createName" htmlEscape="false"  readonly="true"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">最后修改时间:</label>
					<div class="controls">
						<form:input path="createDate" htmlEscape="false"  readonly="true"/>
					</div>
				</div>
				</div>
				<c:if test="${topic.QFrom == 'QA' || topic.QFrom == 'EXAM'}">
				<div class="widget-box transparent collapsed" id='qFromDiv'>
				    <div class='widget-header widget-header-flat'>
					    <c:if test="${topic.QFrom == 'QA'}">
					    <h3><small> <i class="fa fa-hand-o-right blue"></i> 答疑信息</small></h3>
					    </c:if>
					    <c:if test="${topic.QFrom == 'EXAM'}">
					    <h3><small> <i class="fa fa-hand-o-right blue"></i> 考试信息</small></h3>
					    </c:if>
						<div class="widget-toolbar">
						   <a href="javascript:void(0)" data-action="collapse"><i class="fa fa-chevron-down"></i></a>
						</div>
				    </div>
				    <div class="widget-body">
				     <div class="widget-body-inner" style="display: none;">
				     <div class="widget-main no-padding">
				            <div class="control-group formSep">
								<label class="control-label">答疑编号:</label>
								<div class="controls">
									<form:input path="fromObj.code" htmlEscape="false" maxlength="50" class="required" readonly="true"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">标题:</label>
								<div class="controls">
									<form:input path="fromObj.title" htmlEscape="false" maxlength="400" cssStyle="width:95%;" class="required" readonly="true"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">内容:</label>
								<div class="controls">
								    <form:textarea path="fromObj.content"  maxlength="2000" cssStyle="width:95%;" rows="4" class="required" readonly="true"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">答案:</label>
								<div class="controls">
								    <form:textarea path="fromObj.answer"  maxlength="2000" cssStyle="width:95%;" rows="6" class="required" readonly="true"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">业务类型:</label>
								<div class="controls">
								    <tags:treeselect id="qBusinessType" name="qBusinessTypeId" value="${topic.fromObj.businessTypeId}" labelName="qBusinessTypeName" labelValue="${topic.fromObj.businessTypeName}"
									      title="业务类型" url="${ctx}/platform/businessType/treeSelect" cssClass="required" disabled="disabled"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">行业:</label>
								<div class="controls">
									<tags:treeselect id="qIndutry" name="qIndutryId" value="${topic.fromObj.indutryId}" labelName="qIndutryName" labelValue="${topic.fromObj.indutryName}"
									      title="行业" url="${ctx}/platform/indutry/treeSelect" cssClass="required" disabled="disabled"/>
								</div>
							</div>
							<div class="control-group formSep">
								<label class="control-label">税种关联:</label>
								<div class="controls" style="overflow: auto;">
									<table id="sample-table" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th class="tc">序号</th>
												<th>涉及税种</th>
												<th>涉及税种明细项</th>
												<th>涉及法规文号</th>
												<th>涉及法规名称</th>
												<th>涉及法规内容</th>
												<th>全国通用</th>
												<th>适用省/市</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${topic.fromObj.qaRelas}" var="vo" varStatus="i">
											    <tr>
													<td>${i.index + 1}</td>
													<td>${vo.categoryName}</td>
													<td>${vo.categoryItemName}</td>
													<td>${vo.larDocNum}</td>
													<td>${vo.larDocName}</td>
													<td>${fns:abbr(vo.larDocContent,30)}</td>
													<td>${vo.applyProvince}</td>
													<td>${vo.applyCity}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!-- 版本号 -->
							<form:hidden path="fromObj.version"/>
				        </div>
				        </div>
				   </div>
				</div>
				<c:if test="${topic.id eq '-1' }">
					<!-- 添加是否命题 -->
					<div class="control-group formSep">
						<label class="control-label">是否编题:</label>
						<div class="controls">
						     <label class='radio inline'>
						       <input type="radio" id="makeQFlagY" name="makeQFlag" value="1" <c:if test="${ topic.fromObj.makeQFlag eq '1'}">checked="checked"</c:if>/>是
						     </label>
						     <label class='radio inline'>
						       <input type="radio" id="makeQFlagN" name="makeQFlag" value="0" <c:if test="${ topic.fromObj.makeQFlag eq '0'}">checked="checked"</c:if>/>否
						     </label>
						</div>
					</div>
				</c:if>
				</c:if>
				<div class="form-actions">
						<input id="submitBtn" class="btn btn-primary" type="submit" value="&nbsp;保&nbsp;存&nbsp;"/>
						<c:if test="${topic.optionType == 'FINALLY_VERITY'}">
						  <input id="finishBtn" class="btn" type="button" value="&nbsp;发&nbsp;布&nbsp;"/>
						</c:if>
						<c:if test="${not(topic.optionType == 'FINALLY_VERITY')}">
						  <input id="finishBtn" class="btn" type="button" value="&nbsp;完&nbsp;成&nbsp;"/>
						</c:if>
						<input id="delBtn" class="btn" type="button" value="&nbsp;删&nbsp;除&nbsp;"/>
						<input id="cancelBtn" class="btn" type="button" value="&nbsp;关 &nbsp;闭&nbsp;"/>
				</div>
			</form:form>
		  </div>
		  <div class="bills" style="width: 39%;display: none;position: fixed;left:55%; overflow: auto;" id="secBills">
		      <div class="page-header">
		          <c:if test="${topic.QFrom == 'QA'}">
					    <h3>试题来源<small> &gt;&gt; 答疑信息</small></h3>
			      </c:if>
			      <c:if test="${topic.QFrom == 'EXAM'}">
			            <h3>试题来源<small> &gt;&gt; 考试信息</small></h3>
			      </c:if>
		      </div>
		      <div class='bills-content'></div>
		   </div>
		</div>
		
		<!-- 可以双屏显示 试题和答疑信息 -->
		<div class="settings-container" id="settings-container">
			<div class="settings-btn"> <i class="icon-cog"></i></div>
			<div class="settings-box" style="width:180px;">
				<div class="control-group">
					<div class="controls">
					     <input type="checkbox" id="multSetting" value="true"/>
					</div>
					<label class="control-label">双屏显示(试题-答疑)</label>
				</div>
			</div>
		</div>
	</body>
</html>
