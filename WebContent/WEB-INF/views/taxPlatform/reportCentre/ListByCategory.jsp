<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>报表中心 - 行业统计</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script src="${ctxModules}/taxPlatform/topic_report.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		//图表数据
		Topic.chartByCategory('I-main'); 
		
		//折叠事件
		Public.initPublicEvent();
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body class="white">
<div class="row-fluid wrapper">
   <div class="bills" style="width: 93%;">
      <!-- 图表  -->
      <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="tab-content overflow-visible">
			        <div id="I-main" class="chart-main tab-pane active"></div>
			        <div id="II-main" class="chart-main tab-pane"></div>
			        <div id="III-main" class="chart-main tab-pane"></div>
		        </div>
		     </div>
		     </div>
	   </div>
	  </div>
	  <!-- 列表 -->
	  <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <table id="sample-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="tc">序号</th>
							<th>税种</th>
							<th>试题数量</th>
							<th>答疑汇编</th>
							<th>考试汇编</th>
							<th>自定义</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stats}" var="vo" varStatus="i">
						    <tr>
								<td class="tc">${i.index + 1}</td>
								<td class="tc">${vo.CATEGORY_NAME}</td>
								<td class="tc">${vo.TOPIC_TOTAL}</td>
								<td class="tc">${vo.QA_TOTAL}</td>
								<td class="tc">${vo.EXAM_TOTAL}</td>
								<td class="tc">${vo.CUSTOM_TOTAL}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		     </div>
		     </div>
	    </div> 
	  </div>
   </div>
</div>
</body>
</html>