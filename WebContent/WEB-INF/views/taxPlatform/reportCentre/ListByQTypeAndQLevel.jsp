<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script src="${ctxModules}/taxPlatform/topic_report.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/common-ui.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-form/jquery.select2.min.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		//图表数据
		Topic.chartByQTypeAndQLevel('I-main'); 
		
		//tab 切换  -- 取消默认的事件
		$(document).on('click.tmt.widget','[data-toggle]',function(e){
			var n = $(this);
			var p = n.data("toggle"), selector = n.attr('href').replace(/.*#(?=[^\s]*$)/, '');
			var level = n.data("level");
			if ( n.parent('li').hasClass('active') ) return;
			//图表数据
			Topic.chartByQTypeAndQLevel(selector, level);
		});
		
		//折叠事件
		Public.initPublicEvent();
		//税种查询
		Public.autoCombo('createId', "${ctx}/system/user/jSonList");
		
		$(document).on('click','#expBtn',function(){
			var param = [];
			    param.push({name:'createId',value:($('#createId').val())});
			Public.doExport('${ctx}/platform/report/export',param);
		});
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body class="white">
<div class="row-fluid wrapper">
   <div class="bills" style="width: 93%;">
      <!-- 图表  -->
      <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
			<div class="widget-toolbar no-border">
			   <ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#I-main" data-level='I'>容易</a></li>
					<li><a data-toggle="tab" href="#II-main" data-level='II'>一般</a></li>
					<li><a data-toggle="tab" href="#III-main" data-level='III'>很难</a></li>
			   </ul>
			</div>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="tab-content overflow-visible">
			        <div id="I-main" class="chart-main tab-pane active"></div>
			        <div id="II-main" class="chart-main tab-pane"></div>
			        <div id="III-main" class="chart-main tab-pane"></div>
		        </div>
		     </div>
		     </div>
	   </div>
	  </div>
	  <!-- 列表 -->
	  <div class="widget-box transparent" id="list">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （题型）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="top">
			          <form name="queryForm" id="queryForm" action="${ctx}/platform/report/listByQTypeAndQLevel#list" class="form-horizontal form-fluid" method="post">
							<tags:token ignore="true"/>
							<div class="fr">
							  <div class="control-group" style="margin-right: 10px;">
								<label class="control-label">命题人:</label>
								<div class="controls">
									<input type='text' id='createId' name='createId' maxlength="64" class="required" value='${createId}' data-name='${createName}'/>
								</div>
							  </div>
							  <input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
							  <input id="expBtn" class="btn" type="button" value="导出"/>
							</div>
			          </form>
		        </div>
		        <table id="sample-table" class="table sample-table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="15%">题型</th>
							<th>难度</th>
							<th>试题数量</th>
							<th>答疑汇编</th>
							<th>考试汇编</th>
							<th>自定义</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stats}" var="vo" varStatus="i">
						    <tr>
								<td class="tc type">${vo.Q_TYPE}</td>
								<td class="tc level">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.TOPIC_TOTAL}</td>
								<td class="tc">${vo.QA_TOTAL}</td>
								<td class="tc">${vo.EXAM_TOTAL}</td>
								<td class="tc">${vo.CUSTOM_TOTAL}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		     </div>
		     </div>
	    </div> 
	  </div>
   </div>
</div>
</body>
</html>