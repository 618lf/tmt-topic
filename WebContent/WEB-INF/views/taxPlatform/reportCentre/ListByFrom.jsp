<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>报表中心 - 行业统计</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script src="${ctxModules}/taxPlatform/topic_report.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		//图表数据
		Topic.chartByQTypeAndQLevel('I-main'); 
		
		//tab 切换  -- 取消默认的事件
		$(document).on('click.tmt.widget','[data-toggle]',function(e){
			var n = $(this);
			var p = n.data("toggle"), selector = n.attr('href').replace(/.*#(?=[^\s]*$)/, '');
			var from = n.data("from");
			if ( n.parent('li').hasClass('active') ) return;
			//图表数据
			Topic.chartByQTypeAndQLevel(selector, level);
		});
		
		//折叠事件
		Public.initPublicEvent();
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body class="white">
<div class="row-fluid wrapper">
   <div class="bills" style="width: 93%;">
      <!-- 图表  -->
      <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
			<div class="widget-toolbar no-border">
			   <ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#I-main" data-from='QA'>答疑汇编</a></li>
					<li><a data-toggle="tab" href="#II-main" data-from='EXAMR'>考试汇编</a></li>
					<li><a data-toggle="tab" href="#III-main" data-from='CUSTOM'>自定义</a></li>
			   </ul>
			</div>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="tab-content overflow-visible">
			        <div id="I-main" class="chart-main tab-pane active"></div>
			        <div id="II-main" class="chart-main tab-pane"></div>
			        <div id="III-main" class="chart-main tab-pane"></div>
		        </div>
		     </div>
		     </div>
	   </div>
	  </div>
	  <!-- 列表 -->
	  <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （试题来源）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <table id="sample-table" class="table sample-table table-striped table-bordered table-hover">
					<thead>
					    <tr>
							<th colspan="2">试题来源</th>
							<th colspan="2">入库试题</th>
							<th colspan="2">不编试题</th>
							<th colspan="2">待编试题</th>
						</tr>
						<tr>
							<th>来源类型</th>
							<th>数量</th>
							<th>数量</th>
							<th>比重</th>
							<th>数量</th>
							<th>比重</th>
							<th>数量</th>
							<th>比重</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stats}" var="vo" varStatus="i">
						    <tr>
								<td class="tc">${vo.Q_FROM}</td>
								<td class="tr">${vo.QA_TOTAL}</td>
								<td class="tr">${vo.RK_TOTAL}</td>
								<td class="tr">${vo.RK_RATE}%</td>
								<td class="tr">${vo.NMQ_TOTAL}</td>
								<td class="tr">${vo.NMQ_RATE}%</td>
								<td class="tr">${vo.MQ_TOTAL}</td>
								<td class="tr">${vo.MQ_RATE}%</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		     </div>
		     </div>
	    </div> 
	  </div>
   </div>
</div>
</body>
</html>