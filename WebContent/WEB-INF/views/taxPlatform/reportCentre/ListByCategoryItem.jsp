<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>报表中心 - 税种明细统计</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script src="${ctxModules}/taxPlatform/topic_report.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/common-ui.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-form/jquery.select2.min.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		//图表数据
		Topic.chartByCategory('I-main'); 
		
		//折叠事件
		Public.initPublicEvent();
		
		//税种查询
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body class="white">
<div class="row-fluid wrapper">
   <div class="bills" style="width: 93%;">
      <!-- 图表  -->
      <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="tab-content overflow-visible">
			        <div id="I-main" class="chart-main tab-pane active"></div>
			        <div id="II-main" class="chart-main tab-pane"></div>
			        <div id="III-main" class="chart-main tab-pane"></div>
		        </div>
		     </div>
		     </div>
	   </div>
	  </div>
	  <!-- 列表 -->
	  <div class="widget-box transparent" id="list">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （税种明细）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="top">
			          <form name="queryForm" id="queryForm" action="${ctx}/platform/report/listByCategoryItem#list" class="form-horizontal form-fluid" method="post">
							<tags:token ignore="true"/>
							<div class="fr">
							  <div class="control-group" style="margin-right: 10px;">
								<label class="control-label">税种:</label>
								<div class="controls">
									<input type='text' id='categoryId' name='categoryId' maxlength="64" class="required" value='${categoryId}' data-name='${categoryName}'/>
								</div>
							  </div>
							  <input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
							</div>
			          </form>
		        </div>
		        <table id="sample-table" class="table sample-table table-striped table-bordered table-hover">
					<thead>
					    <tr>
							<th class="tc" rowspan="2">序号</th>
							<th rowspan="2">税种</th>
							<th rowspan="2">税种明细</th>
							<th colspan="2">单项选择题</th>
							<th colspan="2">多项选择题</th>
							<th colspan="2">不定项选择题</th>
							<th colspan="2">判断题</th>
							<th colspan="2">计算题</th>
							<th colspan="2">合计</th>
						</tr>
						<tr>
							<th>难度</th>
							<th>数量</th>
							<th>难度</th>
							<th>数量</th>
							<th>难度</th>
							<th>数量</th>
							<th>难度</th>
							<th>数量</th>
							<th>难度</th>
							<th>数量</th>
							<th>难度</th>
							<th>数量</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stats}" var="vo" varStatus="i">
						    <tr>
								<td class="tc">${i.index + 1}</td>
								<td class="tc">${vo.CATEGORY_NAME}</td>
								<td class="">${vo.CATEGORY_ITEM_NAME}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.OC_TOTAL}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.MC_TOTAL}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.OMC_TOTAL}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.TFNG_TOTAL}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.COUNT_TOTAL}</td>
								<td class="tc">${vo.Q_LEVEL}</td>
								<td class="tc">${vo.TOPIC_TOTAL}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		     </div>
		     </div>
	    </div> 
	  </div>
   </div>
</div>
</body>
</html>