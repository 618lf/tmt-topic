<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script src="${ctxModules}/taxPlatform/topic_common.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common-file.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/common-file.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/template-native.js" type="text/javascript" ></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var storageFlagFmt = function(cellvalue, options, rowObject) {
			if(cellvalue==1) { return "是";}
			else if(cellvalue==0) { return "否";}
			else if(cellvalue==2) { return "待定";}
			return "未筛选";
		};
		var makeQFlagsFmt = function(cellvalue, options, rowObject){
			if(cellvalue==1) { return "是";}
			return "否";
		};
		var optionsFmt = function (cellvalue, options, rowObject) {
			return Public.billsOper(cellvalue, options, rowObject);
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/platform/qa/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!1, 
				rownumbers: !0,//序号
				multiselect:!0,//定义是否可以多选
				multiboxonly: false,
				colNames: [' ', '是否入库', '是否编题', 'ID', '答疑编号', '标题', '发布日期', '发布机关', '业务类型', '行业', '税种', '税种明细', '筛选人', '筛选时间', '命题人', '命题时间'],
				colModel: [
                    {name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt,frozen:true},
                    {name:'storageFlag', index:'storageFlag',align:'center',width:80,sortable:true,formatter:storageFlagFmt,frozen:true},
                    {name:'makeQFlag', index:'makeQFlag',align:'center',width:80,sortable:true,formatter:makeQFlagsFmt,frozen:true,hidden:true},
                    {name:'id', index:'id', width:80,sortable:false,hidden:true,frozen:true},
	                {name:'code', index:'code', width:100, align:'center', sortable:true,frozen:true},
	                {name:'title', index:'title', width:220,sortable:false,frozen:true},
	                {name:'publishDate', index:'publishDate', align:'center', width:100,sortable:true},
					{name:'publishOffice', index:'publishOffice',align:'center',width:140,sortable:true},
	                {name:'businessTypeName', index:'businessTypeName',align:'left',width:80,sortable:false},
					{name:'indutryName', index:'indutryName',align:'left',width:80,sortable:false},
					{name:'categorys', index:'categorys', width:150,sortable:false,frozen:true},
					{name:'categoryItems', index:'categoryItems', width:150,sortable:false,frozen:true},
					{name:'updateName', index:'updateName',align:'center',width:100,sortable:false},
					{name:'updateDate', index:'updateDate',align:'center',width:120,sortable:false},
					{name:'makeName', index:'makeName',align:'center',width:100,sortable:false,hidden:true},
					{name:'makeDate', index:'makeDate',align:'center',width:120,sortable:false,hidden:true}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		$('#dataGrid').on('click','.edit',function(e){
        	Public.openOnTab('qa-edit', '答疑汇编详情',"${ctx}/platform/qa/form?id="+$(this).attr('data-id'));
		});
		
		//添加查询的条件
		Public.combo('makeQFlag');Public.combo('storageFlag');
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
		Public.autoCombo('indutryId',"${ctx}/platform/indutry/jSonList");
		Public.autoCombo('businessTypeId',"${ctx}/platform/businessType/jSonList");
		Public.autoCombo('publishOffice', "${ctx}/platform/qa/jSonPublishOffice");
		
		var delQa = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
				Public.deletex("确定删除选中的答疑？","${ctx}/platform/qa/delete",param,function(data){
					if(!!data.success) {
						Public.success('删除答疑成功');
						Public.doQuery();
					} else {
						Public.error(data.msg);
					}
				});
			} else {
				Public.error("请选择要删除的答疑!");
			}
		};
		// --- 删除 ---
		$(document).on('click','#delBtn',function(e){
			var checkeds = $('#grid').getGridParam('selarrrow');
			delQa(checkeds);
		});
		$('#dataGrid').on('click','.delete',function(e){
			delQa($(this).attr('data-id'));
		});
		//导入
		$(document).on('click','#impBtn',function(e){
			Public.openImportWindow($("#impDiv").val(), "#impForm", '${ctx}/platform/qa/doImport', '导入答疑');
		});
	}
};
$(function(){
	THISPAGE._init();
});
//上一个答疑,自动翻页
var preQa = function(cId){
	var _ids = $('#grid').jqGrid('getDataIDs');
	var _pId = null, _index = 0, _fIndex = -1;
	$.each(_ids,function(index,item){
		if( !_pId ) {//还没找到就加一
			_index ++;
		}
		if( !_pId && item == cId && index !=0 ) {
			_pId = _ids[index-1];
		}
		if( item == cId) {
			_fIndex = index;
		}
	});
	_pId = (_pId == null && _index == _ids.length && _ids.length != 0 && _fIndex == -1)?_ids[0]:_pId;
	return singleSelect(_pId);
};
//下一个答疑,自动翻页
var nextQa = function(cId){
	var _ids = $('#grid').jqGrid('getDataIDs');
	var _nId = null, _index = 0, _fIndex = -1;
	$.each(_ids,function(index,item){
		if(!_nId) {//还没找到就加一
			_index ++;
		}
		if( !_nId && item == cId && index !=_ids.length-1 ) {
			_nId = _ids[index+1]; _fIndex = index;
		}
		if( item == cId) {
			_fIndex = index;
		}
	});
	_nId = (_nId == null && _index == _ids.length && _ids.length != 0 && _fIndex == -1)?_ids[0]:_nId;
	return singleSelect(_nId);
};
//单独选择当前行
var singleSelect = function(id){
	if(!!id) {
		var checkeds = $('#grid').getGridParam('selarrrow');
		$.each(checkeds,function(index,item){
			$('#grid').jqGrid('setSelection',item);
		});
		$('#grid').jqGrid('setSelection',id);
	}
	return id;
};
//刷新当前页面
var reload = function(){
	$('#grid').trigger("reloadGrid");
}
</script>
</head>
<body style="overflow: hidden;">
<tags:message content="${message}" />
<div class="wrapper">
    <div class="wrapper-inner">
		<div class="top">
		    <form name="queryForm" id="queryForm">
		        <input type="hidden" name="qFrom" id="qFrom" value="QA"/>
				<div class="fl">
				  <div class="ui-btn-menu ui-btn-menu-fl">
				      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
				         <strong>点击查询</strong><b></b>
				      </span>
				      <div class="dropdown-menu" style="width: 630px;">
				           <div class="control-group formSep">
								<label class="control-label">标题:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="title"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">问:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="content"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">答:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="answer"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">发布日期:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="publishDate"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">发布机关:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="publishOffice" name="publishOffice"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">业务类型:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="businessTypeId" name="businessTypeId"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">行业:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="indutryId" name="indutryId"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">是否入库:</label>
								<div class="controls">
									<select id="storageFlag" name="storageFlag">
									   <option value=""></option>
									   <option value="1">是</option>
									   <option value="0">否</option>
									   <option value="2">待定</option>
									   <option value="-1">未筛选</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep" style="display: none;">
								<label class="control-label">是否编题:</label>
								<div class="controls">
									<select id="makeQFlag" name="makeQFlag">
									   <option value=""></option>
									   <option value="1">是</option>
									   <option value="0">否</option>
									</select>
								</div>
						   </div>
						   <div class="control-group formSep"  style="display: none;">
								<label class="control-label">关键字:</label>
								<div class="controls">
									<input type="text" class="input-txt" name="keyWords"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">税种:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="categoryId" name="categoryId"/>
								</div>
						   </div>
						   <div class="control-group formSep">
								<label class="control-label">税种明细:</label>
								<div class="controls">
									<input type="text" class="input-txt" id="categoryItemId" name="categoryItemId"/>
								</div>
						   </div>
					       <div class="ui-btns"> 
				              <input class="btn btn-primary query" type="button" value="查询"/>
				              <input class="btn reset" type="button" value="重置"/>
				           </div> 
				      </div>
				  </div>
				  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
				</div>
				<div class="fr">
				   <input type="button" class="btn btn-primary" id='impBtn' value="&nbsp;导&nbsp;入&nbsp;">
				   <input type="button" class="btn" id='delBtn' value="&nbsp;删&nbsp;除&nbsp;">&nbsp;
				</div>
			</form>
		</div>
	</div>
	<div id="dataGrid" class="autoGrid">
		<table id="grid"></table>
		<div id="page"></div>
	</div> 
</div>
<textarea id="impDiv" style="display: none;">
   <div class="row-fluid">
   <form id="impForm" method="post" class="form-horizontal" enctype="multipart/form-data">
      <tags:token/>
      <div class="control-group formSep">
		<label class="control-label">选择文件(*.xls):</label>
		<div class="controls">
		     <input type="file" name="file" class="required selectFile"/>
		</div>
	  </div>
	  <div class="control-group formSep">
		<label class="control-label">选择模版:</label>
		<div class="controls">
		     <select id="templateId" name="templateId" class='required'>
		         <c:forEach items="${templates}" var="template">
			    	 <option value="${template.id}">${template.name}</option>
			     </c:forEach>
		     </select>
		</div>
	  </div>
   </form>
   </div>
</textarea>
</body>
</html>