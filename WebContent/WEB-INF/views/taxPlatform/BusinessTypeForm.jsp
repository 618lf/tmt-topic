<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#title").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/platform/businessType/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header">
			<h3>业务类型<small> &gt;&gt; 编辑</small></h3>
		</div>
		<form:form id="inputForm" modelAttribute="businessType" action="${ctx}/platform/businessType/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<div class="control-group formSep">
				<label class="control-label">业务类型编号:</label>
				<div class="controls">
					<form:input path="code" htmlEscape="false" maxlength="20" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">业务类型名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="100" class="required"/>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="btn btn-primary" type="submit" value="保 存"/>
				<input id="cancelBtn" class="btn" type="button" value="返回"/>
			</div>
		</form:form>
	  </div>
	</div>
</body>
</html>