<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>403</title>
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script type="text/javascript">
$(function(){
	//关闭
	$(document).on('click','#backList',function(){
		Public.closeTab('topic-edit');
		//Public.refreshWindow('${ctx}/platform/qa/initTopicMake');
	});
});
</script>
</head>
<body>
<div class="row-fluid wrapper">
    <div class="bills" style="width: 93%;height: 90%;">
	    <div class="page-header">
			<h3><i class="icon32 icon-color icon-cancel"></i>&nbsp;试题已经被其他用户编制试题,不能重复编辑,请重新选择</h3>
		</div>
		<h3 class="lighter smaller">
		     错误信息：${errorMsg}
		</h3>
		<hr>
		<div class="space"></div>
		<div class="tc">
			<a href="javascript:void(0)" class="btn btn-grey" id="backList">
				<i class="icon-off"></i>&nbsp;关闭
			</a>
		</div>
	</div>
</div>
</body>
</html>