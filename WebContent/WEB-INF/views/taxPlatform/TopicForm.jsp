<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ taglib prefix="fnt" uri="/WEB-INF/tld/fns-tax.tld"%>  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>试题--命题</title>
<meta name="decorator" content="topicForm"/>
<script type="text/javascript">
$(function(){
	THISPAGE._init();
	$(document).on('click', '#finishBtn', function(e){
		Public.confirmx('完成命题,进入试题初审阶段,确认完成命题?', function(){
			$('#optionType').val('INIT_VERITY');
			$('#inputForm').submit();
		});
	});
	
	//初始化是否命题
	if( $('#id').val() == '-1' && $('#qFrom').val() == 'QA' && !!$('#qId').val() ) {
		$('#topicInfo').hide();
		$('#qFromDiv .widget-header').hide();
		$('#qFromDiv .widget-body-inner').show();
	}
	//是否编题选择
	$(document).on('click', '#makeQFlagY', function(e){
		var qFrom = $('#qFrom').val(), qFromDiv = $('#qFromDiv'), firstBills = $('#firstBills'),secBills = $('#secBills');
    	if(!((qFrom == 'QA' || qFrom == 'EXAM') && !!qFromDiv.get(0))) { return;}
		$('#qFromDiv .widget-header').show();
		$('#topicInfo').show();
		$('#qFromDiv .widget-body-inner').hide();
		
		firstBills.css('width','50%');
		secBills.css('width','39%').show();
		secBills.find('.bills-content').html(qFromDiv.find('.widget-main').html());
		qFromDiv.hide();
		secBills.css({height:($(window).outerHeight() - ($.browser.msie?45:46))});
		$('#inputForm').addClass('form-min');
		
	});
	//是否编题选择
	$(document).on('click', '#makeQFlagN', function(e){
		$('#topicInfo').hide();
		$('#qFromDiv .widget-header').hide();
		$('#qFromDiv .widget-body-inner').show();
		var qFrom = $('#qFrom').val(), qFromDiv = $('#qFromDiv'), firstBills = $('#firstBills'),secBills = $('#secBills');
    	if(!((qFrom == 'QA' || qFrom == 'EXAM') && !!qFromDiv.get(0))) { return;}
		
    	firstBills.css('width','93%');
		secBills.hide().find('.bills-content').html('');
		qFromDiv.show();
		$('#inputForm').removeClass('form-min');
	});
});
</script>
</head>
<body>
	<div class="page-header">
		<h3>试题<small> &gt;&gt; 命题</small></h3>
		<c:if test="${ topic.type == 'INIT_VERITY' || topic.type == 'FINALLY_VERITY' || topic.type == 'FINALLY'}">
			<div class="page-toolbar no-border">
			  <ul class="nav nav-tabs" id="recent-tab">
					<li class="active"><a data-toggle="tab" href="javascript:void(0)" data-type='INIT'>命题</a></li>
					<c:if test="${topic.type == 'FINALLY_VERITY' || topic.type == 'FINALLY'}">
					<li><a data-toggle="tab" href="javascript:void(0)" data-type='INIT_VERITY'>初审</a></li>
					<li><a data-toggle="tab" href="javascript:void(0)" data-type='FINALLY_VERITY'>终审</a></li>
					</c:if>
					<c:if test="${topic.type == 'INIT_VERITY' }">
					<li><a data-toggle="tab" href="javascript:void(0)" data-type='INIT_VERITY'>初审</a></li>
					</c:if>
			  </ul>
			</div>
		</c:if>
	</div>
</body>
</html>