<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#inputForm").validate(
			Public.validate({
				//使用 ajax 提交
				submitHandler: function(form){
					var that = this;
					Public.loading('正在提交，请稍等...');
					Public.ajaxSubmit(form, '${ctx}/platform/qaRela/save', null, function(data){
						Public.success('关联税种成功');
						if( !!THISPAGE.callback ) {
							THISPAGE.callback(data,that);
						}
					});
				},
				errorPlacement: function(error, element) {
					element.addClass('text_error');
				}
			})
		);
	},
	addEvent : function(){
		Public.combo('applyCity');
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
		
		//如果没有数据,则从
		if($('#id').val() == '-1' || $('#id').val() == '' || $('#larDocContent').val() == '') {//新添加的一行
		    $('#larDocContent').val(parent.getContext());
		}
		//$('#qaTitle').html(parent.getTitle());
	},
	submit : function(param, callback){
		if(!!param.qaId && param.qaId != -1) {
			if( !param.id || param.id == -1) {//新增状态
			   $("#qaId").val(param.qaId);
			}
			this.callback = callback;
			$('#inputForm').submit();
		} else {
			Public.error('请先保存试题,然后关联税种信息!');
		}
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
 body{
    background-color: #fff;
 }
</style>
</head>
<body>
   <div class="row-fluid wrapper white">
		<form:form id="inputForm" modelAttribute="qaRela" action="${ctx}/platform/qaRela/save" method="post" class="form-horizontal form-fluid form-min">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="qaId"/>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">税种<font color="red">*</font>:</label>
				<div class="controls">
				    <input type='text' id='categoryId' name='categoryId' maxlength="64" class="required"
				           value='${qaRela.categoryId}' data-name='${qaRela.categoryName}'/>
				</div>
			</div>
			<div class="control-group" style="float: right">
				<label class="control-label">文件号<font color="red">*</font>:</label>
				<div class="controls">
					<form:input path="larDocNum" htmlEscape="false" maxlength="200" class="required" cssStyle="width:290px;"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">税种明细<font color="red">*</font>:</label>
				<div class="controls">
					<input type='text' id='categoryItemId' name='categoryItemId' maxlength="64" class="required"
				           value='${qaRela.categoryItemId}' data-name='${qaRela.categoryItemName}'/>
				</div>
			</div>
			<div class="control-group" style="float: right">
				<label class="control-label">文件名<font color="red">*</font>:</label>
				<div class="controls">
					<form:input path="larDocName" htmlEscape="false" maxlength="200" class="required" cssStyle="width:290px;"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">文件内容<font color="red">*</font>:</label>
				<div class="controls">
					<form:textarea path="larDocContent"  maxlength="2000" cssStyle="width:640px;" rows="10" class="required"/>
				</div>
			</div>
			</div>
			<div class="clearfix">
			<div class="control-group">
				<label class="control-label">全国通用<font color="red">*</font>:</label>
				<div class="controls">
					<label class='radio inline'>
				       <form:radiobutton path="applyProvince" value="1" data-form='uniform' class="required"/>&nbsp;是
				    </label>
				    <label class='radio inline'>
				       <form:radiobutton path="applyProvince" value="0" data-form='uniform' class="required"/>&nbsp;否
				   </label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">适用省/市:</label>
				<div class="controls">
					<select id="applyCity" name="applyCity">
					    <option value=""></option>
					    <c:forEach items="${fns:getDictList('TAX_PLATFORM_P_CS')}" var="vo">
						   <option value="${vo.value}" <c:if test="${qaRela.applyCity eq vo.value}"> selected="selected"</c:if>>${vo.value}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			</div>
		</form:form>
   </div>
</body>
</html>