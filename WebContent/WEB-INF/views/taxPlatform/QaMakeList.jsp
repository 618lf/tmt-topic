<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var statusFmt = function(cellvalue, options, rowObject) {
			if(cellvalue=='DOING') { return "命题中";}
			else if(cellvalue=='DONE'){return "命题完成";}
			else if(cellvalue=='NO'){return "不命题";}
			else{return "未命题";}
		};
		var optionsFmt = function (cellvalue, options, rowObject) {
			if(rowObject.topic){
				return "<a class='qa-edit' data-id='"+rowObject.topic.id+"' data-qid='"+rowObject.id+"' title='编辑'>编制</a>";
			} else {
				return "<a class='qa-edit' data-id='' data-qid='"+rowObject.id+"' title='编辑'>编制</a>";
			}
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/platform/qa/jSonTopicMakeList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				rownumbers: !0,//序号
				multiselect:true,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', '操作', '状态', '答疑编号', '标题', '业务类型', '行业', '税种', '税种明细', '命题人', '命题时间'],
				colModel: [
                    {name:'id', index:'id', width:80,sortable:false,hidden:true},
                    {name:'options', index:'options',align:'center',width:60,sortable:false,formatter:optionsFmt},
                    {name:'makeStatus', index:'makeStatus',align:'center', width:60,sortable:true,formatter:statusFmt},
	                {name:'code', index:'code', width:70, align:'center', sortable:true},
	                {name:'title', index:'title', width:200,sortable:true},
	                {name:'businessTypeName', index:'businessTypeName',width:80,sortable:false},
					{name:'indutryName', index:'indutryName',align:'center',width:80,sortable:false},
					{name:'categorys', index:'categorys', width:150,sortable:false,frozen:true},
					{name:'categoryItems', index:'categoryItems', width:150,sortable:false,frozen:true},
					{name:'makeName', index:'makeName',align:'center',width:100,sortable:false},
					{name:'makeDate', index:'makeDate',align:'center',width:120,sortable:false}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		$('#dataGrid').on('click','.qa-edit',function(e){
			var param = 'id='+$(this).data('id')+'&qId=' + $(this).data('qid')
			            + '&qFrom=QA';
        	Public.openOnTab('topic-edit', '答疑汇编-试题编制',"${ctx}/platform/topic/form?"+param);
		});
		$(document).on('click','#cancelBtn',function(e){
			Public.closeTab('qa-topic-add');
		});
		
		//添加查询的条件
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
		Public.autoCombo('indutryId',"${ctx}/platform/indutry/jSonList");
		Public.autoCombo('businessTypeId',"${ctx}/platform/businessType/jSonList");
		Public.autoCombo('publishOffice', "${ctx}/platform/qa/jSonPublishOffice");
		Public.combo('makeStatus');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
}
</style>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <div class="ui-btn-menu ui-btn-menu-fl">
					      <span class="ui-btn ui-menu-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 630px;">
					           <div class="control-group formSep">
									<label class="control-label">标题:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="title"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">问:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="answer"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">答:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="content"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">命题状态:</label>
									<div class="controls">
										<select id="makeStatus" name="makeStatus">
										   <option value=""></option>
										   <option value="NONE">未命题</option>
										   <option value="DOING">命题中</option>
										   <option value="DONE">命题完成</option>
										   <option value="NO">不命题</option>
										</select>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">发布日期:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="publishDate"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">发布机关:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="publishOffice" name="publishOffice"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">业务类型:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="businessTypeId" name="businessTypeId"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">行业:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="indutryId" name="indutryId"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">税种:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="categoryId" name="categoryId"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">税种明细:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="categoryItemId" name="categoryItemId"/>
									</div>
							   </div>
							   
						       <div class="ui-btns"> 
					              <input class="btn btn-primary query" type="button" value="查询"/>
					              <input class="btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="btn" id="cancelBtn" value="&nbsp;关&nbsp;闭&nbsp;">&nbsp;
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>