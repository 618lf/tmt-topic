<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxModules}/taxPlatform/topic_common.js" type="text/javascript"></script>
<script src="${ctxModules}/taxPlatform/taxDoc.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
		this.initFormState();
	},
	bindValidate : function(){
		$("#title").focus();
		$("#inputForm").validate(
			Public.validate({submitHandler: function(form){
				//验证税种
				if( $("input[name='storageFlag']:checked").val()==1 && ($('.icon-edit:first').data('id') == -1 || !$('.icon-edit:first').data('id'))) {
					Public.error('请关联税种');
					return;
				}
				//验证法规
				if( $("input[name='storageFlag']:checked").val()== 1 ) {//是
					var bfalg = true;
					$('.larDocNum').each(function(index,item){
						if( bfalg && !$(item).text() ) {
							bfalg = false;
						}
					});
					$('.larDocName').each(function(index,item){
						if( bfalg && !$(item).text() ) {
							bfalg = false;
						}
					});
					$('.larDocContent').each(function(index,item){
						if( bfalg && !$(item).text() ) {
							bfalg = false;
						}
					});
					if(!bfalg) {
						Public.error('请填写完整法规[文号,名称,内容],都必须填写完整');
						return;
					}
				}
				//业务类型
				if( $("input[name='storageFlag']:checked").val()==1 && !$('#businessTypeId').val()) {
					Public.error('请填写业务类型');
					return;
				}
				//行业
				if( $("input[name='storageFlag']:checked").val()==1 && !$('#indutryId').val()) {
					Public.error('请填写行业');
					return;
				}
				//是否编题
				//if( $("input[name='storageFlag']:checked").val()==1 && $("input[name='makeQFlag']:checked").length == 0 ) { //makeQFlag
				//	Public.error('请选择是否编题');
				//	return;
				//}
				Public.loading('正在提交，请稍等...');
				form.submit();
			}})
		);
	},
	addEvent : function(){
		$(document).on('click', '#cancelBtn', function(e){
			Public.closeTab('qa-edit');
		});
        Public.tags('keyWords');
        Public.uniform();
        //初始化事件
        Topic.initQaEvent(webRoot + '/admin/platform/qaRela/form');
        //是否入库事件
        $(document).on('change',"input[name='storageFlag']",function(){
        	var n = $(this);
        	var v = n.val();
        	if(v == 1) {//选择是
        		$('.qaOtherInfo').show();
        	} else {//选择否
        		$('.qaOtherInfo').hide();
        	}
        });
        var cId = $('#id').val();
        var refreshWindow = function(id){
        	Public.refreshWindow(webRoot + '/admin/platform/qa/form',{id:id});
        };
        //上一个答疑
        $(document).on('click','#prev',function(){
        	var _qaListW = Public.getTabWindowByName('答疑汇编');
        	var _pId = _qaListW.preQa(cId)
        	if(!_pId) {
        		Public.error("已经是第一条答疑,请到'答疑汇编'翻页");
        	}else{
        		refreshWindow(_pId);
        	}
        });
        //下一个答疑
        $(document).on('click','#next',function(){
        	var _qaListW = Public.getTabWindowByName('答疑汇编');
        	var _nId = _qaListW.nextQa(cId)
        	if(!_nId) {
        		Public.error("已经是最后一条答疑,请到'答疑汇编'翻页");
        	}else {
        		refreshWindow(_nId);
        	}
        });
        //刷新列表
		if('${reload}' == 'true') { //保存之后
			var _qaListW = Public.getTabWindowByName('答疑汇编');
			_qaListW.reload();
		}
        //折叠事件
		Public.initPublicEvent();
        //文本框选中事件
		TaxDoc.initSelectText('#answer');
	},
	initFormState : function(){
		//初始化表单状态
		if($('#editFlag').val() == 'false') {
			$("input[type='text']").attr("readonly","readonly");
			$("input[type='radio']").attr("readonly","readonly");
			$("input[type='checkbox']").attr("readonly","readonly");
			$("textarea").attr("readonly","readonly");
			$('#submitBtn').addClass('disabled');
			$('#submitBtn').attr('disabled','disabled');
		}
		if( $("input[name='storageFlag']:checked").val() != 1){
			$('.qaOtherInfo').hide();
		}
	}
};
$(function(){
	THISPAGE._init();
});
//添加一行的回调函数
Topic.addRowCallback = function(row){
	$(row).children('td').each(function(index, item){
		if( !$(item).hasClass('tc') ) {
			$(item).text('');
		} else {
			$(item).find('i').data('id','-1');
		}
	});
};
//获取内容
var getContext = function(){
	return $('#answer').text();
}
//获取内容
var getTitle = function(){
	return $('#qaTitle').html();
}
</script>
<style type="text/css">
#I-main .control-group{float: left;}
#I-main .control-group .control-label{width: 100px;}
#I-main .control-group .controls{margin-left: 120px;}
#I-main .formSep{ margin:0;padding:0; border: 0;}
#I-main hr{display: inline-block;height: 1;width: 100%;margin: 10px 0 10px;border:0;border-bottom: 1px dashed #dcdcdc;}
</style>
</head>
<body class="has-scroll-y">
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills" style="width: 93%;">
	    <div class="page-header" style="text-align: center;">
			<h3><small id="qaTitle"><a href="${qa.sourceUrl}" target="_blank">${qa.title}</a></small></h3><br/>
			<h3><small>【 ${qa.publishDate} - ${qa.publishOffice}】</small></h3>
			<div class="page-toolbar no-border">
			    <a href="javascript:void(0)" class="btn-prev btn-prev-dis" id="prev" title="上一答疑"><b></b></a>
			    <a href="javascript:void(0)" class="btn-next btn-next-dis" id="next" title="下一答疑"><b></b></a>
			</div>
		</div>
		<form:form id="inputForm" modelAttribute="qa" action="${ctx}/platform/qa/save" method="post" class="form-horizontal form-min">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="version"/>
			<form:hidden path="type"/>
			<form:hidden path="editFlag"/>
			<form:hidden path="qFrom"/>
			<div class="control-group formSep" style="font-size: 14px;">
				<label class="control-label" style="font-size: 14px;">问:</label>
				<div class="controls">
				    ${qa.content}
				</div>
			</div>
			<div class="control-group formSep" style="font-size: 14px;">
				<label class="control-label" style="font-size: 14px;">答:</label>
				<div class="controls" id="answer">
				    ${qa.answer}
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">是否入库:</label>
				<div class="controls">
				     <c:if test="${not(qa.modifyMakeFlag)}">
				         <form:radiobutton path="storageFlag" value="1" data-form='uniform' class="required" cssStyle="display:none;"/>&nbsp;是
				     </c:if>
				     <c:if test="${qa.modifyMakeFlag}">
				         <label class='radio inline'>
					       <form:radiobutton path="storageFlag" value="1" data-form='uniform' class="required"/>&nbsp;是
					     </label>
					     <label class='radio inline'>
					       <form:radiobutton path="storageFlag" value="2" data-form='uniform' class="required"/>&nbsp;待定
					     </label>
					     <label class='radio inline'>
					       <form:radiobutton path="storageFlag" value="0" data-form='uniform' class="required"/>&nbsp;否
					     </label>
				     </c:if>
				</div>
			</div>
			<div class='qaOtherInfo'>
			
			<div class="widget-box transparent collapsed" id='qFromDiv'>
			     <div class='widget-header widget-header-flat'>
					<div class="widget-toolbar no-border">
					   <ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#I-main" data-level='I'>业务类型  </a></li>
							<li><a data-toggle="tab" href="#II-main" data-level='II'>税种</a></li>
							<li><a data-toggle="tab" href="#III-main" data-level='III'>备注</a></li>
					   </ul>
					</div>
				 </div>
				 <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main no-padding">
				        <div class="tab-content overflow-visible">
					        <div id="I-main" class="chart-main tab-pane active">
						        <div class="clearfix">
						        <c:if test="${qa.editFlag}">
									<div class="control-group formSep">
										<label class="control-label">业务类型:</label>
										<div class="controls">
										    <tags:treeselect id="businessType" name="businessTypeId" value="${qa.businessTypeId}" labelName="businessTypeName" labelValue="${qa.businessTypeName}"
											      title="业务类型" url="${ctx}/platform/businessType/treeSelect" cssClass="required"/>
										</div>
									</div>
									<div class="control-group formSep">
										<label class="control-label">行业:</label>
										<div class="controls">
											<tags:treeselect id="indutry" name="indutryId" value="${qa.indutryId}" labelName="indutryName" labelValue="${qa.indutryName}"
											      title="行业" url="${ctx}/platform/indutry/treeSelect" cssClass="required"/>
										</div>
									</div>
								</c:if>
								
								<c:if test="${not(qa.editFlag)}">
									<div class="control-group formSep">
										<label class="control-label">业务类型:</label>
										<div class="controls">
										    <tags:treeselect id="businessType" name="businessTypeId" value="${qa.businessTypeId}" labelName="businessTypeName" labelValue="${qa.businessTypeName}"
											      title="业务类型" url="${ctx}/platform/businessType/treeSelect" cssClass="required" disabled="disabled"/>
										</div>
									</div>
									<div class="control-group formSep">
										<label class="control-label">行业:</label>
										<div class="controls">
											<tags:treeselect id="indutry" name="indutryId" value="${qa.indutryId}" labelName="indutryName" labelValue="${qa.indutryName}"
											      title="行业" url="${ctx}/platform/indutry/treeSelect" cssClass="required" disabled="disabled"/>
										</div>
									</div>
								</c:if>
								</div>
								<hr/>
								<div class="clearfix">
								<div class="control-group formSep">
									<label class="control-label">关键字:</label>
									<div class="controls">
									    <form:input path="keyWords" cssStyle="width:800px;"/>
									</div>
								</div>
								</div>
								<div class="control-group formSep" style="display: none;">
									<label class="control-label">是否编题:</label>
									<div class="controls">
									     <form:hidden path="makeQFlag" value="1"/>&nbsp;是
									</div>
								</div>
					        </div>
					        <div id="II-main" class="chart-main tab-pane">
					             <table id="sample-table" class="table sample-table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="tc">序号</th>
											<th>涉及税种</th>
											<th>涉及税种明细项</th>
											<th>涉及法规文号</th>
											<th>涉及法规名称</th>
											<th>涉及法规内容</th>
											<th>全国通用</th>
											<th>适用省/市</th>
											<c:if test="${qa.editFlag}">
											  <th class="qa_rela_option"></th>
											</c:if>
										</tr>
									</thead>
									<tbody>
									    <c:if test="${fn:length(qa.qaRelas) == 0}">
										    <tr>
												<td class="index tc"></td>
												<td class="categoryName"></td>
												<td class="categoryItemName"></td>
												<td class="larDocNum"></td>
												<td class="larDocName"></td>
												<td class="larDocContent"></td>
												<td class="applyProvince"></td>
												<td class="applyCity"></td>
												<c:if test="${qa.editFlag}">
												    <td class="tc">
												        <i class="fa fa-pencil edit" data-id='-1' data-qid='${qa.id}'></i>
												        <i class="fa fa-plus add" data-id='-1' data-qid='${qa.id}'></i>
												        <i class="fa fa-minus delete" data-id='-1' data-qid='${qa.id}' data-del='true'></i>
													</td>
												</c:if>
											</tr>
									    </c:if>
										<c:forEach items="${qa.qaRelas}" var="vo" varStatus="i">
										    <tr>
												<td class="index tc">${i.index + 1}</td>
												<td class="categoryName">${vo.categoryName}</td>
												<td class="categoryItemName">${vo.categoryItemName}</td>
												<td class="larDocNum">${vo.larDocNum}</td>
												<td class="larDocName">${vo.larDocName}</td>
												<td class="larDocContent">${vo.larDocContent}</td>
												<td class="applyProvince">
												    <c:if test="${vo.applyProvince eq '1'}">是</c:if>
												    <c:if test="${vo.applyProvince eq '0'}">否</c:if>
												</td>
												<td class="applyCity">${vo.applyCity}</td>
												<c:if test="${qa.editFlag}">
												    <td class="tc">
												        <i class="fa fa-pencil edit" data-id='${vo.id}' data-qid='${qa.id}'></i>
												        <i class="fa fa-plus add" data-id='${vo.id}' data-qid='${qa.id}'></i>
												        <i class="fa fa-minus delete" data-id='${vo.id}' data-qid='${qa.id}' data-del='true'></i>
													</td>
												</c:if>
											</tr>
										</c:forEach>
									</tbody>
								</table>
					        </div>
					        <div id="III-main" class="chart-main tab-pane">
								<div class="control-group formSep">
									<label class="control-label">备注1:</label>
									<div class="controls">
									    <form:textarea path="remarks"  maxlength="500" cssStyle="width:95%;" rows="2"/>
									</div>
								</div>
								<div class="control-group formSep">
									<label class="control-label">备注2:</label>
									<div class="controls">
									    <form:textarea path="remarks2"  maxlength="500" cssStyle="width:95%;" rows="2"/>
									</div>
								</div>
								<div class="control-group formSep">
									<label class="control-label">备注3:</label>
									<div class="controls">
									    <form:textarea path="remarks3"  maxlength="500" cssStyle="width:95%;" rows="2"/>
									</div>
								</div>
					        </div>
				        </div>
				     </div>
				     </div>
			     </div>
			</div>
			<div class="control-group formSep" style="display:none;">
				<label class="control-label">筛选人:</label>
				<div class="controls">
				     <form:hidden path="updateId" readonly="true"/>
				     <form:input path="updateName" readonly="true"/>
				</div>
			</div>
			<div class="control-group formSep" style="display:none;">
				<label class="control-label">筛选时间:</label>
				<div class="controls">
				     <form:input path="updateDate" readonly="true"/>
				</div>
			</div>
			<div class="control-group formSep" style="display:none;">
				<label class="control-label">命题人:</label>
				<div class="controls">
				     <form:hidden path="makeId" readonly="true"/>
				     <form:input path="makeName" readonly="true"/>
				</div>
			</div>
			<div class="control-group formSep" style="display:none;">
				<label class="control-label">命题时间:</label>
				<div class="controls">
				     <form:input path="makeDate" readonly="true"/>
				</div>
			</div>
			</div>
			<div class="form-actions">
					<input id="submitBtn" class="btn btn-primary" type="submit" value="保 存"/>
					<input id="cancelBtn" class="btn" type="button" value="关 闭"/>
			</div>
		</form:form>
	  </div>
	</div>
</body>
</html>