<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ taglib prefix="fnt" uri="/WEB-INF/tld/fns-tax.tld"%>  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>试题--命题</title>
<meta name="decorator" content="topicForm"/>
<script type="text/javascript">
$(function(){
	THISPAGE._init();
	$(document).on('click', '#finishBtn', function(e){
		Public.confirmx("确认发布,试题发布后可以生成试卷?",function(){
			$('#optionType').val('FINALLY');
			$('#inputForm').submit();
		});
	});
	//显示不同的信息 -- 红色展示
	Public.postAjax(webRoot + '/admin/platform/topic/compare',{id:($('#id').val())},function(data){
		var diff = false;
		if(data.obj) {
			$.each(data.obj,function(key,value){
				if( value === false && !!$('#' +key).get(0) ) {
					$('#' +key).closest(".control-group").addClass('red');
					diff = true;
				}
			});
		}
		if(diff) {
			$('.page-header').find('h3:first-child').after('<span class="label label-important">注：</span>&nbsp;红色标题表示初审时信息有修改');
		}
	});
});
</script>
</head>
<body>
<div class="page-header">
	<h3>试题<small> &gt;&gt; 终审</small></h3>
	<div class="page-toolbar no-border">
	  <ul class="nav nav-tabs" id="recent-tab">
			<li><a data-toggle="tab" href="javascript:void(0)" data-type='INIT'>命题</a></li>
			<li><a data-toggle="tab" href="javascript:void(0)" data-type='INIT_VERITY'>初审</a></li>
			<li class="active"><a data-toggle="tab" href="javascript:void(0)" data-type='FINALLY_VERITY'>终审</a></li>
	  </ul>
	</div>
</div>
</body>
</html>