<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>试题审核  -- 终审</title>
<meta name="decorator" content="list"/>
<script src="${ctxModules}/taxPlatform/topic_common.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text =  "<i class='fa fa-list-alt detail' data-id='"+rowObject.id+"' title='查看'></i>";
			return text;
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/platform/topic/jSonFList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!1, 
				rownumbers: !0,//序号
				multiselect:true,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', ' ' , '试题编码', '题型', '难度', '来源', '题干', '税种', '税种明细', '是否公开', '命题人员', '命题时间', '初审人', '初审时间', '终审人', '终审时间'],
				colModel: [
                    {name:'id', index:'id', width:80,sortable:false,hidden:true,frozen:true},
                    {name:'options', index:'options',align:'center',width:80,sortable:false,frozen:true,formatter:optionsFmt},
	                {name:'code', index:'code', width:120, align:'center', sortable:false,frozen:true},
	                {name:'qtypeStr', index:'qtypeStr', width:100,align:'center',sortable:false,frozen:true},
	                {name:'qlevelStr', index:'qlevelStr', width:80,align:'center',sortable:false,frozen:true},
	                {name:'qfromStr', index:'qfromStr', width:80,align:'center',sortable:false,frozen:true},
	                {name:'content', index:'content', width:150,sortable:false,frozen:true},
	                {name:'categorys', index:'categorys', width:120,sortable:false},
					{name:'categoryItems', index:'categoryItems', width:120,sortable:false},
	                {name:'status', index:'status', width:80,align:'center',sortable:false,formatter:Topic.statusFmt},
	                {name:'createName', index:'createName', width:100,align:'center',sortable:false},
	                {name:'createDate', index:'createDate', width:120,sortable:false},
	                {name:'vauditName', index:'vauditName', width:100,align:'center',sortable:false},
	                {name:'vauditDate', index:'vauditDate', width:120,sortable:false},
	                {name:'fvauditName', index:'fvauditName', width:100,align:'center',sortable:false},
	                {name:'fvauditDate', index:'fvauditDate', width:120,sortable:false}
				]
			})
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		Topic.initListEvent();
		
		//格式化显示
		Public.combo('qType');
		Public.combo('qLevel');
		Public.combo('qFrom');
		Public.combo('type');
		//人员
		Public.autoCombo('createId', "${ctx}/system/user/jSonList");
		Public.autoCombo('categoryId', "${ctx}/platform/category/jSonList");
		Public.autoCascadeCombo('categoryItemId', "${ctx}/platform/categoryItem/jSonList",null,'categoryId');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
			        <input type="hidden" value="FINALLY" name="optionType" id="optionType"/>
					<div class="fl">
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">题型:</label>
									<div class="controls">
										<select name="qType" id="qType">
										     <option value=""></option>
										     <option value="ONE_CHOICE">单项选择题</option>
										     <option value="MULT_CHOICE">多项选择题</option>
										     <option value="ONE_MORE_CHOICE">不定项选择题</option>
										     <option value="TFNG">判断题</option>
										     <option value="COUNTING">计算题</option>
										</select>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">难度:</label>
									<div class="controls">
										<select name="qLevel" id="qLevel">
										     <option value=""></option>
										     <option value="I">容易</option>
										     <option value="II">一般</option>
										     <option value="III">很难</option>
										</select>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">来源:</label>
									<div class="controls">
										<select name="qFrom" id="qFrom">
										     <option value=""></option>
										     <option value="QA">答疑汇编</option>
										     <option value="EXAM">考试汇编</option>
										     <option value="CUSTOM">自定义</option>
										</select>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">题干:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="name"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">税种:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="categoryId" name="categoryId"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">税种明细:</label>
									<div class="controls">
										<input type="text" class="input-txt" id="categoryItemId" name="categoryItemId"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">命题人:</label>
									<div class="controls">
										<input type='text' id='createId' name='createId' maxlength="64" class="required"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="btn btn-primary query" type="button" value="查询"/>
					              <input class="btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="btn btn-primary" id="pBtn" value="&nbsp;公开&nbsp;">&nbsp;
					   <input type="button" class="btn" id="npBtn" value="&nbsp;不公开&nbsp;">&nbsp;
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>