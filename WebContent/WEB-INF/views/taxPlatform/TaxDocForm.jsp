<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#title").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			Public.closeTab('doc-edit');
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
  p.MsoNormal{
    margin: 10px;
  }
</style>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header" style="text-align: center;">
			<h3><small>${taxDoc.docName}</small></h3><br/>
			<h3><small>【 <fmt:formatDate value="${taxDoc.pubDate}" pattern="yyyy-MM-dd"/> - ${taxDoc.docNum}】【${taxDoc.enabled}】</small></h3>
		</div>
		<div>${taxDoc.docContent}</div>
		<h3 style="text-align: center;"><small><a href="javascript:void(0)" id="cancelBtn">【关闭】</a></small></h3><br/>
	  </div>
	</div>
</body>
</html>