<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" session="false"%>
<%Object errorCodeObj = request.getAttribute("shiroLoginFailure");String errorMsg = null;
  if( errorCodeObj != null) {
	  Integer errorCode = (Integer) errorCodeObj; 
	  if(errorCode == 40001) {
		  errorMsg = "用户名或密码错误.";
	  } else if(errorCode == 40002) {
		  errorMsg = "验证码错误.";
	  } else if(errorCode == 40003) {
		  errorMsg = "用户被锁定,请联系管理员.";
	  } else {
		  errorMsg = "用户名或密码错误.";
	  }
  }
%>
<html>
<head>
<title>TMT基础开发平台</title>
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/login.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/bootstrap/2.3.2/js/bootstrap.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#loginForm").validate({
			rules: {
				validateCode: {remote: "${pageContext.request.contextPath}/servlet/validateCodeServlet"}
			},
			messages: {
				username: {required: "请填写用户名."},password: {required: "请填写密码."},
				validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
			},
			errorLabelContainer: "#messageBox",
			errorPlacement: function(error, element) {
				error.appendTo($("#loginError").parent());
			} 
		});
	});
	//如果在框架中，则跳转刷新上级页面 --- 有bug,如果在页面输入的第三方登录，则无限循环（虽然不可能在页面输入第三方登录）
	if(self.frameElement && (self.frameElement.tagName=="IFRAME"||self.frameElement.tagName=="FRAME")){
		top.location.href="${ctx}/login";//直接这样写，更好一点
	}
</script>
</head>
<body>
	<div class="header"></div>
    <div class="main" id="mainBg" style="background-color: #f5f5f5;">
		<div>
			<div id="messageBox" class="alert alert-error <%=errorMsg==null?"hide":""%>"><button data-dismiss="alert" class="close">×</button>
				<label id="loginError" class="error"><%=errorMsg%></label>
			</div>
		</div>
		<div class="login">
			<div class="loginForm">
			    <form id="loginForm" class="form-signin" action="${ctx}/login" method="post">
			        <tags:token/>
			        <h2 class="form-signin-heading">TMT基础开发平台</h2>
			        <div class="control-group">
			           <label class="input-label control-label" for="username">登录名</label>
			           <div class="controls">
			             <input type="text" id="username" name="username" class="input-block-level required" value="${username}" placeholder="用户名/邮箱">
			           </div>
			        </div>
			        <div class="control-group">
				        <label class="input-label control-label" for="password">密码</label>
						<div class="controls">
						  <input type="password" id="password" name="password" class="input-block-level required" value="${password}" placeholder="密码">
						</div>
			        </div>
			        <c:if test="${isValidateCodeLogin}">
			        <div class="control-group validateCode">
			            <label class="input-label mid" for="validateCode" style="display: inline-block;">验证码</label>
						<tags:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
			        </div>
					</c:if>
					<div class="control-group clearfix">
						<div class="forgetPwdLine" style="display: inline-block;float: left;">						
							 <label for="rememberMe" title="十天内免登录" style="display: inline-block;"> 
							 <input type="checkbox" id="rememberMe" name="rememberMe"/> 十天内免登录（公共环境慎用）
							 </label>
						</div>
						<div class="forgetPwdLine" style="display: none;float: right;">	
							<label for="rememberMe" title="找回密码" style="display: inline-block;"> 
							 <a class="forgetPwd" target="_blank" title="找回密码">忘记密码了?</a></label>					
						</div>
					</div>
					<div class="control-group clearfix">
						<input class="btn btn-large btn-primary" type="submit" value="登 录" style="width:100%;float: left;"/>
					</div>
			    </form>
			</div>
		</div>
    </div>
    <div id="footer" class="footer">
		<div class="footer-inner" id="footerInner">
		    <a href="${ctx}/guestbook" target="_self">公共留言</a> | <a href="${ctx}/search" target="_self">全站搜索</a> | <a target="_self">站点地图</a> | <a href="mailto:618lf@163.com">技术支持</a> | <a href="${pageContext.request.contextPath}${fns:getAdminPath()}" target="_self">后台管理</a>
		</div>
	</div>
</body>
</html>
