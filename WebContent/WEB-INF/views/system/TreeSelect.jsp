<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<title>数据选择</title>
	<link href="${ctxStatic}/common/common.css" rel="stylesheet" type="text/css"/>
	<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
	<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/common/common.js" type="text/javascript"></script>
	<link href="${ctxStatic}/jquery-ztree/3.5.12/css/zTreeStyle/zTreeStyle.min.css" rel="stylesheet" type="text/css"/>
    <script src="${ctxStatic}/jquery-ztree/3.5.12/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
    <script src="${ctxStatic}/jquery-ztree/3.5.12/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		var key, lastValue = "", nodeList = [];
		var tree, setting = {view:{selectedMulti:false},check:{enable:"${checked}",nocheckInherit:true},
				data:{simpleData:{enable:true}},
				view:{
					fontCss:function(treeId, treeNode) {
						return (!!treeNode.highlight) ? {"font-weight":"bold"} : {"font-weight":"normal"};
					}
				},
				callback:{beforeClick:function(id, node){
						if("${checked}" == "true"){
							tree.checkNode(node, !node.checked, true, true);
							return false;
						}
					}, 
					onDblClick:function(){
						top.$.jBox.getBox().find("button[value='ok']").trigger("click");
					}
				}};
		$(document).ready(function(){
			Public.postAjax("${url}${fn:indexOf(url,'?')==-1?'?':'&'}extId=${extId}&module=${module}&t="+new Date().getTime(), null, function(zNodes){
				if(typeof(zNodes) == 'string') {
					zNodes = $.parseJSON(zNodes);
				}
				// 初始化树结构
				tree = $.fn.zTree.init($("#tree"), setting, zNodes);
				
				// 默认展开一级节点
				var nodes = tree.getNodesByParam("level", 0);
				for(var i=0; i<nodes.length; i++) {
					tree.expandNode(nodes[i], true, false, false);
				}
				// 默认选择节点
				if( !!"${selectIds}" ) {
					var ids = "${selectIds}".split(",");
					for(var i=0; i<ids.length; i++) {
						if( !ids[i]) continue;
						var node = tree.getNodeByParam("id", ids[i]);
						if("${checked}" == "true"){
							try{tree.checkNode(node, true, true);}catch(e){}
							tree.selectNode(node, false);
						}else{
							tree.selectNode(node, true);
						}
					}
				}
			});
			key = $("#key");
			key.bind("focus", focusKey).bind("blur", blurKey).bind("change keydown cut input propertychange", searchNode);
		});
	  	function focusKey(e) {
			if (key.hasClass("empty")) {
				key.removeClass("empty");
			}
		}
		function blurKey(e) {
			if (key.get(0).value === "") {
				key.addClass("empty");
			}
			searchNode(e);
		}
		function searchNode(e) {
			// 取得输入的关键字的值
			var value = $.trim(key.get(0).value);
			
			// 按名字查询
			var keyType = "name";
			if (key.hasClass("empty")) {
				value = "";
			}
			
			// 如果和上次一次，就退出不查了。
			if (lastValue === value) {
				return;
			}
			
			// 保存最后一次
			lastValue = value;
			
			// 如果要查空字串，就退出不查了。
			if (value === "") {
				return;
			}
			updateNodes(false);
			nodeList = tree.getNodesByParamFuzzy(keyType, value);
			updateNodes(true);
		}
		function updateNodes(highlight) {
			for(var i=0, l=nodeList.length; i<l; i++) {
				nodeList[i].highlight = highlight;				
				tree.updateNode(nodeList[i]);
				tree.expandNode(nodeList[i].getParentNode(), true, false, false);
			}
		}
	</script>
</head>
<body style="background-color: #fff">
    <div id="tree" class="ztree" style="padding:15px 20px;"></div>
</body>