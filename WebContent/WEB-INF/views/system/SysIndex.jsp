<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>TMT基础开发平台</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/layout.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="${ctxStatic}/bootstrap/2.3.2/js/bootstrap.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-tab/tabs.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-tab/tabs.css" rel="stylesheet" />
<script src="${ctxStatic}/common/common-extra.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/SysIndex.js" type="text/javascript"></script>
</head>
<body style="overflow: hidden;">
    <div id="header" class="navbar header">
        <div class="navbar-container">
		        <a id="tmt-home" class="tmt-home" href="javascript:void(0)"><i></i></a>
                <a class="brand" href="javascript::void(0)"><span class="first">TMT</span><span class="second">基础开发平台</span></a>
                <ul id="topMenu" class="nav">
                    <c:forEach items="${fns:getTopMenuList()}" var="menu">
                       <li><a class="menu" href="javascript:void(0)" data-id='${menu.id}' data-href='${menu.href}' data-type='${menu.type}'>${menu.name}</a></li>
                    </c:forEach>
	            </ul>
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="javascript::void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img class="nav-user-photo" src="${ctxStatic}/img/user.png" alt="Jason's Photo">
                            <span class="user-info">
								您好, <sys:user/>
							</span>
							<i class="icon-chevron-down icon-white"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript::void(0)" id="persionInfo"><i class="icon-user"></i>&nbsp;个人信息</a></li>
                            <li class="divider"></li>
                            <li><a href="${ctx}/logout"><i class="icon-off"></i>&nbsp;退出系统</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="${ctx}/logout">
                            <i class="icon-off  icon-white"></i>退出
                        </a>
                    </li>
                </ul>
        </div>
    </div>
	<div id="section" class="section">
	     <div class="row-fluid">
				<div class="sidebar" id="sidebar" href='${ctx}/system/menu/userMenu.action' style="overflow: hidden;"></div>
                <div class="content" id="content">
                    <div class="content-body" id="content-body" style="padding-top: 7px;">
                        <div id="page-tab" class="page-tab"></div>
                    </div>
                </div>
         </div>
	</div> 
</body>
</html>