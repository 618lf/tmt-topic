<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/role/initList?id="+$('#id').val();
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header">
	       <h3>角色管理 <small> &gt;&gt; 编辑</small></h3>
	    </div>
		<form:form id="inputForm" modelAttribute="role" action="${ctx}/system/role/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<div class="control-group formSep">
				<label class="control-label">角色名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">角色编码:</label>
				<div class="controls">
					<form:input path="code" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">数据范围:</label>
				<div class="controls">
				    <form:input path="dataScope" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">所属组织:</label>
				<div class="controls">
					<tags:treeselect id="office" name="officeId" value="${role.office.id}" labelName="officeName" labelValue="${role.office.name}"
					      title="所属组织" url="${ctx}/system/office/treeSelect" extId="-1" cssClass="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">选择菜单:</label>
				<div class="controls">
				     <tags:multiselect labelName="menuNames" labelValue="${role.menuNames}" name="menuIds"
				           value="${role.menuIds}" id="menuTag" defaultText="输入菜单 ..." selectUrl="${ctx}/system/menu/treeSelect" title="菜单" checked="true"></tags:multiselect>  
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">选择按钮权限:</label>
				<div class="controls">
				     <tags:multiselect labelName="optionNames" labelValue="${role.optionNames}" name="optionIds"
				           value="${role.optionIds}" id="optionTag" defaultText="输入菜单 ..." selectUrl="${ctx}/system/menu/treeSelect/option" title="菜单" checked="true"></tags:multiselect>  
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="btn btn-primary" type="submit" value="保 存"/>
				<input id="cancelBtn" class="btn" type="button" value="返 回"/>
			</div>
		</form:form>
	  </div>
	</div>
</body>
</html>