<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#title").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/group/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header">
			<h3>用户组<small> &gt;&gt; 编辑</small></h3>
		</div>
		<form:form id="inputForm" modelAttribute="group" action="${ctx}/system/group/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<div class="control-group formSep">
				<label class="control-label">用户组名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="100" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">用户组编码:</label>
				<div class="controls">
					<form:input path="code" htmlEscape="false" maxlength="100" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
			    <label class="control-label">用户组角色:</label>
				<div class="controls">
				     <tags:multiselect labelName="roleNames" labelValue="${group.roleNames}" name="roleIds"
				           value="${group.roleIds}" id="roleTag" defaultText="输入菜单 ..." selectUrl="${ctx}/system/role/treeSelect" title="角色" checked="true"></tags:multiselect>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">用户组描述:</label>
				<div class="controls">
					<form:textarea path="remarks" htmlEscape="false" rows="5" cssStyle="width:75%;"/>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="btn btn-primary" type="submit" value="保 存"/>
				<input id="cancelBtn" class="btn" type="button" value="返回"/>
			</div>
		</form:form>
	  </div>
	</div>
</body>
</html>