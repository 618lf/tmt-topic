<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			return "<i class='detail' data-id='"+rowObject.id+"' title='明细'></i>";
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/data/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!1, 
				rownumbers: !0,//序号
				multiselect:false,//定义是否可以多选
				multiboxonly: false,
				rowNum:100,
				colNames: ['表名', ''],
				colModel: [
	                {name:'name', index:'name', width:150,sortable:false},
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		
		//备份
		$(document).on('click','#backBtn',function(){
			Public.loading('数据备份中...可能需要几分钟,请不要操作数据库');
			Public.postAjax('${ctx}/system/data/backup',null,function(data){
				Public.loaded();
				if( data.success ) {
					Public.success("执行成功");
				} else {
					Public.error(data.msg);
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fr">
					   <input type="button" id="backBtn" class="btn btn-success" value="&nbsp;备&nbsp;份&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>