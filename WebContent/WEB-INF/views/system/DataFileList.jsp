<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text = "<span class='operate-wrap'><a href='javascript:void(0)' class='revert' data-id='"+rowObject.fileName+"' title='恢复'>恢复</a>";
			    text +="<a href='javascript:void(0)' data-id='"+rowObject.fileName+"' title='删除'>删除</a>";
			    text +="<a href='javascript:void(0)' data-id='"+rowObject.fileName+"' title='下载'>下载</a></span>";
			return text;
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/data/jSonFileList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:!1, 
				rownumbers: !0,//序号
				multiselect:false,//定义是否可以多选
				multiboxonly: false,
				rowNum:100,
				colNames: ['文件名', '时间', '大小',' '],
				colModel: [
	                {name:'fileName', index:'fileName', width:300,sortable:false},
	                {name:'modifyTime', index:'modifyTime',align:'center', width:200,sortable:false},
	                {name:'size', index:'size', width:120,align:'center',sortable:false,formatter:function(cellvalue, options, rowObject){
	                	return cellvalue + "KB";
	                }},
					{name:'options', index:'options',align:'center',width:150,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		//备份
		$(document).on('click','#backBtn',function(){
			Public.loading('数据备份中...可能需要几分钟,请不要操作数据库');
			Public.postAjax('${ctx}/system/data/backup',null,function(data){
				Public.loaded();
				if( data.success ) {
					Public.success("执行成功");
				} else {
					Public.error(data.msg);
				}
			});
		});
		//还原
		$(document).on('click','.revert',function(){
			var fileName = $(this).data('id');
			Public.loading('数据还原中...可能需要几分钟,请不要操作数据库');
			Public.postAjax('${ctx}/system/data/revert',null,function(data){
				Public.loaded();
				if( data.success ) {
					Public.success("执行成功");
				} else {
					Public.error(data.msg);
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
 .operate-wrap{
   display: inline-block;
 }
  .operate-wrap a{
    margin: 0 8px;
    color: #555555;
  }
</style>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fr">
					   <input type="button" id="backBtn" class="btn btn-success" value="&nbsp;开始备份&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>