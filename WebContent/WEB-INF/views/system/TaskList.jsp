<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			return Public.billsOper(cellvalue, options, rowObject);
		};
		var nextExecuteTimeFmt = function(cellvalue, options, rowObject){
			if(rowObject.manualOperation >=1){
				return "等待立即执行";
			}
			return cellvalue;
		};
		var manualFmt = function(cellvalue, options, rowObject){
			if( cellvalue <= 0 ) {
				return '否';
			} else {
				return '是';
			}
		};
		var taskTypeFmt = function(cellvalue, options, rowObject){
			if( cellvalue == 'PERIOD_TASK' ) {
				return '周期任务';
			} else {
				return '一次性任务';
			}
		};
		var allowExecuteFmt = function(cellvalue, options, rowObject){
			if( !cellvalue ) {
				return '无限制';
			} else {
				return cellvalue;
			}
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/task/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				colNames: ['任务Id','任务名称', '任务类型', '计划执行次数', '已经执行次数', '上次执行时间','预计下次执行时间', '状态', '立即执行', '操作'],
				colModel: [
	                {name:'id', index:'id', width:150,sortable:false,hidden:true},
					{name:'name', index:'name', width:150,sortable:false},
					{name:'type', index:'type',width:100,sortable:false,formatter:taskTypeFmt},
					{name:'allowExecuteCount', index:'allowExecuteCount',align:'center',width:120,sortable:false,formatter:allowExecuteFmt},
					{name:'yetExecuteCount', index:'yetExecuteCount', align:'center', width:100,sortable:false},
					{name:'preExecuteTime', index:'preExecuteTime',align:'center',width:150,sortable:false},
					{name:'nextExecuteTime', index:'nextExecuteTime',align:'center',width:150,sortable:false,formatter:nextExecuteTimeFmt},
					{name:'taskStatusName', index:'taskStatusName',align:'center',width:80,sortable:false},
					{name:'manualOperation', index:'manualOperation',align:'center',width:80,sortable:false,formatter:manualFmt},
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		Public.initBtnMenu();
		
		var deleteTask = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
				Public.deletex("确定删除选中的定时任务？","${ctx}/system/task/delete",param,function(data){
					if(!!data){
						Public.doQuery();
					} else {
						Public.error("删除错误!");
					}
				});
			} else {
				Public.error("请选择要删除的定时任务!");
			}
		};
		
		//添加
		$(document).on('click','#addBtn',function(){
			window.location.href = "${ctx}/system/task/form";
		});
		
		//执行
		$(document).on('click','#excBtn',function(){
			var param = [];
			var checkeds = $('#grid').getGridParam('selarrrow');
			if( !!checkeds && checkeds.length) {
				$.each(checkeds,function(index,item){
					var taskId = $('#grid').getRowData(item).id;
					param.push({name:'idList',value:taskId});
				});
				Public.executex("确定执行选中的定时任务？","${ctx}/system/task/execute",param,function(data){
					if(!!data){
						Public.doQuery();
					} else {
						Public.error("立即执行错误!");
					}
				});
			} else {
				Public.error("请选择要执行的任务!");
			}
		});
		
		//暂停
		$(document).on('click','#waitBtn',function(){
			var param = [];
			var checkeds = $('#grid').getGridParam('selarrrow');
			if( !!checkeds && checkeds.length) {
				$.each(checkeds,function(index,item){
					var taskId = $('#grid').getRowData(item).id;
					param.push({name:'idList',value:taskId});
				});
				Public.executex("确定暂停选中的定时任务？","${ctx}/system/task/lock",param,function(data){
					if(!!data){
						Public.doQuery();
					} else {
						Public.error("暂停错误!");
					}
				});
			} else {
				Public.error("请选择要暂停的任务!");
			}
		});
		
		//删除
		$(document).on('click','#delBtn',function(){
			var checkeds = $('#grid').getGridParam('selarrrow');
			deleteTask(checkeds);
		});
		
		//行 事件
		$(document).on('click', '.edit', function(){
			window.location.href = "${ctx}/system/task/form?id="+$(this).attr('data-id');
		});
		//行 事件
		$('#dataGrid').on('click','.delete',function(e){
			deleteTask($(this).attr('data-id'));
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
			        <input type="hidden" name="orgId" id="orgId" value="${orgId}"/>
					<div class="fl">
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">任务名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="name"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="btn btn-primary" type="button" onclick="Public.doQuery()" value="查询"/>
					              <input class="btn" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="btn btn-primary" id="addBtn" onclick="optionsFmt4E()" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" class="btn btn-success" id="excBtn" onclick="optionsFmt4Exc()" value="&nbsp;立即执行&nbsp;">
					   <input type="button" class="btn" id="waitBtn" onclick="optionsFmt4Lock()" value="&nbsp;暂&nbsp;停&nbsp;">
					   <input type="button" class="btn" id="delBtn" onclick="optionsFmt4D()" value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>