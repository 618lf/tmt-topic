<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(Public.validate());
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/task/initList";
		});
		Public.combo('#type,#businessObject');
		Public.uniform();
	}
};
$(function(){
	THISPAGE._init();
});	
</script>
</head>
<body>
	<div class="row-fluid wrapper">
	    <div class="bills">
	      <div class="page-header">
	       <h3>任务管理 <small> &gt;&gt; 编辑</small></h3>
	      </div>
	      <form:form id="inputForm" modelAttribute="task" action="${ctx}/system/task/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<tags:message content="${message}"/>
			<div class="control-group formSep">
				<label class="control-label">任务名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">任务类型:</label>
				<div class="controls">
				    <form:select path="type" class="required">
				       <form:option value="PERIOD_TASK">周期任务</form:option>
				       <form:option value="ONCE_TASK">一次性任务</form:option>
				    </form:select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">执行间隔（秒 分 时 天  月 周年）:</label>
				<div class="controls">
				    <form:input path="cronExpression" htmlEscape="false" maxlength="200"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">允许执行的次数:</label>
				<div class="controls">
					<form:input path="allowExecuteCount" htmlEscape="false" maxlength="200"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">业务对象:</label>
				<div class="controls">
					<form:select path="businessObject" items="${business}" itemLabel="label" itemValue="value"/>
				</div>
			</div> 
			<div class="control-group formSep">
				<label class="control-label">状态:</label>
				<div class="controls">
					 <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="NEW" data-form='uniform'/>&nbsp;待启动
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="RUNABLE" data-form='uniform'/>&nbsp;等待执行
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="RUNNING" data-form='uniform'/>&nbsp;执行中
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="WAIT" data-form='uniform'/>&nbsp;暂停
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="FINISH" data-form='uniform'/>&nbsp;结束
				     </label>
				</div>
			</div> 
			<div class="form-actions">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>
				<input id="cancelBtn" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
			</div>
		  </form:form>
	    </div>
	</div>
</body>
</html>