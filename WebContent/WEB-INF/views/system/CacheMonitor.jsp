<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>报表中心 - 行业统计</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		//折叠事件
		Public.initPublicEvent();
		this.initChart();
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body class="white">
<div class="row-fluid wrapper">
   <div class="bills" style="width: 93%;">
      <!-- 图表  -->
      <div class="widget-box transparent">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>缓存  - （统计）</small></h3>
		    <div class="widget-toolbar no-border">
			   <ul class="nav nav-tabs">
			        <c:forEach items="${caches}" var="vo">
				       <li <c:if test="${vo eq selectCache}">class="active"</c:if> ><a data-toggle="tab" href="#I-main" data-level='${vo}'>${vo}</a></li>
				    </c:forEach>
			   </ul>
			</div>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		        <div class="tab-content overflow-visible">
			        <div id="I-main" class="tab-pane active">
			             <ul>
			                 <li>名称：${cacheStat.name}</li>
			                 <li>对象数量：${cacheStat.size}</li>
			                 <li>内存中对象数量：${cacheStat.sizeM}</li>
			                 <li>磁盘中对象数量：${cacheStat.sizeD}</li>
			                 <li>内存中对象大小：${cacheStat.sizeMM}</li>
			                 <li>磁盘中对象大小：${cacheStat.sizeDM}</li>
			                 <li>/：${cacheStat.sizeHM}</li>
			                 <li>命中次数：${cacheStat.hits}</li>
			                 <li>内存命中次数：${cacheStat.memoryHits}</li>
			                 <li>磁盘命中次数：${cacheStat.diskHits}</li>
			                 <li>丢失次数：${cacheStat.cacheMisses}</li>
			                 <li>缓存读取的已经被销毁的对象丢失次数：${cacheStat.evictionCount}</li>
			             </ul>
			        
			        </div>
		        </div>
		     </div>
		     </div>
	   </div>
	  </div>
	  <!-- 列表 -->
	  <div class="widget-box transparent" id="list">
	    <div class='widget-header widget-header-flat'>
		    <h3><span class="icon32 icon-rssfeed"></span><small>缓存  - （元素）</small></h3>
	    </div>
	    <div class="widget-body">
		     <div class="widget-body-inner">
		     <div class="widget-main">
		       <div class="top">
			          <form name="queryForm" id="queryForm" action="${ctx}/system/cache/monitor#list" class="form-horizontal form-fluid" method="post">
							<tags:token ignore="true"/>
							<div class="fl">
							  <div class="control-group" style="margin-right: 10px;">
								<label class="control-label">缓存:</label>
								<div class="controls">
									<select name="name">
									  <c:forEach items="${caches}" var="vo">
									     <option <c:if test="${vo eq selectCache}">selected="selected"</c:if> value="${vo}">${vo}</option>
									  </c:forEach>
									</select>
								</div>
							  </div>
							  <input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
							</div>
			          </form>
		        </div>
		        <table id="sample-table" class="table sample-table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="tc">序号</th>
							<th width="10%">值</th>
							<th>创建时间</th>
							<th>最后访问时间</th>
							<th>最后修改时间</th>
							<th>过期时间</th>
							<th>命中次数</th>
							<th>存活时间</th>
							<th>空闲时间</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${elements}" var="vo" varStatus="i">
						    <tr>
								<td class="tc">${i.index + 1}</td>
								<td class="tl">${vo.value}</td>
								<td class="tc">${vo.createDate}</td>
								<td class="tc">${vo.accessDate}</td>
								<td class="tc">${vo.lastUpdateDate}</td>
								<td class="tc">${vo.expirationDate}</td>
								<td class="tc">${vo.hitCount}</td>
								<td class="tc">${vo.timeToLive}</td>
								<td class="tc">${vo.timeToIdle}</td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
		     </div>
		     </div>
	    </div> 
	  </div>
   </div>
</div>
</body>
</html>