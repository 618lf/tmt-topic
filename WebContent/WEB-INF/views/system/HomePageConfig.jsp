<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		 this.addEvent();
	 },
	 addEvent : function(){
		 var that = this;
		 //绑定菜单打开事件
		 Public.pageTab();
		 $('input').iCheck({
			    checkboxClass: 'icheckbox_square',
			    radioClass: 'iradio_square',
			    increaseArea: '20%' // optional
		 });
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
</head>
<body>
	<div class="wrapper cf">
		<div class="top">&nbsp;</div>
		<div class="col-main">
			<div class="main-wrap cf">
				<div class="m-top cf"></div>
				<ul class="quick-links">
				    <c:forEach items="${menus}" var="menu">
				        <li><a href="${ctx}/${menu.href}"
						    rel="pageTab" data-id="menu" parentopen="true" title="菜单管理">
						      <span class="helper-font-48"><i class="${menu.iconClass}"></i></span>${menu.name}
						    </a>
					    </li>
				    </c:forEach>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>