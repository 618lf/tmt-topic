<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ taglib prefix="fnc" uri="/WEB-INF/tld/fns-com.tld"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>模版配置</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(t){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#templateName").focus();
		$("#inputForm").validate({
				submitHandler: function(form){
					Public.loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					element.addClass('text_error');
				}
		});
	},
	addEvent : function(){
        $("#btnBack").on('click',function(){
        	window.location.href = "${ctx}/system/excel/initList";
		});
        Public.initSimpleTableEvent('sample-table');
        $(document).on('focus','#inputForm input', function(){
        	$(this).removeClass('text_error');
        });
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills" style="width: 93%;">
	     <div class="page-header">
	         <h3>模版配置 <small> &gt;&gt; 编辑</small></h3>
	     </div>
	     <form:form id="inputForm" modelAttribute="template" action="${ctx}/system/excel/save" method="post" class="form-horizontal form-fluid" cssStyle="margin:0px;">
	     <tags:token/>
	     <form:hidden path="id"/>
	     <div class="clearfix">
	        <div class="control-group">
				<label class="control-label">模版名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="100" class="required"/>
				</div>
			 </div>
			 <div class="control-group">
					<label class="control-label">模版类型:</label>
					<div class="controls">
						<form:input path="type" htmlEscape="false" maxlength="100" class="required"/>
					</div>
			 </div>
			 <div class="control-group">
					<label class="control-label">目标类:</label>
					<div class="controls">
						<form:input path="targetClass" htmlEscape="false" maxlength="100" class="required"/>
					</div>
			 </div>
			 <div class="control-group">
					<label class="control-label">扩展属性:</label>
					<div class="controls">
						<form:input path="extendAttr" htmlEscape="false" maxlength="100"/>
					</div>
			 </div>
			 <div class="control-group">
					<label class="control-label">开始行:</label>
					<div class="controls">
						<form:select path="startRow">
						  <form:option value="3">3</form:option>
						  <form:option value="4">4</form:option>
						  <form:option value="5">5</form:option>
						</form:select>
					</div>
			 </div>
	     </div>
	     <div class="widget-box transparent">
		    <div class='widget-header widget-header-flat'>
			    <h3><small>模版列配置</small></h3>
		    </div>
		    <div class="widget-body">
			     <div class="widget-body-inner">
			     <div class="widget-main  no-padding">
			     <table id="sample-table" class="table sample-table table-striped table-bordered">
					<thead>
						<tr>
							<th class="tc">序号</th>
							<th>操作</th>
							<th>Excel列名称</th>
							<th width="8%">Excel列序号</th>
							<th>类属性</th>
							<th width="8%">数据类型</th>
							<th>格式化表达式</th>
							<th width="8%">校验</th>
							<th>校验表达式</th>
						</tr>
					</thead>
					<tbody>
					    <c:if test="${fn:length(template.items) == 0 }">
					    <tr class='sample-table-row'>
							<td class="index tc">1</td>
							<td class="tc options"><i class="fa fa-plus add" title="添加"></i><i class="fa fa-remove delete" title="删除"></i></td>
							<td class="tc"><input  name="items.id" type='hidden' maxlength="50" value='-1'/><input  name="items.name" type='text' class="required" maxlength="50"/></td>
							<td class="tc"><select name="items.columnName" class="columnName">
							       <c:forEach items="${fnc:getExcelColumns(26)}" var="vo">
							        <option value="${vo.key}">${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.property" type='text' class="required"  maxlength="50"/></td>
							<td class="tc"><select name="items.dataType" class="dataType">
							       <c:forEach items="${fnc:getDataTypes()}" var="vo">
							        <option value="${vo.key}">${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.dataFormat" type='text'  maxlength="200"/></td>
							<td class="tc"><select name="items.verifyType" class="verifyType">
							       <c:forEach items="${fnc:getVerifyTypes()}" var="vo">
							        <option value="${vo.key}">${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.verifyFormat" type='text' maxlength="200"/></td>
						</tr>
					    </c:if>
					    <c:forEach items="${template.items}" var="item" varStatus="i">
					    <tr class='sample-table-row'>
							<td class="index tc">${i.index +1}</td>
							<td class="tc options"><i class="fa fa-plus add" title="添加"></i><i class="fa fa-remove delete" title="删除"></i></td>
							<td class="tc"><input  name="items.id" type='hidden' maxlength="50" value='${item.id}'/><input  name="items.name" type='text' class="required" maxlength="50" value="${item.name}"/></td>
							<td class="tc"><select name="items.columnName" class="columnName">
							       <c:forEach items="${fnc:getExcelColumns(26)}" var="vo">
							        <option value="${vo.key}" <c:if test="${item.columnName eq vo.key}">selected="selected"</c:if>>${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.property" type='text' class="required"  maxlength="50" value="${item.property}"/></td>
							<td class="tc"><select name="items.dataType" class="dataType">
							       <c:forEach items="${fnc:getDataTypes()}" var="vo">
							        <option value="${vo.key}" <c:if test="${item.dataType eq vo.key}">selected="selected"</c:if>>${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.dataFormat" type='text'  maxlength="200" value='${item.dataFormat}'/></td>
							<td class="tc"><select name="items.verifyType" class="verifyType">
							       <c:forEach items="${fnc:getVerifyTypes()}" var="vo">
							        <option value="${vo.key}" <c:if test="${item.verifyType eq vo.key}">selected="selected"</c:if>>${vo.value}</option>
							       </c:forEach>
							    </select></td>
							<td class="tc"><input  name="items.verifyFormat" type='text' maxlength="200" value='${item.verifyFormat}'/></td>
						</tr>
					    </c:forEach>
					</tbody>
				 </table>
			     </div>
			     </div>
			</div>
		 </div>
		 <div class="form-actions">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>
				<input id="btnBack" class="btn" type="button" value="返回"/>
		 </div>
	     </form:form>
	   </div>
	</div>
</body>
</html>