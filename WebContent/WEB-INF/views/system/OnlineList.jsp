<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/online/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				rownumbers: !0,//序号
				multiselect:false,//定义是否可以多选
				multiboxonly: false,
				colNames: ['ID', '用户名称', '登录IP', '用户代理', '登录服务器', '当前状态', '登录系统时间', '退出系统时间'],
				colModel: [
	                {name:'id', index:'id', width:80,sortable:false,hidden:true},
	                {name:'userName', index:'userName', width:100,sortable:false},
	                {name:'host', index:'host', width:100,sortable:false},
					{name:'userAgent', index:'userAgent', width:200,sortable:false},
					{name:'systemHost', index:'systemHost',width:100,sortable:false},
					{name:'status', index:'status',width:100,sortable:false},
					{name:'startTime', index:'startTime',align:'center',width:100,sortable:false},
					{name:'endTime', index:'endTime',align:'center',width:100,sortable:false}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		Public.autoCombo('#userName','${ctx}/system/user/jSonList');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">用户:</label>
									<div class="controls">
										<input type="text" name="userName" id='userName'/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="btn btn-primary query" type="button" value="查询"/>
					              <input class="btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="btn btn-primary" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="btn btn-primary" value="&nbsp;强制退出&nbsp;">
					   <input type="button" class="btn btn-success" value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>