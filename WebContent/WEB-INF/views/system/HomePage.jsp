<!DOCTYPE html>
<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="chart"/>
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<link href="${ctxStatic}/common/quick-menu.css" rel="stylesheet" />
<link href="${ctxStatic}/common/opa-icons.css" rel="stylesheet" />
<script src="${ctxModules}/taxPlatform/topic_report.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	 _init : function(){
		 this.loadData();
		 this.loadCharts();
		 this.addEvent();
	 },
	 loadData : function(){
		 this.loadAudit();
		 //图表数据
		 Topic.chartByQTypeAndQLevel('I-main');
		 
		 //图表数据
		 Topic.chartByBusinessType('main2');
		
		 //图表数据
		 Topic.chartByIndutry('main3');
		 
		 //图表数据
		 Topic.chartByCategory('main4');
	 },
	 loadAudit : function(){
		 Public.loading();
		 var template = '<li><b>{{=auditCount}}</b><span class="es" title="{{=name}}"><i class="icon-list"></i>&nbsp;{{=name}}</span></li>';
		 //异步获取审核数据
		 Public.postAjax('${ctx}/platform/topic/audit',{},function(data){
			 var html = [];
			 $.each(data.obj,function(index,item){
				 html.push(Public.runTemplate(template, item));
			 });
			 $('#ajaxList').html(html.join(''));
			 Public.loaded();
		 });
	 },
	 loadCharts : function(){
		//tab 切换  -- 取消默认的事件
		$(document).on('click.tmt.widget','[data-toggle]',function(e){
			var n = $(this);
			var p = n.data("toggle"), selector = n.attr('href').replace(/.*#(?=[^\s]*$)/, '');
			var level = n.data("level");
			if ( n.parent('li').hasClass('active') ) return;
			//图表数据
			Topic.chartByQTypeAndQLevel(selector, level);
		});
	 },
	 addEvent : function(){
		 var that = this;
		 //绑定菜单打开事件
		 Public.pageTab();
		 $(document).on('click','#report-refresh',function(){
			 that.loadAudit();
		 });
		 //折叠事件
		 Public.initPublicEvent();
		 
		 //查找事件
		 $(document).on('click','.search-btn',function(){
			var form = $(this).closest(".form-search");
			var url = form.attr('action');
			var param = "?query=" + form.find('.search-query').val();
			Public.openOnTab('doc-search', '税收法规查找', url + param );
		 });
	 }
};
$(function(){
    THISPAGE._init();
});
</script>
<style type="text/css">
.chart-main {
	height: 400px;
	overflow: hidden;
	padding: 10px;
	margin-bottom: 10px;
	border: 1px solid #e3e3e3;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body>
<div class="wrapper cf">
	<div class="top">
	    <strong><span id="greetings">${dateSx}好</span>，<span id="username"><sys:user/></span></strong>
	    <sys:authenticated>我是登录用户</sys:authenticated>
	    <sys:rememberMe>我是记住我用户</sys:rememberMe>
	</div>
	<div class="col-main">
	    <div class="main-wrap cf">
	       <div class="widget-box transparent">
			     <div class='widget-header widget-header-flat'>
				    <h3><span class="icon32 icon-messages"></span><small>法规检索</small></h3>
					<div class="widget-toolbar no-border">
					   
					</div>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				       <form class="form-search" action="${ctx}/platform/taxdoc/initSearcher" method="get">
					       <div class="input-append">
						    <input type="text" class="search-query" placeholder="查找法规..."/>
						    <input type="button" class="btn search-btn" value="查找"/>
						   </div>
				       </form>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
	    <div class="main-wrap cf">
	       <div class="widget-box transparent">
			     <div class='widget-header widget-header-flat'>
				    <h3><span class="icon32 icon-messages"></span><small>快捷菜单</small></h3>
					<div class="widget-toolbar no-border">
					   
					</div>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				        <ul class='quick-menu'>
				            <c:forEach items="${menus}" var="menu">
				              <li><a href="${webRoot}${menu.href}" class="${menu.quickMenu}" rel="pageTab" data-id="qc-${menu.id}" parentopen="true" title="${menu.name}">${menu.name}</a></li>
				            </c:forEach>
						</ul>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
		<div class="main-wrap cf">
			<div class="widget-box transparent">
			    <div class='widget-header widget-header-flat'>
				    <h3><span class="icon32 icon-rssfeed"></span><small>试题统计  - （难度）</small></h3>
					<div class="widget-toolbar no-border">
					   <ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#I-main" data-level='I'>容易</a></li>
							<li><a data-toggle="tab" href="#II-main" data-level='II'>一般</a></li>
							<li><a data-toggle="tab" href="#III-main" data-level='III'>很难</a></li>
					   </ul>
					</div>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				        <div class="tab-content overflow-visible">
					        <div id="I-main" class="chart-main tab-pane active"></div>
					        <div id="II-main" class="chart-main tab-pane"></div>
					        <div id="III-main" class="chart-main tab-pane"></div>
				        </div>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
		<div class="main-wrap cf">
			<div class="widget-box transparent">
			     <div class='widget-header widget-header-flat'>
				    <h3><small> <i class="icon-tasks"></i> 试题统计  - （业务类型 ） <span class="badge badge-important">10</span></small></h3>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				        <div id="main2" class="chart-main"></div>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
		<div class="main-wrap cf">
			<div class="widget-box transparent">
			     <div class='widget-header widget-header-flat'>
				    <h3><small> <i class="icon-tasks"></i> 试题统计 - （行业） <span class="badge badge-important">10</span></small></h3>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				        <div id="main3" class="chart-main"></div>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
		
		<div class="main-wrap cf">
			<div class="widget-box transparent">
			     <div class='widget-header widget-header-flat'>
				    <h3><small> <i class="icon-tasks"></i> 试题统计 - （税种） <span class="badge badge-important">10</span></small></h3>
			    </div>
			    <div class="widget-body">
				     <div class="widget-body-inner">
				     <div class="widget-main">
				        <div id="main4" class="chart-main"></div>
				     </div>
				     </div>
			   </div>
			</div>
		</div>
	</div>
	<div class="col-extra">
		<div class="extra-wrap">
			<h2>待办事项</h2>
			<div class="list">
				<ul id="ajaxList" class="">
					<li><b></b><span class="es" title=""></span></li>
				</ul>
				<div class="sp">
					 <a id="report-refresh"><i class="fa fa-refresh"></i>&nbsp;刷新</a>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>