<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title><sitemesh:title/> - Powered By TMT</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="renderer" content="webkit">
		<script src="${ctxStatic}/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
		<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet"/>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
		<link   href="${ctxStatic}/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
		<link   href="${ctxStatic}/common/common-taxsns.css" rel="stylesheet"/>
		<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
		<sitemesh:head/>
	</head>
	<body class="white">
		<div id="header" class="navbar navbar-default navbar-fixed-top">
		    <div class="container">
			   <div class="navbar-header">
			      <a href="#" class="navbar-brand">税务公社</a>
			   </div>
			   <ul class="nav navbar-nav">
		        <li class="active"><a href="#">首页</a></li>
		        <li><a href="#">比赛</a></li>
		        <li><a href="#">测评</a></li>
		        <li><a href="#">微课</a></li>
		        <li><a href="#">答疑</a></li>
		        <li><a href="#">创作分享计划</a></li>
		       </ul>
		       <sys:isGuest>
			       <ul class="nav navbar-nav navbar-right">
			        <li><a href="${ctxFront}/login">登录</a></li>
			        <li><a href="#">注册</a></li>
			       </ul>
		       </sys:isGuest>
		       <sys:isUser>
			       <ul class="nav navbar-nav navbar-right">
			        <li><a href="#">欢迎:<sys:user/></a></li>
			        <li><a href="${ctxFront}/logout">退出</a></li>
			       </ul>
		       </sys:isUser>
		    </div>
		</div>
		<div id="main" class="container-fluid" style="margin-top: 50px;">
			<div class="container">
			  <sitemesh:body/>
			</div>
		</div>
		<div id="footer" class="navbar navbar-default">
            <div class="container"></div>
        </div>
	</body>
</html>