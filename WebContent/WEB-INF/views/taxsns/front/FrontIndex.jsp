<%@ include file="/WEB-INF/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<title>税务公社</title>
	<meta name="decorator" content="default_front"/>
	<meta name="description" content="税务公社  ${description}"/>
	<meta name="keywords" content="税务公社  ${keywords}"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//登录 -- 支持ajax和form表单的登录，退出
			$(document).on('click','#ajaxLogin',function(){
				var param = [];
				    param.push({name:'username', value:($('#username').val())});
				    param.push({name:'password', value:($('#password').val())});
				    param.push({name:'validateCode', value:($('#validateCode').val())});
				Public.postAjax('${ctxFront}/login?timeid='+ Math.random(),param,function(data){
					if(data.code == 1) {
						window.document.location.href="${ctxFront}";
					}else{
						alert(data.msg);
					}
					if(!!data.captcha) {//加载验证码 -- 每次跟换验证码
						$('#captcha').show();
						$('#captcha').find('.validateCodeRefresh').click();
						$('#validateCode').val('');
					}
				});
			});
			//退出 -- 支持ajax和form表单的登录，退出
			$(document).on('click','#ajaxLogout',function(){
				Public.postAjax('${ctxFront}/logout?timeid='+ Math.random(),null,function(data){
					if(data.code == 1) {
						window.document.location.href="${ctxFront}";
					}
				});
			});
		});
	</script>
</head>
<body>
    <!-- 前端登录和后端登录需要分开 -->
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	    <sys:isGuest>
	    <form method="post" action="${ctx}/login" id="loginForm">
	         <input type="text" id="username" name="username" class="input-block-level required" value="SuperAdmin">
	         <input type="password" id="password" name="password" class="input-block-level required" value="hello1">
	         <div id="captcha" style="display: none;">
	           <tags:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
	         </div>
	         <input type="button" class="btn" value="登录" id="ajaxLogin"/>
	    </form>
	    </sys:isGuest>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
	<div class="bs-callout bs-callout-waring">
	    <h4>我的首页<small></small></h4>
	</div>
</body>
</html>