<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%Object errorCodeObj = request.getAttribute("shiroLoginFailure");String errorMsg = null;
  if( errorCodeObj != null) {
	  Integer errorCode = (Integer) errorCodeObj; 
	  if(errorCode == 40001) {
		  errorMsg = "用户名或密码错误.";
	  } else if(errorCode == 40002) {
		  errorMsg = "验证码错误.";
	  } else if(errorCode == 40003) {
		  errorMsg = "用户被锁定,请联系管理员.";
	  } else {
		  errorMsg = "用户名或密码错误.";
	  }
  }
%>
<html>
<head>
	<title>税务公社</title>
	<meta name="decorator" content="default_front"/>
	<meta name="description" content="税务公社  ${description}"/>
	<meta name="keywords" content="税务公社  ${keywords}"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
    <!-- 前端登录和后端登录需要分开 -->
	<div class="bs-callout bs-callout-waring">
	    <h4>表单登录<small><%=errorMsg==null?"":errorMsg%></small></h4>
	    <sys:isGuest>
	    <form action="${ctxFront}/login" id="loginForm" method="post">
	         <input type="text" id="username" name="username" class="input-block-level required" value="SuperAdmin">
	         <input type="password" id="password" name="password" class="input-block-level required" value="hello1">
	         <c:if test="${isValidateCodeLogin}">
	            <tags:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
	         </c:if>
	         <input type="submit" class="btn" value="登录" id="ajaxLogin"/>
	    </form>
	    </sys:isGuest>
	</div>
</body>
</html>