<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ attribute name="ignore" type="java.lang.Boolean" required="false" description="是否取消token验证"%>
<input id="token" name="token" readonly="readonly" type="hidden" value="${token}"/>
<c:if test="${true == ignore}">
<input id="token_ignore" name="token.ignore" readonly="readonly" type="hidden" value="true"/> 
</c:if>