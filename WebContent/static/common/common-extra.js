if (!("Public" in window)) {
	window.Public = {};
}
Public.config = {
	cookie_expiry : 604800,
	storage_method : 2
};
Public.settings = {
	is : function(b, a) {
		return (Public.data.get("settings", b + "-" + a) == 1);
	},
	exists : function(b, a) {
		return (Public.data.get("settings", b + "-" + a) !== null);
	},
	set : function(b, a) {
		Public.data.set("settings", b + "-" + a, 1);
	},
	unset : function(b, a) {
		Public.data.set("settings", b + "-" + a, -1);
	},
	remove : function(b, a) {
		Public.data.remove("settings", b + "-" + a);
	},
	sidebar_collapsed : function(c) {
		c = c || false;
		var e = document.getElementById("sidebar");
		var d = document.getElementById("sidebar-collapse").querySelector('[class*="fa-"]');
		var f = document.getElementById("content");
		var b = d.getAttribute("data-icon1");
		var a = d.getAttribute("data-icon2");
		if (c) {
			Public.addClass(e, "menu-min");
			Public.addClass(f, "content-max");
			Public.removeClass(d, b);
			Public.addClass(d, a);
			Public.settings.set("sidebar", "collapsed");
		} else {
			Public.removeClass(e, "menu-min");
			Public.removeClass(f, "content-max");
			Public.removeClass(d, a);
			Public.addClass(d, b);
			Public.settings.unset("sidebar", "collapsed");
		}
	},
};
Public.settings.check = function(c, e) {
	if (!Public.settings.exists(c, e)) {
		return;
	}
	var a = Public.settings.is(c, e);
	var b = {
		"navbar-fixed" : "navbar-fixed-top",
		"sidebar-fixed" : "sidebar-fixed",
		"breadcrumbs-fixed" : "breadcrumbs-fixed",
		"sidebar-collapsed" : "menu-min",
		"main-container-fixed" : "container"
	};
	var d = document.getElementById(c);
	if (a != Public.hasClass(d, b[c + "-" + e])) {
		Public.settings[c.replace("-", "_") + "_" + e](a);
	}
};
Public.data_storage = function(e, c) {
	var b = "Public.";
	var d = null;
	var a = 0;
	if ((e == 1 || e === c) && "localStorage" in window
			&& window.localStorage !== null) {
		d = Public.storage;
		a = 1;
	} else {
		if (d == null && (e == 2 || e === c) && "cookie" in document
				&& document.cookie !== null) {
			d = Public.cookie;
			a = 2;
		}
	}
	this.set = function(h, g, i, k) {
		if (!d) {
			return
		}
		if (i === k) {
			i = g;
			g = h;
			if (i == null) {
				d.remove(b + g);
			} else {
				if (a == 1) {
					d.set(b + g, i);
				} else {
					if (a == 2) {
						d.set(b + g, i, Public.config.cookie_expiry);
					}
				}
			}
		} else {
			if (a == 1) {
				if (i == null) {
					d.remove(b + h + "." + g);
				} else {
					d.set(b + h + "." + g, i);
				}
			} else {
				if (a == 2) {
					var j = d.get(b + h);
					var f = j ? JSON.parse(j) : {};
					if (i == null) {
						delete f[g];
						if (Public.sizeof(f) == 0) {
							d.remove(b + h);
							return;
						}
					} else {
						f[g] = i;
					}
					d.set(b + h, JSON.stringify(f), Public.config.cookie_expiry);
				}
			}
		}
	};
	this.get = function(h, g, j) {
		if (!d) {
			return null;
		}
		if (g === j) {
			g = h;
			return d.get(b + g);
		} else {
			if (a == 1) {
				return d.get(b + h + "." + g);
			} else {
				if (a == 2) {
					var i = d.get(b + h);
					var f = i ? JSON.parse(i) : {};
					return g in f ? f[g] : null;
				}
			}
		}
	};
	this.remove = function(g, f, h) {
		if (!d) {
			return
		}
		if (f === h) {
			f = g;
			this.set(f, null);
		} else {
			this.set(g, f, null);
		}
	};
};
Public.cookie = {
	get : function(c) {
		var d = document.cookie, g, f = c + "=", a;
		if (!d) {
			return
		}
		a = d.indexOf("; " + f);
		if (a == -1) {
			a = d.indexOf(f);
			if (a != 0) {
				return null;
			}
		} else {
			a += 2;
		}
		g = d.indexOf(";", a);
		if (g == -1) {
			g = d.length;
		}
		return decodeURIComponent(d.substring(a + f.length, g));
	},
	set : function(b, e, a, g, c, f) {
		var h = new Date();
		if (typeof (a) == "object" && a.toGMTString) {
			a = a.toGMTString();
		} else {
			if (parseInt(a, 10)) {
				h.setTime(h.getTime() + (parseInt(a, 10) * 1000));
				a = h.toGMTString();
			} else {
				a = "";
			}
		}
		document.cookie = b + "=" + encodeURIComponent(e)
				+ ((a) ? "; expires=" + a : "") + ((g) ? "; path=" + g : "")
				+ ((c) ? "; domain=" + c : "") + ((f) ? "; secure" : "");
	},
	remove : function(a, b) {
		this.set(a, "", -1000, b);
	}
};
Public.storage = {
	get : function(a) {
		return window.localStorage.getItem(a);
	},
	set : function(a, b) {
		window.localStorage.setItem(a, b);
	},
	remove : function(a) {
		window.localStorage.removeItem(a);
	}
};
Public.sizeof = function(c) {
	var b = 0;
	for ( var a in c) {
		if (c.hasOwnProperty(a)) {
			b++;
		}
	}
	return b;
};
Public.hasClass = function(b, a) {
	return (" " + b.className + " ").indexOf(" " + a + " ") > -1;
};
Public.addClass = function(c, b) {
	if (!Public.hasClass(c, b)) {
		var a = c.className;
		c.className = a + (a.length ? " " : "") + b;
	}
};
Public.removeClass = function(b, a) {
	Public.replaceClass(b, a);
};
Public.replaceClass = function(c, b, d) {
	var a = new RegExp(("(^|\\s)" + b + "(\\s|$)"), "i");
	c.className = c.className.replace(a, function(e, g, f) {
		return d ? (g + d + f) : " ";
	}).replace(/^\s+|\s+$/g, "");
};
Public.toggleClass = function(b, a) {
	if (Public.hasClass(b, a)) {
		Public.removeClass(b, a);
	} else {
		Public.addClass(b, a);
	}
};
Public.data = new Public.data_storage(Public.config.storage_method);