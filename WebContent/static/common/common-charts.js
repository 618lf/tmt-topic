var Chart = Chart ||{};
//是否初始化
Chart._INIT = true;

//类型
Chart._TYPE = {
        LINE:    'echarts/chart/line',
        BAR:     'echarts/chart/bar',
        SCATTER: 'echarts/chart/scatter',
        K:       'echarts/chart/k',
        PIE:     'echarts/chart/pie',
        RADAR:   'echarts/chart/radar',
        FORCE:   'echarts/chart/force',
        CHORD:   'echarts/chart/chord',
        GAUGE:   'echarts/chart/gauge',
        FUNNEL:  'echarts/chart/funnel'	
};
//初始化
Chart._init = function(){
	//入口
	var fileLocation = '../echarts/js/echarts';
    require.config({
        paths:{ 
            'echarts': fileLocation,
            'echarts/chart/line': fileLocation,
            'echarts/chart/bar': fileLocation,
            'echarts/chart/scatter': fileLocation,
            'echarts/chart/k': fileLocation,
            'echarts/chart/pie': fileLocation,
            'echarts/chart/radar': fileLocation,
            'echarts/chart/map': fileLocation,
            'echarts/chart/chord': fileLocation,
            'echarts/chart/force': fileLocation,
            'echarts/chart/gauge': fileLocation,
            'echarts/chart/funnel': fileLocation,
            
        }
    });
    this._INIT = false;
};
//加载图形
Chart.loadChart = function(type, dom, option, callback){
	var that = this;
	if(!that._INIT) {that._init();}
	// 按需加载
	require(
	    (function(){
	      var requires = ['echarts'];
	          requires.push(type);
	      return requires;
	    })(),
	    function(ec, theme){
	    	var myChart = ec.init(document.getElementById(dom), that.theme); 
	    	    myChart.setOption(option); 
	    	if( !!callback ) {
	    		callback(myChart, ec);
	    	}
	    }
	);
};
//柱状图
Chart.loadBarChart = function(dom, option, callback) {
	var _option = {
		title : {text: '柱状统计图'},
	    tooltip : { show:true, trigger: 'item' },
	    yAxis : [  { type : 'value' } ],
	    toolbox: {
	        show : true,
	        orient: 'vertical',
            x: 'right',
            y: 'top',
	        feature : {
	            mark : {show: true},
	            dataView : {show: true, readOnly: false},
	            magicType : {show: true, type: ['line', 'bar']},
	            restore : {show: true},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true
	};
	Chart.loadChart(this._TYPE.BAR, dom, $.extend({},_option,option), callback);
};
