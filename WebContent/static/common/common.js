/**
 * 公共函数库，主要是一些JS工具函数，各种插件的公共设置
 * @author HenryYan
 */
(function($) {
	
	//String startWith
	String.prototype.startWith=function(str){     
	  var reg=new RegExp("^"+str);     
	  return reg.test(this);        
	}; 
	
	//String endWith
	String.prototype.endWith=function(str){     
	  var reg=new RegExp(str+"$");     
	  return reg.test(this);        
	};
	
	/**
     * 获取元素的outerHTML
     */
    $.fn.outerHTML = function() {
        // IE, Chrome & Safari will comply with the non-standard outerHTML, all others (FF) will have a fall-back for cloning
        return (!this.length) ? this : (this[0].outerHTML ||
        (function(el) {
            var div = document.createElement('div');
            div.appendChild(el.cloneNode(true));
            var contents = div.innerHTML;
            div = null;
            return contents;
        })(this[0]));
    };
    
    //判断:当前元素是否是被筛选元素的子元素
    $.fn.isChildOf = function(b){
        return (this.parents(b).length > 0);
    };

    //判断:当前元素是否是被筛选元素的子元素或者本身
    $.fn.isChildAndSelfOf = function(b){
        return (this.closest(b).length > 0);
    };
    
    //ajax 全局设置
    $(document).ajaxSend(function(event, jqXHR, ajaxOptions) {
    	jqXHR.setRequestHeader("token", $.cookie('token'));
    });
    
    //普通的表格维护
    STCore = {
		_add : function(row){
			var newRow = $(row).clone();
			newRow.find("input:text").val('');
			newRow.find("input:hidden").val('');
			newRow.find("select").val('');
			$(row).after(newRow);
			return newRow;
		},
		_del : function(table,row){
			if(table.rows.length == 2){
				return false;
			}
			table.deleteRow(row.rowIndex);
			return true;
		},
		_update : function(row,values){
			$.each(values,function(index,value){
				$(row).find("." + index).each(function(index,item){
					if( $(item).is(':radio') || $(item).is(':checkbox') || $(item).is(':input') ) {
						$(item).val(value);
					} else if( value != null) {
						$(item).text(value);
					}
				});
			});
		},
		_reset : function(table){
			for( var i=1; i< table.rows.length;i++){
			   var cell = table.rows[i].cells[0];
			   if($(cell).hasClass('index')) {
				   cell.innerHTML = i;
			   }
			} 
		}
	};
	$.fn.simpleTable = function(o,t,v){
		var returnValue;
		switch(o){
		  case 'add':returnValue = STCore._add(t);break;
		  case 'del':returnValue = STCore._del(this.get(0), t);break;
		  case 'update':returnValue = STCore._update(t,v);break;
		}
		STCore._reset(this.get(0));
		return returnValue;
	};
})(jQuery);

/**
 * 工具类
 */
var Public = Public || {};
Public.isIE6 = !window.XMLHttpRequest;

//快捷键
Public.keyCode = {
	ALT: 18,
	BACKSPACE: 8,
	CAPS_LOCK: 20,
	COMMA: 188,
	COMMAND: 91,
	COMMAND_LEFT: 91, // COMMAND
	COMMAND_RIGHT: 93,
	CONTROL: 17,
	DELETE: 46,
	DOWN: 40,
	END: 35,
	ENTER: 13,
	ESCAPE: 27,
	HOME: 36,
	INSERT: 45,
	LEFT: 37,
	MENU: 93, // COMMAND_RIGHT
	NUMPAD_ADD: 107,
	NUMPAD_DECIMAL: 110,
	NUMPAD_DIVIDE: 111,
	NUMPAD_ENTER: 108,
	NUMPAD_MULTIPLY: 106,
	NUMPAD_SUBTRACT: 109,
	PAGE_DOWN: 34,
	PAGE_UP: 33,
	PERIOD: 190,
	RIGHT: 39,
	SHIFT: 16,
	SPACE: 32,
	TAB: 9,
	UP: 38,
	F7: 118,
	F12: 123,
	S: 83,
	WINDOWS: 91 // COMMAND
};
/**
 * 节点赋100%高度
 *
 * @param {object} obj 赋高的对象
*/
Public.setAutoHeight = function(obj){
  if(!obj || obj.length < 1){
	return ;
  }
  Public._setAutoHeight(obj);
	$(window).bind('resize', function(){
		Public._setAutoHeight(obj);
  });
};
Public._setAutoHeight = function(obj){
	obj = $(obj);
	var winH = $(window).height();
	var h = winH - obj.offset().top - (obj.outerHeight() - obj.height());
	obj.height(h);
};
//Ajax请求，
//url:请求地址， params：传递的参数[{name,value}]， callback：请求成功回调  
Public.postAjax = function(url, params, callback){    
	$.ajax({  
	   type: "POST",
	   url: url,  
	   cache: false,  
	   async: true,  
	   dataType: "json",  
	   data: params,  
	   //当异步请求成功时调用  
	   success: function(data, status){  
		   callback(data);  
	   },  
	   //当请求出现错误时调用  只要状态码不是200 都会执行这个
	   error: function(x, s, e){
		    var msg = $.parseJSON(x.responseText).msg;
			Public.openWindow(msg,"系统错误",800,500,{
				buttons:{"关闭":true},
				submit:function(v, h, f){
				}, loaded:function(h){
		        }, closed:function(){ 
		        }
		    });
	   }  
	});  
};
//Ajax请求，
//url:请求地址， params：传递的参数{...}， callback：请求成功回调  
Public.getAjax = function(url, params, callback){    
	$.ajax({  
	   type: "GET",
	   url: url,  
	   cache: false,  
	   async: true,  
	   dataType: "json",  
	   data: params,  
	   //当异步请求成功时调用  
	   success: function(data, status){  
		   callback(data);  
	   },  
	   //当请求出现错误时调用  只要状态码不是200 都会执行这个
	   error: function(x, s, e){
		   var msg = $.parseJSON(x.responseText).msg;
		   Public.openWindow(msg,"系统错误",800,500,{
				buttons:{"关闭":true},
				submit:function(v, h, f){
				}, loaded:function(h){
		        }, closed:function(){ 
		        }
		   });
	   }  
	});  
};
//同步执行表单, form 表单提交
Public.ajaxSubmit = function(form, url, check, success){
	$(form).ajaxSubmit({
		url: url,
		dataType:"json",
		beforeSubmit : check,  
		success: function(data){
			Public.loaded();
			if( typeof(data) == 'string' ) {
				data = $.parseJSON(data);
			}
			if(success) {
				success(data);
			}
		},
		error : function(x){
			Public.loaded();
			var msg = $.parseJSON(x.responseText).msg;
		    Public.openWindow(msg,"系统错误",800,500,{
				buttons:{"关闭":true},
				submit:function(v, h, f){
				}, loaded:function(h){
		        }, closed:function(){ 
		        }
		    });
		}
	});
};
//列表添加删除
Public.billsOper = function (val, opt, row, add) {
	var text =  "<i class='fa fa-pencil edit' data-id='"+row.id+"' title='编辑'></i>";
		if(!!add) {
		   text += "<i class='fa fa-plus add'  data-id='"+row.id+"' title='添加子节点'></i>";
		}
	    text += "<i class='fa fa-remove delete' data-id='"+row.id+"' title='删除'></i>";
	return text;
};
//
Public.setGrid = function(param){
	var adjust = param || 64;
	var gridW = $("#dataGrid").innerWidth() - 2, gridH = $(window).height() - $("#dataGrid").offset().top - adjust;
	return {
		w : gridW,
		h : gridH
	};
};

//重设表格宽高
Public.resizeGrid = function(adjustH, adjustW){
	var grid = $("#grid");
	Public.delayPerform(function(){
		grid.jqGrid('setGridHeight', ($(window).height() - $("#dataGrid").offset().top - 65));
		grid.jqGrid('setGridWidth', ($("#dataGrid").innerWidth() - 2),true);
	},100).done(function(){
		grid.jqGrid('setGridHeight', ($(window).height() - $("#dataGrid").offset().top - 65));
		grid.jqGrid('setGridWidth', ($("#dataGrid").innerWidth() - 2),true);
	});
};

//默认的表格
Public.defaultGrid = function( params ){
	var options  = params||{};
	if( !options.form && !$('#queryForm') ) {
		alert("请设置form");
	}
	var formObj = options.form || $('#queryForm');
	var defaults = {
			datatype: "json",//xml，local，json，jsonnp，script，xmlstring，jsonstring，clientside
			mtype:'POST',//POST或者GET，默认GET
			rowNum:15,
			rowList:[15,25,50,100],
			viewrecords:true,//定义是否显示总记录数
			autoencode:true,//对url进行编码
			autowidth:true,//如果为ture时，则当表格在首次被创建时会根据父元素比例重新调整表格宽度。如果父元素宽度改变，为了使表格宽度能够自动调整则需要实现函数：setGridWidth
			loadtext:'数据加载中...',//当请求或者排序时所显示的文字内容
			multiselect:true,//定义是否可以多选（复选框）
			multiboxonly: true,
			altRows: true,
			gridview: true,
			rownumbers: !1,//序号
			cellEdit: !1,//是否可以编辑
			pager:"#page",
			page:1,//设置初始的页码
			pagerpos:'left',//指定分页栏的位置
			recordpos:'right',//定义了记录信息的位置： left, center, right
			recordtext:'当前显示{0} - {1} 条记录   共 {2} 条记录',//显示记录数信息。{0} 为记录数开始，{1}为记录数结束。 viewrecords为ture时才能起效，且总记录数大于0时才会显示此信息
			shrinkToFit:true,//此属性用来说明当初始化列宽度时候的计算类型，如果为ture，则按比例初始化列宽度。如果为false，则列宽度使用colModel指定的宽度
			jsonReader : { 
			      root: "data",   
			      page: "param.pageIndex",   
			      total: "param.pageCount",   
			      records: "param.recordCount",   
			      repeatitems: false   
		    },
		    prmNames:{
		    	page:"param.pageIndex",
		    	rows:'param.pageSize',
		    	sort: 'param.sortField',
		    	order: 'param.sortType',
		    	search:'search', 
		    	nd:'nd',
		    	npage:null
		    },
		    loadError:function(xhr,status,error){
		    	Public.error("数据加载失败:" + status);
		    },
		    beforeRequest:function(){
		    	var that = $(this);
		    	that.jqGrid('setGridParam',{postData:(function(form){
		    		var obj = {};
		    		$.each(form.serializeArray(),function(index,item){
		    			if(!(item.name in obj)){  
		    	            obj[item.name]=item.value;  
		    	        }  
		    		});
		    		return obj;
		    	})($(formObj))});
		    }
	};
	return $.extend({},defaults,options);
};
//可编辑的表格
Public.defaultEditGrid = function( params ){
	var options  = params||{};
	var defaults = {
			datatype: "clientSide",
			height: "100%",
            rownumbers: !0,
            gridview: !0,
            onselectrow: !1,
            cmTemplate: { sortable: !1, title: !1},
            forceFit: !0,
            rowNum: 1e3,
            cellEdit: !1,
            cellsubmit: "clientArray",
            localReader: {
                root: "rows",
                records: "records",
                repeatitems: !1,
                id: "id"
            },
            jsonReader: {
                root: "data.entries",
                records: "records",
                repeatitems: !1,
                id: "id"
            },
            loadonce: !0,
            footerrow: !1,
            userDataOnFooter: !0,
            userData: {},
            loadComplete: function (t) {},
            gridComplete: function () {},
            afterEditCell: function (t, e, i, a) {},
            formatCell: function () {},
            beforeSubmitCell: function () {},
            afterSaveCell: function (t, i, a, r, n) {},
            loadError: function (t, e) {}
	};
	return $.extend({},defaults,options);
};
//树型表格  -- 不支持冻结列
//第一列必须是 expandColumn 指定的列
Public.treeGrid = function( params ){
	var options  = params||{};
	var defaults = {
		treeGrid: true,
        treeGridModel: 'adjacency'
	};
	return Public.defaultGrid($.extend({},defaults,options));
};
//回车事件
Public.bindEnterDo = function(obj, func){
	var args = arguments;
	$(obj).on('keydown', 'input[type="text"]:visible:not(:disabled)', function(e){
		if (e.keyCode == '13') {
			if (typeof func == 'function') {
				var _args = Array.prototype.slice.call(args, 2 );
				func.apply(null,_args);
			}
		}
	});
};
//初始化公用事件
Public.initPublicEvent = function(){
	//widget 事件
	$(document).on('click.tmt.widget','[data-action]',function(e){
		e.preventDefault();
		var n = $(this);
		var p = n.data("action");
		var b = n.closest(".widget-box");
		if (p == "collapse") { //下拉事件
			var j = b.hasClass("collapsed") ? "show" : "hide";
			var f = j == "show" ? "shown" : "hidden";
			var c;
			b.trigger(c = $.Event(j + ".tmt.widget"));
			if (c.isDefaultPrevented()) {
				return
			}
			var g = b.find(".widget-body");
			var m = n.find("[class*=fa-]").eq(0);
			var h = m.attr("class").match(/fa\-(.*)\-(up|down)/);
			var d = "fa-" + h[1] + "-down";
			var i = "fa-" + h[1] + "-up";
			var l = g.find(".widget-body-inner");
			if (l.length == 0) {
				g = g.wrapInner( '<div class="widget-body-inner"></div>').find(":first-child").eq(0)
			} else {
				g = l.eq(0)
			}
			var e = 300;
			var k = 200;
			if (j == "show") {
				if (m) {
					m.addClass(i).removeClass(d)
				}
				b.removeClass("collapsed");
				g.slideUp(0, function() {
					g.slideDown(e, function() {
						b.trigger(c = $.Event(f + ".tmt.widget"))
					})
				})
			} else {
				if (m) {
					m.addClass(d).removeClass(i)
				}
				g.slideUp(k, function() {
					b.addClass("collapsed");
					b.trigger(c = $.Event(f + ".tmt.widget"))
				})
			}
		}
	});
	
	//tab 切换  -- 取消默认的事件
	$(document).on('click.tmt.widget','[data-toggle]',function(e){
		e.preventDefault();
		var n = $(this);
		var p = n.data("toggle"), selector = n.data('target') , previous  , $target , e;
		var b = n.closest(".widget-box");
		if (p == "tab") { //tab 轮询事件
			 if (!selector) {
		        selector = n.attr('href')
		        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
	         }
			 if ( n.parent('li').hasClass('active') ) return;
			 var $ul = n.closest('ul.nav-tabs:not(.dropdown-menu)');
			 previous = $ul.find('.active:last a')[0];
			 e = $.Event('show', {
		        relatedTarget: previous
		     });
		     n.trigger(e);
		     if (e.isDefaultPrevented()) return;
		     $target = $(selector);
		     var activate = function(element, container, callback){
		    	 $active = container.find('> .active');
		    	 transition = callback && $.support.transition && $active.hasClass('fade');
			     var next = function(){
			    	 $active.removeClass('active');
				     element.addClass('active');
				     
				     if (transition) {
				         element[0].offsetWidth // reflow for transition
				         element.addClass('in')
			         } else {
			             element.removeClass('fade')
			         }
				     callback && callback()
			     };
			     transition ? $active.one($.support.transition.end, next) : next()
			     $active.removeClass('in')
		     };
		     activate(n.parent('li'), $ul);
		     activate($target, $target.parent(), function () {
		         n.trigger({  type: 'shown' , relatedTarget: previous })
		     });
		}
	});
	
	//setting-box 事件
	$(document).on('click.tmt.widget','.settings-btn',function(e){
		$(this).toggleClass("open");
		$(this).closest('.settings-container').find('.settings-box').toggleClass("open");
	});
};
//初始化查询组键事件
Public.initBtnMenu = function(){
	//菜单按钮
	$(document).on('click.tmt.menu-btn','.ui-btn-menu .ui-menu-btn',function(e){
		if($(this).hasClass("ui-btn-dis")) {
			return false;
		}
		$(this).parent().toggleClass('ui-btn-menu-cur');
		$(this).blur();
		e.preventDefault();
	});
	//组合按钮
	$(document).on('click.tmt.group-btn','.ui-btn-menu .ui-menu-btn',function(e){
		if($(this).hasClass("ui-btn-dis")) {
			return false;
		}
		$(this).parent().toggleClass('open');
		$(this).blur();
		e.preventDefault();
	});
	//其他按钮
	$(document).bind('click.menu',function(e){
		var target  = e.target || e.srcElement;
		//下面两个是点击页面其他地方关闭下拉菜单
		$('.ui-btn-menu').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.dropdown-menu',menu).is(':visible')){
				 menu.removeClass('ui-btn-menu-cur');
			};
		});
		$('.ui-btn-group').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.dropdown-menu',menu).is(':visible')){
				 menu.removeClass('open');
			};
		});
		//查询事件
		if( $(target).hasClass("query") ) {
			$(target).closest(".ui-btn-menu").removeClass('ui-btn-menu-cur');
			Public.doQuery();
		}
		//重置事件
		if( $(target).hasClass("reset") ) {
			Public.resetQuery();
		}
		//更多条件
		if($(target).attr('id') == 'conditions-trigger') {
			  e.preventDefault();
			  if (!$(target).hasClass('conditions-expand')) {
					$('#more-conditions').stop().slideDown(200, function(){
					   $('#conditions-trigger').addClass('conditions-expand').html('收起更多<b></b>');
					   $('#filter-reset').css('display', 'inline');
				 	 });
			  } else {
				  	$('#more-conditions').stop().slideUp(200, function(){
					  $('#conditions-trigger').removeClass('conditions-expand').html('更多条件<b></b>');
					  $('#filter-reset').css('display', 'none');
				  	});
			  };	
		}
	});
	//jgrid 的 jBox引发窗口变化问题
	$(window).resize(function(){
		//Public.resizeGrid();
	});
};
Public.tipType = {
	 INFO:'info',
	 WARNING:'warning',
	 SUCCESS:'success',
	 ERROR:'error',
	 LOADING:'loading'
};
Public.alert = function(mess, type, callback){
	top.$.jBox.tip.mess = null;
	top.$.jBox.tip(mess,type,{persistent:true,opacity:0});
	if(typeof(callback) == 'function'){
		callback();
	}
};
Public.info = function(mess){
	Public.alert(mess,Public.tipType.INFO);
};
Public.error = function(mess){
	Public.alert(mess,Public.tipType.ERROR);
};
Public.success = function(mess, callback) {
	Public.alert(mess,Public.tipType.SUCCESS, callback);
};
Public.loading = function(mess) {
	Public.alert(mess||'数据加载中...',Public.tipType.LOADING);
};
Public.loaded = function(delay) {
	Public.delayPerform(function(){
		top.$.jBox.closeTip();
	}, delay||500);
};
Public.closeWindow = function(wId){
	//关闭窗体
	$.jBox.close(wId);
}; 
//确认对话框
Public.confirmx = function(mess, ok, cancel){
	top.$.jBox.confirm(mess,'系统提示',function(v,h,f){
		if(v=='ok'){
			if( typeof(ok) == 'function' ) {
				ok();
			}
		} else {
			if( typeof(cancel) == 'function' ) {
				cancel();
			}
		}
	},{buttonsFocus:1});
	return false;
};
//执行某个动作
Public.executex = function(mess, url, param, ok, cancel) {
	Public.confirmx(mess,function(){
		Public.loading('正在提交，请稍等...');
		Public.postAjax(url,param,function(data){
    		if(typeof(data) == "string") {
          		 data = $.parseJSON(data);
          	}
    		top.$.jBox.closeTip();
    		ok(data);
        });
	}, cancel);
};
//删除
Public.deletex = function(mess, url, param, ok) {
	Public.executex(mess,url,param,ok,null);
};
//对话框  html
Public.openWindow = function(content,title,width,height,options, ok, cancel){
	var defaults =  {
			top:'20px',
			buttons:{"确定":"ok", "关闭":true},
			submit:function(v, h, f){
				if ( v=="ok" && !!ok ){
					var iframe = null;
					if( content.startWith("iframe:")) {
						iframe = h.find('iframe').get(0).contentWindow;
					}
					return ok(h, iframe,  f);
				}
				return true;
			},loaded:function(h){//加载完后执行的函数
	          $(".jbox-content", top.document).css("overflow-y","hidden");
	          $(".jbox-content", document).css("overflow-y","hidden");
	          //网页的滚动条
	          $("body.has-scroll-y").eq(0).css("overflow-y","hidden");
	        }, closed:function(){//关闭窗口执行的函数
	           if(!!cancel) {
	        	   cancel();
	           }
	           $("body.has-scroll-y").eq(0).css("overflow-y","auto");
	        }
	};
	if($.jBox){
		$.jBox.open(content, title, width, height, $.extend({},defaults, options));
	} else {
		top.$.jBox.open(content, title, width, height, $.extend({},defaults, options));
	}
};
//对话框  url
Public.openUrlWindow = function(url, title, width, height, options, ok, cancel){
	Public.openWindow("iframe:"+url, title, width, height, options, ok, cancel);
};
//打开一个窗体
Public.windowOpen = function(url, name, width, height){
	var top=parseInt((window.screen.height-height)/2,10),left=parseInt((window.screen.width-width)/2,10),
		options="location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,modal=yes,alwaysRaised=yes,"+
		"resizable=yes,scrollbars=yes,"+"width="+width+",height="+height+",top="+top+",left="+left;
	window.open(url ,name , options);
};

/**
 * 替换当前页面
 */
Public.refreshWindow = function(url, param, target){
	var htmlStr = [];
	var formName = 'form'+Math.random();
	htmlStr.push('<form method="post" name="'+formName);
	htmlStr.push('" action="'+url);
	htmlStr.push('" style="visibility: hidden"');
	htmlStr.push('>');
	$.each(param||[],function(i,item){
		htmlStr.push('<input type="hidden" name="'+i);
		htmlStr.push('" value="'+item);
		htmlStr.push('"/>');
	});
	htmlStr.push('<input type="hidden" name="token.ignore" value="true"/>');
	htmlStr.push('</form>');
	$('body').append(htmlStr.join(''));
	var randomForm = $(document.getElementsByName(formName).item(0));
	randomForm.attr('target',target||'_self');
	randomForm.submit();
	randomForm.remove();
};
//重置查询条件框
Public.resetQuery = function(){
	$(".dropdown-menu").find('.select2-offscreen').each(function(index,item){//格式化为select2的
		$(item).select2('val','');
	});
	$(".dropdown-menu").find("select").each(function(index,item){
		if(!$(item).hasClass('select2-offscreen')) {
			$(item).val('');
		}
	});
	$(".dropdown-menu").find("input[type='text']").each(function(index,item){
		if(!$(item).hasClass('select2-offscreen')) {
			$(item).val('');
		}
	});
	$(".dropdown-menu").find("textarea").val("");
};
//列表框的查询
Public.doQuery = function(gridName){
	$('#' + (gridName||'grid')).jqGrid('setGridParam',{page:1}).trigger("reloadGrid");
};

//表单列表的添加删除事件
Public.billsEvent = function(obj, type, flag){
	var _self = obj;
	//新增row
	$('#dataGrid').on('click', '.ui-icon-plus', function(e){
		var rowId = $(this).parent().data('id');
		var newId = $('#grid tbody tr').length;
		var datarow = { id: _self.newId };
		var su = $("#grid").jqGrid('addRowData', _self.newId, datarow, 'after', rowId);
		if(su) {
			$(this).parents('td').removeAttr('class');
			$(this).parents('tr').removeClass('selected-row ui-state-hover');
			$("#grid").jqGrid('resetSelection');
			_self.newId++;
		}
	});
	//删除row
	$('#dataGrid').on('click', '.ui-icon-trash', function(e){
		if($('#grid tbody tr').length === 2) {
			Public.error("至少保留一条分录！");
			return false;
		}
		var rowId = $(this).parent().data('id');
		var su = $("#grid").jqGrid('delRowData', rowId);
	});
	//取消row编辑状态
	$(document).bind('click.cancel', function(e){
		if(!$(e.target).closest(".ui-jqgrid-bdiv").length > 0 && curRow !== null && curCol !== null){
		   $("#grid").jqGrid("saveCell", curRow, curCol);
		   curRow = null;
		   curCol = null;
		};
	});
};

/*批量绑定页签打开*/
Public.pageTab = function() {
	$(document).on('click', '[rel=pageTab]', function(e){
		e.preventDefault();
		var tabid = $(this).attr('data-id'), url = $(this).attr('href'), showClose = $(this).attr('showClose'), text = $(this).attr('title') || $(this).text(),parentOpen = $(this).attr('parentOpen');
		if(!(url === 'javascript:void(0)' || url === '#')){
			var sTab = {};
			if(parentOpen){ sTab = parent.tab; } else { sTab = tab; }
			if(sTab.isTabItemExist(tabid)){
				sTab.selectTabItem(tabid);
				sTab.reload(tabid);
			} else {
				sTab.addTabItem({tabid: tabid, text: text, url: url, showClose: showClose});
			}
		}
	});
};
/*打开某个地址 -- url应该是已经编码过*/
Public.openOnTab = function(tabid, title, url){
	var sTab = {};
	if(parent.tab){ sTab = parent.tab; } else { sTab = tab; }
	if(sTab.isTabItemExist(tabid)){
		sTab.selectTabItem(tabid);
		sTab.removeTabItem(tabid);
	}
	sTab.addTabItem({tabid: tabid, text: title, url: url, showClose: 'true'});
};
/*打开某个地址*/
Public.closeTab = function(tabid){
	var sTab = {};
	if(parent.tab){ sTab = parent.tab; } else { sTab = tab; }
	if( sTab.isTabItemExist(tabid)){
		sTab.selectTabItem(tabid);
		sTab.removeTabItem(tabid);
	};
};
/*选择某个tab的内容对象,通过名称*/
Public.getTabWindowByName = function(tabName){
	var sTab = {};
	if(parent.tab){ sTab = parent.tab; } else { sTab = tab; }
	var tabs = sTab.getTabidList();
	var _window = null;
	if( !! tabs ) {
		parent.$.each(tabs,function(index,item){
			if( !_window && parent.$("li[tabid=" + item + "]").children('a').text() == tabName) {
				_window = parent.$("iframe", ".l-tab-content-item[tabid=" + item + "]");
			}
		});
	}
	if(!!(_window.get(0))) {
		return _window.get(0).contentWindow;
	}
	return null;
}

/**
 * 表单的验证
 */
Public.validate = function(options){
	var defaults = {
		submitHandler: function(form){
			Public.loading('正在提交，请稍等...');
			form.submit();
		},
		errorContainer: "#messageBox",
		errorPlacement: function(error, element) {
			$("#messageBox").text("输入有误，请先更正。");
			if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
				error.appendTo(element.closest(".controls"));
			} else {
				error.insertAfter(element);
			};
		}
	};
	return $.extend({},defaults,options);
};

/**
 * combo
 * data 可以是数组，或对象（默认为远程取数据）
 */
Public.combo = function(t, data, options) {
	var defaults = {
		placeholder: "请选择..."
	};
	var _options = $.extend({},defaults,options);
	var _t = (typeof(t) == 'string')?(t.startWith('#')?t:'#'+t):t;
	$(_t).select2(_options);
	return _t;
};

/**
 * 级联
 * 自动完成 用query获取数据
 * param 指向其他参数
 * relaObj 是dom对象
 */
Public.autoCascadeCombo = function(t, url, param, relaObj, options) {
	var _relaObj = (typeof(relaObj) == 'string')?(relaObj.startWith('#')?relaObj:'#'+relaObj):relaObj;
	if($(_relaObj).get(0) == undefined) {
		Public.error('级联对象设置错误!');
		return false;
	}
	var defaults = {
		data: function (term, page) {
			return $.extend({},{
            	name: term,
                'param.pageSize': 10,
                'param.pageIndex': page,
                apikey: "ju6z9mjyajq2djue3gbvv26t"
            },param||{});
        },
        results: function (page) {
            var more = (page.param.pageIndex * page.param.pageSize) < page.param.recordCount;
            var values = [];
            $.each(page.data,function(index,item){
            	values.push({'id':item.id,'text':item.name});
            });
            return {results: values, more: more};
        }
	};
	var _ajax = $.extend({},defaults,{url:url});
	var _options = $.extend({}, options||{}, {query:function(settings){
		var _param = {};
		$(_relaObj).each(function(index,item){
			_param[item.name] = item.value;
		});
		_param = $.extend({},_param,_ajax.data(settings.term,settings.page));
		//获取数据
		Public.postAjax(_ajax.url, _param , function(page){
			settings.callback(_ajax.results(page));
		});
	}, initSelection: function(element, callback){
		var id=$(element).val();var name = $(element).data('name');
		callback({id:id,text:name});
	}});
	
	var _t = Public.combo(t, null, _options); 
	
	//给级联的对象注册select 事件
	$(_relaObj).on('change',function(e){
		$(_t).select2("val", "");
	});
};

/**
 * 自动完成 有分页
 * param 指向其他参数
 */
Public.autoCombo = function(t, url, param, options) {
	var defaults = {
		data: function (term, page) {
			return $.extend({},{
            	name: term,
                'param.pageSize': 10,
                'param.pageIndex': page,
                apikey: "ju6z9mjyajq2djue3gbvv26t"
            },param||{});
        },
        results: function (page) {
            var more = (page.param.pageIndex * page.param.pageSize) < page.param.recordCount;
            var values = [];
            $.each(page.data,function(index,item){
            	values.push({'id':item.id,'text':item.name});
            });
            return {results: values, more: more};
        }
	};
	var _ajax = $.extend({},defaults,{url:url});
	var _options = $.extend({}, options||{}, {ajax:_ajax, initSelection: function(element, callback){
		var id=$(element).val();var name = $(element).data('name');
		callback({id:id,text:name});
	}});
	Public.combo(t, null, _options);
};

/**
 * tags
 * t  -- input
 * tags -- 默认可选的项
 * options -- 属性扩展
 */
Public.tags = function(t, tags, options){
	var _tags = $.merge([], tags||[]); 
	var _options = $.extend({},{tags:_tags, placeholder: "请输入..."});
	Public.combo(t, null, _options);
};

/**
 * multCombo
 */
Public.multCombo = function(t, data, options) {
	var _t = (typeof(t) == 'string')?(t.startWith('#')?t:'#'+t):t;
	$(_t).each(function(index,item){
		$(item).attr('multiple','multiple');//添加属性
	});
	Public.combo(t, data, options);
};

/**
 * 将设置为 data-form=uniform 转换
 */
Public.uniform = function(){
	$('[data-form=uniform]').uniform();
};

/**
 * 延时执行 -- 返回延时对象,可以添加回调函数
 */
Public.delayPerform = function(task, delay){
	var dtd = $.Deferred();
	var tasks = function(){
		task();
		dtd.resolve();
	};
	setTimeout(tasks,delay||0);
	return dtd.promise();
};

/**
 * 不再使用
 * JCT 模版技术  template string 格式的模版
 */
Public.jCT = function(template){
	var Instance = new jCT(template);
	Instance.Build();
	return Instance.GetView();
};

/**
 * 使用artTemplate 模版技术(js原生)
 * artTemplate 模版技术  content  可以是对象,元素id,元素,字符串,对象格式{}
 *                    context  js对象  上下文
 *                    escape   是否格式化html，默认是true
 * -- 会格式化包含的html代码（如果字符串中包含html代码，将转换，页面不能解析）
 *               
 */
Public.runTemplate = function(content, context, escape){
	var _escape = (escape != undefined)?escape:true;//默认为true
	template.config('escape',_escape);
	var _get = template.get,_r = null;
	if( !(/^[a-zA-Z-\d]+$/g).test(content) ) {//替换get实现
		template.get = function(a){
			return template.compile(content.replace(/^\s*|\s*$/g, ''), { filename: '_TEMP', cache: false, openTag: '{{', closeTag: '}}'});
		}
	}
	_r = template(content, context);
	template.get = _get;//还原
	template.config('escape',true);
	return _r;
};

/**
 * 初始化普通表格的事件: 简单的添加一行, 删除一行(作为参考)
 * table tableId
 */
Public.initSimpleTableEvent = function(table){
	$(document).on('click',"table[id='"+table+"'] .add",function(){
		var row = this.parentNode.parentNode;
		var v = $('#' + table).simpleTable('add',row);
	});
    $(document).on('click',"table[id='"+table+"'] .delete",function(){
    	var row = this.parentNode.parentNode;
    	var v = $('#' + table).simpleTable('del',row);
	});
};

/**
 *  检查导入的文件的格式,及正确性
 *  uploadForm 表单,对表单进行验证
 *  suffix 后缀默认".xls,.xlsx" 不区分大小写
 */
Public.checkFile = function(uploadForm, suffix){
	var flag = true;
	var _suffix = suffix||'.xls,.xlsx'.toUpperCase();
	$(uploadForm).find('.required').each(function(index,item){//必填项验证
		if( flag && !$(item).val() && !$(item).is(":file")){
			$(item).addClass('text_error');
			flag = false;
		} else if( flag && !$(item).val()){
			alert('请选择文件');
			flag = false;
		}
	});
	$(uploadForm).find("[type='file']").each(function(index,item){//文件验证
		var filename = $(item).val();
		if( flag && (!filename || !filename.lastIndexOf('.') < 0) ) {
			alert('请选择有效的文件');
			flag = false;
		}
		var prifix = filename.substring(filename.lastIndexOf('.'),filename.length).toUpperCase();
		if(flag && _suffix.indexOf(prifix) < 0) {
			alert('请选择有效的文件');
			flag = false;
		}
	});
	return flag;
};

/**
 *  弹出导入框,并执行导入
 *  content 内容
 *  form 内容中的表单
 *  url  提交的url
 *  title 标题
 */
Public.openImportWindow = function(content, form, url, title, success, error){
	Public.openWindow(content, title, 500, 230, {
		buttons:{"导入":"mok", "关闭":true},
		submit:function(v, h, f){
			if( v == "mok" ){
				Public.doImport(form, url, success, error);
			} else {
				return true;
			}
			return false;
		},loaded:function(){
			$(".jbox-content", top.document).css("overflow-y","hidden");
	        $(".jbox-content", document).css("overflow-y","hidden");
	        if( Public.singleFile) {
	        	Public.singleFile("input[type='file'].selectFile");
	        }
		}
	});
};

/**
 * 执行导入操作 IE 不支持ajax上传文件，会用iframe来模拟，所以会比较慢，
 * form 导入的表单
 * url  导入的url
 */
Public.doImport = function(form, url, success, error){
	$(form).ajaxSubmit({
		url: url,
		dataType:"json",
		iframe: true,
		beforeSubmit:function(){
			var bflag = Public.checkFile($(form));
			if(bflag) {
				Public.loading('数据导入中...');
			}
			return bflag;
		},
		success: function(data){
			if(typeof(data) == "string") {
         	   data = $.parseJSON(data);
         	}
			Public.closeWindow();
			if( data.success ) {
				Public.success('导入数据成功');
				Public.doQuery();
				if(!!success) {
					success(data.obj);
				}
			} else if( !!error ){ //定义了自定义的错误处理方式
				Public.loaded();
				error(data.obj);
			} else { //错误,但没定义错误的处理方式,默认是弹出来
				Public.loaded();
				if( !data.obj && data.msg) { //没有错误的明细
					Public.error(data.msg);
				} else if(data.obj) {//有错误的明细
					var template='<div style="padding:20px 0px 5px 10px;">{{var errors = obj}}<table id="sample-table" class="table sample-table table-striped table-bordered table-hover">  <thead> <tr> <th class="tc">序号</th> <th>错误行</th> <th>错误列</th><th>错误描述</th> </tr></thead>  <tbody>{{for( var i = 0,j = errors.length;i < j;i++){var error = errors[i];}}<tr><td class="tc">{{= i+1}}</td><td class="tc">{{= error.row}}</td><td class="tc">{{= error.column}}</td><td>{{= error.msg}}</td></tr>{{ } }}</tbody> </table> </div>';
					var msg = Public.runTemplate(template, data);
					Public.openWindow(msg,"导入数据错误",800,500,{
						buttons:{"关闭":true},
						submit:function(v, h, f){
						}, loaded:function(h){
				        }, closed:function(){ 
				        }
				    });
				}
			}
		}
	});
};
/**
 *  导出 
 *  url 导出的地址
 *  param 参数对象:[{name:'id',value:'1'},{name:'id',value:'2'},{name:'name',value:'11'},{name:'name',value:'22'}] --- 请使用这样的格式
 */
Public.doExport = function( url, param ){
	var htmlStr = [];
	var formName = 'form'+ Math.random();
	htmlStr.push('<form method="post" name="'+formName);
	htmlStr.push('" action="'+url);
	htmlStr.push('" style="visibility: hidden"');
	htmlStr.push('>');
	$.each(param,function(i,item){
		htmlStr.push('<input type="hidden" name="export.'+item.name);
		htmlStr.push('" value="'+item.value);
		htmlStr.push('"/>');
	});
	htmlStr.push('<input type="hidden" name="token" value="'+$.cookie('token')+'"/>');
	htmlStr.push('<input type="hidden" name="holdToken" value="true"/>');
	htmlStr.push('</form>');
	$('body').append(htmlStr.join(''));
	$('.export-iframe').remove();
	var iframeName = 'iframe' + Math.random();
	$('body').append('<iframe name="'+iframeName+'" src="" width="0" height="0" style="display: none;" class="export-iframe"></iframe>');
	var randomForm = $(document.getElementsByName(formName).item(0));
	randomForm.attr('target', iframeName);
	randomForm.submit();
	randomForm.remove();
};
/**
 * 引入js和css文件
 */
Public.include = function(id, path, file){
	if (document.getElementById(id)==null){
        var files = typeof file == "string" ? [file] : file;
        for (var i = 0; i < files.length; i++){
            var name = files[i].replace(/^\s|\s$/g, "");
            var att = name.split('.');
            var ext = att[att.length - 1].toLowerCase();
            var isCSS = ext == "css";
            var tag = isCSS ? "link" : "script";
            var attr = isCSS ? " type='text/css' rel='stylesheet' " : " type='text/javascript' ";
            var link = (isCSS ? "href" : "src") + "='" + path + name + "'";
            document.write("<" + tag + (i==0?" id="+id:"") + attr + link + "></" + tag + ">");
        }
	}
};

/**
 * 获取选中的文本
 */
Public.getSelectText = function(){
	var an,ao;
	if($.browser.msie) {
		an = document.selection.createRange();
	} else {
		if ((ao = window.getSelection()) && ao.rangeCount) {
			an = ao.getRangeAt(0)
		}
	}
	return !an ? false : ($.browser.msie? an.text : an.toString());
};
/**
 * 单选用户
 */
Public.singleSelectUser = function(){
	
};

/**
 * 多选用户
 */
Public.multiSelectUser = function(){
	
};