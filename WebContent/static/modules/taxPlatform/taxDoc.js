var TaxDoc = TaxDoc ||{};
/**
 * 选中文本事件
 * target : #answer
 */
TaxDoc.initSelectText = function(target){
	
	var buttons = ['<div id="reader-helper-el" style="z-index: 9001;display: block; position: absolute; width: auto; height: 49px; line-height: 32px; text-align: center; border-radius: 1px; cursor: pointer; z-index: 1000; right: auto!important; font-size: 14px;display: none;" data-on="0">',
	               '<div class="tips-wrap"> <span class="trangle"></span><div class="inner" style="box-shadow: 0 0 3px #e6e6e6; margin: 5px; background: #fff; border: 1px solid #c4c4c4; border-radius: 2px;">',
	               '<a href="javascript:void(0)" id="reader-search-name" style="text-decoration: none; color: #6ea084; padding: 0 10px;">标题搜索</a><span> | </span>',
	               '<a href="javascript:void(0)" id="reader-search-num" style="text-decoration: none; color: #6ea084; padding: 0 10px;">文号搜索</a><span> | </span>',
	               '<a href="javascript:void(0)" id="reader-search-content" style="text-decoration: none; color: #6ea084; padding: 0 10px;">内容搜索</a>',
	               '</div></div></div>'
	              ].join("");
	
	var an = $(buttons).appendTo($('body'));
	//文本选择事件
	$(document).on("mouseup", target, function(e){
		if (typeof e.button === "undefined" || e.button === 2) {
			return;
		}
		var _event = event;
		setTimeout(function() {
			var text = Public.getSelectText();
			if(!!text) {
				an.show();
				var left = _event.pageX;
				var top = _event.pageY;
				if( left + $('#reader-helper-el').width() > $(window).width()) {
					left = left - $('#reader-helper-el').width() + 20;
				}
				an.get(0).style.top = top + 10 + "px";
				an.get(0).style.left = left - 20 +"px";
				an.data('on','1');
			}
		},30);
	});
	
    //点击其他地关闭
    $(document).bind('click.menu',function(e){
    	var target  = e.target || e.srcElement;
    	$('#reader-helper-el').each(function(){
			var an = $(this);
			if( $(target).closest(an).length == 0 && an.data('on') == '1'){
				an.get(0).style.top = "-99999px";
				an.get(0).style.left = "-99999px";
				an.data('on','0');
				an.hide();
			};
		});
    	
    	var text = encodeURI(Public.getSelectText());
    	if( !!text ) {
    		//按文件名查询
        	if($(target).attr('id') == 'reader-search-name') {
        		var url = webRoot + "/admin/platform/taxdoc/initList?docName=" + text;
        		Public.openOnTab("taxdoc-find","税收法规",url);
        	}
        	//按文号查询
        	if($(target).attr('id') == 'reader-search-num') {
        		var url = webRoot + "/admin/platform/taxdoc/initList?docNum=" + text;
        		Public.openOnTab("taxdoc-find","税收法规",url);
        	}
        	//按内容查询
        	if($(target).attr('id') == 'reader-search-content') {
        		var url = webRoot + "/admin/platform/taxdoc/initSearcher?query=" + text;
        		Public.openOnTab('doc-search', '税收法规查找', url);
        	}
    	}
    });
};