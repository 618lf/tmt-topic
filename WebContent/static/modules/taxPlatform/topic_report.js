var Topic = Topic||{};

//试题题型和难度统计数据 -- 按难度切换
Topic.chartByQTypeAndQLevel = function(dom, qLevel){
	Public.postAjax( webRoot + '/admin/platform/report/chartByQTypeAndQLevel',{qLevel:(qLevel||'I')},function(data){
	     var legend = {
			 data:['小计', '答疑汇编','考试汇编','自定义']	 
		 };
		 var xAxis = [{
            type : 'category',
            data:['单项选择题','多项选择题','不定项选择题','判断题','计算题']
		 }];
		 var series = [];
		 $.each(legend.data,function(index, item){
			 var oneData = series[index];
			 oneData = oneData || { name : item, type:'bar'};
			 //取数据
			 $.each(data.obj,function(i, d){
				 var _data = oneData.data || [];
				 if( item == '小计') {
					 _data.push(d.TOPIC_TOTAL);
				 }else if( item == '答疑汇编') {
					 _data.push(d.QA_TOTAL);
				 }else if( item == '考试汇编') {
					 _data.push(d.EXAM_TOTAL);
				 }else if( item == '自定义') {
					 _data.push(d.CUSTOM_TOTAL);
				 }
				 oneData.data = _data;
			 });
			 series[index] = oneData;
		 });
		 var option = {
			 title : {text: '试题统计（题型）'}, 
			 legend : legend,
			 xAxis : xAxis,
			 series : series
		 };
		 Chart.loadBarChart(dom, option, function(){});
    });
};

//按业务类型统计
Topic.chartByBusinessType = function(dom){
	Public.postAjax( webRoot + '/admin/platform/report/chartByBusinessType',{},function(data){
		var legend = {
			 data:['试题数量', '答疑汇编','考试汇编','自定义']	 
		 };
		 var xAxis = [{
            type : 'category',
            data:(function(){
            	var _data = [];
            	$.each(data.obj, function(index,item){
            		if(item.BUSINESS_TYPE_NAME == null){
            			item.BUSINESS_TYPE_NAME = "";
            		}
            		_data.push(item.BUSINESS_TYPE_NAME);
            	});
            	return _data;
            })()
		 }];
		 var series = [];
		 $.each(legend.data,function(index, item){
			 var oneData = series[index];
			 oneData = oneData || { name : item, type:'bar'};
			 //取数据
			 $.each(data.obj,function(i, d){
				 var _data = oneData.data || [];
				 if( item == '试题数量') {
					 _data.push(d.TOPIC_TOTAL);
				 }else if( item == '答疑汇编') {
					 _data.push(d.QA_TOTAL);
				 }else if( item == '考试汇编') {
					 _data.push(d.EXAM_TOTAL);
				 }else if( item == '自定义') {
					 _data.push(d.CUSTOM_TOTAL);
				 }
				 oneData.data = _data;
			 });
			 series[index] = oneData;
		 });
		 var option = {
			 title : {text: '试题统计（业务类型）'}, 
			 legend : legend,
			 xAxis : xAxis,
			 series : series
		 };
		 Chart.loadBarChart(dom, option, function(){});
	});
};

//按行业统计
Topic.chartByIndutry = function(dom){
	Public.postAjax( webRoot + '/admin/platform/report/chartByIndutry',{},function(data){
		var legend = {
			 data:['试题数量', '答疑汇编','考试汇编','自定义']	 
		 };
		 var xAxis = [{
            type : 'category',
            data:(function(){
            	var _data = [];
            	$.each(data.obj, function(index,item){
            		if(item.INDUTRY_NAME == null){
            			item.INDUTRY_NAME = "";
            		}
            		_data.push(item.INDUTRY_NAME);
            	});
            	return _data;
            })()
		 }];
		 var series = [];
		 $.each(legend.data,function(index, item){
			 var oneData = series[index];
			 oneData = oneData || { name : item, type:'bar'};
			 //取数据
			 $.each(data.obj,function(i, d){
				 var _data = oneData.data || [];
				 if( item == '试题数量') {
					 _data.push(d.TOPIC_TOTAL);
				 }else if( item == '答疑汇编') {
					 _data.push(d.QA_TOTAL);
				 }else if( item == '考试汇编') {
					 _data.push(d.EXAM_TOTAL);
				 }else if( item == '自定义') {
					 _data.push(d.CUSTOM_TOTAL);
				 }
				 oneData.data = _data;
			 });
			 series[index] = oneData;
		 });
		 var option = {
			 title : {text: '试题统计（行业）'}, 
			 legend : legend,
			 xAxis : xAxis,
			 series : series
		 };
		 Chart.loadBarChart(dom, option, function(){});
	});
};

//按税种统计
Topic.chartByCategory = function(dom){
	Public.postAjax( webRoot + '/admin/platform/report/chartByCategory',{},function(data){
		var legend = {
			 data:['试题数量', '答疑汇编','考试汇编','自定义']	 
		 };
		 var xAxis = [{
            type : 'category',
            data:(function(){
            	var _data = [];
            	$.each(data.obj, function(index,item){
            		if(item.CATEGORY_NAME == null){
            			item.CATEGORY_NAME = "";
            		}
            		_data.push(item.CATEGORY_NAME);
            	});
            	return _data;
            })()
		 }];
		 var series = [];
		 $.each(legend.data,function(index, item){
			 var oneData = series[index];
			 oneData = oneData || { name : item, type:'bar'};
			 //取数据
			 $.each(data.obj,function(i, d){
				 var _data = oneData.data || [];
				 if( item == '试题数量') {
					 _data.push(d.TOPIC_TOTAL);
				 }else if( item == '答疑汇编') {
					 _data.push(d.QA_TOTAL);
				 }else if( item == '考试汇编') {
					 _data.push(d.EXAM_TOTAL);
				 }else if( item == '自定义') {
					 _data.push(d.CUSTOM_TOTAL);
				 }
				 oneData.data = _data;
			 });
			 series[index] = oneData;
		 });
		 var option = {
			 title : {text: '试题统计（税种）'}, 
			 legend : legend,
			 xAxis : xAxis,
			 series : series
		 };
		 Chart.loadBarChart(dom, option, function(){});
	});
};
