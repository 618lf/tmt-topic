var Topic = Topic||{};
//添加表格一行的事件 
Topic.addRowCallback = function(){};
Topic.initQaEvent = function(url){
	//普通table的添加和删除详情
	$("table[id='sample-table']").on('click', '.edit', function(e){
		var row = this.parentNode.parentNode;
		var id = $(this).data('id');
		var qid = $(this).data('qid');
		Public.openUrlWindow(url + '?id='+id,'关联税种及法规<small>(注：带<font color="red">*</font>必填)</small>',800,480,null,function(obj, child, maps){
			if( child ) {
				var param = {id:id,qaId:qid};
				child.THISPAGE.submit(param, function(data,obj){
					var qaRela = data.obj;
					$('#sample-table').simpleTable('update', row, qaRela);
					$(row).find('i').data('id',qaRela.id);
					$(row).find('i').data('qid',qaRela.qId);
					//关闭窗体
					Public.closeWindow();
				});
			}
			return false;//只能是false
		});
	});
	//普通table的添加和删除
	$("table[id='sample-table']").on('click', '.add', function(e){
		var row = this.parentNode.parentNode;
		var v = $('#sample-table').simpleTable('add',row);
		//清空v的内容
		Topic.addRowCallback(v)
	});
    $("table[id='sample-table']").on('click', '.delete', function(e){
    	var id = $(this).data('id'); var del = $(this).data('del');
    	var row = this.parentNode.parentNode;
    	if(id != '' && del == true) {
    		Public.deletex('确定删除税种关联？',webRoot + '/admin/platform/qaRela/delete',{idList:id},function(data){
    			var v = $('#sample-table').simpleTable('del',row);
    			if(!v){//最后一行
    				Topic.addRowCallback(row)
    			}
    		});
    	} else {
    		var v = $('#sample-table').simpleTable('del',row);
    		if(!v){//最后一行
				Topic.addRowCallback(row)
			}
    	}
	});
};
//列表格式化函数
Topic.statusFmt = function (cellvalue, options, rowObject) {
	if(cellvalue == '1'){return '公开'}
	return '未公开';
};
Topic.typesFmt = function (cellvalue, options, rowObject) {
	if(cellvalue == 'FINALLY'){return '发布'}
	else if(cellvalue == 'FINALLY_VERITY'){return '终审'}
	else if(cellvalue == 'INIT_VERITY'){return '初审'}
	else{return '命题';}
};
Topic.optionsFmt = function (cellvalue, options, rowObject) {
	return Public.billsOper(cellvalue, options, rowObject);
};
//得到下一步操作的类型(页面重写)
Topic.getNextOptionType = function(e){
	alert("请重写,返回正确的操作类型");
	return false;
};
//是否公开试题
Topic.toPublic = function(status, title){
	var checkeds = $('#grid').getGridParam('selarrrow');
	if( !!checkeds && checkeds.length && typeof(checkeds) === 'object' ) {
		var param = [];
		if(typeof(checkeds) === 'object'){
			$.each(checkeds,function(index,item){
				var id = $('#grid').getRowData(item).id;
				param.push({id:id,status:status});
			});
		}
		Public.deletex("确"+title+"选中的试题？", webRoot + "/admin/platform/topic/toPublic",{postData:JSON.stringify(param)},function(data){
			if(!!data.success) {
				Public.success(title+'试题成功');
				Public.doQuery();
			} else {
				Public.error(data.msg);
			}
		});
	} else {
		Public.error("请选择要"+title+"的试题!");
	}
};
//初始化列表页面的一些公用事件
Topic.initListEvent = function(){
	var delTopic = function(checkeds){
		if( !!checkeds && checkeds.length ) {
			var param = [];
			if(typeof(checkeds) === 'object'){
				$.each(checkeds,function(index,item){
					var id = $('#grid').getRowData(item).id;
					param.push({name:'idList',value:id});
				});
			} else {
				param.push({name:'idList',value:checkeds});
			}
			param.push({name:'optionType',value:($('#optionType').val())});
			Public.deletex("确定删除选中的试题？", webRoot + "/admin/platform/topic/delete",param,function(data){
				if(!!data.success) {
					Public.success('删除试题成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		} else {
			Public.error("请选择要删除的试题!");
		}
	};
	$('#dataGrid').on('click','.detail',function(e){
		var param = 'id='+$(this).data('id');
		    param += '&optionType=FINALLY';
		Public.openOnTab('topic-edit', '试题编制',webRoot + "/admin/platform/topic/form?"+param);
	});
	$('#dataGrid').on('click','.edit',function(e){
		var param = 'id='+$(this).data('id');
		    param += '&optionType='+ ($('#optionType').val());
    	Public.openOnTab('topic-edit', '试题编制',webRoot + "/admin/platform/topic/form?"+param);
	});
	$('#dataGrid').on('click','.delete',function(e){
		delTopic($(this).attr('data-id'));
	});
	$(document).on('click','#delBtn',function(e){
		var checkeds = $('#grid').getGridParam('selarrrow');
		delTopic(checkeds);
	});
	//完成
	$(document).on('click','#finishBtn',function(e){
		var optionType = Topic.getNextOptionType(e);
		if(optionType == false) return;
		var checkeds = $('#grid').getGridParam('selarrrow');
		if( !!checkeds && checkeds.length && typeof(checkeds) === 'object' ) {
			var param = [];
			if(typeof(checkeds) === 'object'){
				$.each(checkeds,function(index,item){
					var id = $('#grid').getRowData(item).id;
					var version = $('#grid').getRowData(item).version;
					param.push({id:id,version:version,optionType:optionType});
				});
			}
			Public.deletex("确完成选中的试题？", webRoot + "/admin/platform/topic/finish",{postData:JSON.stringify(param)},function(data){
				if(!!data.success) {
					Public.success('完成试题成功');
					Public.doQuery();
				} else {
					Public.error(data.msg);
				}
			});
		} else {
			Public.error("请选择要完成的试题!");
		}
	});
	//公开 - 不公开
	$(document).on('click','#pBtn',function(){
		Topic.toPublic(1,'公开');
	});
    $(document).on('click','#npBtn',function(){
    	Topic.toPublic(0,'不公开');
	});
}