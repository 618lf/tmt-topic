var THISPAGE = {
	TEMPLAET:{
		CHOICE:	     "<label class='{{=type}}'><span class='option-label'>选项&nbsp;{{=index}}&nbsp;</span>"
		            +"<input type='{{=type}}' id='{{=index}}_radio' name='skey' value='{{=index}}' data-form='uniform'/>&nbsp;"
		            +"<input type='checkbox' id='{{=index}}_option' name='soption' value='{{=index}}' checked='checked' style='display:none;' readonly='readonly'/>&nbsp;"
		            +"<input type='text' id='{{=index}}_salisa' name='salisa' class='text_line' value='{{=value}}'/>&nbsp;"
		            +"<i class='icon-remove red'></i></label>",
		COUNTING:    '<input id="skey" name="skey" class="required number" type="text" value="" maxlength="20"/>',
		TFNG:        '<label class="radio inline"> <input id="skey1" name="skey" type="radio" value="1" data-form="uniform" class="required"/>&nbsp;是 </label>'
		            +'<label class="radio inline"> <input id="skey2" name="skey" type="radio" value="0" data-form="uniform" class="required"/>&nbsp;否 </label>'
	},
	CHOICE :['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
	_init : function(){
		this.bindValidate();
		this.addEvent();
		this.initFormState();
	},
	bindValidate : function(){
		$("#inputForm").validate(
			Public.validate({submitHandler: function(form){
				//验证skey 和  salisa 框架不好验证
				var bFalg = true;
				//是否编题,点了否的，不验证,不会保存编题信息
				var makeQ = $("input[name='makeQFlag']");
				var makeQs = $("input[name='makeQFlag']:checked");
				if( !!makeQ.get(0) && !makeQs.get(0)) {
					Public.error('请选择是否编题');
					return;
				}
				if( (!!makeQs.get(0) && makeQs.val() == 1) || !makeQs.get(0) ) {
					var qType = $('#qType').val();
					//答案
					if( qType == 'ONE_CHOICE' || qType == 'MULT_CHOICE' || qType == 'ONE_MORE_CHOICE') {
						if( !$("input[name='skey']:checked").eq(0).get(0) ) {//没有选中值
							bFalg = false;
						}
						if(bFalg) {
							$("input[name='salisa']").each(function(index,item){
								if( bFalg && !$(item).val() ){
									bFalg = false;
								}
							});
						}
	            	}
					//税种
					if( $('#sample-table .categoryId:first').val() == -1 || $('#sample-table .categoryId:first').val() == "") {
						Public.error('请关联税种');
						return;
					}
					//验证法规
					if( $("input[name='storageFlag']:checked").val()== 1 ) {//是
						var bfalg = true;
						$('.larDocNum').each(function(index,item){
							if( bfalg && !$(item).text() ) {
								bfalg = false;
							}
						});
						$('.larDocName').each(function(index,item){
							if( bfalg && !$(item).text() ) {
								bfalg = false;
							}
						});
						$('.larDocContent').each(function(index,item){
							if( bfalg && !$(item).text() ) {
								bfalg = false;
							}
						});
						if(!bfalg) {
							Public.error('请填写完整法规[文号,名称,内容],都必须填写完整');
							return;
						}
					}
				}
				if(bFalg) {
					Public.loading('正在提交，请稍等...');
					form.submit();
				} else {
					Public.error('请选择正确的答案,和填写选择项内容');
				}
			}})
		);
	},
	addEvent : function(){
		$(document).on('click', '#cancelBtn', function(e){
			Public.closeTab('topic-edit');
		});
		$(document).on('click', '#delBtn', function(e){
			var param = [{name:'idList',value:($('#id').val())}];
			    param.push({name:'optionType',value:($('#type').val())});
			Public.deletex("删除之后如果来源是答疑,则将此答疑设置为不需要命题,确定删除试题？",webRoot + "/admin/platform/topic/delete",param,function(data){
				if(!!data.success) {
					Public.success('删除试题成功');
					Public.closeTab('topic-edit');
				} else {
					Public.error(data.msg);
				}
			});
		});
        $('#qType').on('change', function(e){
        	var qType = $(this).val();
        	var context = {}, count = 1, html='', useTemplate= null;
        	if( qType == 'ONE_CHOICE' || qType == 'MULT_CHOICE' || qType == 'ONE_MORE_CHOICE') {
        		useTemplate = THISPAGE.TEMPLAET.CHOICE
        		if(qType == 'ONE_CHOICE') {
        			context.type = 'radio';
        		} else {
        			context.type = 'checkbox';
        		}
        	} else{
        		useTemplate = THISPAGE.TEMPLAET[qType]
        	}
        	if( qType == 'ONE_CHOICE' || qType == 'MULT_CHOICE' || qType == 'ONE_MORE_CHOICE' ) {
        		//获取之前设置的类型
        		if( !!$('#options label.checkbox').eq(0).get(0) || 
        				(!!$('#options label.radio').eq(0).get(0) && !$('#options label.radio').eq(0).hasClass('inline'))) {
        			var labels = [];
        			//转换
        			$('#options label.checkbox').each(function(index,item){
        				labels.push(item);
        			});
        			//转换
        			$('#options label.radio').each(function(index,item){
        				labels.push(item);
        			});
        			//生成最新的数据
        			$.each(labels, function(index,item){
        				context.index = $(item).find("[name='soption']").val();
        				context.value = $(item).find("[name='salisa']").val();
        				html += Public.runTemplate(useTemplate, context);
        			});
        		} else {
        			for(var i = 0; i < 4; i++) { //默认4个选项
        				context.index = THISPAGE.CHOICE[count++ - 1];
        				context.value = "";
            			html += Public.runTemplate(useTemplate, context);
            		}
        		}
        	} else {
        		html = Public.runTemplate(useTemplate, context);
        	}
        	$('#options').html(html);
        });
        //添加事件
        $(document).on('click','#add_option',function(){
        	var qType = $('#qType').val();
        	if( !(qType == 'ONE_CHOICE' || qType == 'MULT_CHOICE' || qType == 'ONE_MORE_CHOICE') ) {
        		return false;
        	}
        	var context = {}, count= 1;
        	if( !!$('#options label.checkbox').eq(0).get(0) || 
    				(!!$('#options label.radio').eq(0).get(0) && !$('#options label.radio').eq(0).hasClass('inline'))) {
        		count = $('#options label.radio').length;
        	    if(count == 0) {
        		   count = $('#options label.checkbox').length;
        		}
        	}
        	var useTemplate = THISPAGE.TEMPLAET.CHOICE;
        	if(qType == 'ONE_CHOICE') {
    			context.type = 'radio';
    		} else {
    			context.type = 'checkbox';
    		}
        	context.index = THISPAGE.CHOICE[(++count-1)];
        	context.value = "";
        	var html = Public.runTemplate(useTemplate, context);
        	$('#options').append(html);
        });
        //删除事件
        $(document).on('click','#options .icon-remove',function(e){
        	var label = $(this).parent();
        	$(label).remove();
        });
        //公用事件
        Public.initPublicEvent();
        
        Public.combo('#qType,#qLevel,#qFrom,#status');
        //初始值
        $('#qFrom').select2('readonly',true);
        if(!$('#id').val() || $('#id').val() =='-1') {
        	$('#qType').select2('val','ONE_CHOICE',true);
        }
        //切换事件
        $(document).on('click','[data-toggle]',function(e){
        	e.preventDefault();
    		var n = $(this);
    		var p = n.data("toggle");
    		if(p == 'tab') {
    			var optionType = n.data("type");
    			var id = $('#id').val();
    			var token = $('#token').val();
    			Public.refreshWindow( webRoot + '/admin/platform/topic/form',{id:id,optionType:optionType,token:token});
    		}
        });
        
        //税种关联事件
        Topic.initQaEvent(webRoot + '/admin/platform/topicRela/form');
        
        //多屏显示效果
        $(document).on('click','#multSetting',function(e){
        	var qFrom = $('#qFrom').val(), qFromDiv = $('#qFromDiv'), firstBills = $('#firstBills'),secBills = $('#secBills');
        	if(!((qFrom == 'QA' || qFrom == 'EXAM') && !!qFromDiv.get(0))) { return;}
        	if( $(this).attr('checked') == 'checked' ) {
        		firstBills.css('width','50%');
        		secBills.css('width','39%').show();
        		secBills.find('.bills-content').html(qFromDiv.find('.widget-main').html());
        		qFromDiv.hide();
        		secBills.css({height:($(window).outerHeight() - ($.browser.msie?45:46))});
        		$('#inputForm').addClass('form-min');
        	} else {
        		firstBills.css('width','93%');
        		secBills.hide().find('.bills-content').html('');
        		qFromDiv.show();
        		$('#inputForm').removeClass('form-min');
        	}
        });
	},
	initFormState : function(){
		//初始化表单状态
		if($('#editFlag').val() == 'false') {
			$("input[type='text']").attr("readonly","readonly");
			$("input[type='radio']").attr("readonly","readonly");
			$("input[type='checkbox']").attr("readonly","readonly");
			$("textarea").attr("readonly","readonly");
			$('#qType').select2('readonly',true);
			$('#qLevel').select2('readonly',true);
			$('#qFrom').select2('readonly',true);
			$('#status').select2('readonly',true);
			$('#add_option').hide();
			$('#options i').hide();
			$('#submitBtn').addClass('disabled');
			$('#submitBtn').attr('disabled','disabled');
			$('#finishBtn').attr('disabled','disabled');
			$('#delBtn').attr('disabled','disabled');
		}
		//初始化汇编信息的折起状态
		if($('#qFrom').val() == 'CUSTOM'){
			$('[data-action]').click();
		}
	}
};
//添加一行之后的回调事件
Topic.addRowCallback = function(row){
	$(row).children('td').each(function(index, item){
		//操作列
		$(item).find('i').data('id','-1');
		$(item).find('input').val('');
		$(item).find('label').text('');
	});
}