package com.tmt;

import com.tmt.base.system.utils.DictUtils;

/**
 * 系统常量,考虑是否做成配置项
 * @author lifeng
 */
public class Constants {

	//微信注册会员的组织结构CODE
	public static String WX_R_OFFICE_CODE = "WX_R_OFFICE_CODE";
	//网站注册会员的组织结构CODE
	public static String SITE_R_OFFICE_CODE = "SITE_R_OFFICE_CODE";
	
	//微信注册会员的用户组code,可以有多个
	public static String WX_R_GROUP_CODES = "WX_R_GROUP_CODES";
	public static String SITE_R_GROUP_CODES = "SITE_R_GROUP_CODES";
	
	//微信客服接口有效时长
	public static int WX_AGENT_API_VALID_TIME = 48;
	
	/**
	 * 从数据字段读取数据
	 * @return
	 */
	public static String getValueFromDict(String code) {
		return DictUtils.getDictValue(code);
	}
}
