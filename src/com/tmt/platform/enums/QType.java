package com.tmt.platform.enums;

/**
 * 编题类型
 * @author lifeng
 */
public enum QType {

	ONE_CHOICE("单项选择题"),
	MULT_CHOICE("多项选择题"),
	ONE_MORE_CHOICE("不定项选择题"),
	TFNG("判断题"),
	COUNTING("计算题");
	
	private String name;
	private QType(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 是否是选择题
	 * @param qType
	 * @return
	 */
	public static Boolean isChoice(QType qType) {
		if(QType.ONE_CHOICE == qType || QType.MULT_CHOICE == qType || QType.ONE_MORE_CHOICE == qType) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
