package com.tmt.platform.enums;

/**
 * 命题进度
 * @author lifeng
 */
public enum MakeStatus {

	NONE,//默认
	DOING,//进行中(创建命题,但没发送审核)
	DONE,//完成(点了发送审核)
	NO;//不命题
	
	/**
	 * 命题  -- 命题
	 * @param status
	 * @return
	 */
	public static Boolean makeYes(MakeStatus status){
		if( status == DOING || status == DONE) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
