package com.tmt.platform.enums;

/**
 * 是否有效
 * @author lifeng
 */
public enum Enabled {

	FUILL("全文有效"),PART("条款失效"),NONE("全文失效");
	private String name;
	
	private Enabled(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static Enabled valueof(String name){
		if("全文有效".equals(name)) {
			return FUILL;
		} else if("部分有效".equals(name)){
			return PART;
		} else if("全文失效".equals(name)){
			return NONE;
		}
		return null;
	}
}
