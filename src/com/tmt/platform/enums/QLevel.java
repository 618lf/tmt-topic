package com.tmt.platform.enums;

/**
 * 试题难度
 * @author lifeng
 */
public enum QLevel {

	I("容易"),II("一般"),III("很难");
	private String name;
	private QLevel(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
