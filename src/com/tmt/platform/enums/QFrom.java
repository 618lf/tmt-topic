package com.tmt.platform.enums;

import com.tmt.platform.utils.TaxConstants;

/**
 * 试题来源
 * @author lifeng
 */
public enum QFrom {

	QA("答疑汇编"),EXAM("考试汇编"),CUSTOM("自定义");
	
	private String name;
	private QFrom(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//返回与来源对应的code生成规则
	public String getQaCodeSeq(){
		if(this == QA) {
			return TaxConstants.QA_CODE_SEQ;
		}
		if(this == CUSTOM) {
			return TaxConstants.CUSTOM_CODE_SEQ;
		}
		return TaxConstants.EXAM_CODE_SEQ;
	}
	
	//返回与来源对应的前缀
	public String getQaCodePre(){
		if(this == QA) {
			return "QA";
		}
		if(this == CUSTOM) {
			return "C";
		}
		return "E";
	}
}
