package com.tmt.platform.enums;

import com.tmt.base.common.exception.BaseRuntimeException;

/**
 * 关联类型
 * @author lifeng
 *
 */
public enum QaRelaType {
	
	DEL("删除"), INIT("命题"), INIT_VERITY("初审"), FINALLY_VERITY("终审"), FINALLY("完成");
	
	private String name;
	private QaRelaType(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean isNextOption(QaRelaType type){
		if(this == QaRelaType.INIT && type == QaRelaType.INIT_VERITY) {
			return Boolean.TRUE;
		}else if(this == QaRelaType.INIT_VERITY && type == QaRelaType.FINALLY_VERITY) {
			return Boolean.TRUE;
		}else if(this == QaRelaType.FINALLY_VERITY && type == QaRelaType.FINALLY) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 得到当前类型的上一状态
	 * @return
	 */
	public QaRelaType getPreOption() {
		if(this == QaRelaType.INIT_VERITY) {
			return QaRelaType.INIT;
		} else if(this == QaRelaType.FINALLY_VERITY) {
			return QaRelaType.INIT_VERITY;
		}
		return null;
	}
	
	/**
	 * 转化
	 * @param value
	 * @return
	 */
	public static QaRelaType valueOf(Object value){
		try{
			if( value != null ){
				return QaRelaType.valueOf(String.valueOf(value));
			}
		}catch(Exception e){}
		throw new BaseRuntimeException("格式化Type出错");
	}
}
