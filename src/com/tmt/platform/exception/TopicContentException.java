package com.tmt.platform.exception;

import com.tmt.base.common.exception.BaseRuntimeException;

/**
 * 题目内容错误
 * @author lifeng
 */
public class TopicContentException extends BaseRuntimeException{

	private static final long serialVersionUID = -4585613399145690943L;

	public TopicContentException(String msg) {
		super(msg);
	}
	
	public TopicContentException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
