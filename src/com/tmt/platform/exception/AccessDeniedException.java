package com.tmt.platform.exception;

import com.tmt.base.common.exception.BaseRuntimeException;

/**
 * 无权限
 * @author lifeng
 */
public class AccessDeniedException extends BaseRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5247832537222366649L;
	
	public AccessDeniedException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public AccessDeniedException(String msg) {
		super(msg);
	}
}
