package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.CacheUtils;
import com.tmt.platform.qa.dao.CategoryDao;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.utils.TaxConstants;

@Service
public class CategoryService extends BaseService<Category, String>{
	
	@Autowired
	private CategoryDao CategoryDao;
	
	@Override
	protected BaseIbatisDAO<Category, String> getEntityDao() {
		return CategoryDao;
	}
	
	@Transactional
	public void save(Category category) {
		if( IdGen.isInvalidId(category.getId()) ) {
			this.insert(category);
		} else {
			this.update(category);
		}
		CacheUtils.evict(TaxConstants.PLATFORM_CATEGORY_CACHE_KEY);
	}
	
	/**
	 * 判断是否存在于 olds 中 
	 * @param category
	 * @param olds
	 * @return
	 */
	@Transactional
	public Category save(Category category, List<Category> olds) {
		if( category != null) {
			Boolean bFalg = Boolean.FALSE;
			if( olds != null && olds.size() != 0) {
				for(Category old: olds) {
					if( old != null && old.compareTo(category) ) {//只要是判断code,如果一样,则只更新
						bFalg = Boolean.TRUE; category.setId(old.getId()); break;
					}
				}
			}
			this.save(category);
			if(!bFalg) {//新增的
				olds.add(category);
			}
		}
		return category;
	}
	
	/**
	 * 校验是否可以删除
	 * @param businessType
	 * @return
	 */
	public int checkedDelete(Category category) {
		return this.countByCondition("checkedDelete", category);
	}
	
	/**
	 * 缓存
	 * @return
	 */
	public List<Category> queryCategorys(){
		List<Category> categorys = CacheUtils.get(TaxConstants.PLATFORM_CATEGORY_CACHE_KEY);
		if( categorys == null) {
			categorys = this.findNoOtherCategorys();
			CacheUtils.put(TaxConstants.PLATFORM_CATEGORY_CACHE_KEY, categorys);
		}
		return categorys;
	}
	
	/**
	 * 过滤掉其他的税种（其他税种是临时用的）
	 * @return
	 */
	public List<Category> findNoOtherCategorys() {
		List<Category> categorys = this.findAll();
		List<Category> _categorys = Lists.newArrayList();
		for(Category category: categorys) {
			if( !TaxConstants.CATEGORYS_OTHER_CODE.equals(category.getCode())) {
				_categorys.add(category);
			}
		}
		return _categorys;
	}
	
	/**
	 * 查找其他的税种
	 * @return
	 */
	public Category findOtherCategory() {
		Category category = this.queryForObject("findByCode", TaxConstants.CATEGORYS_OTHER_CODE);
		if( category == null) {
			category = new Category();
			category.setId(IdGen.INVALID_ID);
			category.setCode(TaxConstants.CATEGORYS_OTHER_CODE);
			category.setName("其他");
			this.save(category);
		}
		return category;
	}
	
}
