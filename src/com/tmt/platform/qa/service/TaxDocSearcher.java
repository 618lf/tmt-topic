package com.tmt.platform.qa.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.stereotype.Service;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.TaxDoc;

/**
 * 法规全文索引查找 -- 先简单点，只是单节点的lucene
 * @author lifeng
 */
@Service
public class TaxDocSearcher {

	//全文索引位置
	private static String LUCENE_INDEX_PATH = "D:/lucene/taxdoc";
	private Analyzer analyzer ;
	private Directory directory;
	
	public TaxDocSearcher(){
		try{
			this.directory = new SimpleFSDirectory(new File(LUCENE_INDEX_PATH));
			this.analyzer =  new IKAnalyzer();
		}catch(Exception e){}
	}
	
	//创建索引
	public Boolean createIndex(List<TaxDoc> docs) throws CorruptIndexException, IOException, ParseException{
		IndexWriter writer = null;
		try{
			boolean exist = IndexReader.indexExists(this.directory);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, this.analyzer);
			config.setOpenMode(OpenMode.CREATE_OR_APPEND);
			writer = new IndexWriter(this.directory, config);
			
			if(exist) {//先删除
				//创建法规文件索引
				for( TaxDoc doc: docs ) {
					writer.deleteDocuments(this.createQuery(null, doc.getId()));
				}
			}
			//创建法规文件索引
			for( TaxDoc doc: docs ) {
				writer.addDocument(this.createDocument(doc));
			}
		}finally{
			if( writer != null) {
				writer.close();
			}
		}
		return Boolean.TRUE;
	}
	//创建文档
	private Document createDocument(TaxDoc doc) {
		Document document = new Document();
		document.add(new Field("id", doc.getId(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		document.add(new Field("docName", doc.getDocName() == null ? "": doc.getDocName(), Field.Store.YES, Field.Index.ANALYZED));
		document.add(new Field("docNum", doc.getDocNum() == null ? "": doc.getDocNum(), Field.Store.YES, Field.Index.ANALYZED));
		document.add(new Field("categorys", doc.getCategorys() ==null?"":doc.getCategorys(), Field.Store.YES, Field.Index.ANALYZED));
		document.add(new Field("docContent", doc.getDocContent() == null ? "": doc.getDocContent(), Field.Store.YES, Field.Index.ANALYZED));
		return document;
	}
	
	/**
	 * 分页查找
	 * @param query
	 * @return
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 * @throws ParseException 
	 * @throws InvalidTokenOffsetsException 
	 */
	public Page searchPage(String query, Page page) throws CorruptIndexException, IOException, ParseException, InvalidTokenOffsetsException{
		int pageNo = page.getParam().getPageIndex();
		int pageSize = page.getParam().getPageSize();
		IndexReader reader = IndexReader.open(this.directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		try{
			Query _query = this.createQuery(query, null);
			QueryScorer queryScorer = new QueryScorer(_query);
			Formatter formatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");   
			Highlighter hl = new Highlighter(formatter, queryScorer);  
			//查找数据
			TopDocs docs = searcher.search(_query, pageNo * pageSize);
			ScoreDoc[] hits = docs.scoreDocs;
	        //支持分页
	        int endIndex = pageNo * pageSize;
			int len = hits.length;
			if (endIndex > len) {
				endIndex = len;
			}
	        List<TaxDoc> _docs = Lists.newArrayList();
	        for (int i = (pageNo - 1) * pageSize; docs != null && i < endIndex; i++) {  
	        	 Document doc = searcher.doc(hits[i].doc);  
	        	 String id = doc.get("id");
	        	 String docName = doc.get("docName");
	        	 String docNum = doc.get("docNum");
	        	 String categorys = doc.get("categorys");
	        	 String docContent = doc.get("docContent");//StringUtil3.abbr(doc.get("docContent"), 500);
	        	 
	        	 Fragmenter fragmenter = new SimpleSpanFragmenter(queryScorer, docContent.length());
	        	 hl.setTextFragmenter(fragmenter);
	        	 //注：如果没找到高亮的字符串，则返回null
	        	 String _docContent = hl.getBestFragment(analyzer, "docContent", docContent);
	        	 
	        	 TaxDoc _doc = new TaxDoc();
	        	 _doc.setId(id);
	        	 _doc.setDocName(docName);
	        	 _doc.setDocNum(docNum);
	        	 _doc.setCategorys(categorys);
	        	 _doc.setDocContent(_docContent == null?docContent:_docContent);
	        	 _docs.add(_doc);
	        }
	        page.setData(_docs);
	        page.getParam().setRecordCount(docs.totalHits);//总数
	        return page;
		}finally{
			reader.close();  
			searcher.close();
		}
	}
	
	//创建查询条件 query 可以用空格分开, 实现：精确过滤查询
	private Query createQuery(String query, String id) throws ParseException {
		BooleanQuery bq = new BooleanQuery();
		Query q;
		if( StringUtil3.isNotBlank(query)) {
			MultiFieldQueryParser _p = new MultiFieldQueryParser(Version.LUCENE_36,new String[]{DOC_CONTENT}, analyzer);
			//空格分开的词的交集
			_p.setDefaultOperator(QueryParser.Operator.AND);
			q = _p.parse(query);
//			q = MultiFieldQueryParser.parse(Version.LUCENE_36, query,
//					new String[]{DOC_CONTENT}, new BooleanClause.Occur[]{BooleanClause.Occur.MUST}, analyzer);
			bq.add(q, BooleanClause.Occur.MUST);
		}
		if( StringUtil3.isNotBlank(id)) {
			q = new TermQuery(new Term(ID, id));
			bq.add(q, BooleanClause.Occur.MUST);
		}
		// 下面这个 应该都只是一个关键字的搜索
//		//1. 只能搜索一个关键字，如果传入的不是一个关键字（多个关键字,空格分开也不行），则查询不到数据
//		Query q = new TermQuery(new Term(DOC_CONTENT, query));
//		//2. 组合搜索多个关键字(怎么识别多个关键词,用空格分开)
//		BooleanQuery bq = new BooleanQuery();
//		bq.add(new TermQuery(new Term(DOC_CONTENT, "增值税")), BooleanClause.Occur.MUST);
//		bq.add(new TermQuery(new Term(DOC_CONTENT, "公告")), BooleanClause.Occur.MUST_NOT);
//		//3. 前缀搜索 需要MemoryIndex-- 一个关键字
//		PrefixQuery prefixquery = new PrefixQuery(new Term(DOC_CONTENT, "增"));
//		//4. 短语收索
//		Term term1 = new Term(DOC_CONTENT, "根据");
//		Term term2 = new Term(DOC_CONTENT, "市局");
//
//		PhraseQuery phrasequery = new PhraseQuery();
//		phrasequery.setSlop(10);
//		phrasequery.add(term1);
//		phrasequery.add(term2);
//		//5. 多短语搜索（没有成功） 
//		Term term = new Term(DOC_CONTENT, "增"); // 前置关键字
//		Term term3 = new Term(DOC_CONTENT, "值"); // 搜索关键字
//		Term term4 = new Term(DOC_CONTENT, "值"); // 搜索关键字
//
//		MultiPhraseQuery multiPhraseQuery = new MultiPhraseQuery();
//		multiPhraseQuery.add(term);
//		multiPhraseQuery.add(new Term[] { term3, term4 });
//	    //6. 模糊搜索(顾名思义) -- 一个关键字
//		Term term6 = new Term(DOC_CONTENT, "指项");
//        FuzzyQuery fuzzyquery=new FuzzyQuery(term6, 0.8f); 
//        //7. 通配符搜索（顾名思义） -- 一个关键字
//        Term term7 =new Term(DOC_CONTENT, "增?纳税");
//        WildcardQuery wildcardQuery=new WildcardQuery(term7);
//        //8. 正则表达式搜索（顾名思义，这个类引入lucene-queries-3.5.0.jar包）
//        Term term8= new Term(DOC_CONTENT, ".*税");
//        RegexQuery regexQuery = new RegexQuery(term8);
		return bq;
	}
	
	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	public Directory getDirectory() {
		return directory;
	}

	public void setDirectory(Directory directory) {
		this.directory = directory;
	}
	
	private static String ID = "id";
	private static String DOC_NAME = "docName";
	private static String DOC_NUM = "docNum";
	private static String CATEGORYS = "categorys";
	private static String DOC_CONTENT = "docContent";
	@SuppressWarnings("unused")
	private static String[] QUERY_FIELD = {DOC_NAME, DOC_NUM, CATEGORYS, DOC_CONTENT};
	public static final BooleanClause.Occur[] QUERY_FLAGS = {BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD };
}
