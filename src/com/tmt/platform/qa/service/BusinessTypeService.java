package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.BusinessTypeDao;
import com.tmt.platform.qa.entity.BusinessType;
import com.tmt.platform.utils.TaxConstants;

@Service
public class BusinessTypeService extends BaseService<BusinessType, String>{
	
	@Autowired
	private BusinessTypeDao businessTypeDao;
	
	@Override
	protected BaseIbatisDAO<BusinessType, String> getEntityDao() {
		return businessTypeDao;
	}
	
	@Transactional
	public void save(BusinessType businessType) {
		if( IdGen.isInvalidId(businessType.getId()) ) {
			this.insert(businessType);
		} else {
			this.update(businessType);
		}
	}
	
	@Transactional
	public BusinessType save(BusinessType businessType, List<BusinessType> olds) {
		if( businessType != null) {
			Boolean bFalg = Boolean.FALSE;
			if( olds != null && olds.size() != 0) {
				for(BusinessType old: olds) {
					if( old != null && old.compareTo(businessType) ) {//只要是判断code,如果一样,则只更新
						bFalg = Boolean.TRUE; businessType.setId(old.getId()); break;
					}
				}
			}
			this.save(businessType);
			if(!bFalg) {//新增的
				olds.add(businessType);
			}
		}
		return businessType;
	}
	
	/**
	 * 批量导入
	 * @param Business
	 */
	@Transactional
	public void batchImport(List<BusinessType> business){
		if( business != null && business.size() != 0) {
			List<BusinessType> olds = this.findAll();
			for(BusinessType item: business) {
				this.save(item, olds);
			}
		}
	}
	/**
	 * 校验是否可以删除
	 * @param businessType
	 * @return
	 */
	public int checkedDelete(BusinessType businessType) {
		return this.countByCondition("checkedDelete", businessType);
	}
	
	/**
	 * 获得默认的业务类型,可以配置
	 * @return
	 */
	public BusinessType getPublicBt(){
		return this.queryForObject("findByCode", TaxConstants.BUSINESS_PUBLIC_CODE);
	}
}
