package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.CategoryItemDao;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.CategoryItem;

@Service
public class CategoryItemService extends BaseService<CategoryItem, String>{
	
	@Autowired
	private CategoryItemDao categoryItemDao;
	@Autowired
	private CategoryService categoryService;
	
	@Override
	protected BaseIbatisDAO<CategoryItem, String> getEntityDao() {
		return categoryItemDao;
	}
	
	@Transactional
	public void save(CategoryItem indutry) {
		if( IdGen.isInvalidId(indutry.getId()) ) {
			this.insert(indutry);
		} else {
			this.update(indutry);
		}
	}
	
	@Transactional
	public CategoryItem save(CategoryItem item, List<CategoryItem> olds) {
		if( item != null ) {
			Boolean bFalg = Boolean.FALSE;
			if( olds != null && olds.size() != 0) {
				for(CategoryItem old: olds) {
					if( old != null && old.compareTo(item) ) {//只要是判断code,如果一样,则只更新
						bFalg = Boolean.TRUE; item.setId(old.getId()); break;
					}
				}
			}
			this.save(item);
			if(!bFalg) {//新增的
				olds.add(item);
			}
		}
		return item;
	}
	
	/**
	 * 批量导入
	 */
	@Transactional
	public void batchImport(List<CategoryItem> items){
		if( items != null && items.size() != 0 ) {
			List<Category> oldCategorys = this.categoryService.findAll();
			List<CategoryItem> oldCategoryItems = this.findAll();
			for( CategoryItem item: items ) {
				Category category = this.categoryService.save(item.getCategory(), oldCategorys);
				item.setCategory(category);
				item.setCategoryId(category.getId());
				item = this.save(item, oldCategoryItems);
			}
		}
	}
	
	/**
	 * 校验是否可以删除
	 * @param businessType
	 * @return
	 */
	public int checkedDelete(CategoryItem item) {
		return this.countByCondition("checkedDelete", item);
	}
}
