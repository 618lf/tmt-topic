package com.tmt.platform.qa.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.qa.dao.TopicRelaDao;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.CategoryItem;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicRela;

@Service
public class TopicRelaService extends BaseService<TopicRela, String>{
	
	@Autowired
	private TopicRelaDao topicRelaDao;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CategoryItemService categoryItemService;
	
	@Override
	protected BaseIbatisDAO<TopicRela, String> getEntityDao() {
		return topicRelaDao;
	}
	
	/**
	 * 批量更新
	 * @param topic
	 * @param options
	 */
	@Transactional
	public void saveByTopic(Topic topic) {
		if(topic.getType() != QaRelaType.FINALLY){
			List<TopicRela> olds = this.queryRelasByTopicAndType(topic.getId(), topic.getType());
			if(olds != null && olds.size() != 0) {
				this.batchDelete(olds);
			}
			List<TopicRela> relas = topic.getRelas();
			if( relas != null && relas.size() != 0) {
				for(TopicRela rela: relas){
					rela.setTopicId(topic.getId());
					rela.setType(topic.getType());
				}
				this.batchInsert(relas);
			}
		}
	}
	
	/**
	 * 得到试题所有状态下的答案
	 * @param topicId
	 * @return
	 */
	public List<TopicRela> queryAllRelas(String topicId) {
		return this.queryRelasByTopicAndType(topicId,null);
	}
	
	public List<TopicRela> queryRelasByTopicAndType(String topicId, QaRelaType type) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("TOPIC_ID", topicId);
		if( type != null) {
			param.put("TYPE", (type==QaRelaType.FINALLY?QaRelaType.FINALLY_VERITY:type).name());
		}
		return this.queryForList("queryRelasByTopicAndType", param);
	}
	
	//填充税种等相关信息
	public TopicRela fillCategory(TopicRela topicRela){
		if( topicRela != null ) {
			//查询税种，和税种明细
			Category category = categoryService.get(topicRela.getCategoryId());
			CategoryItem categoryItem = categoryItemService.get(topicRela.getCategoryItemId());
			if(category != null) {
				topicRela.setCategoryName(category.getName());
			}
			if(categoryItem != null) {
				topicRela.setCategoryItemName(categoryItem.getName());
			}
		}
		return topicRela;
	}
}
