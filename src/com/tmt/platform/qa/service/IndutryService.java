package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.IndutryDao;
import com.tmt.platform.qa.entity.Indutry;
import com.tmt.platform.utils.TaxConstants;

@Service
public class IndutryService extends BaseService<Indutry, String>{
	
	@Autowired
	private IndutryDao indutryDao;
	
	@Override
	protected BaseIbatisDAO<Indutry, String> getEntityDao() {
		return indutryDao;
	}
	
	@Transactional
	public void save(Indutry indutry) {
		if( IdGen.isInvalidId(indutry.getId()) ) {
			this.insert(indutry);
		} else {
			this.update(indutry);
		}
	}
	
	@Transactional
	public Indutry save(Indutry indutry, List<Indutry> olds) {
		if( indutry != null) {
			Boolean bFalg = Boolean.FALSE;
			if( olds != null && olds.size() != 0) {
				for(Indutry old: olds) {
					if( old != null && old.compareTo(indutry) ) {//只要是判断code,如果一样,则只更新
						bFalg = Boolean.TRUE; indutry.setId(old.getId()); break;
					}
				}
			}
			this.save(indutry);
			if(!bFalg) {//新增的
				olds.add(indutry);
			}
		}
		return indutry;
	}
	
	/**
	 * 批量导入
	 * @param Business
	 */
	@Transactional
	public void batchImport(List<Indutry> indutrys){
		if( indutrys != null && indutrys.size() != 0) {
			List<Indutry> olds = this.findAll();
			for(Indutry item: indutrys) {
				this.save(item, olds);
			}
		}
	}
	
	/**
	 * 校验是否可以删除
	 * @param businessType
	 * @return
	 */
	public int checkedDelete(Indutry indutry) {
		return this.countByCondition("checkedDelete", indutry);
	}
	
	/**
	 * 得到通用行业 code为 000;
	 * @return
	 */
	public Indutry getPublicIndutry() {
		return this.queryForObject("findByCode", TaxConstants.INDUTRY_PUBLIC_CODE);
	}
	
}
