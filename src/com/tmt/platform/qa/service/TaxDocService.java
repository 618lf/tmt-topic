package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.TaxDocDao;
import com.tmt.platform.qa.entity.TaxDoc;
import com.tmt.platform.qa.utils.TaxDocs;

@Service
public class TaxDocService extends BaseService<TaxDoc, String>{

	@Autowired
	private TaxDocDao taxDocDao;
	
	@Override
	protected BaseIbatisDAO<TaxDoc, String> getEntityDao() {
		return taxDocDao;
	}
	
	/**
	 * 批量保存 -- 先删除以前的，在保存新的(简单点),以后改成更新的
	 * @param docs
	 */
	@Transactional
	public void batchSave(List<TaxDoc> docs) {
		try{
			List<TaxDoc> _docs = this.findAll();
			TaxDocs.build(_docs);
			List<TaxDoc> updates = Lists.newArrayList();
			List<TaxDoc> inserts = Lists.newArrayList();
			for(TaxDoc doc: docs) {
				TaxDoc old = TaxDocs.find(doc.getSourceUrl());
				if(old != null) { //主要是添加文件类型
//					if(StringUtil3.isNotBlank(doc.getDocType())) {
//						String oldDocType = old.getDocType() == null?",":old.getDocType();
//						String docType = new StringBuilder(",").append(doc.getDocType()).append(",").toString();
//						if( StringUtil3.isBlank(oldDocType) || oldDocType.indexOf(docType) == -1) {
//							docType = new StringBuilder(oldDocType).deleteCharAt(oldDocType.length()-1).append(docType).toString();
//							old.setDocType(docType);
//						}
//					}
//					if( old.getPubDate() == null && doc.getPubDate() != null) {
//						old.setPubDate(doc.getPubDate());
//					}
					old.setPubOffice(doc.getPubOffice());
					updates.add(old);//暂时只修改文件类型
				} else {
					inserts.add(doc);
				}
			}
			this.batchInsert(inserts);
			this.batchUpdate("updateDocType", updates);
		}catch(Exception e){ //没回归事务
			throw new BaseRuntimeException("获取税收法规信息失败：", e);
		}finally{
			TaxDocs.remove();
		}
	}
	
	/**
	 * 按法规名称查找一个 like查找
	 * @param docName
	 * @return
	 */
	public TaxDoc queryChinaTaxByDocName(String docName) {
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		c.andLike("DOC_NAME", "docName", docName);
		c.andEqualTo("PUB_OFFICE", "pubOffice", "总局法规");
		List<TaxDoc> docs = this.queryByCondition(qc);
		return docs != null && docs.size() != 0?docs.get(0):null;
	}
}
