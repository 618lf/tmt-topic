package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.exception.AccessDeniedException;
import com.tmt.platform.qa.dao.TopicDao;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicContent;
import com.tmt.platform.qa.entity.TopicOption;
import com.tmt.platform.qa.entity.TopicRela;
import com.tmt.platform.utils.TaxConstants;

@Service
public class TopicService extends BaseService<Topic, String>{
	
	@Autowired
	private TopicDao topicDao;
	@Autowired
	private QaService qaService;
	@Autowired
	private TopicContentService contentService;
	@Autowired
	private TopicOptionService optionsService;
	@Autowired
	private TopicRelaService relaService;
	
	@Override
	protected BaseIbatisDAO<Topic, String> getEntityDao() {
		return topicDao;
	}
	
	@Transactional
	public void save(Topic topic) {
		QaRelaType nextOption = topic.getOptionType();//指定的状态
		try{
			if( IdGen.isInvalidId(topic.getId()) ) {
				topic.setType(QaRelaType.INIT);
				String nextSeq = (String)IdGen.getInstance().generateId(TaxConstants.TOPIC_CODE_SEQ);
				topic.setCode(StringUtil3.appendTo(new StringBuilder("Q"), StringUtil3.leftPad(nextSeq, 8, "0")).toString());
				this.insert(topic);
			}
			//只有完成命题才需要写入数据
			if(topic.getType() == QaRelaType.INIT|| nextOption == QaRelaType.FINALLY) {
				this.updateVersion(topic);
			} else {
				this.updateVersion("updateVersion",topic);
			}
			//试题内容
			contentService.saveByTopic(topic);
			//试题选项 -- 改变类型,则删除之前的选项
			optionsService.saveByTopic(topic);
			//关联的税种
			relaService.saveByTopic(topic);
			//修改命题来源的状态
			this.updateFrom(topic);
			//准备下一步的数据
			this.finishTopic(topic, nextOption);
		}catch(Exception e){
			throw new BaseRuntimeException(e);
		}
	}
	
	/**
	 * 查询指定状态下的试题
	 * @param topic
	 * @return
	 */
	public Topic getByType(Topic topic){
		QaRelaType nextOption = topic.getOptionType();//指定的状态
		if( topic != null && !IdGen.isInvalidId(topic.getId())) {
		    topic = this.get(topic.getId());
		    if( topic != null) {
		    	//默认查询状态
		    	if( nextOption == null ){nextOption = topic.getType(); }
		    	//选项
				List<TopicOption> options = optionsService.queryOptionsByTopicAndType(topic.getId(), nextOption);
				topic.setOptions(options);
				//内容
				TopicContent content = contentService.getContentByTopicAndType(topic.getId(), nextOption);
				topic.setTopicContent(content);
				//税种关联
				List<TopicRela> relas = relaService.queryRelasByTopicAndType(topic.getId(), nextOption);
				topic.setRelas(relas);
			}
		}
		if( topic == null) {
			throw new AccessDeniedException("试题已删除,请刷新列表重试");
		}
		//试题的来源的数据
		topic = this.findFrom(topic);
		//保持nextOption 不变
		topic.setOptionType(nextOption);
		return topic;
	}
	
	public Topic getConyentByType(Topic topic){
		if( topic != null && StringUtil3.isNotBlank(topic.getId())) {
			QaRelaType optionType = topic.getOptionType();
			if( topic.getType() == null) {//数据不完整
				topic = this.get(topic.getId());
				topic.setOptionType(optionType);//操作类型不能丢失
			}
			//内容
			TopicContent content = contentService.getContentByTopicAndType(topic.getId(), topic.getType());
			topic.setTopicContent(content);
		}
		return topic;
	}
	
	public Topic findFrom(Topic topic) {
		Qa qa = null;
		if( topic.getQFrom() != null && !IdGen.isInvalidId(topic.getQId())) {
			qa = qaService.getWithOthers(topic.getQId());
			if( QFrom.QA == topic.getQFrom()) {// 感觉应该可以看啊 -- 但最好不能编辑，这样好点
				if( qa == null || ( topic.getType() == QaRelaType.INIT && !qa.checkMaker()) ) {//答疑不存在,或者答疑命题人不是当前用户,返回一个错误页面（前提，如果是命题阶段）
					throw new AccessDeniedException(StringUtil3.format("%s'%s'%s", "无权使用答疑",(qa== null?"":qa.getTitle()),"编制试题！"));
				}
			}
			if( IdGen.isInvalidId(topic.getId()) ) {//新增状态
				topic.setBusinessTypeId(qa.getBusinessTypeId());
				topic.setBusinessTypeName(qa.getBusinessTypeName());
				topic.setIndutryId(qa.getIndutryId());
				topic.setIndutryName(qa.getIndutryName());
				topic.setQaRelas(qa.getQaRelas());
			}
		}  else {
			topic.setQFrom(QFrom.CUSTOM);
			qa = new Qa();
			qa.setId(IdGen.INVALID_ID);
		}
		topic.setFromObj(qa);
		return topic;
	}
	
	@Transactional
	public void updateFrom(Topic topic){
		if( topic.getQFrom() != null && QFrom.QA == topic.getQFrom()) {
			qaService.saveFrom(topic);
		}
	}
	
	/**
	 * 不命题
	 * @param topic
	 */
	@Transactional
	public void updateFormNo(Topic topic){
		if( topic.getQFrom() != null && QFrom.QA == topic.getQFrom()) {
			qaService.updateFormNo(topic);
		}
	}
	
	/**
	 * 删除所有与试题相关的信息,相应状态下
	 */
	@Transactional
	public void batchDelete(List<Topic> topics){
		List<TopicContent> contents = Lists.newArrayList();
		List<TopicOption>  options = Lists.newArrayList();
		List<TopicRela> relas = Lists.newArrayList();
		for(Topic topic: topics) {
			//试题内容
			List<TopicContent> temps = this.contentService.getAllContent(topic.getId());
			if(temps != null) {
				contents.addAll(temps);
			}
			//试题答案
			List<TopicOption> tempOptions = this.optionsService.queryAllOptions(topic.getId());
			if(tempOptions != null) {
				options.addAll(tempOptions);
			}
			//试题税种关联
			List<TopicRela> tempRelas = this.relaService.queryAllRelas(topic.getId());
			if( tempRelas != null) {
				relas.addAll(tempRelas);
			}
			topic.setOptionType(QaRelaType.DEL);
			//试题来源 
			this.updateFrom(topic);
		}
		this.contentService.batchDelete(contents);
		this.optionsService.batchDelete(options);
		this.relaService.batchDelete(relas);
		super.batchDelete(topics);
	}
	
	/**
	 * 完成命题
	 * @param topics
	 */
	@Transactional
	public Boolean finish(List<Topic> topics){
		if( topics != null && topics.size() != 0) {
			for( Topic topic: topics ) {
				Integer version = topic.getVersion();
				QaRelaType nextOption =  topic.getOptionType();
				//查询出当前状态的数据
				topic.setOptionType(null);
				topic = this.getByType(topic);
				topic.setVersion(version);//保存前台的版本
				topic.setOptionType(nextOption);
				//直接保存
				this.save(topic);
			}
		}
		return Boolean.TRUE;
	}
	
	/**
	 * 完成试题 -- 有版本验证
	 * @param topic
	 * @param nextOption 指定完成的状态
	 */
	private void finishTopic(Topic topic, QaRelaType nextOption) {
		topic.setOptionType(topic.getType());//判断权限前需要知道当前的状态
		if( topic.getType().isNextOption(nextOption) && topic.getEditFlag() ) { //如果是当前下一步的的操作,有编辑权限
			//操作下一步
			topic.setType(nextOption);
			this.updateVersion("updateType",topic);//版本验证
			//试题内容
			contentService.saveByTopic(topic);
			//试题选项 -- 改变类型,则删除之前的选项
			optionsService.saveByTopic(topic);
			//关联的税种
			relaService.saveByTopic(topic);
			return;
		} else if( !topic.getType().isNextOption(nextOption)) {
			return;
		}
		throw new BaseRuntimeException("无权限");
	}
	
	/**
	 * 是否公开
	 * @param topics
	 */
	@Transactional
	public void toPublic(List<Topic> topics){
		if( topics != null && topics.size() != 0) {
			this.batchUpdate("updatePublic", topics);
		}	
	}
	/**
	 * 审核的列表
	 * @param qc
	 * @param param
	 * @return
	 */
	public Page queryForAuditPage(QueryCondition qc, PageParameters param){
		return this.queryForPageList("findAuditByCondition", qc, param);
	}
	
	/**
	 * 审核的数量
	 * @param qc
	 * @return
	 */
	public Integer countAuditTopic(QueryCondition qc){
		Integer count = this.countByCondition("findAuditByConditionStat", qc);
		return count == null ? 0: count;
	}
	
	/**
	 * 批量导入
	 * @param items
	 */
	@Transactional
	public void batchImport(List<Topic> items) {
		if(items != null) {
			//批量保存
			for(Topic topic: items) {
				this.save(topic);
			}
		}
	}
}
