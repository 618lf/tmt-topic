package com.tmt.platform.qa.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.exception.StaleObjectStateException;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.platform.enums.MakeStatus;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.qa.dao.QaDao;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.utils.Qas;
import com.tmt.platform.utils.TaxPlatformUtils;

/**
 * 答疑服务类
 * @author lifeng
 */
@Service
public class QaService extends BaseService<Qa, String>{
	
	private static String PUB_OFFICES = "PUB_OFFICES";
	
	@Autowired
	private QaDao qaDao;
	@Autowired
	private QaRelaService qaRelaService;
	
	@Override
	protected BaseIbatisDAO<Qa, String> getEntityDao() {
		return qaDao;
	}
	
	/**
	 * 保存到服务器,根据code判断是否需要更新
	 * Code和来源的网站是唯一的
	 * 一次保存一个发布机关的
	 * @param qas
	 */
	@Transactional
	public void save(List<Qa> qas){
		try{
			Qa one = (qas != null && qas.size() != 0)?qas.get(0):null;
			if( one != null ) {
				List<Qa> inserts = Lists.newArrayList();
				List<Qa> updates = Lists.newArrayList();
				List<Qa> olds = this.queryByPublishOffice(one.getPublishOffice());
				Qas.build(olds, one);
				for( Qa newQa : qas ) {
					String key = StringUtil3.isNotBlank(one.getSourceUrl()) ? newQa.getSourceUrl(): (newQa.getTitle() + newQa.getPublishDate());
					Qa old =  Qas.find(key);
					if( old != null) { //存在更新为最新的
						if(StringUtil3.isNotBlank(newQa.getAnswer())) {
							old.setAnswer(newQa.getAnswer());
						}
						if(StringUtil3.isNotBlank(newQa.getContent())) {
							old.setContent(newQa.getContent());
						}
						if(StringUtil3.isNotBlank(newQa.getTitle())) {
							old.setTitle(newQa.getTitle());
						}
						if(StringUtil3.isNotBlank(newQa.getPublishDate())) {
							old.setPublishDate(newQa.getPublishDate());
						}
						updates.add(old);
					} else {
						Qas.setCode(newQa);
						inserts.add(newQa);
					}
				}
				this.batchUpdate("updateQaInfo",updates);
				this.batchInsert(inserts);
			}
		}catch(Exception e){ //没回归事务
			throw new BaseRuntimeException("获取答疑信息失败：",e);
		}finally{
			Qas.remove();
		}
	}
	
	/**
	 * 页面保存 -- 命题在这个阶段永远是可以命题
	 * @param qa
	 * @throws StaleObjectStateException 
	 */
	@Transactional
	public void save(Qa qa) throws StaleObjectStateException {
		if( qa.getEditFlag() ) {
			if(IdGen.isInvalidId(qa.getId())) {
				Qas.setCode(qa);
				this.insert(qa);
			} else {
				//判断之前是否设置入库，如果是则不修改筛选人，否则修改筛选人
				String storageFlag = this.queryStorageFlagByPk(qa.getId());
				if(Qa.YES.equals(storageFlag)) {
					//不更新筛选人
					this.updateVersion("updateWithNotUpdater",qa);
				} else {//全部更新
					this.updateVersion(qa);
				}
			}
			//不入库就删除税种关联信息。
			if(Qa.NO.equals(qa.getStorageFlag())) {
				List<QaRela> qaRelas = this.qaRelaService.queryByQaId(qa.getId());
				this.qaRelaService.batchDelete(qaRelas);
			}
		}
	}
	/**
	 * 查询税种的其他信息
	 * @param id
	 * @return
	 */
	public Qa getWithOthers(String id){
		Qa qa = this.get(id);
		if( qa != null ) {
			List<QaRela> qaRelas = this.qaRelaService.queryByQaId(id);
			qa.setQaRelas(qaRelas);
		}
		return qa;
	}
	
	@Transactional
	public void saveFrom(Topic topic){
		if( topic.getOptionType() == QaRelaType.DEL ){
			Qa qa = this.get(topic.getQId());
			this.clearStatus(qa);
		} else if(topic.getType() == QaRelaType.INIT && (topic.getOptionType() == QaRelaType.INIT 
				|| topic.getOptionType() == QaRelaType.INIT_VERITY )){//只有开始命题或命题完成才修改来源的状态
			MakeStatus status = MakeStatus.NONE;
			if( topic.getOptionType() == QaRelaType.INIT) {
				status = MakeStatus.DOING;
			} else {
				status = MakeStatus.DONE;
			}
			Qa qa = (Qa)topic.getFromObj();qa.setId(topic.getQId());
			this.updateStatus(qa, status);
		}
	}
	
	@Transactional
	public void updateFormNo(Topic topic){
		Qa qa = (Qa)topic.getFromObj();qa.setId(topic.getQId());
		qa.setMakeQFlag(Qa.NO);
		qa.setMakeId(UserUtils.getUser().getId());
		qa.setMakeName(UserUtils.getUser().getName());
		qa.setMakeDate(DateUtil3.getTimeStampNow());
		qa.setMakeStatus(MakeStatus.NO);
		this.updateVersion("updateMakeInfo", qa);//修改命题人信息  -- 版本验证
	}
	
	/**
	 * 需要使用版本验证
	 * @param id
	 * @param status
	 */
	private void updateStatus(Qa qa, MakeStatus status){
		if( qa != null ) {
			qa.setMakeQFlag(Qa.YES);
			qa.setMakeId(UserUtils.getUser().getId());
			qa.setMakeName(UserUtils.getUser().getName());
			qa.setMakeDate(DateUtil3.getTimeStampNow());
			qa.setMakeStatus(status);
			this.updateVersion("updateMakeInfo", qa);//修改命题人信息  -- 版本验证
		}
	}
	
	/**
	 * 清除命题信息
	 * @param qa
	 */
	private void clearStatus(Qa qa){
		if( qa != null ) {
			qa.setMakeId(null);
			qa.setMakeName(null);
			qa.setMakeDate(null);
			qa.setMakeStatus(null);
			qa.setMakeQFlag(Qa.NO);//设置成没命题
			this.updateVersion("updateMakeInfo", qa);//修改命题人信息
		}
	}
	
	/**
	 * 初始化我的待命题列表
	 * @param qc
	 * @param param
	 * @return
	 */
	public Page queryInitTopicList(QueryCondition qc, PageParameters param){
		return this.queryForPageList("findInitTopicList", qc, param);
	}
	
	/**
	 * 待初始化命题列表
	 * @param qc
	 * @return
	 */
	public Integer countInitTopicList(QueryCondition qc) {
		Integer count  = this.countByCondition("findInitTopicListStat",qc);
		return count == null?0:count;
	}
	
	/**
	 * 测试是否可以删除 ,设置了入库,或命题
	 * @param qa
	 * @return
	 */
	public Boolean checkDelete(Qa qa) {
		if(Qa.NO.equals(qa.getStorageFlag()) || !StringUtil3.isNotBlank(qa.getStorageFlag())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 批量删除
	 */
	@Transactional
	public void batchDelete(List<Qa> qas) {
		List<QaRela> qaRelas = Lists.newArrayList();
		for( Qa qa: qas ) {
			List<QaRela> relas = this.qaRelaService.queryByQaId(qa.getId());
			if( relas != null ) {
				qaRelas.addAll(relas);
			}
		}
		this.qaRelaService.batchDelete(qaRelas);
		super.batchDelete(qas);
	}
	
	/**
	 * 获取发布机关
	 * @param name
	 * @param param
	 * @return
	 */
	public Page queryPublishOfficePage(String name, PageParameters param) {
		String key = new StringBuilder(PUB_OFFICES).toString();
		Map<String,Object> qc = Maps.newHashMap();
		List<Map<String,Object>> offices = TaxPlatformUtils.get(key);
		if( offices == null) {
			offices = this.queryForMapList("queryPublishOfficePage", qc);
			param.setRecordCount(10);
			TaxPlatformUtils.put(key, offices);
		}
		Page page = new Page();
		page.setData(offices);
		page.setPage(param);
		return page;
	}
	
	/**
	 * 查询所有指定税务机关的答疑
	 * @param name
	 * @return
	 */
	public List<Qa> queryByPublishOffice(String name){
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		c.andEqualTo("PUBLISH_OFFICE", "publishOffice", name);
		return this.queryByCondition(qc);
	}
	
	/**
	 * 读取答疑的是否入库的属性
	 * @param id
	 * @return
	 */
	public String queryStorageFlagByPk(String id){
		return this.queryForAttr("findStorageFlagByPk", id);
	}
}
