package com.tmt.platform.qa.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.enums.QType;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.qa.dao.TopicOptionDao;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicOption;

@Service
public class TopicOptionService extends BaseService<TopicOption, String>{
	
	@Autowired
	private TopicOptionDao TopicOptionDao;
	
	@Override
	protected BaseIbatisDAO<TopicOption, String> getEntityDao() {
		return TopicOptionDao;
	}
	
	/**
	 * 批量更新
	 * @param topic
	 * @param options
	 */
	@Transactional
	public void saveByTopic(Topic topic) {
		if(topic.getType() != QaRelaType.FINALLY){
			List<TopicOption> olds = this.queryOptionsByTopicAndType(topic.getId(), topic.getType());
			if(olds != null && olds.size() != 0) {
				this.batchDelete(olds);
			}
			List<TopicOption> options = topic.getOptions();
			if(QType.isChoice(topic.getQType())) {
				for( TopicOption option: options) {
					option.setTopicId(topic.getId());
					option.setContentId(topic.getTopicContent().getId());
					option.setType(topic.getType());
				}
			}
			if( options != null && options.size() != 0) {
				this.batchInsert(options);
			}
		}
	}
	
	/**
	 * 得到试题所有状态下的答案
	 * @param topicId
	 * @return
	 */
	public List<TopicOption> queryAllOptions(String topicId) {
		return this.queryOptionsByTopicAndType(topicId,null);
	}
	
	public List<TopicOption> queryOptionsByTopicAndType(String topicId, QaRelaType type) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("TOPIC_ID", topicId);
		if( type != null) {
			param.put("TYPE", (type==QaRelaType.FINALLY?QaRelaType.FINALLY_VERITY:type).name());
		}
		return this.queryForList("queryOptionsByTopicAndType", param);
	}
}
