package com.tmt.platform.qa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.PubOfficeDao;
import com.tmt.platform.qa.entity.PubOffice;

@Service
public class PubOfficeService extends BaseService<PubOffice, String>{
	
	@Autowired
	private PubOfficeDao pubOfficeDao;
	
	@Override
	protected BaseIbatisDAO<PubOffice, String> getEntityDao() {
		return pubOfficeDao;
	}
}
