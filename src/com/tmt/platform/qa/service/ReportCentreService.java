package com.tmt.platform.qa.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.dao.ReportCentreDao;
import com.tmt.platform.qa.entity.Topic;

@Service
public class ReportCentreService extends BaseService<Map<String,Object>, String>{

	@Autowired
	private ReportCentreDao reportCentreDao;
	
	@Override
	protected BaseIbatisDAO<Map<String, Object>, String> getEntityDao() {
		return reportCentreDao;
	}
	
	/**
	 * 按类型和难度统计数据，可以按难度查询数据
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByQTypeAndQLevel(Topic topic){
		Map<String,Object> params = Maps.newHashMap();
		if( topic != null && topic.getQLevel() != null) {
			params.put("Q_LEVEL", topic.getQLevel().name());
		}
		if( topic != null && StringUtil3.isNotBlank(topic.getCreateId()) ) {
			params.put("CREATE_ID", topic.getCreateId());
		}
		return this.queryForList("statByQTypeAndQLevel", params);
	}
	
	/**
	 * 按行业统计数据，可以按行业查询数据
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByIndutry(String indutryName){
		Map<String,Object> params = Maps.newHashMap();
		if( indutryName != null) {
			params.put("INDUTRY_NAME", indutryName);
		}
		return this.queryForList("statByIndutry", params);
	}
	
	/**
	 * 按行业统计数据，可以按行业查询数据
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByBusinessType(String businessTypeName){
		Map<String,Object> params = Maps.newHashMap();
		if( businessTypeName != null) {
			params.put("BUSINESS_TYPE_NAME", businessTypeName);
		}
		return this.queryForList("statByBusinessType", params);
	}
	
	/**
	 * 按行业统计数据，可以按行业查询数据
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByCategory(String categoryName){
		Map<String,Object> params = Maps.newHashMap();
		if( categoryName != null) {
			params.put("CATEGORY_NAME", categoryName);
		}
		return this.queryForList("statByCategory", params);
	}
	
	/**
	 * 按行业统计数据，可以按行业查询数据
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByFrom(){
		Map<String,Object> params = Maps.newHashMap();
		return this.queryForList("statByFrom", params);
	}
	
	/**
	 * 按行业统计数据，可以按税种查询
	 * @param qLevel
	 * @return
	 */
	public List<Map<String,Object>> statByCategoryItem(String categoryId, String categoryName){
		Map<String,Object> params = Maps.newHashMap();
		if( StringUtil3.isNotBlank(categoryId)) {
			params.put("CATEGORY_ID", categoryId);
		}
		if( StringUtil3.isNotBlank(categoryName)) {
			params.put("CATEGORY_NAME", categoryName);
		}
		return this.queryForList("statByCategoryItem", params);
	}
}
