package com.tmt.platform.qa.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.exception.TopicContentException;
import com.tmt.platform.qa.dao.TopicContentDao;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicContent;

@Service
public class TopicContentService extends BaseService<TopicContent, String>{
	
	@Autowired
	private TopicContentDao topicContentDao;
	
	@Override
	protected BaseIbatisDAO<TopicContent, String> getEntityDao() {
		return topicContentDao;
	}
	
	@Transactional
	public void saveByTopic(Topic topic) {
		if(topic.getType() != QaRelaType.FINALLY){
			TopicContent content = this.getContentByTopic(topic);
			if( IdGen.isInvalidId(content.getId()) ) {
				this.insert(content);
			} else {
				this.update(content);
			}
			//临时存储
			topic.setTopicContent(content);
		}
	}
	
	/**
	 * 得到试题相应的内容
	 * @return
	 */
	public TopicContent getContentByTopic(Topic topic) {
		TopicContent content = this.getContentByTopicAndType(topic.getId(), topic.getType());
		if( content == null ) {//当前状态下没数据
			content = TopicContent.getByTopic(topic);
		} else {//如果存在则更新
			content = content.update(topic);
		}
		return content;
	}
	
	/**
	 * 查询指定类型的 的试题内容: INIT("初始"), INIT_VERITY("初审"), FINALLY_VERITY("终审");
	 * @param topicId
	 * @param type
	 * @return
	 */
	public TopicContent getContentByTopicAndType(String topicId, QaRelaType type ){
		Map<String,Object> param = Maps.newHashMap();
		param.put("TOPIC_ID", topicId);
		param.put("TYPE", (type==QaRelaType.FINALLY?QaRelaType.FINALLY_VERITY:type).name());
		List<TopicContent> contents = this.queryForList("queryContentByTopicAndType", param);
		if(contents!= null && contents.size()>1) {
			throw new TopicContentException("试题内容错误");
		}
		return contents!= null&&contents.size() ==1?contents.get(0):null;
	}
	
	/**
	 * 得到试题所有状态下的内容
	 * @param topicId
	 * @return
	 */
	public List<TopicContent> getAllContent(String topicId){
		Map<String,Object> param = Maps.newHashMap();
		param.put("TOPIC_ID", topicId);
		return this.queryForList("queryContentByTopicAndType", param);
	}
	
}
