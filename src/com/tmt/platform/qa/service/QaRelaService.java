package com.tmt.platform.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.platform.qa.dao.QaRelaDao;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;

@Service
public class QaRelaService extends BaseService<QaRela, String>{
	
	@Autowired
	private QaRelaDao qaRelaDao;
	@Autowired
	private QaService qaService;
	
	@Override
	protected BaseIbatisDAO<QaRela, String> getEntityDao() {
		return qaRelaDao;
	}
	
	@Transactional
	public void save(QaRela qaRela) {
		//如果无汇编信息,则保存汇编的信息
		if(IdGen.isInvalidId(qaRela.getQaId())) {
			Qa qa = new Qa(); qa.setId(IdGen.INVALID_ID);
			qaService.save(qa);
		}
		if( IdGen.isInvalidId(qaRela.getId()) ) {
			this.insert(qaRela);
		} else {
			this.update(qaRela);
		}
	}
	
	@Transactional
	public void batchSave(Qa qa, List<QaRela> qaRelas) {
		List<QaRela> relas = this.queryByQaId(qa.getId());
		if( relas != null) {
			super.batchDelete(relas);
		}
		this.batchInsert(qaRelas);
	}
	
	public List<QaRela> queryByQaId(String qaId) {
		return this.queryForList("queryByQaId", qaId);
	}
}
