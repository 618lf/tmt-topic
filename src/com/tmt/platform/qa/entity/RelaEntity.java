package com.tmt.platform.qa.entity;

import com.tmt.base.common.entity.IdEntity;

/**
 * 试题关系
 * @author lifeng
 */
public abstract class RelaEntity extends IdEntity<String>{

	private static final long serialVersionUID = 7223148360900233098L;

    protected String categoryId;
    protected String categoryItemId;
    protected String larDocNum;
    protected String larDocName;
    protected String larDocContent;
    protected String applyProvince;
    protected String applyCity;
    protected String categoryName;
    protected String categoryItemName;
    
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryItemName() {
		return categoryItemName;
	}
	public void setCategoryItemName(String categoryItemName) {
		this.categoryItemName = categoryItemName;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryItemId() {
		return categoryItemId;
	}
	public void setCategoryItemId(String categoryItemId) {
		this.categoryItemId = categoryItemId;
	}
	public String getLarDocNum() {
		return larDocNum;
	}
	public void setLarDocNum(String larDocNum) {
		this.larDocNum = larDocNum;
	}
	public String getLarDocName() {
		return larDocName;
	}
	public void setLarDocName(String larDocName) {
		this.larDocName = larDocName;
	}
	public String getLarDocContent() {
		return larDocContent;
	}
	public void setLarDocContent(String larDocContent) {
		this.larDocContent = larDocContent;
	}
	public String getApplyProvince() {
		return applyProvince;
	}
	public void setApplyProvince(String applyProvince) {
		this.applyProvince = applyProvince;
	}
	public String getApplyCity() {
		return applyCity;
	}
	public void setApplyCity(String applyCity) {
		this.applyCity = applyCity;
	}
}
