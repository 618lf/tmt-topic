package com.tmt.platform.qa.entity;

import com.tmt.base.common.entity.IdEntity;

/**
 * 发布机关
 * @author lifeng
 */
public class PubOffice extends IdEntity<String>{
   
	private static final long serialVersionUID = 7095235644402204298L;
	
	private String id;
    private String name;
    private String area;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
}