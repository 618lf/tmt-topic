package com.tmt.platform.qa.entity;

import com.tmt.base.common.utils.StringUtil3;

/**
 * 税法文件
 * @author lifeng
 */
public class Doc {
	
	private String larDocNum;//文件号
	private String larDocName;//文件名
	private String larDocContent;//文件内容
	
	public String getLarDocNum() {
		return larDocNum;
	}
	public void setLarDocNum(String larDocNum) {
		this.larDocNum = larDocNum;
	}
	public String getLarDocName() {
		return larDocName;
	}
	public void setLarDocName(String larDocName) {
		this.larDocName = larDocName;
	}
	public String getLarDocContent() {
		return larDocContent;
	}
	public void setLarDocContent(String larDocContent) {
		this.larDocContent = larDocContent;
	}
	
	public static Doc newDoc(String larDocName, String larDocNum, String larDocContent) {
		Doc doc = new Doc();
		doc.setLarDocName(StringUtil3.isNotBlank(larDocName)?larDocName:"");
		doc.setLarDocNum(StringUtil3.isNotBlank(larDocNum)?larDocNum:"");
		doc.setLarDocContent(StringUtil3.isNotBlank(larDocContent)?larDocContent:"");
		return doc;
	}
}
