package com.tmt.platform.qa.entity;

import java.util.Map;

import com.google.common.collect.Maps;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QaRelaType;

/**
 * 试题 - 税种
 * @author lifeng
 */
public class TopicRela extends RelaEntity{
    
    private static final long serialVersionUID = 6904187568725518547L;
	private String topicId;
    private QaRelaType type;
    
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryItemName() {
		return categoryItemName;
	}
	public void setCategoryItemName(String categoryItemName) {
		this.categoryItemName = categoryItemName;
	}

	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryItemId() {
		return categoryItemId;
	}
	public void setCategoryItemId(String categoryItemId) {
		this.categoryItemId = categoryItemId;
	}
	public String getLarDocNum() {
		return larDocNum;
	}
	public void setLarDocNum(String larDocNum) {
		this.larDocNum = larDocNum;
	}
	public String getLarDocName() {
		return larDocName;
	}
	public void setLarDocName(String larDocName) {
		this.larDocName = larDocName;
	}
	public String getLarDocContent() {
		return larDocContent;
	}
	public void setLarDocContent(String larDocContent) {
		this.larDocContent = larDocContent;
	}
	public String getApplyProvince() {
		return applyProvince;
	}
	public void setApplyProvince(String applyProvince) {
		this.applyProvince = applyProvince;
	}
	public String getApplyCity() {
		return applyCity;
	}
	public void setApplyCity(String applyCity) {
		this.applyCity = applyCity;
	}
	public QaRelaType getType() {
		return type;
	}
	public void setType(QaRelaType type) {
		this.type = type;
	}
	
	public Map<String,Boolean> compareTo( TopicRela o ){
		Map<String,Boolean> result = Maps.newHashMap();
		if(o != null) {
			if( (StringUtil3.isNotBlank(this.getCategoryId()) && StringUtil3.isNotBlank(o.getCategoryId())
				   && this.getCategoryId().equals(o.getCategoryId()) )
				|| ( !StringUtil3.isNotBlank(this.getCategoryId()) && !StringUtil3.isNotBlank(o.getCategoryId()) )) {
				result.put("categoryId", Boolean.TRUE);
			} else {
				result.put("categoryId", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getCategoryItemId()) && StringUtil3.isNotBlank(o.getCategoryItemId())
				   && this.getCategoryItemId().equals(o.getCategoryItemId()) )
				|| ( !StringUtil3.isNotBlank(this.getCategoryItemId()) && !StringUtil3.isNotBlank(o.getCategoryItemId()) )) {
				result.put("categoryItemId", Boolean.TRUE);
			} else {
				result.put("categoryItemId", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getLarDocNum()) && StringUtil3.isNotBlank(o.getLarDocNum())
				   && this.getLarDocNum().equals(o.getLarDocNum()) )
				|| ( !StringUtil3.isNotBlank(this.getLarDocNum()) && !StringUtil3.isNotBlank(o.getLarDocNum()) )) {
				result.put("larDocNum", Boolean.TRUE);
			} else {
				result.put("larDocNum", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getLarDocName()) && StringUtil3.isNotBlank(o.getLarDocName())
				   && this.getLarDocName().equals(o.getLarDocName()) )
				|| ( !StringUtil3.isNotBlank(this.getCategoryItemId()) && !StringUtil3.isNotBlank(o.getLarDocName()) )) {
				result.put("larDocName", Boolean.TRUE);
			} else {
				result.put("larDocName", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getLarDocContent()) && StringUtil3.isNotBlank(o.getLarDocContent())
				   && this.getLarDocContent().equals(o.getLarDocContent()) )
				|| ( !StringUtil3.isNotBlank(this.getLarDocContent()) && !StringUtil3.isNotBlank(o.getLarDocContent()) )) {
				result.put("larDocContent", Boolean.TRUE);
			} else {
				result.put("larDocContent", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getApplyProvince()) && StringUtil3.isNotBlank(o.getApplyProvince())
				   && this.getApplyProvince().equals(o.getApplyProvince()) )
				|| ( !StringUtil3.isNotBlank(this.getApplyProvince()) && !StringUtil3.isNotBlank(o.getApplyProvince()) )) {
				result.put("applyProvince", Boolean.TRUE);
			} else {
				result.put("applyProvince", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getApplyCity()) && StringUtil3.isNotBlank(o.getApplyCity())
				   && this.getApplyCity().equals(o.getApplyCity()) )
				|| ( !StringUtil3.isNotBlank(this.getApplyCity()) && !StringUtil3.isNotBlank(o.getApplyCity()) )) {
				result.put("applyCity", Boolean.TRUE);
			} else {
				result.put("applyCity", Boolean.FALSE);
			}
		}
		
		if( !result.isEmpty()) {
			for(String key : result.keySet()) {
				if(result.get(key) != null && result.get(key) == Boolean.TRUE) {
					result.clear();result.put("relas", Boolean.FALSE);
					break;
				}
			}
		} else {
			result.clear();
		}
		return result;
	}
	
	public TopicRela(){}
	
	public TopicRela(QaRela qaRela){
		this.categoryId = qaRela.getCategoryId();
		this.categoryItemId = qaRela.getCategoryItemId();
		this.larDocNum = qaRela.getLarDocNum();
		this.larDocName = qaRela.getLarDocName();
		this.larDocContent = qaRela.getLarDocContent();
		this.applyProvince = qaRela.getApplyProvince();
		this.applyCity = qaRela.getApplyCity();
		this.categoryName = qaRela.getCategoryName();
		this.categoryItemName = qaRela.getCategoryItemName();
	}
}