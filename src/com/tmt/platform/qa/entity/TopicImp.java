package com.tmt.platform.qa.entity;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import com.google.common.collect.Lists;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QLevel;
import com.tmt.platform.enums.QType;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.qa.utils.Topics;


/**
 * 题目 - 导入对应(导入的时候才有用)
 * @author lifeng
 */
public class TopicImp extends Topic{
   
	private static final long serialVersionUID = 5724444567876757325L;
	
	private String optionOne;
    private String optionTwo;
    private String optionThree;
    private String optionFour;
    private String optionFive;
    
	public String getOptionFive() {
		return optionFive;
	}
	public void setOptionFive(String optionFive) {
		this.optionFive = optionFive;
	}
	public String getOptionOne() {
		return optionOne;
	}
	public void setOptionOne(String optionOne) {
		this.optionOne = optionOne;
	}
	public String getOptionTwo() {
		return optionTwo;
	}
	public void setOptionTwo(String optionTwo) {
		this.optionTwo = optionTwo;
	}
	public String getOptionThree() {
		return optionThree;
	}
	public void setOptionThree(String optionThree) {
		this.optionThree = optionThree;
	}
	public String getOptionFour() {
		return optionFour;
	}
	public void setOptionFour(String optionFour) {
		this.optionFour = optionFour;
	}
	
	/**
	 * 属性转换
	 * @return
	 */
	public Topic toTopic(){
		Topic topic = new Topic();
		try {
			//date数据会有问题
			ConvertUtils.register(new DateConverter(null), java.util.Date.class);   
			BeanUtils.copyProperties(topic, this);
			
			topic.setQFrom(QFrom.CUSTOM);
			topic.setQLevel(QLevel.valueOf(topic.getQlevelStr()));
			topic.setQType(QType.valueOf(topic.getQtypeStr()));
			topic.setType(QaRelaType.INIT);
			if( QType.isChoice(topic.getQType())) { //选择题
				List<TopicOption> options = Lists.newArrayList();
				TopicOption option = fetchOption("A",optionOne);
				if( option != null) {
					options.add(option);
				}
				option = fetchOption("B",optionTwo);
				if( option != null) {
					options.add(option);
				}
				option = fetchOption("C",optionThree);
				if( option != null) {
					options.add(option);
				}
				option = fetchOption("D",optionFour);
				if( option != null) {
					options.add(option);
				}
				option = fetchOption("E",optionFive);
				if( option != null) {
					options.add(option);
				}
				topic.setOptions(options);
			}
			//解析答案
			topic = Topics.dealImpTopic(topic);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return topic;
	}
	
	private TopicOption fetchOption(String soption, String salisa){
		if(StringUtil3.isNotBlank(salisa)) {
			TopicOption option = new TopicOption();
			option.setSoption(soption);
			option.setSalisa(salisa);
			return option;
		}
		return null;
	}
}