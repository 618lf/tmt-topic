package com.tmt.platform.qa.entity;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.tmt.base.common.entity.IdEntity;

/**
 * 税收法规文件库 -- 少一个文件类型
 * @author lifeng
 */
public class TaxDoc extends IdEntity<String>{

	private static final long serialVersionUID = 3405721252437466931L;
	
    private String docName;//文件名
    private String docNum;//文件号
    private String docType;//文件类型
    private String docContent;//存储一个地址
    private String enabled;//是否有效
    private Date docDate;//成文日期
    private Date pubDate;//发布日期
    private String pubOffice;//发布机关
    private String categorys;//税种
    private String sourceUrl;
    
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocNum() {
		return docNum;
	}
	public void setDocNum(String docNum) {
		this.docNum = docNum;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocContent() {
		return docContent;
	}
	public void setDocContent(String docContent) {
		this.docContent = docContent;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	@JSONField(format = "yyyy-MM-dd")
	public Date getDocDate() {
		return docDate;
	}
	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}
	
    @JSONField(format = "yyyy-MM-dd")
	public Date getPubDate() {
		return pubDate;
	}
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}
	public String getPubOffice() {
		return pubOffice;
	}
	public void setPubOffice(String pubOffice) {
		this.pubOffice = pubOffice;
	}
	public String getCategorys() {
		return categorys;
	}
	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}
	
	public String getDocFileName(){
		String docFileName = new StringBuilder("").append(this.getDocName().trim()).append("")
	             .append(this.getDocNum().trim()).append(".txt").toString();
		return docFileName;
	}
}
