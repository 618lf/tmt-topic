package com.tmt.platform.qa.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.tmt.base.common.orm.annotion.TableAlias;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.BaseEntity;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.platform.enums.MakeStatus;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.utils.TaxConstants;

/**
 * 汇编
 * @author lifeng
 */
@TableAlias(value="QA")
public class Qa extends BaseEntity<String>{
   
	private static final long serialVersionUID = -520447467995125888L;
	private QFrom qFrom;//区分：答疑、考试
	private String code;
    private String title;
    private String content;
    private String answer;
    private String businessTypeId;
    private String indutryId;
    private String publishDate;//发布日期，写错了
    private String publishOffice;
    private String publishCode;
    private String remarks2;
    private String remarks3;
    private String keyWords;
    private String storageFlag;//是否入库, 入库人使用修改人,一段有修改人,则其他人不能修改
    private String makeQFlag;//是否编题
    private String makeId;//编题人ID
    private String makeName;//编题人姓名
    private Date makeDate;//编题日期
    private MakeStatus makeStatus;//编题状态
    private String businessTypeName; //存储到数据库
    private String indutryName;//存储到数据库
    private String sourceUrl;
    private List<QaRela> qaRelas;//答疑关联的税种等信息
    private Topic topic;//编制的试题
    private QaRelaType type;//命题的状态 和对应试题的命题状态一样的意思
    
    //查询时适用
    private String categoryId;
    private String categoryItemId;
    
    //得到税种 和税种明细(多个用,隔开)
    private String categorys;
    private String categoryItems;
    
	public QFrom getQFrom() {
		return qFrom;
	}
	public void setQFrom(QFrom qFrom) {
		this.qFrom = qFrom;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryItemId() {
		return categoryItemId;
	}
	public void setCategoryItemId(String categoryItemId) {
		this.categoryItemId = categoryItemId;
	}
	public String getCategorys() {
		return categorys;
	}
	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}
	public String getCategoryItems() {
		return categoryItems;
	}
	public void setCategoryItems(String categoryItems) {
		this.categoryItems = categoryItems;
	}
	public String getStorageFlag() {
		return storageFlag;
	}
	public void setStorageFlag(String storageFlag) {
		this.storageFlag = storageFlag;
	}
	public QaRelaType getType() {
		return type;
	}
	public void setType(QaRelaType type) {
		this.type = type;
	}
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	public List<QaRela> getQaRelas() {
		return qaRelas;
	}
	public void setQaRelas(List<QaRela> qaRelas) {
		this.qaRelas = qaRelas;
	}
	public String getPublishCode() {
		return publishCode;
	}
	public void setPublishCode(String publishCode) {
		this.publishCode = publishCode;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	public String getBusinessTypeName() {
		return businessTypeName;
	}
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}
	public String getIndutryName() {
		return indutryName;
	}
	public void setIndutryName(String indutryName) {
		this.indutryName = indutryName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getBusinessTypeId() {
		return businessTypeId;
	}
	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}
	public String getIndutryId() {
		return indutryId;
	}
	public void setIndutryId(String indutryId) {
		this.indutryId = indutryId;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getPublishOffice() {
		return publishOffice;
	}
	public void setPublishOffice(String publishOffice) {
		this.publishOffice = publishOffice;
	}
	public String getRemarks2() {
		return remarks2;
	}
	public void setRemarks2(String remarks2) {
		this.remarks2 = remarks2;
	}
	public String getRemarks3() {
		return remarks3;
	}
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getMakeQFlag() {
		return makeQFlag;
	}
	public void setMakeQFlag(String makeQFlag) {
		this.makeQFlag = makeQFlag;
	}
	public String getMakeId() {
		return makeId;
	}
	public void setMakeId(String makeId) {
		this.makeId = makeId;
	}
	public String getMakeName() {
		return makeName;
	}
	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}
	
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	public Date getMakeDate() {
		return makeDate;
	}
	public void setMakeDate(Date makeDate) {
		this.makeDate = makeDate;
	}
	public MakeStatus getMakeStatus() {
		return makeStatus;
	}
	public void setMakeStatus(MakeStatus makeStatus) {
		this.makeStatus = makeStatus;
	}
	
	/**
	 * 返回是否可以修改是否命题: 没有命题人才能修改是否入库
	 * @return
	 */
	@JSONField(serialize=false)
	public Boolean getModifyMakeFlag() {
		if(this.getMakeStatus() != null && this.getMakeStatus() != MakeStatus.NONE) {
			return Boolean.FALSE;
		}
		if( StringUtil3.isBlank(this.getMakeId()) ) {
			 return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 返回是否可以编辑 -- 筛选人可以随时编辑数据
	 * 12-4   添加一个用户拥有修改所有数据的权限（不能修改筛选人）
	 * 12-4   如果设置为否，则其他用户可以编辑（待定的不可以）
	 * @return
	 */
	@JSONField(serialize=false)
	public Boolean getEditFlag() {
		User user = UserUtils.getUser();
		if( user.getId().equals(this.getUpdateId()) || StringUtil3.isBlank(this.getUpdateId()) 
				|| UserUtils.isPermitted(TaxConstants.QA_UPDATE_ALL)
				|| (StringUtil3.isBlank(this.getStorageFlag()) || Qa.NO.equals(this.getStorageFlag()))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 验证命题人 -- 命题人是后面产生的
	 * @param user
	 * @return
	 */
	public Boolean checkMaker() {
		 User user = UserUtils.getUser();
		 if( MakeStatus.makeYes(this.makeStatus) && StringUtil3.isNotBlank(this.getMakeId()) && !user.getId().equals(this.getMakeId())) {
			 return Boolean.FALSE;
		 }
		 return Boolean.TRUE;
	}
	
	public String toString(){
		return StringUtil3.format("%s;%s;%s", this.getContent(),this.getPublishDate(),this.getSourceUrl());
	}
}