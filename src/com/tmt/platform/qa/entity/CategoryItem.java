package com.tmt.platform.qa.entity;

import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 税种明细
 * @author lifeng
 */
public class CategoryItem extends IdEntity<String>{
    
	private static final long serialVersionUID = -5300670651704646670L;
	private String code;
    private String name;
    private String categoryId;
    private String categoryName;
    private String delFlag;
    private Category category = new Category();
    
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public Boolean compareTo(CategoryItem old) {
		if( old == null ) {
			return Boolean.FALSE;
		}
		if(StringUtil3.isNotBlank(this.code) && StringUtil3.isNotBlank(old.getCode())
				&& this.code.equals(old.getCode())
				&& this.category.compareTo(old.getCategory())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}