package com.tmt.platform.qa.entity;

import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 业务类型
 * @author lifeng
 */
public class BusinessType extends IdEntity<String>{
	
	private static final long serialVersionUID = 3775499470322172191L;
	private String code;
    private String name;
    private String delFlag;
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public Boolean compareTo(BusinessType old) {
		if( old == null ) {
			return Boolean.FALSE;
		}
		if(StringUtil3.isNotBlank(this.code) && StringUtil3.isNotBlank(old.getCode())
				&& this.code.equals(old.getCode())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
    
}