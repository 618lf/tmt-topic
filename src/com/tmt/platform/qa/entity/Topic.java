package com.tmt.platform.qa.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.system.entity.BaseEntity;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QLevel;
import com.tmt.platform.enums.QType;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.utils.TaxConstants;

/**
 * 题目
 * @author lifeng
 */
public class Topic extends BaseEntity<String>{
   
	private static final long serialVersionUID = -8124871107383549676L;
	
	private String code;
    private String name;
    private String status;
    private String qId;
    private QFrom qFrom;
    private QType qType;
    private QLevel qLevel;
    private Integer postMinute;
    private String content;
    private String skey;
    private String keyDesc;
    private QaRelaType type;//相当于进度:命题,初审,终审，不会存在多条数据
    private String businessTypeId;
    private String indutryId;
    private Object fromObj;//来源
    private List<TopicOption> options;
    private List<TopicRela> relas;//税种关联
    private TopicContent preContent;//上一状态的内容内容信息,用于比较修改项
    private TopicContent topicContent;//内容信息
    private QaRelaType optionType;//操作类别,默认和当前类型相同
    
    //显示的列
    private String auditName;//审核人
    private String businessTypeName;
    private String indutryName;
    
    //发布列表需要显示的列
    private String vauditName;//初审人
    private Date vauditDate;//初审时间
    private String fvauditName;//终审人
    private Date fvauditDate;//终审时间
    
    //是否编题
    private String makeQFlag;//是否编题
    
    //qType 和  qLevel 格式化列
    private String qtypeStr;
    private String qlevelStr;
    
    //查询时适用税种和税种明细
    private String categoryId;
    private String categoryItemId;
    
    //得到税种 和税种明细(多个用,隔开)
    private String categorys;
    private String categoryItems;
    
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryItemId() {
		return categoryItemId;
	}
	public void setCategoryItemId(String categoryItemId) {
		this.categoryItemId = categoryItemId;
	}
	public String getCategorys() {
		return categorys;
	}
	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}
	public String getCategoryItems() {
		return categoryItems;
	}
	public void setCategoryItems(String categoryItems) {
		this.categoryItems = categoryItems;
	}
	public String getMakeQFlag() {
		return makeQFlag;
	}
	public void setMakeQFlag(String makeQFlag) {
		this.makeQFlag = makeQFlag;
	}
	public List<TopicRela> getRelas() {
		return relas;
	}
	public void setRelas(List<TopicRela> relas) {
		this.relas = relas;
	}
	public String getBusinessTypeId() {
		return businessTypeId;
	}
	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}
	public String getIndutryId() {
		return indutryId;
	}
	public void setIndutryId(String indutryId) {
		this.indutryId = indutryId;
	}
	public String getBusinessTypeName() {
		return businessTypeName;
	}
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}
	public String getIndutryName() {
		return indutryName;
	}
	public void setIndutryName(String indutryName) {
		this.indutryName = indutryName;
	}
	public TopicContent getPreContent() {
		return preContent;
	}
	public void setPreContent(TopicContent preContent) {
		this.preContent = preContent;
	}
	public String getAuditName() {
		return auditName;
	}
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	@JSONField(serialize=false)
	public TopicContent getTopicContent() {
		return topicContent;
	}
    //如果在sqlmap文件中适用topicContent.xxx 但topicContent没初始化，则会调用这个方法
	public void setTopicContent(TopicContent topicContent) {
		this.topicContent = topicContent;
		if( this.topicContent != null){//用topicContent 替换topic的内容
			this.setContent(this.topicContent.getContent());
			this.setKeyDesc(this.topicContent.getKeyDesc());
			this.setPostMinute(this.topicContent.getPostMinute());
			this.setQLevel(this.topicContent.getQLevel());
			this.setQType(this.topicContent.getQType());
			this.setSkey(this.topicContent.getSkey());
			//操作人 和操作时间
			this.setCreateId(this.topicContent.getCreateId());
			this.setCreateName(this.topicContent.getCreateName());
			this.setCreateDate(this.topicContent.getCreateDate());
		}
	}
	public QaRelaType getOptionType() {
		if(optionType == null) {
			return this.getType();
		}
		return optionType;
	}
	public void setOptionType(QaRelaType optionType) {
		this.optionType = optionType;
	}
	public QaRelaType getType() {
		return type;
	}
	public void setType(QaRelaType type) {
		this.type = type;
	}
	public List<TopicOption> getOptions() {
		return options;
	}
	public void setOptions(List<TopicOption> options) {
		this.options = options;
	}
	public Object getFromObj() {
		return fromObj;
	}
	public void setFromObj(Object fromObj) {
		this.fromObj = fromObj;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getQId() {
		return qId;
	}
	public void setQId(String qId) {
		this.qId = qId;
	}
	public QFrom getQFrom() {
		return qFrom;
	}
	public String getQfromStr() {
		if(qFrom != null) {
			return qFrom.getName();
		}
		return "";
	}
	public void setQFrom(QFrom qFrom) {
		this.qFrom = qFrom;
	}
	public QType getQType() {
		return qType;
	}
	public String getQtypeStr() {
		if(qType != null) {
			qtypeStr = qType.getName();
		}
		return qtypeStr;
	}
	public String getQlevelStr() {
		if(qLevel != null) {
			qlevelStr = qLevel.getName();
		}
		return qlevelStr;
	}
	public void setQtypeStr(String qtypeStr) {
		this.qtypeStr = qtypeStr;
	}
	public void setQlevelStr(String qlevelStr) {
		this.qlevelStr = qlevelStr;
	}
	public void setQType(QType qType) {
		this.qType = qType;
	}
	public QLevel getQLevel() {
		return qLevel;
	}
	public void setQLevel(QLevel qLevel) {
		this.qLevel = qLevel;
	}
	public Integer getPostMinute() {
		return postMinute;
	}
	public void setPostMinute(Integer postMinute) {
		this.postMinute = postMinute;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	public String getKeyDesc() {
		return keyDesc;
	}
	public void setKeyDesc(String keyDesc) {
		this.keyDesc = keyDesc;
	}
	
	public void setQaRelas(List<QaRela> qaRelas) {
		this.relas = Lists.newArrayList();
		if( qaRelas != null && qaRelas.size() != 0) {
			for(QaRela qaRela: qaRelas) {
				this.relas.add(new TopicRela(qaRela));
			}
		}
	}
	
	/**
	 * 返回表单是否可以编辑  Json 的时候不序列化这个方法
	 * 是否可以编辑
	 * @return
	 */
	@JSONField(serialize=false)
	public Boolean getEditFlag() {
		//查询命题的表单
		if( IdGen.isInvalidId(this.getId())) {
			return Boolean.TRUE;
		}
		QaRelaType optionType = this.getOptionType();
		if( optionType == null) {//没有type
			return Boolean.TRUE;
		}
		if( optionType == QaRelaType.DEL) {//模糊删除模式
			optionType = this.getType();//使用当前状态删除,判断当前状态下是否有删除权限
		}
		if( optionType == QaRelaType.INIT ) {//查询命题的数据
			if(this.getType() == QaRelaType.INIT && UserUtils.getUser().getId().equals(this.getCreateId())) {
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		} else if( optionType == QaRelaType.INIT_VERITY ) {
			if(this.getType()== QaRelaType.INIT_VERITY 
					&& UserUtils.isPermitted(TaxConstants.TOPIC_AUDIT_INIT_VERITY)
					&& this.getTopicContent().getType() == QaRelaType.INIT_VERITY
					&& this.getTopicContent().checkCreate()) {
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		} else if( optionType == QaRelaType.FINALLY_VERITY ) {
			if(this.getType()== QaRelaType.FINALLY_VERITY 
					&& UserUtils.isPermitted(TaxConstants.TOPIC_AUDIT_FINALLY_VERITY)
					&& this.getTopicContent().getType() == QaRelaType.FINALLY_VERITY
					&& this.getTopicContent().checkCreate()) {
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		return Boolean.FALSE;
	}
	public String getVauditName() {
		return vauditName;
	}
	public void setVauditName(String vauditName) {
		this.vauditName = vauditName;
	}
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	public Date getVauditDate() {
		return vauditDate;
	}
	public void setVauditDate(Date vauditDate) {
		this.vauditDate = vauditDate;
	}
	public String getFvauditName() {
		return fvauditName;
	}
	public void setFvauditName(String fvauditName) {
		this.fvauditName = fvauditName;
	}
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	public Date getFvauditDate() {
		return fvauditDate;
	}
	public void setFvauditDate(Date fvauditDate) {
		this.fvauditDate = fvauditDate;
	}
}