package com.tmt.platform.qa.entity;

import java.util.Map;

import com.google.common.collect.Maps;
import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QaRelaType;

/**
 * 题目选择题的选项
 * @author lifeng
 */
public class TopicOption extends IdEntity<String>{
    
	private static final long serialVersionUID = -7537984737761943038L;
	
	private String topicId;
    private String contentId;
    private String salisa;
    private String soption;
    private QaRelaType type;
    private String delFlag;
    
    public TopicOption(){}
    public TopicOption(String salisa, String soption) {
    	this.salisa = salisa;
    	this.soption = soption;
    }
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getSalisa() {
		return salisa;
	}
	public void setSalisa(String salisa) {
		this.salisa = salisa;
	}
	public String getSoption() {
		return soption;
	}
	public void setSoption(String soption) {
		this.soption = soption;
	}
	public QaRelaType getType() {
		return type;
	}
	public void setType(QaRelaType type) {
		this.type = type;
	}
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public Map<String,Boolean> compareTo( TopicOption o ){
		Map<String,Boolean> result = Maps.newHashMap();
		if(o != null) {
			if( (StringUtil3.isNotBlank(this.getSalisa()) && StringUtil3.isNotBlank(o.getSalisa())
				   && this.getSalisa().equals(o.getSalisa()) )
				|| ( !StringUtil3.isNotBlank(this.getSalisa()) && !StringUtil3.isNotBlank(o.getSalisa()) )) {
				result.put("salisa", Boolean.TRUE);
			} else {
				result.put("salisa", Boolean.FALSE);
			}
			if( (StringUtil3.isNotBlank(this.getSoption()) && StringUtil3.isNotBlank(o.getSoption())
				   && this.getSoption().equals(o.getSoption()) )
				|| ( !StringUtil3.isNotBlank(this.getSoption()) && !StringUtil3.isNotBlank(o.getSoption()) )) {
				result.put("soption", Boolean.TRUE);
			} else {
				result.put("soption", Boolean.FALSE);
			}
		}
		if( !result.isEmpty()) {
			for(String key : result.keySet()) {
				if(result.get(key) != null && result.get(key) == Boolean.TRUE) {
					result.clear();result.put("options", Boolean.FALSE);
					break;
				}
			}
		} else {
			result.clear();
		}
		return result;
	}
}