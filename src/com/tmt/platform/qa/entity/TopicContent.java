package com.tmt.platform.qa.entity;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.Maps;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.BaseEntity;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.platform.enums.QLevel;
import com.tmt.platform.enums.QType;
import com.tmt.platform.enums.QaRelaType;

/**
 * 
 * 题目初审 ,终审的内容
 * @author lifeng
 */
public class TopicContent extends BaseEntity<String>{
   
	private static final long serialVersionUID = 7154584248259668106L;
	
	private String topicId;
    private QType qType;
    private QLevel qLevel;
    private Integer postMinute;
    private String content;
    private String skey;
    private String keyDesc;
    private QaRelaType type;
    private String businessTypeId;
    private String indutryId;
    
    private String businessTypeName;
    private String indutryName;
    
    public String getBusinessTypeId() {
		return businessTypeId;
	}
	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}
	public String getIndutryId() {
		return indutryId;
	}
	public void setIndutryId(String indutryId) {
		this.indutryId = indutryId;
	}
	public String getBusinessTypeName() {
		return businessTypeName;
	}
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}
	public String getIndutryName() {
		return indutryName;
	}
	public void setIndutryName(String indutryName) {
		this.indutryName = indutryName;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public QType getQType() {
		return qType;
	}
	public void setQType(QType qType) {
		this.qType = qType;
	}
	public QLevel getQLevel() {
		return qLevel;
	}
	public void setQLevel(QLevel qLevel) {
		this.qLevel = qLevel;
	}
	public Integer getPostMinute() {
		return postMinute;
	}
	public void setPostMinute(Integer postMinute) {
		this.postMinute = postMinute;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	public String getKeyDesc() {
		return keyDesc;
	}
	public void setKeyDesc(String keyDesc) {
		this.keyDesc = keyDesc;
	}
	public QaRelaType getType() {
		return type;
	}
	public void setType(QaRelaType type) {
		this.type = type;
	}
	
	public TopicContent update(Topic topic) {
		this.setContent(topic.getContent());
		this.setKeyDesc(topic.getKeyDesc());
		this.setPostMinute(topic.getPostMinute());
		this.setQLevel(topic.getQLevel());
		this.setQType(topic.getQType());
		this.setSkey(topic.getSkey());
		this.setTopicId(topic.getId());
		return this;
	}
	
	/**
	 * 创建相应状态下的内容
	 * @param topic
	 * @return
	 */
	public static TopicContent getByTopic(Topic topic) {
		TopicContent content = new TopicContent();
		content.setContent(topic.getContent());
		content.setKeyDesc(topic.getKeyDesc());
		content.setPostMinute(topic.getPostMinute());
		content.setQLevel(topic.getQLevel());
		content.setQType(topic.getQType());
		content.setSkey(topic.getSkey());
		content.setTopicId(topic.getId());
		content.setType(topic.getType());
		return content;
	}
	
	/**
	 * 验证创建人是否是自己
	 * @return
	 */
	public Boolean checkCreate(){
		if(this.getCreateId() == null
				|| this.getCreateId().equals(UserUtils.getUser().getId())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 新增时会自动设置创建人,重写此方法,不设置创建人
	 * 点击保存时,才会写创建人
	 * 审核人需要等到点击保存时才知道具体是谁
	 */
	@Override
	public String prePersist(Serializable key ) {
		String pk = super.prePersist(key);
		if( this.getType() != QaRelaType.INIT ) {
			this.createId = null;
			this.createName = null;
			this.createDate = null;
		}
		return pk;
	}
	
	//保存时设置审核人
	@Override
	public void preUpdate() {
		this.createId = UserUtils.getUser().getId();
		this.createName = UserUtils.getUser().getName();
		this.createDate = DateUtil3.getTimeStampNow();
	}
	
	/**
	 * 比较两个内容是否相等
	 * @param o
	 * @return
	 */
	public Map<String,Boolean> compareTo( TopicContent o ){
		Map<String,Boolean> result = Maps.newHashMap();
		if( o != null) {
			if((this.qType != null && o.getQType() != null && this.qType == o.getQType())
		       ||( this.qType == null && o.getQType() == null )) {
				result.put("qType", Boolean.TRUE);
			} else {
				result.put("qType", Boolean.FALSE);
			}
			
			if((this.qLevel != null && o.getQLevel() != null && this.qLevel == o.getQLevel())
		       ||( this.qLevel == null && o.getQLevel() == null )) {
				result.put("qLevel", Boolean.TRUE);
			} else {
				result.put("qLevel", Boolean.FALSE);
			}
			
			if((this.postMinute != null && o.getPostMinute() != null && this.postMinute.compareTo(o.getPostMinute()) == 0)
		       ||( this.postMinute == null && o.getPostMinute() == null )) {
				result.put("postMinute", Boolean.TRUE);
			} else {
				result.put("postMinute", Boolean.FALSE);
			}
			
			//内容比较
			if( (StringUtil3.isNotBlank(this.getContent()) && StringUtil3.isNotBlank(o.getContent())
				   && this.getContent().equals(o.getContent()) )
				|| ( !StringUtil3.isNotBlank(this.getContent()) && !StringUtil3.isNotBlank(o.getContent()) )) {
				result.put("content", Boolean.TRUE);
			} else {
				result.put("content", Boolean.FALSE);
			}
			//答案
			if( (StringUtil3.isNotBlank(this.getSkey()) && StringUtil3.isNotBlank(o.getSkey())
				   && this.getSkey().equals(o.getSkey()) )
				|| ( !StringUtil3.isNotBlank(this.getSkey()) && !StringUtil3.isNotBlank(o.getSkey()) )) {
				result.put("skey", Boolean.TRUE);
			} else {
				result.put("skey", Boolean.FALSE);
			}
			//解析
			if( (StringUtil3.isNotBlank(this.getKeyDesc()) && StringUtil3.isNotBlank(o.getKeyDesc())
				   && this.getKeyDesc().equals(o.getKeyDesc()) )
				|| ( !StringUtil3.isNotBlank(this.getKeyDesc()) && !StringUtil3.isNotBlank(o.getKeyDesc()) )) {
				result.put("keyDesc", Boolean.TRUE);
			} else {
				result.put("keyDesc", Boolean.FALSE);
			}
			//业务类型
			if( (StringUtil3.isNotBlank(this.getBusinessTypeId()) && StringUtil3.isNotBlank(o.getBusinessTypeId())
				   && this.getBusinessTypeId().equals(o.getBusinessTypeId()) )
				|| ( !StringUtil3.isNotBlank(this.getBusinessTypeId()) && !StringUtil3.isNotBlank(o.getBusinessTypeId()) )) {
				result.put("businessTypeId", Boolean.TRUE);
			} else {
				result.put("businessTypeId", Boolean.FALSE);
			}
			//行业
			if( (StringUtil3.isNotBlank(this.getIndutryId()) && StringUtil3.isNotBlank(o.getIndutryId())
				   && this.getIndutryId().equals(o.getIndutryId()) )
				|| ( !StringUtil3.isNotBlank(this.getIndutryId()) && !StringUtil3.isNotBlank(o.getIndutryId()) )) {
				result.put("indutryId", Boolean.TRUE);
			} else {
				result.put("indutryId", Boolean.FALSE);
			}
		}
		return result;
	}
}