package com.tmt.platform.qa.entity;


/**
 * 
 * 汇编 的税种关联
 * @author lifeng
 */
public class QaRela extends RelaEntity{
   
	private static final long serialVersionUID = 6904187568725518547L;
	private String qaId;
    
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryItemName() {
		return categoryItemName;
	}
	public void setCategoryItemName(String categoryItemName) {
		this.categoryItemName = categoryItemName;
	}
	public String getQaId() {
		return qaId;
	}
	public void setQaId(String qaId) {
		this.qaId = qaId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryItemId() {
		return categoryItemId;
	}
	public void setCategoryItemId(String categoryItemId) {
		this.categoryItemId = categoryItemId;
	}
	public String getLarDocNum() {
		return larDocNum;
	}
	public void setLarDocNum(String larDocNum) {
		this.larDocNum = larDocNum;
	}
	public String getLarDocName() {
		return larDocName;
	}
	public void setLarDocName(String larDocName) {
		this.larDocName = larDocName;
	}
	public String getLarDocContent() {
		return larDocContent;
	}
	public void setLarDocContent(String larDocContent) {
		this.larDocContent = larDocContent;
	}
	public String getApplyProvince() {
		return applyProvince;
	}
	public void setApplyProvince(String applyProvince) {
		this.applyProvince = applyProvince;
	}
	public String getApplyCity() {
		return applyCity;
	}
	public void setApplyCity(String applyCity) {
		this.applyCity = applyCity;
	}
}