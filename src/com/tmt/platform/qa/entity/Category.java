package com.tmt.platform.qa.entity;

import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 税种
 * @author lifeng
 */
public class Category extends IdEntity<String>{
   
	private static final long serialVersionUID = 2401081163528662984L;
	private String code;
    private String name;
    private String delFlag;
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public Boolean compareTo(Category old) {
		if( old == null ) {
			return Boolean.FALSE;
		}
		if(StringUtil3.isNotBlank(this.code) && StringUtil3.isNotBlank(old.getCode())
				&& this.code.equals(old.getCode())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * 税种名称是否包含在 content 中
	 * @param content
	 * @return
	 */
	public Boolean nameMatches(String content) {
		if(!(StringUtil3.isNotBlank(content) && StringUtil3.isNotBlank(this.name))) {
			return Boolean.FALSE;
		}
		int position = StringUtil3.indexOf(content, this.name);
		if( position != -1 && "增值税".equals(this.name) ) {
			return nameMatches(content,position);//循环查找
		} else if( position != -1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	protected Boolean nameMatches(String content, int startPos) {
		int position = StringUtil3.indexOf(content, this.name, startPos);
		if( position != -1 && "增值税".equals(this.name) && position - StringUtil3.length("土地") >= 2 
				&& !"土地".equals(StringUtil3.substring(content, position - StringUtil3.length("土地"),StringUtil3.length("土地")))) {
			return Boolean.TRUE;
		}
		if(!( startPos + StringUtil3.length(this.name) >= StringUtil3.length(content) + 1 )) {
			return nameMatches(content, startPos + StringUtil3.length(this.name));
		}
		return Boolean.FALSE;
	}

}