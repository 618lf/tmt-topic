package com.tmt.platform.qa.task.handler;

import com.tmt.platform.qa.entity.Qa;

/**
 * 列的值的处理器
 * @author lifeng
 */
public interface IColumnHandler {

	/**
	 * @param qa
	 * @param property
	 * @return
	 */
	public Qa handler(Qa qa);
}
