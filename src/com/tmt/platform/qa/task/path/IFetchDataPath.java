package com.tmt.platform.qa.task.path;

import java.util.Iterator;


/**
 * 获取信息的接口
 * @author lifeng
 */
public interface IFetchDataPath {

	/**
	 * 指定一个路径，可以是文件或文件夹返回 jSon 的迭代
	 * @param path
	 * @return
	 */
	public Iterator<String> iterateJsons();
}
