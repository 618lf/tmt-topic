package com.tmt.platform.qa.task.path;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.filefilter.IOFileFilter;

import com.tmt.base.common.utils.FileUtils;
import com.tmt.base.common.utils.RegexpUtil;
import com.tmt.base.common.utils.RegexpUtil.RegexpMatcher;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 获取本地的文件 默认的格式，支持正则表达式验证
 * @author lifeng
 */
public abstract class LocalRegFetchDataPath implements IFetchDataPath{

	//路劲匹配表达式
	protected static String PATH_REG = "^([^;]+)(;\\[reg='(.+)'\\]){0,1}$";
	private String fileSuffix = ".json";
	private String rootDir;//group 1
	private String dirReg;//group 2
	private RegexpMatcher matcher;
	
	public LocalRegFetchDataPath(){
		matcher = RegexpUtil.newRegexpMatcher();
	}
	
	/**
	 * 本地路径解析读取json文件,支持正则表达式查找
	 * 例如： D:/jsons;[reg='D:/jsons/国家税务.*']
	 *     只要配置的路径或自路径匹配reg表达式，都作为数据源搜索
	 */
	@Override
	public Iterator<String> iterateJsons() {
		matcher.setPattern(PATH_REG);
		String path = this.getLocalPath();
		Iterator<String> it = matcher.getGroups(path, RegexpUtil.asOptions("g")).iterator();
		if( it != null && it.hasNext()) {//0,1,2,3
			if(it.hasNext()) { it.next(); }
			if(it.hasNext()) { rootDir = it.next(); }//1
			if(it.hasNext()) { it.next(); }
			if(it.hasNext()) { dirReg = it.next(); }//3
		}
		if(StringUtil3.isNotBlank(dirReg)) {
			matcher.setPattern(dirReg);
		}
		File pathFile = new File(rootDir);
		List<File> files = new ArrayList<File>();
		if( pathFile.exists() && pathFile.isDirectory()) {//指定的目录
			files.addAll(this.listFile(pathFile));
		} else if(pathFile.exists()){
			files.add(pathFile);//指定的具体的文件
		}
		List<String> jSons = new ArrayList<String>();
		try{
			if( files != null && files.size() != 0 ) {
				for( File file: files) {
					String jSon = FileUtils.readFileToString(file);
					jSons.add(jSon);
				}
			}
		}catch(Exception e){e.printStackTrace();}
		return jSons.iterator();
	}
	
	/**
	 * 可以工子类实现,取文件的方式
	 * @param pathFile
	 * @return
	 */
	protected Collection<File> listFile(File pathFile) {
		return FileUtils.listFiles(pathFile, new IOFileFilter(){
			@Override
			public boolean accept(File pathname) {
				if(StringUtil3.endsWith(pathname.getName(), getFileSuffix())) {
					return Boolean.TRUE;
				}
				return Boolean.FALSE;
			}

			@Override
			public boolean accept(File dir, String name) {
				return Boolean.FALSE;
			}
		}, new IOFileFilter(){
			@Override
			public boolean accept(File pathname) {
				if(StringUtil3.isNotBlank(dirReg)) {//全字符串匹配
					return Pattern.matches(dirReg,pathname.getAbsolutePath());
				}
				return Boolean.TRUE;
			}

			@Override
			public boolean accept(File dir, String name) {
				return Boolean.FALSE;
			}
		});
	}
	public String getFileSuffix() {
		return fileSuffix;
	}

	/**
	 * 子类重写
	 * @return
	 */
	public abstract String getLocalPath();
}
