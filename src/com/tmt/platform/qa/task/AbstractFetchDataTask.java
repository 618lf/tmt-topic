package com.tmt.platform.qa.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tmt.base.common.service.TaskExecutor;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.service.QaService;
import com.tmt.platform.qa.task.parser.IParser;
import com.tmt.platform.qa.task.path.IFetchDataPath;

/**
 * 答疑汇编抽取数据 -- 只处理json格式的数据
 * @author lifeng
 */
public abstract class AbstractFetchDataTask implements TaskExecutor, IFetchDataPath{
	
	protected static Logger logger = LoggerFactory.getLogger(AbstractFetchDataTask.class);
	
	@Autowired
	private QaService qaService;
	protected IFetchDataPath fetchDataPath;
	protected IParser parser;
	
	@Override
	public Boolean doTask() {
		try{
			logger.info(StringUtil3.format("[%s]:%s", this.getClass().getName()),"start...");
			Iterator<String> jSons = this.iterateJsons();
			if(!jSons.hasNext()) {
				return Boolean.TRUE;
			}
			List<Qa> qas = new ArrayList<Qa>();
			while(jSons.hasNext()) {
				Qa qa = this.parse(jSons.next());
				qa.setQFrom(QFrom.QA);
				qas.add(qa);
				logger.info(StringUtil3.format("%s:title=%s", "FetchData", qa.getTitle()));
			}
			//保存到数据库
			qaService.save(qas);
			logger.info(StringUtil3.format("[%s]:%s", this.getClass().getName()),"end...");
		}catch(Exception e){e.printStackTrace();}
		return Boolean.TRUE;
	}

	@Override
	public Iterator<String> iterateJsons() {
		if( this.fetchDataPath == null) {
			this.fetchDataPath = getFetchDataPath();
		}
		return this.fetchDataPath.iterateJsons();
	}
	
	public Qa parse(String jSon) {
		if( this.parser == null) {
			this.parser = this.getParser();
		}
		return this.parser.parse(jSon);
	}
	
	/**
	 * 子类可以重写，如果有其他的需要
	 * 取具体的路径
	 * @return
	 */
	public abstract IFetchDataPath getFetchDataPath();
	
	/**
	 * 子类可以重写，如果有其他的需要
	 * 可以自定义，并注册字段解析的方法
	 * @return
	 */
	public abstract IParser getParser();
}
