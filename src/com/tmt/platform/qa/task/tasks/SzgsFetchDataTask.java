package com.tmt.platform.qa.task.tasks;

import org.springframework.stereotype.Component;

import com.tmt.platform.qa.task.DefaultFetchDataTask;
import com.tmt.platform.utils.TaxConstants;

/**
 * 深圳地税抽取数据任务
 * @author lifeng
 */
@Component("SzgsFetchDataTask")
public class SzgsFetchDataTask extends DefaultFetchDataTask{

	public SzgsFetchDataTask() {
		super(TaxConstants.QA_FETCH_DATA_PATH_CODE_SZGS);
	}
}
