package com.tmt.platform.qa.task.tasks;

import org.springframework.stereotype.Component;

import com.tmt.platform.qa.task.DefaultFetchDataTask;
import com.tmt.platform.utils.TaxConstants;

/**
 * 深圳地税抽取数据任务
 * @author lifeng
 */
@Component("SzdsFetchDataTask")
public class SzdsFetchDataTask extends DefaultFetchDataTask{

	public SzdsFetchDataTask() {
		super(TaxConstants.QA_FETCH_DATA_PATH_CODE_SZDS);
	}
}
