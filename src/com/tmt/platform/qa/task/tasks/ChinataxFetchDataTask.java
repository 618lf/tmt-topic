package com.tmt.platform.qa.task.tasks;

import java.util.Iterator;
import java.util.Vector;

import org.springframework.stereotype.Component;

import com.tmt.base.common.utils.RegexpUtil;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.utils.RegexpUtil.RegexpMatcher;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.task.DefaultFetchDataTask;
import com.tmt.platform.utils.TaxConstants;

/**
 * 国家税务总局获取数据任务
 * @author lifeng
 */
@Component("ChinataxFetchDataTask")
public class ChinataxFetchDataTask extends DefaultFetchDataTask{

	public ChinataxFetchDataTask() {
		super(TaxConstants.QA_FETCH_DATA_PATH_CODE_CT);
	}

	@Override
	public Qa dealWithContent(Qa qa) {
		RegexpMatcher matcher = RegexpUtil.newRegexpMatcher();
		String content = qa.getContent();
		matcher.setPattern("^问：(.*)答：(.*)$");
		Vector<String> contents = matcher.getGroups(content, RegexpUtil.asOptions("g"));
		if( contents == null) {
			qa.setContent(qa.getTitle());
			qa.setAnswer(StringUtil3.removeStart(qa.getAnswer(), "答："));
		} else {
			Iterator<String> it = contents.iterator();
			if(it != null) {
				if(it.hasNext()) {it.next();}
				if(it.hasNext()) {qa.setContent(it.next());}
				if(it.hasNext()) {qa.setAnswer(it.next());}
			}
		}
		return super.dealWithContent(qa);
	}

	@Override
	public Qa dealWithPublishDate(Qa qa) {
		RegexpMatcher matcher = RegexpUtil.newRegexpMatcher();
		//发布日期的解析
		String publishDate = qa.getPublishOffice();
		matcher.setPattern("^发布日期：(.*)来源：(.*)$");
		Vector<String> contents = matcher.getGroups(publishDate, RegexpUtil.asOptions("g"));
		if( contents == null) {
			qa.setPublishDate(StringUtil3.removeStart(publishDate,"发布日期："));
		} else {
			Iterator<String> it = contents.iterator();
			if(it != null) {
				if(it.hasNext()) {it.next();}
				if(it.hasNext()) {qa.setPublishDate(it.next());}
				if(it.hasNext()) {qa.setPublishOffice(it.next());}
			}
		}
		if( StringUtil3.isNotBlank(qa.getPublishDate())) {
			String value = StringUtil3.replace(qa.getPublishDate(), "年", "-");
			       value = StringUtil3.replace(value, "月", "-");
			       value = StringUtil3.replace(value, "日", "");
			qa.setPublishDate(value);
		}
		qa.setPublishOffice("国家税务总局纳税服务司");
		return super.dealWithPublishDate(qa);
	}
	
}
