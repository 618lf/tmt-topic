package com.tmt.platform.qa.task.tasks;

import java.util.Iterator;
import java.util.Vector;

import org.springframework.stereotype.Component;

import com.tmt.base.common.utils.RegexpUtil;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.utils.RegexpUtil.RegexpMatcher;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.task.DefaultFetchDataTask;
import com.tmt.platform.utils.TaxConstants;

/**
 * 广东省国家税务局
 * @author lifeng
 */
@Component("GdgsFetchDataTask")
public class GdgsFetchDataTask extends DefaultFetchDataTask{

	public GdgsFetchDataTask() {
		super(TaxConstants.QA_FETCH_DATA_PATH_CODE_GDGS);
	}
	
	@Override
	public Qa dealWithContent(Qa qa) {
		RegexpMatcher matcher = RegexpUtil.newRegexpMatcher();
		String content = qa.getContent();
		if( StringUtil3.isNotBlank(content)) {
			matcher.setPattern("^问：(.*)答：(.*)$");
			Vector<String> contents = matcher.getGroups(content, RegexpUtil.asOptions("g"));
			if( contents == null) {
				qa.setContent(qa.getTitle());
				qa.setAnswer(StringUtil3.removeStart(qa.getAnswer(), "答："));
			} else {
				Iterator<String> it = contents.iterator();
				if(it != null) {
					if(it.hasNext()) {it.next();}
					if(it.hasNext()) {qa.setContent(it.next());}
					if(it.hasNext()) {qa.setAnswer(it.next());}
				}
			}
		}
		return super.dealWithContent(qa);
	}

	@Override
	public Qa dealWithPublishDate(Qa qa) {
		return super.dealWithPublishDate(qa);
	}

	@Override
	public Qa dealWithPublishOffice(Qa qa) {
		qa.setPublishOffice("广东省国家税务局");
		return super.dealWithPublishOffice(qa);
	}
	
}
