package com.tmt.platform.qa.task.tasks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.service.TaskExecutor;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.TaxDoc;
import com.tmt.platform.qa.service.TaxDocService;
import com.tmt.platform.qa.task.handler.IColumnHandler;
import com.tmt.platform.qa.task.parser.DefaultParser;
import com.tmt.platform.qa.task.parser.IParser;
import com.tmt.platform.qa.task.path.IFetchDataPath;
import com.tmt.platform.qa.task.path.LocalRegFetchDataPath;
import com.tmt.platform.utils.TaxConstants;

/**
 * 税收法规
 * @author lifeng
 */
@Component("taxDocFetchData")
public class TaxDocFetchData implements TaskExecutor, IFetchDataPath{

	protected static Logger logger = LoggerFactory.getLogger(TaxDocFetchData.class);
	
	protected IFetchDataPath fetchDataPath;
	protected IParser parser;
	@Autowired
	private TaxDocService taxDocService;
	
	@Override
	public Iterator<String> iterateJsons() {
		if( this.fetchDataPath == null) {
			this.fetchDataPath = getFetchDataPath();
		}
		return this.fetchDataPath.iterateJsons();
	}

	@Override
	public Boolean doTask() {
		try{
			logger.info(StringUtil3.format("[%s]:%s", this.getClass().getName()),"start...");
			Iterator<String> jSons = this.iterateJsons();
			if(!jSons.hasNext()) {
				return Boolean.TRUE;
			}
			List<Qa> qas = new ArrayList<Qa>();
			while(jSons.hasNext()) {
				Qa qa = this.parse(jSons.next());
				qas.add(qa);
				logger.info(StringUtil3.format("%s:title=%s", "FetchData", qa.getTitle()));
			}
			List<TaxDoc> docs = this.toDoc(qas);
			this.taxDocService.batchSave(docs);
			logger.info(StringUtil3.format("[%s]:%s", this.getClass().getName()),"end...");
		}catch(Exception e){e.printStackTrace();}
		return Boolean.TRUE;
	}
	
	public Qa parse(String jSon) {
		if( this.parser == null) {
			this.parser = this.getParser();
		}
		return this.parser.parse(jSon);
	}
	
	/**
	 * 子类可以重写，如果有其他的需要
	 * 取具体的路径
	 * @return
	 */
	public IFetchDataPath getFetchDataPath() {
		return new LocalRegFetchDataPath(){
			private String path;
			@Override
			public String getLocalPath() {
				DictService dictService = SpringContextHolder.getBean(DictService.class);
				Dict dict = dictService.getDictByCode(TaxConstants.DOC_FETCH_DATA_PATH_CODE);
				if( dict != null) {
					path = dict.getRemarks();//存储在描述中
				}
				return path;
			}
		};
	}
	
	/**
	 * 子类可以重写，如果有其他的需要
	 * 可以自定义，并注册字段解析的方法
	 * @return
	 */
	public IParser getParser() {
		IParser parser = new DefaultParser();
		parser.register(new IColumnHandler(){
			@Override
			public Qa handler(Qa qa) {
				try{
					return TaxDocFetchData.this.dealQa(qa);
				}catch(Exception e){
					throw new BaseRuntimeException(qa.toString(),e);
				}
			}
		});
		return parser;
	}
	
	/**
	 * 处理
	 * @param qa
	 * @return
	 */
	private Qa dealQa(Qa qa) {
		String sourceUrl = qa.getSourceUrl();
		sourceUrl = sourceUrl.replaceAll("\\.\\./", "");
		sourceUrl = sourceUrl.replaceAll("\\./", "");
		sourceUrl = sourceUrl.replaceAll("&amp;", "&");
		qa.setSourceUrl(sourceUrl);
		return qa;
	}
	
	private List<TaxDoc> toDoc(List<Qa> qas) {
		List<TaxDoc> docs = Lists.newArrayList();
		for(Qa qa: qas) {
			TaxDoc doc = new TaxDoc();
			doc.setDocName(qa.getTitle());
			doc.setDocNum(qa.getPublishCode());
			if(StringUtil3.isNotBlank(qa.getPublishDate())) {
			   doc.setPubDate(DateUtil3.getFormatDate(qa.getPublishDate(), "yyyy-MM-dd"));
			}
			doc.setPubOffice("国家税务总局");
			doc.setSourceUrl(qa.getSourceUrl());
			doc.setDocType(qa.getKeyWords());
			docs.add(doc);
		}
		return docs;
	}
}
