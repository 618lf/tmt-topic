package com.tmt.platform.qa.task;

import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;
import com.tmt.platform.qa.task.path.IFetchDataPath;
import com.tmt.platform.qa.task.path.LocalRegFetchDataPath;

public class DefaulTxtFetchDataTask extends DefaultFetchDataTask{

	public DefaulTxtFetchDataTask(String path) {
		super(path);
	}

	@Override
	public IFetchDataPath getFetchDataPath() {
		return new LocalRegFetchDataPath(){
			
			private String path;
			
			@Override
			public String getLocalPath() {
				DictService dictService = SpringContextHolder.getBean(DictService.class);
				Dict dict = dictService.getDictByCode(DefaulTxtFetchDataTask.this.path);
				if( dict != null) {
					path = dict.getRemarks();//存储在描述中
				}
				return path;
			}
			
			@Override
			public String getFileSuffix() {
				return ".txt";
			}
		};
	}
}
