package com.tmt.platform.qa.task.parser;

import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.task.handler.IColumnHandler;

/**
 * 解析jSon
 * @author lifeng
 */
public interface IParser {

	/**
	 * 将jSon解析成Qa对象
	 * @param jSon
	 * @return
	 */
	public Qa parse(String jSon);
	
	/**
	 * 注册列解析器
	 * @param handler
	 */
	public void register(IColumnHandler handler);
}
