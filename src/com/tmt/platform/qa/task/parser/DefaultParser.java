package com.tmt.platform.qa.task.parser;

import java.util.ArrayList;
import java.util.List;

import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.utils.JsonMapper;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.task.handler.IColumnHandler;

/**
 * 默认的解析器
 * @author lifeng
 */
public class DefaultParser implements IParser{

	//处理器
	private List<IColumnHandler> handlers = new ArrayList<IColumnHandler>();
	
	@Override
	public Qa parse(String jSon) {
		try{
			Qa qa = JsonMapper.fromJson(this.trimJSon(jSon), Qa.class);
			for(IColumnHandler handler: handlers) {
				qa = handler.handler(qa);
			}
			return qa;
		}catch(Exception e){
			throw new BaseRuntimeException("格式化JSON失败:" +jSon , e);
		}
	}
	
	public void register(IColumnHandler handler) {
		handlers.add(handler);
	}
	
	//去掉所有的空白，换行等字符默认,普通的html标签
	public String trimJSon( String jSon) {
		jSon = jSon.replace((char)12288, ' ');
		jSon = StringUtil3.remove(jSon, "\\n");
		jSon = StringUtil3.remove(jSon, "\\t");
		jSon = StringUtil3.remove(jSon, " ");
		jSon = StringUtil3.remove(jSon, "&nbsp;");
		jSon = StringUtil3.replaceHtml(jSon);
		jSon = StringUtil3.trim(jSon);
		return jSon;
	}
}
