package com.tmt.platform.qa.task;

import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.task.handler.IColumnHandler;
import com.tmt.platform.qa.task.parser.DefaultParser;
import com.tmt.platform.qa.task.parser.IParser;
import com.tmt.platform.qa.task.path.IFetchDataPath;
import com.tmt.platform.qa.task.path.LocalRegFetchDataPath;

/**
 * 默认的实现
 * @author lifeng
 *
 */
public class DefaultFetchDataTask extends AbstractFetchDataTask{

	//获取数据路劲
	protected String path;
	
	public DefaultFetchDataTask(String path){
		this.path = path;
	}
	/**
	 * 本地路径解析读取json文件,支持正则表达式查找
	 * 例如： D:/jsons;[reg='D:/jsons/国家税务.*']
	 *     只要配置的路径或自路径匹配reg表达式，都作为数据源搜索
	 */
	@Override
	public IFetchDataPath getFetchDataPath() {
		return new LocalRegFetchDataPath(){
			private String path;
			@Override
			public String getLocalPath() {
				DictService dictService = SpringContextHolder.getBean(DictService.class);
				Dict dict = dictService.getDictByCode(DefaultFetchDataTask.this.path);
				if( dict != null) {
					path = dict.getRemarks();//存储在描述中
				}
				return path;
			}
		};
	}

	/**
	 * 如何解析json文件
	 */
	@Override
	public IParser getParser() {
		IParser parser = new DefaultParser();
		parser.register(new IColumnHandler(){
			@Override
			public Qa handler(Qa qa) {
				try{
					return DefaultFetchDataTask.this.dealQa(qa);
				}catch(Exception e){
					throw new BaseRuntimeException(qa.toString(),e);
				}
			}
		});
		return parser;
	}
	
	//处理相关的信息
	private Qa dealQa(Qa qa) {
		this.dealWithTitle(qa);
		this.dealWithContent(qa);
		this.dealWithAnswer(qa);
		this.dealWithPublishDate(qa);
		this.dealWithPublishOffice(qa);
		return qa;
	}
	
	/**
	 * 处理内容
	 * @param qa
	 * @return
	 */
	public Qa dealWithTitle(Qa qa) {
		return qa;
	}
	/**
	 * 处理内容
	 * @param qa
	 * @return
	 */
	public Qa dealWithContent(Qa qa) {
		return qa;
	}
	/**
	 * 处理答案 -- 格式化,
	 * 在 规定：后换行
	 * 在  一、前加入  换行
	 * 在（一）前加入 换行制表
	 * @param qa
	 * @return
	 */
	public Qa dealWithAnswer(Qa qa) {
		String answer = qa.getAnswer();
		if( StringUtil3.isNotBlank(answer) ) {
			StringUtil3.replaceChars(answer, "规定：", "规定：\\n\\t");
		}
		return qa;
	}
	/**
	 * 处理发布日期
	 * @param qa
	 * @return
	 */
	public Qa dealWithPublishDate(Qa qa) {
		return qa;
	}
	/**
	 * 处理发布机关
	 * @param qa
	 * @return
	 */
	public Qa dealWithPublishOffice(Qa qa) {
		return qa;
	}
}

