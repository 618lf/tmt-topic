package com.tmt.platform.qa.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.entity.ExcelTemplate;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.qa.entity.PubOffice;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;
import com.tmt.platform.qa.utils.Qas;
import com.tmt.platform.qa.utils.Relas;

/**
 * 答疑汇编
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/platform/qa")
public class QaController extends BaseFromController {

	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = {"initList"})
	public String initList(Qa qa, Model model, HttpServletResponse response){
		List<ExcelTemplate> templates = this.templateService.queryByTargetClass(Qa.class.getName());
		model.addAttribute("templates", templates);
		return "/taxPlatform/QaList";
	}
	
	/**
	 * 初始化页面的数据  -- 根据权限过滤数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Qa qa, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(qa, qc);
		         c = initQaFilterQc(c);//添加按权限过滤数据
		if(StringUtil3.isNotBlank(qa.getMakeQFlag())) {
			c.andEqualTo("(CASE WHEN MAKE_Q_FLAG IS NULL  THEN '0' ELSE MAKE_Q_FLAG END)", "MAKE_Q_FLAG", qa.getMakeQFlag());
		}
		if(StringUtil3.isNotBlank(qa.getStorageFlag())) {
			c.andEqualTo("(CASE WHEN STORAGE_FLAG IS NULL THEN '-1' ELSE STORAGE_FLAG END)", "STORAGE_FLAG", qa.getStorageFlag());
		}
		c.andEqualTo("Q_FROM", "Q_FROM", qa.getQFrom());
		qc.setOrderByClause( param.orderBy(Qa.class,"QA.PUBLISH_DATE DESC") );
		page = qaService.queryForPage(qc, param);
		return this.dealPageInfo(page);
	}
	
	/**
	 * 答疑表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(Qa qa, String reload, Model model){
		if(qa != null && !IdGen.isInvalidId(qa.getId())) {
		   qa = this.qaService.getWithOthers(qa.getId());
		   //如果没有关联税种信息同时是否入库设置的否或未筛选
		   this.initQaRela(qa);
		   //答案格式化
		   qa = Qas.answerFormate(qa);
		} else {
		   if(qa == null) {
			   qa = new Qa();
		   }
		   qa.setId(IdGen.INVALID_ID);
		   qa.setQFrom(QFrom.QA);
		}
		model.addAttribute("qa", qa);
		model.addAttribute("reload",reload);
		return "/taxPlatform/QaForm";
	}
	
	/**
	 * 答疑修改
	 * @param indutry
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(Qa qa, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, qa) ){
			return form(qa, null, model);
		}
		this.qaService.save(qa); qa = this.qaService.get(qa.getId());
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改答疑", qa.getTitle(), "成功"));
		redirectAttributes.addAttribute("id", qa.getId());
		redirectAttributes.addAttribute("reload", Boolean.TRUE);//刷新列表
		return "redirect:"+Globals.getAdminPath()+"/platform/qa/form";
	}
	
	/**
	 * 删除
	 * @param idList
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList, Model model) {
		if( idList != null && idList.length != 0) {
			List<Qa> qas = Lists.newArrayList();
			for( String id: idList) {
				Qa qa = this.qaService.get(id);
				if( !this.qaService.checkDelete(qa) ) {
					return AjaxResult.error("选择的答疑中包含：未筛选，已入库的答疑,请重新选择");
				}
				qas.add(qa);
			}
			this.qaService.batchDelete(qas);
		}
		return AjaxResult.success();
	}
	
	/**
	 * 待编制列表(答疑)
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initTopicMake"})
	public String initTopicMake(Qa qa, Model model){
		return "/taxPlatform/QaMakeList";
	}
	
	/**
	 * 我的待编制列表  -- 只有入了库的并且设置了编题的才显示出来
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonTopicMakeList"})
	public Page jSonTopicMakeList(Qa qa, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(qa, qc);
		c = this.initQaMakeFilterQc(c);
		//按是否命题过滤数据 -- 只要入库的都显示出来
		//c.andEqualTo("(CASE WHEN MAKE_Q_FLAG = 1 THEN 1 ELSE 0 END)", "MAKE_Q_FLAG", Qa.YES);//确定命题
		c.andEqualTo("(CASE WHEN STORAGE_FLAG IS NULL THEN '-1' ELSE STORAGE_FLAG END)", "STORAGE_FLAG", Qa.YES);//确定入库
		qc.setOrderByClause("(CASE WHEN MAKE_Q_FLAG = 1 THEN 1 ELSE 0 END)");
		page = qaService.queryInitTopicList(qc, param);
		return this.dealPageInfo(page);
	}
	
	/**
	 * 发布机关
	 * @param name
	 * @param model
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonPublishOffice"})
	public Page jSonPublishOffice(String name, Model model, Page page) {
		PageParameters param = page.getParam();
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		         c = this.initOfficeFilterQc(c);
		if(StringUtil3.isNotBlank(name)) {
			c.andLike("NAME", "name", name);
		}
		page = this.pubOfficeService.queryForPage(qc, param);
		if(page != null && page.getData() != null && page.getData().size() != 0 ) {
			List<PubOffice> offices = page.getData();
			for(PubOffice office: offices) {
				office.setId(office.getName());
			}
		}
		return page;
	}
	
	//处理page中的内容
	private Page dealPageInfo(Page page){ 
		//答案和回答不必显示全部
		if( page != null && page.getData() != null && page.getData().size() != 0) {
			List<Qa> qas = page.getData();
			for( Qa qaTemp: qas ) {
				qaTemp.setTitle(StringUtil3.insertEach(qaTemp.getTitle(), 18, "\n"));
				qaTemp.setContent(null);
				qaTemp.setAnswer(null);
				//设置税种和税明细
				List<QaRela> qaRelas = this.qaRelaService.queryByQaId(qaTemp.getId());
				if(qaRelas != null) {
					qaTemp.setCategorys(Relas.getCategorys(qaRelas));
					qaTemp.setCategoryItems(Relas.getCategoryItems(qaRelas));
				}
			}
		}
		return page;
	}
}
