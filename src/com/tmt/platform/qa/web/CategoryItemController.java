package com.tmt.platform.qa.web;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.entity.ExcelTemplate;
import com.tmt.base.component.excel.config.service.ExcelTemplateService;
import com.tmt.base.component.excel.util.TemplateExcelUtil;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.web.BaseImpController;
import com.tmt.platform.qa.entity.CategoryItem;
import com.tmt.platform.qa.service.CategoryItemService;

@Controller
@RequestMapping(value = "${adminPath}/platform/categoryItem")
public class CategoryItemController extends BaseImpController{

	@Autowired
	private CategoryItemService categoryItemService;
	@Autowired
	private ExcelTemplateService templateService;
	
	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(CategoryItem categoryItem, Model model){
		List<ExcelTemplate> templates = this.templateService.queryByTargetClass(CategoryItem.class.getName());
		model.addAttribute("templates", templates);
		return "/taxPlatform/CategoryItemList";
	}
	
	/**
	 * 初始化页面的数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(CategoryItem categoryItem, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(categoryItem != null && StringUtil3.isNotBlank(categoryItem.getName())) {
		   c.andLike("GI.NAME", "NAME", categoryItem.getName());
		}
		if( categoryItem != null && StringUtil3.isNotBlank(categoryItem.getCategoryId())) {
			c.andEqualTo("GI.CATEGORY_ID", "CATEGORY_ID", categoryItem.getCategoryId());
		}
		page = categoryItemService.queryForPage(qc, param);
		return page;
	}
	
	/**
	 * 问题表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(CategoryItem categoryItem, Model model){
		if(categoryItem != null && !IdGen.isInvalidId(categoryItem.getId())) {
			categoryItem = this.categoryItemService.get(categoryItem.getId());
		} else {
		   if(categoryItem == null) {
			   categoryItem = new CategoryItem();
		   }
		   categoryItem.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("categoryItem", categoryItem);
		return "/taxPlatform/CategoryItemForm";
	}
	
	/**
	 * 业务类型保存
	 * @param CategoryItem
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(CategoryItem categoryItem, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, categoryItem) ){
			return form(categoryItem, model);
		}
		this.categoryItemService.save(categoryItem);
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改税种明细", categoryItem.getName(), "成功"));
		redirectAttributes.addAttribute("id", categoryItem.getId());
		return "redirect:"+Globals.getAdminPath()+"/platform/categoryItem/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		List<CategoryItem> cateoryItems = Lists.newArrayList();
		Boolean bFlag = Boolean.TRUE;
		for( String id:idList ) {
			CategoryItem categoryItem = new CategoryItem();
			categoryItem.setId(id);
			int iCount = this.categoryItemService.checkedDelete(categoryItem);
			if( iCount > 0) {
				bFlag = Boolean.FALSE;break;
			}
			cateoryItems.add(categoryItem);
		}
		if(!bFlag) {
			return AjaxResult.error("答疑汇编等关联了此税种明细,不能删除");
		}
		this.categoryItemService.batchDelete(cateoryItems);
		return AjaxResult.success();
	}
	
	/**
	 * 其余的部分由父类完成
	 */
	public AjaxResult doImport(String templateId, MultipartFile file) {
		AjaxResult result = TemplateExcelUtil.fetchObjectFromTemplate(templateId, file);
		if( result != null && result.getSuccess()) {
			List<CategoryItem>  items = result.getObj();
			this.categoryItemService.batchImport(items);
		}
		return AjaxResult.success();
	}
}
