package com.tmt.platform.qa.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.TopicRela;
import com.tmt.platform.qa.service.TopicRelaService;

/**
 * 试题税种关联
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/platform/topicRela")
public class TopicRelaController extends BaseController{

	@Autowired
	private TopicRelaService topicRelaService;
	
	/**
	 * 试题税种关联
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(TopicRela topicRela, Model model){
		if(topicRela != null && !IdGen.isInvalidId(topicRela.getId())) {
			topicRela = this.topicRelaService.get(topicRela.getId());
		} else {
		   if(topicRela == null) {
			   topicRela = new TopicRela();
		   }
		   topicRela.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("topicRela", topicRela);
		return "/taxPlatform/TopicRelaForm";
	}
	
	/**
	 * 试题税种关联 - 修改(实际上没保存，只是数据的校验和转换)
	 * @param indutry
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"save"})
	public AjaxResult save(TopicRela topicRela, Model model) {
		if ( !beanValidator(model, topicRela) ){
			return AjaxResult.error("数据验证不通过");
		}
		topicRela = this.topicRelaService.fillCategory(topicRela);
		if( topicRela != null ) {
			topicRela.setApplyProvince( Qa.YES.equals(topicRela.getApplyProvince())?"是":"否");
		}
		AjaxResult result = AjaxResult.success();
		result.setObj(topicRela);
		return result;
	}
}
