package com.tmt.platform.qa.web;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;
import com.tmt.platform.qa.service.QaRelaService;

@Controller
@RequestMapping(value = "${adminPath}/platform/qaRela")
public class QaRelaController extends BaseController{

	@Autowired
	private QaRelaService qaRelaService;
	
	/**
	 * 问题表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(QaRela qaRela, Model model){
		if(qaRela != null && !IdGen.isInvalidId(qaRela.getId())) {
			qaRela = this.qaRelaService.get(qaRela.getId());
		} else {
		   if(qaRela == null) {
			   qaRela = new QaRela();
		   }
		   qaRela.setId(IdGen.INVALID_ID);
		   qaRela.setApplyProvince(Qa.YES);
		}
		model.addAttribute("qaRela", qaRela);
		return "/taxPlatform/QaRelaForm";
	}
	
	/**
	 * 答疑修改
	 * @param indutry
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"save"})
	public AjaxResult save(QaRela qaRela, Model model) {
		if ( !beanValidator(model, qaRela) ){
			return AjaxResult.error("数据验证不通过");
		}
		this.qaRelaService.save(qaRela);
		qaRela = this.qaRelaService.get(qaRela.getId());
		if( qaRela != null ) {
			qaRela.setApplyProvince( Qa.YES.equals(qaRela.getApplyProvince())?"是":"否");
		}
		AjaxResult result = AjaxResult.success();
		result.setObj(qaRela);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		List<QaRela> cateorys = Lists.newArrayList();
		for( String id:idList ) {
			QaRela qaRela = new QaRela();
			qaRela.setId(id);
			cateorys.add(qaRela);
		}
		this.qaRelaService.batchDelete(cateorys);
		return AjaxResult.success();
	}
	
}
