package com.tmt.platform.qa.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.entity.ExcelTemplate;
import com.tmt.base.component.excel.config.service.ExcelTemplateService;
import com.tmt.base.component.excel.util.TemplateExcelUtil;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.web.BaseImpController;
import com.tmt.platform.qa.entity.BusinessType;
import com.tmt.platform.qa.service.BusinessTypeService;

@Controller
@RequestMapping(value = "${adminPath}/platform/businessType")
public class BusinessTypeController extends BaseImpController{

	@Autowired
	private BusinessTypeService businessTypeService;
	@Autowired
	private ExcelTemplateService templateService;
	
	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(BusinessType businessType, Model model){
		List<ExcelTemplate> templates = this.templateService.queryByTargetClass(BusinessType.class.getName());
		model.addAttribute("templates", templates);
		return "/taxPlatform/BusinessTypeList";
	}
	
	/**
	 * 初始化页面的数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(BusinessType businessType, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(businessType != null && StringUtil3.isNotBlank(businessType.getName())) {
			c.andLike("NAME", "NAME", businessType.getName());
		}
		page = this.businessTypeService.queryForPage(qc, param);
		return page;
	}
	
	/**
	 * 业务类型表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(BusinessType businessType, Model model){
		if(businessType != null && !IdGen.isInvalidId(businessType.getId())) {
			businessType = this.businessTypeService.get(businessType.getId());
		} else {
		   if(businessType == null) {
			   businessType = new BusinessType();
		   }
		   businessType.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("businessType", businessType);
		return "/taxPlatform/BusinessTypeForm";
	}
	
	/**
	 * 业务类型保存
	 * @param businessType
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(BusinessType businessType, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, businessType) ){
			return form(businessType, model);
		}
		this.businessTypeService.save(businessType);
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改业务类型", businessType.getName(), "成功"));
		redirectAttributes.addAttribute("id", businessType.getId());
		return "redirect:"+Globals.getAdminPath()+"/platform/businessType/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		List<BusinessType> businessTypes = Lists.newArrayList();
		Boolean bFlag = Boolean.TRUE;
		for( String id:idList ) {
			BusinessType businessType = new BusinessType();
			businessType.setId(id);
			int iCount = this.businessTypeService.checkedDelete(businessType);
			if( iCount > 0) {
				bFlag = Boolean.FALSE;break;
			}
			businessTypes.add(businessType);
		}
		if(!bFlag) {
			return AjaxResult.error("答疑汇编等关联了此业务类型,不能删除");
		}
		this.businessTypeService.batchDelete(businessTypes);
		return AjaxResult.success();
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<BusinessType> businessTypes = this.businessTypeService.findAll();
		for (int i=0; i< businessTypes.size(); i++){
			BusinessType e = businessTypes.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", "O_-1");
			map.put("name", e.getName());
			mapList.add(map);
		}
		Map<String, Object> map = Maps.newHashMap();
		map.put("id", "O_-1");
		map.put("pId", "O_-2");
		map.put("name", "业务类型");
		mapList.add(map);
		return mapList;
	}
	
	/**
	 * 其余部分由父类完成
	 */
	public AjaxResult doImport(String templateId, MultipartFile file) {
		AjaxResult result = TemplateExcelUtil.fetchObjectFromTemplate(templateId, file);
		if( result != null && result.getSuccess()) {
			List<BusinessType> items = result.getObj();
			this.businessTypeService.batchImport(items);
		}
		return AjaxResult.success();
	}
}
