package com.tmt.platform.qa.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.JsonMapper;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.entity.ExcelTemplate;
import com.tmt.base.component.excel.config.service.ExcelTemplateService;
import com.tmt.base.component.excel.util.TemplateExcelUtil;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.base.system.web.BaseImpController;
import com.tmt.platform.enums.MakeStatus;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QaRelaType;
import com.tmt.platform.exception.AccessDeniedException;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicContent;
import com.tmt.platform.qa.entity.TopicImp;
import com.tmt.platform.qa.entity.TopicOption;
import com.tmt.platform.qa.entity.TopicRela;
import com.tmt.platform.qa.service.QaService;
import com.tmt.platform.qa.service.TopicContentService;
import com.tmt.platform.qa.service.TopicOptionService;
import com.tmt.platform.qa.service.TopicRelaService;
import com.tmt.platform.qa.service.TopicService;
import com.tmt.platform.qa.utils.Relas;
import com.tmt.platform.qa.utils.Topics;
import com.tmt.platform.utils.TaxConstants;

@Controller
@RequestMapping(value = "${adminPath}/platform/topic")
public class TopicController extends BaseImpController{

	@Autowired
	private TopicService topicService;
	@Autowired
	private QaService qaService;
	@Autowired
	private TopicContentService contentService;
	@Autowired
	private TopicOptionService optionsService;
	@Autowired
	private TopicRelaService relaService;
	@Autowired
	private ExcelTemplateService templateService;
	
	/**
	 * 试题编制
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(Model model) {
		List<ExcelTemplate> templates = this.templateService.queryByTargetClass(TopicImp.class.getName());
		model.addAttribute("templates", templates);
		return "/taxPlatform/TopicList";
	}
	
	/**
	 * 试题编制 -- 初审
	 * @return
	 */
	@RequestMapping(value = {"initVList"})
	public String initVList() {
		return "/taxPlatform/TopicVList";
	}
	
	/**
	 * 试题编制 -- 终审
	 * @return
	 */
	@RequestMapping(value = {"initFvList"})
	public String initFvList() {
		return "/taxPlatform/TopicFvList";
	}
	
	/**
	 * 试题编制 -- 发布
	 * @return
	 */
	@RequestMapping(value = {"initFList"})
	public String initFList() {
		return "/taxPlatform/TopicFList";
	}
	
	/**
	 * 试题编制  -- 试题的创建人过滤(添加了过滤的权限,如果拥有此权限，则可以查看所有的试题)
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Topic topic, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(topic, qc);
		if( !UserUtils.isPermitted(TaxConstants.TOPIC_VIEW_ALL) ) {//没有此权限，则按照创建人过滤
			c.andEqualTo("CREATE_ID", "CREATE_ID", UserUtils.getUser().getId());
		}
		page = this.topicService.queryForPage(qc, param);
		return this.dealPageInfo(page);
	}
	

	/**
	 * 试题编制 -- 初审
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonVList"})
	public Page jSonVList(Topic topic, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(topic, qc);
		c.andEqualTo("T.TYPE", "TYPE", QaRelaType.INIT_VERITY);
		page = this.topicService.queryForAuditPage(qc, param);
		return this.dealPageInfo(page);
	}
	
	/**
	 * 试题编制 -- 终审
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonFvList"})
	public Page jSonFvList(Topic topic, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(topic, qc);
		c.andEqualTo("T.TYPE", "TYPE", QaRelaType.FINALLY_VERITY);
		page = this.topicService.queryForAuditPage(qc, param);
		//查询初审信息
		if(page != null && page.getData() != null && page.getData().size() != 0) {
			List<Topic> topics = page.getData();
			for(Topic temp: topics) {
				//内容
				TopicContent content = this.contentService.getContentByTopicAndType(temp.getId(), QaRelaType.INIT_VERITY);
				temp.setVauditName(content.getCreateName());
				temp.setVauditDate(content.getCreateDate());
			}
		}
		return this.dealPageInfo(page);
	}
	
	/**
	 * 试题编制 -- 发布
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonFList"})
	public Page jSonFList(Topic topic, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = this.initQc(topic, qc);
		c.andEqualTo("T.TYPE", "TYPE", QaRelaType.FINALLY);
		page = this.topicService.queryForPage(qc, param);
		//查询命题人、初审人、终审人
		if( page != null && page.getData() != null && page.getData().size() != 0 ) {
			List<Topic> topics =  page.getData();
			for( Topic temp: topics ) {
				List<TopicContent> contents = this.contentService.getAllContent(temp.getId());//应该有三个
				for(TopicContent content: contents) {
					if(QaRelaType.INIT_VERITY == content.getType()) {
						temp.setVauditName(content.getCreateName());
						temp.setVauditDate(content.getCreateDate());
					}
					if(QaRelaType.FINALLY_VERITY == content.getType()) {
						temp.setFvauditName(content.getCreateName());
						temp.setFvauditDate(content.getCreateDate());
					}
				}
			}
		}
		return this.dealPageInfo(page);
	}
	
	/**
	 * 试题编辑
	 * @param topic
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(Topic topic, Model model) {
		try{
			if( topic == null || IdGen.isInvalidId(topic.getId())) {
				topic = topic == null ? new Topic():topic;
				topic.setId(IdGen.INVALID_ID);
				topic.setType(QaRelaType.INIT);//命题阶段
				topic.setOptionType(QaRelaType.INIT);
			}
			//查询指定状态下的试题内容
			topic = this.topicService.getByType(topic);
			model.addAttribute("topic", topic);
		}catch(AccessDeniedException e){
			model.addAttribute("errorMsg", e.getMessage());
			return "/taxPlatform/TopicError";
		}
		//根据查询的类型不同返回不同的页面
		QaRelaType optionType = topic.getOptionType();
		if( optionType == QaRelaType.INIT) {
			return "/taxPlatform/TopicForm";
		} else if( optionType == QaRelaType.INIT_VERITY ){
			return "/taxPlatform/TopicVForm";
		} else if( optionType == QaRelaType.FINALLY_VERITY || optionType == QaRelaType.FINALLY ){
			return "/taxPlatform/TopicFvForm";
		}
		return "/taxPlatform/TopicForm";
	}
	
	/**
	 * 试题保存,list参数的绑定,view需要写成options[0].xxx,才能正确绑定
	 * @param topic
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(Topic topic, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes){
		if ( !beanValidator(model, topic) ){
			return form(topic, model);
		}
		//第一保存需要选择是否命题,排除自定义的
		if( !IdGen.isInvalidId(topic.getId()) || (IdGen.isInvalidId(topic.getId()) && Topic.YES.equals(topic.getMakeQFlag()))
				|| QFrom.CUSTOM == topic.getQFrom() ) {
			//获取答案
			String errorMsg = Topics.fetchKeysFromRequest(topic, request);
			if( errorMsg != null) {
				this.addMessage(redirectAttributes, errorMsg);
			}
			//获取税种关联
			errorMsg = Topics.fetchRelasFromRequest(topic, request);
			if( errorMsg != null) {
				this.addMessage(redirectAttributes, errorMsg);
			}
			//获取来源
			errorMsg = Topics.fetchFromFromRequest(topic, request);
			if( errorMsg != null) {
				this.addMessage(redirectAttributes, errorMsg);
			}
			QaRelaType optionType = topic.getType();//保存前的类型
			//存储
			this.topicService.save(topic);
			redirectAttributes.addAttribute("optionType", optionType.name());
		} else if( Topic.NO.equals(topic.getMakeQFlag()) && QFrom.CUSTOM != topic.getQFrom()) {//选择的否,只保存相应的信息
			Topics.fetchFromFromRequest(topic, request);
			this.topicService.updateFormNo(topic);
		}
		redirectAttributes.addAttribute("id", topic.getId());
		redirectAttributes.addAttribute("qId", topic.getQId());
		redirectAttributes.addAttribute("qFrom", topic.getQFrom().name());
		return "redirect:"+Globals.getAdminPath()+"/platform/topic/form";
	}
	
	/**
	 * 完成命题
	 * postData= [{id:'',version:''},{id:'',version:''}]
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"finish"})
	public AjaxResult finish(Model model, HttpServletRequest request, HttpServletResponse response) {
		try{
			List<Topic> topics = Lists.newArrayList();
			String postData = request.getParameter("postData");
			topics = JsonMapper.fromJsonToList(postData, Topic.class);
			this.topicService.finish(topics);
		}catch(Exception e){
			e.printStackTrace();
			return AjaxResult.error(e.getMessage());//数据版本过低,权限错误
		}
		return AjaxResult.success();
	}
	
	/**
	 * 需要支持不同状态下的删除
	 * @param idList
	 * @param request
	 * @param model
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList, HttpServletRequest request, Model model, HttpServletResponse response) {
		try{
			QaRelaType optionType = QaRelaType.valueOf(request.getParameter("optionType"));
			List<Topic> topics = Lists.newArrayList();
			Boolean bFlag = Boolean.TRUE;
			for( String id:idList ) {
				Topic topic = new Topic();
				topic.setId(id); topic.setOptionType(optionType);
				topic = this.topicService.getConyentByType(topic);
				if( topic == null || !topic.getEditFlag()) {//无编辑权限
					bFlag = Boolean.FALSE;
					break;
				}
				topics.add(topic);
			}
			if(!bFlag) {
				return AjaxResult.error("要删除的试题不存在或已发布或无操作权限,请刷新页面重试!");
			}
			this.topicService.batchDelete(topics);
			return AjaxResult.success();
		}catch(Exception e){
			return AjaxResult.error("删除试题出错:系统错误!");
		}
	}
	
	/**
	 * 公开 - 不公开
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"toPublic"})
	public AjaxResult toPublic(Model model, HttpServletRequest request, HttpServletResponse response){
		List<Topic> topics = Lists.newArrayList();
		String postData = request.getParameter("postData");
		topics = JsonMapper.fromJsonToList(postData, Topic.class);
		this.topicService.toPublic(topics);
		return AjaxResult.success();
	}
	
	/**
	 * 比较  -- 返回比较结果
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"compare"})
	public AjaxResult compare(Topic topic){
		//内容比较 - 选项比较
		if(topic == null || IdGen.isInvalidId(topic.getId())) {
			return AjaxResult.success(); 
		}
		topic = this.topicService.get(topic.getId());
		if(topic == null) {
			return AjaxResult.success(); 
		}
		if( topic.getType() == QaRelaType.FINALLY) {
			topic.setType(QaRelaType.FINALLY_VERITY);
		}
		if( topic.getType()== QaRelaType.FINALLY_VERITY && topic.getType().getPreOption() != null) {
			
			QaRelaType preType = topic.getType().getPreOption();
			
			//内容比较
			TopicContent content = this.contentService.getContentByTopicAndType(topic.getId(), preType);
			TopicContent preContent = this.contentService.getContentByTopicAndType(topic.getId(), preType.getPreOption());
			Map<String,Boolean> compareResult = content.compareTo(preContent);
			
			//选择项的比较
			List<TopicOption> options = this.optionsService.queryOptionsByTopicAndType(topic.getId(), topic.getType());
			List<TopicOption> preOptions = this.optionsService.queryOptionsByTopicAndType(topic.getId(), preType.getPreOption());
			
			//税种的比较
			List<TopicRela> relas = this.relaService.queryRelasByTopicAndType(topic.getId(), topic.getType());
			List<TopicRela> preRelas = this.relaService.queryRelasByTopicAndType(topic.getId(), preType.getPreOption());
			if( !compareResult.get("qType") || !compareResult.get("skey")
					|| !((options != null && preOptions != null && options.size() == preOptions.size()) 
						 || ( options == null && preOptions == null )) ) {
				compareResult.put("options", Boolean.FALSE);
			} else if( (options != null && preOptions != null && options.size() == preOptions.size()) ){
				for( int i=0, j= options.size(); i < j; i++ ) {
					TopicOption one = options.get(i);
					TopicOption two = preOptions.get(i);
					Map<String,Boolean> tempR = one.compareTo(two);
					if( !tempR.isEmpty() ){
						compareResult.put("options", Boolean.FALSE);
						break;
					}
				}
			} 
			//税种比较
			if( !((relas != null && preRelas != null && relas.size() == preRelas.size()) 
						 || ( relas == null && preRelas == null )) ) {
				compareResult.put("relas", Boolean.FALSE);
			} else if( (relas != null && preRelas != null && relas.size() == preRelas.size()) ){
				for( int i=0, j= relas.size(); i < j; i++ ) {
					TopicRela one = relas.get(i);
					TopicRela two = preRelas.get(i);
					Map<String,Boolean> tempR = one.compareTo(two);
					if( !tempR.isEmpty() ){
						compareResult.put("relas", Boolean.FALSE);
						break;
					}
				}
			}
			return AjaxResult.success(compareResult); 
		}
		return AjaxResult.success(); 
	}
	
	/**
	 * 当前用户待办
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"audit"})
	public AjaxResult audit(){
		List<Map<String,Object>> audits = Lists.newArrayList();
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		c.andEqualTo("QA.MAKE_ID", "MAKE_ID", UserUtils.getUser().getId());
		c.andNotEqualTo("QA.MAKE_STATUS", "MAKE_STATUS", MakeStatus.DONE);
		//命题的待办 -- 需要加上命题人
		Integer count = this.qaService.countInitTopicList(qc);
		Map<String,Object> initMap = Maps.newHashMap();audits.add(initMap);
		initMap.put("name", QaRelaType.INIT.getName());initMap.put("auditCount", count);
		//初审的待办
		if(UserUtils.isPermitted(TaxConstants.TOPIC_AUDIT_INIT_VERITY)) {//拥有审核权限
			qc.clear();c = qc.createCriteria(); c.andEqualTo("T.TYPE", "TYPE", QaRelaType.INIT_VERITY);
			count = this.topicService.countAuditTopic(qc);
			initMap = Maps.newHashMap();audits.add(initMap);
			initMap.put("name", QaRelaType.INIT_VERITY.getName());initMap.put("auditCount", count);
		}
		//终审的待办
        if(UserUtils.isPermitted(TaxConstants.TOPIC_AUDIT_FINALLY_VERITY)) {//拥有审核权限
        	qc.clear();c = qc.createCriteria();c.andEqualTo("T.TYPE", "TYPE", QaRelaType.FINALLY_VERITY);
        	count = this.topicService.countAuditTopic(qc);
        	initMap = Maps.newHashMap();audits.add(initMap);
    		initMap.put("name", QaRelaType.FINALLY_VERITY.getName());initMap.put("auditCount", count);
		}
        AjaxResult result = AjaxResult.success();
        result.setObj(audits);
		return result;
	}
	
	/**
	 * 初始化查询条件
	 * @param topic
	 * @param qc
	 * @return
	 */
	private Criteria initQc(Topic topic, QueryCondition qc) {
		Criteria c = qc.createCriteria();
		if( topic != null && StringUtil3.isNotBlank(topic.getName())) {
			c.andLike("T.CONTENT", "content", topic.getName());
		}
		if( topic != null && topic.getQFrom() != null) {
			c.andEqualTo("T.Q_FROM", "Q_FROM", topic.getQFrom());
		}
		if( topic != null && topic.getQType() != null) {
			c.andEqualTo("T.Q_TYPE", "Q_TYPE", topic.getQType());
		}
		if( topic != null && topic.getQLevel() != null) {
			c.andEqualTo("T.Q_LEVEL", "Q_LEVEL", topic.getQLevel());
		}
		if( topic != null && topic.getType() != null) {
			c.andEqualTo("T.TYPE", "TYPE", topic.getType());
		}
		if( topic != null && StringUtil3.isNotBlank(topic.getCreateId())) {
			c.andEqualTo("T.CREATE_ID", "createId", topic.getCreateId());
		}
		if(StringUtil3.isNotBlank(topic.getCategoryId()) && StringUtil3.isNotBlank(topic.getCategoryItemId())) {
		   StringBuilder sql = new StringBuilder();
		   try {
			 StringUtil3.appendTo(sql, " EXISTS(",
					   " SELECT 1 FROM TAX_TOPIC_RELA QR WHERE QR.TOPIC_ID = T.ID AND QR.CATEGORY_ID = '", topic.getCategoryId(),
					   "' AND QR.CATEGORY_ITEM_ID = '",topic.getCategoryItemId(),"' )");
		   } catch (Exception e) {}
		   c.andConditionSql(sql.toString());
		} else if(StringUtil3.isNotBlank(topic.getCategoryId())) {
		   StringBuilder sql = new StringBuilder();
		   try {
			 StringUtil3.appendTo(sql, " EXISTS(",
					   "SELECT 1 FROM TAX_TOPIC_RELA QR WHERE QR.TOPIC_ID = T.ID AND QR.CATEGORY_ID = '", topic.getCategoryId(), "' )");
		   } catch (Exception e) {}
		   c.andConditionSql(sql.toString());
		} else if(StringUtil3.isNotBlank(topic.getCategoryItemId())) {
			   StringBuilder sql = new StringBuilder();
			   try {
				 StringUtil3.appendTo(sql, " EXISTS(",
						   "SELECT 1 FROM TAX_TOPIC_RELA QR WHERE QR.TOPIC_ID = T.ID AND QR.CATEGORY_ITEM_ID = '", topic.getCategoryItemId(), "' )");
			   } catch (Exception e) {}
			   c.andConditionSql(sql.toString());
		}
		return c;
	}
	
	//导入
	@Override
	protected  AjaxResult doImport(String templateId, MultipartFile file) {
		AjaxResult result = TemplateExcelUtil.fetchObjectFromTemplate(templateId, file);
		if( result != null && result.getSuccess()) {
			List<TopicImp> items = result.getObj();
			List<Topic> topics = Topics.fromTopicImps(items);
			this.topicService.batchImport(topics);
		}
		return AjaxResult.success();
	}
	
	//处理page中的内容
	private Page dealPageInfo(Page page){ 
		//答案和回答不必显示全部
		if( page != null && page.getData() != null && page.getData().size() != 0) {
			List<Topic> topics = page.getData();
			for( Topic topic: topics ) {
				//设置税种和税明细
				List<TopicRela> qaRelas = this.relaService.queryRelasByTopicAndType(topic.getId(), null);
				if( qaRelas != null ) {
					topic.setCategorys(Relas.getCategorys(qaRelas));
					topic.setCategoryItems(Relas.getCategoryItems(qaRelas));
				}
			}
		}
		return page;
	}
}
