package com.tmt.platform.qa.web;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.tmt.base.common.utils.BigDecimalUtil;
import com.tmt.base.common.utils.JsonMapper;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.exp.ExportController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.platform.enums.QLevel;
import com.tmt.platform.enums.QType;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.service.CategoryService;
import com.tmt.platform.qa.service.ReportCentreService;
import com.tmt.platform.qa.utils.ReportCentreUtils;

/**
 * 
 * 报表中心
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/platform/report")
public class ReportCentreController extends ExportController<Map<String,Object>>{
	
	@Autowired
	private ReportCentreService reportService;
	@Autowired
	private CategoryService categoryService;
	
	/**
	 * 报表中心 -- 综合的页面
	 * @return
	 */
	@RequestMapping(value = {""})
	public String reportCentre(){
		
		return "/taxPlatform/ReportCentre";
	} 
	 
	/**
	 * 按试题类型和试题难度统计试题
	 */
	@ResponseBody
	@RequestMapping(value = {"chartByQTypeAndQLevel"})
	public AjaxResult chartByQTypeAndQLevel(Topic topic) {
		List<Map<String,Object>> stats = this.reportService.statByQTypeAndQLevel(topic);
		AjaxResult result = AjaxResult.success();
		result.setObj(stats);
		return result;
	}
	
	/**
	 * 按试题类型和试题难度统计试题
	 * @return
	 */
	@RequestMapping(value = {"listByQTypeAndQLevel"})
	public String listByQTypeAndQLevel(Topic topic, Model model){
		List<Map<String,Object>> stats = this.statByQTypeAndQLevel(topic);
		model.addAttribute("stats", stats);
		if( topic != null && StringUtil3.isNotBlank(topic.getCreateId())) {
			model.addAttribute("createId", topic.getCreateId());
			model.addAttribute("createName", UserUtils.getUser(topic.getCreateId()).getName());
		}
		return "/taxPlatform/reportCentre/ListByQTypeAndQLevel"; 
	}
	
	/**
	 * 按试题类型和试题难度统计试题
	 * @param topic
	 * @return
	 */
	private List<Map<String,Object>> statByQTypeAndQLevel(Topic topic) {
		List<Map<String,Object>> stats = this.reportService.statByQTypeAndQLevel(topic);
		Map<String,Object> total = Maps.newHashMap();
		total.put("Q_TYPE", "-");total.put("Q_LEVEL", "-");
		//计算合计
		if( stats != null && stats.size() != 0 ) {
			for(Map<String,Object> stat: stats) {
				BigDecimal topicTotal = BigDecimalUtil.valueOf(stat.get("TOPIC_TOTAL"));
				BigDecimal qaTotal = BigDecimalUtil.valueOf(stat.get("QA_TOTAL"));
				BigDecimal examTotal = BigDecimalUtil.valueOf(stat.get("EXAM_TOTAL"));
				BigDecimal customTotal = BigDecimalUtil.valueOf(stat.get("CUSTOM_TOTAL"));
				
				total.put("TOPIC_TOTAL", BigDecimalUtil.add(BigDecimalUtil.valueOf(total.get("TOPIC_TOTAL")), topicTotal));
				total.put("QA_TOTAL", BigDecimalUtil.add(BigDecimalUtil.valueOf(total.get("QA_TOTAL")), qaTotal));
				total.put("EXAM_TOTAL", BigDecimalUtil.add(BigDecimalUtil.valueOf(total.get("EXAM_TOTAL")), examTotal));
				total.put("CUSTOM_TOTAL", BigDecimalUtil.add(BigDecimalUtil.valueOf(total.get("CUSTOM_TOTAL")), customTotal));
			    
				//题型和难度转换
				stat.put("Q_TYPE", QType.valueOf(String.valueOf(stat.get("Q_TYPE"))).getName());
				stat.put("Q_LEVEL", QLevel.valueOf(String.valueOf(stat.get("Q_LEVEL"))).getName());
			}
		}
		stats.add(total);
		return stats;
	}
	
	/**
	 * 按试题类型和试题难度统计试题
	 */
	@ResponseBody
	@RequestMapping(value = {"chartByIndutry"})
	public AjaxResult chartByIndutry() {
		List<Map<String,Object>> stats = this.reportService.statByIndutry(null);
		AjaxResult result = AjaxResult.success();
		result.setObj(stats);
		return result;
	}
	
	/**
	 * 按试题行业统计
	 * @return
	 */
	@RequestMapping(value = {"listByIndutry"})
	public String listByIndutry(String name, Model model){
		List<Map<String,Object>> stats = this.reportService.statByIndutry(name);
		model.addAttribute("stats", stats);
		return "/taxPlatform/reportCentre/ListByIndutry"; 
	}
	
	/**
	 * 按试题类型和试题难度统计试题
	 */
	@ResponseBody
	@RequestMapping(value = {"chartByBusinessType"})
	public AjaxResult chartByBusinessType() {
		List<Map<String,Object>> stats = this.reportService.statByBusinessType(null);
		AjaxResult result = AjaxResult.success();
		result.setObj(stats);
		return result;
	}
	
	/**
	 * 按试题业务类型统计
	 * @return
	 */
	@RequestMapping(value = {"listByBusinessType"})
	public String listByBusinessType(String name, Model model){
		List<Map<String,Object>> stats = this.reportService.statByBusinessType(name);
		model.addAttribute("stats", stats);
		return "/taxPlatform/reportCentre/ListByBusinessType"; 
	}
	
	/**
	 * 按试题税种统计
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"chartByCategory"})
	public AjaxResult chartByCategory(){
		List<Map<String,Object>> stats = this.reportService.statByCategory(null);
		AjaxResult result = AjaxResult.success();
		result.setObj(stats);
		return result;
	}
	
	/**
	 * 按试题税种统计
	 * @return
	 */
	@RequestMapping(value = {"listByCategory"})
	public String listByCategory(String name, Model model){
		List<Map<String,Object>> stats = this.reportService.statByCategory(name);
		model.addAttribute("stats", stats);
		return "/taxPlatform/reportCentre/ListByCategory"; 
	}
	
	/**
	 * 按试题来源统计
	 * @return
	 */
	@RequestMapping(value = {"listByFrom"})
	public String listByFrom(String name, Model model){
		List<Map<String,Object>> stats = this.reportService.statByFrom();
		model.addAttribute("stats", stats);
		return "/taxPlatform/reportCentre/ListByFrom"; 
	}
	
	/**
	 * 按试题税种明细统计
	 * @return
	 */
	@RequestMapping(value = {"listByCategoryItem"})
	public String listByCategoryItem(String categoryId, String categoryName, Model model){
		List<Map<String,Object>> stats = this.reportService.statByCategoryItem(categoryId, categoryName);
		model.addAttribute("stats", stats);
		if( StringUtil3.isNotBlank(categoryId)) {
			Category category = categoryService.get(categoryId);
			model.addAttribute("categoryName", category.getName());
			model.addAttribute("categoryId", category.getId());
		}
		return "/taxPlatform/reportCentre/ListByCategoryItem"; 
	}

	/**
	 * 导出一张Excel
	 */
	@Override
	public Map<String, Object> doExport(Map<String, Object> param, HttpServletRequest request) {
		Topic topic = JsonMapper.fromJson(JsonMapper.toJson(param), Topic.class);
		List<Map<String,Object>> stats = this.statByQTypeAndQLevel(topic);
		return ReportCentreUtils.buildQTypeAndQLevelExpParam(stats);
	}
    
	/**
	 * 导出的参数类型
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Class getTargetClass() {
		return Map.class;
	}
}
