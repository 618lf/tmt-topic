package com.tmt.platform.qa.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.platform.qa.entity.TaxDoc;
import com.tmt.platform.qa.service.TaxDocSearcher;
import com.tmt.platform.qa.service.TaxDocService;
import com.tmt.platform.qa.utils.TaxDocs;

/**
 * 法规文件
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/platform/taxdoc")
public class TaxDocController extends BaseController{
    
	@Autowired
	private TaxDocService taxDocService;
	@Autowired
	private TaxDocSearcher taxDocSearcher;
	
	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(TaxDoc taxDoc, Model model){
		if( taxDoc != null && StringUtil3.isNotBlank(taxDoc.getDocNum())) {
			model.addAttribute("docNum", taxDoc.getDocNum());
		}
		if( taxDoc != null && StringUtil3.isNotBlank(taxDoc.getDocName())) {
			model.addAttribute("docName", taxDoc.getDocName());
		}
		return "/taxPlatform/TaxDocList";
	}
	
	/**
	 * 查询数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(TaxDoc taxDoc, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if( StringUtil3.isNotBlank(taxDoc.getDocType())) {
			c.andLike("DOC_TYPE", "docType", taxDoc.getDocType());
		}
		if( StringUtil3.isNotBlank(taxDoc.getDocName())) {
			c.andLike("DOC_NAME", "docName", taxDoc.getDocName());
		}
		if( StringUtil3.isNotBlank(taxDoc.getDocNum())) {
			String _docNum = TaxDocs.formateLikeQuery(taxDoc.getDocNum());
			c.andLike("DOC_NUM", "docNum", _docNum);
		}
		if( StringUtil3.isNotBlank(taxDoc.getPubOffice())) {
			c.andEqualTo("PUB_OFFICE", "pubOffice", taxDoc.getPubOffice());
		}
		if( StringUtil3.isNotBlank(taxDoc.getCategorys())) {
			c.andLike("CATEGORYS", "categorys", taxDoc.getCategorys());
		}
		page = this.taxDocService.queryForPage(qc, param);
		return page;
	}
	
	/**
	 * 获取法规内容
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"fetchContent"})
	public AjaxResult fetchContent(String[] idList){
		List<TaxDoc> docs = Lists.newArrayList();
		if( idList == null || idList.length == 0) {
			docs = this.taxDocService.findAll();
		} else {
			for(String id: idList) {
				TaxDoc doc = this.taxDocService.get(id);
				docs.add(doc);
			}
		}
		//获取法规的内容
		TaxDocs.fetchContent(docs);
		//保存相关的信息 -- 只能修改相关的属性
		this.taxDocService.batchUpdate(docs);
		return AjaxResult.success();
	}
	
	/**
	 * 
	 * @param taxDoc
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(TaxDoc taxDoc, Model model){
		taxDoc = this.taxDocService.get(taxDoc.getId());
		//读取文件
		taxDoc.setDocContent(TaxDocs.readContent(taxDoc));
		model.addAttribute("taxDoc", taxDoc);
		return "/taxPlatform/TaxDocForm";
	}
	
	/**
	 * 创建全文索引
	 * @param idList
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"createIndex"})
	public AjaxResult createIndex(String[] idList){
		List<TaxDoc> docs = Lists.newArrayList();
		if( idList == null || idList.length == 0) {
			docs = this.taxDocService.findAll();
		} else {
			for(String id: idList) {
				TaxDoc doc = this.taxDocService.get(id);
				docs.add(doc);
			}
		}
		//从文件读取数据
		for(TaxDoc doc: docs) {
			doc.setDocContent(TaxDocs.readContentText(doc));
		}
		try {
			taxDocSearcher.createIndex(docs);
		} catch (Exception e) {
			throw new BaseRuntimeException("创建法规索引出错!", e);
		}
		return AjaxResult.success();
	}
	
	/**
	 * 跳转到检索页面
	 * @return
	 */
	@RequestMapping(value = {"initSearcher"})
	public String initSearcher(String query, Model model){
		if( StringUtil3.isNotBlank(query) ) {
			model.addAttribute("query", query);
		}
		return "/taxPlatform/TaxDocSearch";
	}
	
	/**
	 * 检索法规  -- 分页查询
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"search"})
	public Page search(String query, Page page){
		try{
			return this.taxDocSearcher.searchPage(query, page);
		}catch(Exception e){
		    logger.error("检索法规出错",e);
		}
		return page;
	}
}
