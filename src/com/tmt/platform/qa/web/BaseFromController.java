package com.tmt.platform.qa.web;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.service.ExcelTemplateService;
import com.tmt.base.component.excel.util.TemplateExcelUtil;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.utils.DictUtils;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.base.system.web.BaseImpController;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.qa.entity.BusinessType;
import com.tmt.platform.qa.entity.Indutry;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;
import com.tmt.platform.qa.service.BusinessTypeService;
import com.tmt.platform.qa.service.IndutryService;
import com.tmt.platform.qa.service.PubOfficeService;
import com.tmt.platform.qa.service.QaRelaService;
import com.tmt.platform.qa.service.QaService;
import com.tmt.platform.qa.service.TopicService;
import com.tmt.platform.qa.utils.QaRelas;
import com.tmt.platform.utils.TaxConstants;

/**
 * 基础的汇编
 * @author lifeng
 */
public class BaseFromController extends BaseImpController {
	
	@Autowired
	protected QaService qaService;
	@Autowired
	protected QaRelaService qaRelaService;
	@Autowired
	protected TopicService topicService;
	@Autowired
	protected IndutryService indutryService;
	@Autowired
	protected BusinessTypeService businessTypeService;
	@Autowired
	protected ExcelTemplateService templateService;
	@Autowired
	protected PubOfficeService pubOfficeService;
	
	//初始化查询条件
	protected Criteria initQc(Qa qa, QueryCondition qc) {
		Criteria c = qc.createCriteria();
		if(StringUtil3.isNotBlank(qa.getTitle())) {
		   c.andLike("TITLE", "TITLE", qa.getTitle());
		}
		if(StringUtil3.isNotBlank(qa.getContent())) {
		   c.andLike("CONTENT", "CONTENT", qa.getContent());
		}
		if(StringUtil3.isNotBlank(qa.getAnswer())) {
		   c.andLike("ANSWER", "ANSWER", qa.getAnswer());
		}
		if(StringUtil3.isNotBlank(qa.getPublishDate())) {
		   c.andLike("PUBLISH_DATE", "PUBLISH_DATE", qa.getPublishDate());
		}
		if(StringUtil3.isNotBlank(qa.getPublishOffice())) {
		   c.andEqualTo("PUBLISH_OFFICE", "PUBLISH_OFFICE", qa.getPublishOffice());
		}
		if(StringUtil3.isNotBlank(qa.getBusinessTypeId())) {
		   c.andEqualTo("QA.BUSINESS_TYPE_ID", "BUSINESS_TYPE_ID", qa.getBusinessTypeId());
		}
		if(StringUtil3.isNotBlank(qa.getIndutryId())) {
		   c.andEqualTo("QA.INDUTRY_ID", "INDUTRY_ID", qa.getIndutryId());
		}
		if(StringUtil3.isNotBlank(qa.getKeyWords())) {
		   c.andLike("KEY_WORDS", "KEY_WORDS", qa.getKeyWords());
		}
		if(qa.getMakeStatus() != null) {
		   c.andEqualTo("(CASE WHEN MAKE_STATUS IS NULL THEN 'NONE' ELSE MAKE_STATUS END)", "MAKE_STATUS", qa.getMakeStatus());
		}
		if(StringUtil3.isNotBlank(qa.getCategoryId()) && StringUtil3.isNotBlank(qa.getCategoryItemId())) {
		   StringBuilder sql = new StringBuilder();
		   try {
			 StringUtil3.appendTo(sql, " EXISTS(",
					   " SELECT 1 FROM TAX_QA_RELA QR WHERE QR.QA_ID = QA.ID AND QR.CATEGORY_ID = '", qa.getCategoryId(),
					   "' AND QR.CATEGORY_ITEM_ID = '",qa.getCategoryItemId(),"' )");
		   } catch (Exception e) {}
		   c.andConditionSql(sql.toString());
		} else if(StringUtil3.isNotBlank(qa.getCategoryId())) {
		   StringBuilder sql = new StringBuilder();
		   try {
			 StringUtil3.appendTo(sql, " EXISTS(",
					   "SELECT 1 FROM TAX_QA_RELA QR WHERE QR.QA_ID = QA.ID AND QR.CATEGORY_ID = '", qa.getCategoryId(), "' )");
		   } catch (Exception e) {}
		   c.andConditionSql(sql.toString());
		}
		return c;
	}
	
	//初始化答疑筛选条件 可以为or
	protected String initQaFilterQc() {
		//根据当前用户的相关权限，设置查询条件（对应相关的配置项）
		StringBuilder condition = new StringBuilder();
		try {
			condition = this.getRoleCondition(TaxConstants.QA_FILTER_AREA_TYPE);
			StringBuilder conditionSql = new StringBuilder();
			conditionSql.append("'").append(condition).append(",'");
			return conditionSql.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return condition.toString();
	}
	
	//初始化答疑筛选条件 可以为or
	protected Criteria initQaFilterQc(Criteria c) {
		//根据当前用户的相关权限，设置查询条件（对应相关的配置项）
		String conditionSql = this.initQaFilterQc();
		//权限数据
		//c.andLikeColumn(conditionSql.toString(), "PUBLISH_OFFICE", "QA.PUBLISH_OFFICE");
		if(StringUtil3.isNotBlank(conditionSql)) {
			String[] offices = conditionSql.split(",");
			List<String> _offices = Lists.newArrayList();
			for( String office: offices ) {
				if(StringUtil3.isNotBlank(office)) {
					_offices.add(office);
				}
			}
			c.andIn("QA.PUBLISH_OFFICE", "name", _offices);
		}
		return c;
	}
	
	//初始化答疑筛选条件 可以为or
	protected Criteria initOfficeFilterQc(Criteria c) {
		//根据当前用户的相关权限，设置查询条件（对应相关的配置项）
		String conditionSql = this.initQaFilterQc();
		//权限数据
		//c.andLikeColumn(conditionSql.toString(), "NAME", "NAME");
		if(StringUtil3.isNotBlank(conditionSql)) {
			String[] offices = conditionSql.split(",");
			List<String> _offices = Lists.newArrayList();
			for( String office: offices ) {
				if(StringUtil3.isNotBlank(office)) {
					_offices.add(office);
				}
			}
			c.andIn("NAME", "name", _offices);
		}
		return c;
	}
	
	//初始化答疑编制试题筛选条件(税种过滤)
	protected Criteria initQaMakeFilterQc(Criteria c) {
		try {
			//权限集合 -- 需要和数据字典配合使用
			StringBuilder condition = this.getRoleCondition(TaxConstants.QA_FILTER_TOPIC_CATEGORY_TYPE);
			
			StringBuilder conditionSql = new StringBuilder();
			StringUtil3.appendTo(conditionSql, " EXISTS(",
					   "SELECT 1 FROM TAX_QA_RELA QR, TAX_CATEGORY TC WHERE QR.CATEGORY_ID = TC.ID AND QR.QA_ID = QA.ID AND ",
					   "'", condition, ",'", "LIKE CONCAT(CONCAT('%,', TC.NAME ),',%'))");
			//权限数据
			c.andConditionSql(conditionSql.toString());
		} catch (IOException e) {
			e.printStackTrace(); 
		}
		return c;
	}
	
	//根据分配的权限过滤数据的过滤条件
	private StringBuilder getRoleCondition( String dictType) throws IOException {
		StringBuilder condition = new StringBuilder();
		List<Dict> roles = DictUtils.getDictList(dictType);
		if( roles != null && roles.size() != 0) {
			for( Dict role : roles) { //code为权限,value为数据集合
				if (UserUtils.isPermitted(role.getCode())) { // 拥有此权限，才能过滤相应的数据
					StringUtil3.appendTo(condition, ",", role.getValue());
				}
			}
		}
		if( StringUtil3.isEmpty(condition) ) {//没有则查询不到数据
			StringUtil3.appendTo(condition, ",", "~");
		}
		return condition;
	}
	
	//智能初始化税种和法规
	protected void initQaRela(Qa qa) {
		if( StringUtil3.isBlank(qa.getStorageFlag()) ) {//未筛选,只要是未筛选的
			List<QaRela> qaRelas = QaRelas.buildQaRelas(qa);
			this.qaRelaService.batchSave(qa, qaRelas);
			//重新查询
			List<QaRela> _qaRelas = this.qaRelaService.queryByQaId(qa.getId());
			qa.setQaRelas(_qaRelas);
			//默认业务类型
			BusinessType businessType = this.businessTypeService.getPublicBt();
			if(businessType != null) {
				qa.setBusinessTypeId(businessType.getId());
				qa.setBusinessTypeName(businessType.getName());
			}
			//默认行业
			Indutry indutry = this.indutryService.getPublicIndutry();
			if(indutry != null) {
				qa.setIndutryId(indutry.getId());
				qa.setIndutryName(indutry.getName());
			}
		}
	}
	
	//导入
	@Override
	protected  AjaxResult doImport(String templateId, MultipartFile file) {
		AjaxResult result = TemplateExcelUtil.fetchObjectFromTemplate(templateId, file);
		if( result != null && result.getSuccess()) {
			List<Qa> qas = result.getObj();
			if( qas != null && qas.size() != 0 ) {
				for(Qa qa: qas ) {
					qa.setQFrom(QFrom.QA);
				}
				this.qaService.save(qas);
			}
		}
		return AjaxResult.success();
	}
}
