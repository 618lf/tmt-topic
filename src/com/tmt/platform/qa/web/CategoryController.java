package com.tmt.platform.qa.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.service.CategoryService;

@Controller
@RequestMapping(value = "${adminPath}/platform/category")
public class CategoryController extends BaseController{

	@Autowired
	private CategoryService categoryService;
	
	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(Category category, Model model){
		return "/taxPlatform/CategoryList";
	}
	
	/**
	 * 初始化页面的数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Category category, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(category != null && StringUtil3.isNotBlank(category.getName())) {
		   c.andLike("NAME", "NAME", category.getName());
		}
		page = categoryService.queryForPage(qc, param);
		return page;
	}
	
	/**
	 * 问题表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(Category category, Model model){
		if(category != null && !IdGen.isInvalidId(category.getId())) {
			category = this.categoryService.get(category.getId());
		} else {
		   if(category == null) {
			   category = new Category();
		   }
		   category.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("category", category);
		return "/taxPlatform/CategoryForm";
	}
	
	/**
	 * 业务类型保存
	 * @param category
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(Category category, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, category) ){
			return form(category, model);
		}
		this.categoryService.save(category);
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改税种", category.getName(), "成功"));
		redirectAttributes.addAttribute("id", category.getId());
		return "redirect:"+Globals.getAdminPath()+"/platform/category/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		List<Category> cateorys = Lists.newArrayList();
		Boolean bFlag = Boolean.TRUE;
		for( String id:idList ) {
			Category category = new Category();
			category.setId(id);
			int iCount = this.categoryService.checkedDelete(category);
			if( iCount > 0) {
				bFlag = Boolean.FALSE;break;
			}
			cateorys.add(category);
		}
		if(!bFlag) {
			return AjaxResult.error("答疑汇编等关联了此税种,不能删除");
		}
		this.categoryService.batchDelete(cateorys);
		return AjaxResult.success();
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Category> cateorys = this.categoryService.findAll();
		for (int i=0; i< cateorys.size(); i++){
			Category e = cateorys.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", "O_-1");
			map.put("name", e.getName());
			mapList.add(map);
		}
		Map<String, Object> map = Maps.newHashMap();
		map.put("id", "O_-1");
		map.put("pId", "O_-2");
		map.put("name", "税种");
		mapList.add(map);
		return mapList;
	}
}
