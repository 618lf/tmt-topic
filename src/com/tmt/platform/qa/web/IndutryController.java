package com.tmt.platform.qa.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.config.entity.ExcelTemplate;
import com.tmt.base.component.excel.config.service.ExcelTemplateService;
import com.tmt.base.component.excel.util.TemplateExcelUtil;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.web.BaseImpController;
import com.tmt.platform.qa.entity.Indutry;
import com.tmt.platform.qa.service.IndutryService;

@Controller
@RequestMapping(value = "${adminPath}/platform/indutry")
public class IndutryController extends BaseImpController{

	@Autowired
	private IndutryService indutryService;
	@Autowired
	private ExcelTemplateService templateService;
	
	/**
	 * 初始化页面的查询条件
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"initList"})
	public String initList(Indutry indutry, Model model){
		List<ExcelTemplate> templates = this.templateService.queryByTargetClass(Indutry.class.getName());
		model.addAttribute("templates", templates);
		return "/taxPlatform/IndutryList";
	}
	
	/**
	 * 初始化页面的数据
	 * @param qa
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Indutry indutry, Model model, Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(indutry != null && StringUtil3.isNotBlank(indutry.getName())) {
		   c.andLike("NAME", "NAME", indutry.getName());
		}
		page = indutryService.queryForPage(qc, param);
		return page;
	}
	
	/**
	 * 问题表单
	 * @param qa
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"form"})
	public String form(Indutry indutry, Model model){
		if(indutry != null && !IdGen.isInvalidId(indutry.getId())) {
			indutry = this.indutryService.get(indutry.getId());
		} else {
		   if(indutry == null) {
			   indutry = new Indutry();
		   }
		   indutry.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("indutry", indutry);
		return "/taxPlatform/IndutryForm";
	}
	
	/**
	 * 业务类型保存
	 * @param Indutry
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = {"save"})
	public String save(Indutry indutry, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, indutry) ){
			return form(indutry, model);
		}
		this.indutryService.save(indutry);
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改行业", indutry.getName(), "成功"));
		redirectAttributes.addAttribute("id", indutry.getId());
		return "redirect:"+Globals.getAdminPath()+"/platform/indutry/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		List<Indutry> indutrys = Lists.newArrayList();
		Boolean bFlag = Boolean.TRUE;
		for( String id:idList ) {
			Indutry indutry = new Indutry();
			indutry.setId(id);
			int iCount = this.indutryService.checkedDelete(indutry);
			if( iCount > 0) {
				bFlag = Boolean.FALSE;break;
			}
			indutrys.add(indutry);
		}
		if(!bFlag) {
			return AjaxResult.error("答疑汇编等关联了此行业,不能删除");
		}
		this.indutryService.batchDelete(indutrys);
		return AjaxResult.success();
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Indutry> indutrys = this.indutryService.findAll();
		for (int i=0; i< indutrys.size(); i++){
			Indutry e = indutrys.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", "O_-1");
			map.put("name", e.getName());
			mapList.add(map);
		}
		Map<String, Object> map = Maps.newHashMap();
		map.put("id", "O_-1");
		map.put("pId", "O_-2");
		map.put("name", "行业");
		mapList.add(map);
		return mapList;
	}
	
	/**
	 * 其余的部分由父类完成
	 */
	public AjaxResult doImport(String templateId, MultipartFile file) {
		AjaxResult result = TemplateExcelUtil.fetchObjectFromTemplate(templateId, file);
		if( result != null && result.getSuccess()) {
			List<Indutry> items = result.getObj();
			this.indutryService.batchImport(items);
		}
		return AjaxResult.success();
	}
}
