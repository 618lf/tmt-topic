package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.TopicRela;

@Repository
public class TopicRelaDao extends BaseIbatisDAO<TopicRela, String>{

	@Override
	protected String getNamespace() {
		return "TAX_TOPIC_RELA";
	}
}
