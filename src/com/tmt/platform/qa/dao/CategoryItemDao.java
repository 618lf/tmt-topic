package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.CategoryItem;

@Repository
public class CategoryItemDao extends BaseIbatisDAO<CategoryItem, String>{

	@Override
	protected String getNamespace() {
		return "TAX_CATEGORY_ITEM";
	}
}
