package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.Indutry;

@Repository
public class IndutryDao extends BaseIbatisDAO<Indutry, String>{

	@Override
	protected String getNamespace() {
		return "TAX_INDUTRY";
	}
}
