package com.tmt.platform.qa.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class ReportCentreDao extends BaseIbatisDAO<Map<String,Object>, String>{

	@Override
	protected String getNamespace() {
		return "TAX_REPORT_CENTRE";
	}
}
