package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.TaxDoc;

/**
 * 法规文件
 * @author lifeng
 */
@Repository
public class TaxDocDao extends BaseIbatisDAO<TaxDoc, String>{

	@Override
	protected String getNamespace() {
		return "TAX_DOC";
	}
}
