package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.Category;

@Repository
public class CategoryDao extends BaseIbatisDAO<Category, String>{

	@Override
	protected String getNamespace() {
		return "TAX_CATEGORY";
	}
}
