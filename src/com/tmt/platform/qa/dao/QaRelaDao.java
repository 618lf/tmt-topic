package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.QaRela;

@Repository
public class QaRelaDao extends BaseIbatisDAO<QaRela, String>{

	@Override
	protected String getNamespace() {
		return "TAX_QA_RELA";
	}
}
