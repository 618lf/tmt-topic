package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.PubOffice;

@Repository
public class PubOfficeDao extends BaseIbatisDAO<PubOffice, String>{

	@Override
	protected String getNamespace() {
		return "TAX_PUB_OFFICE";
	}
}
