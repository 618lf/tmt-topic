package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.BusinessType;

@Repository
public class BusinessTypeDao extends BaseIbatisDAO<BusinessType, String>{

	@Override
	protected String getNamespace() {
		return "TAX_BUSINESS_TYPE";
	}
}
