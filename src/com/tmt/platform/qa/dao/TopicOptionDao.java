package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.TopicOption;

@Repository
public class TopicOptionDao extends BaseIbatisDAO<TopicOption, String>{

	@Override
	protected String getNamespace() {
		return "TAX_TOPIC_OPTIONS";
	}
}
