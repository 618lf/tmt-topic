package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.Topic;

@Repository
public class TopicDao extends BaseIbatisDAO<Topic, String>{

	@Override
	protected String getNamespace() {
		return "TAX_TOPIC";
	}
}
