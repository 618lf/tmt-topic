package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.TopicContent;

@Repository
public class TopicContentDao extends BaseIbatisDAO<TopicContent, String>{

	@Override
	protected String getNamespace() {
		return "TAX_TOPIC_CONTENT";
	}
}
