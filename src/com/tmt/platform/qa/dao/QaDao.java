package com.tmt.platform.qa.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.platform.qa.entity.Qa;

@Repository
public class QaDao extends BaseIbatisDAO<Qa, String>{

	@Override
	protected String getNamespace() {
		return "TAX_QA";
	}
}
