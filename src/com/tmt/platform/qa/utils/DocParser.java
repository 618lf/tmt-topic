package com.tmt.platform.qa.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.Lists;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 处理张工提供的excel 文档 转为txt
 * @author lifeng
 */
public class DocParser {

	private static String DOC_REX = "([^\\d/]+)([\\d/]+)(.+)";
	private static Pattern p = Pattern.compile(DOC_REX);
	
	/**
	 * 获得数据
	 * @param m
	 * @return
	 */
	public static String writeDoc(Matcher m){
		String docName = m.group(1).trim();
		String docDate = m.group(2).trim();
		String docNum = m.group(3).trim();
		return StringUtil3.format("%s:%s:%s", docName,docDate,docNum);
	}
	
	public static void main(String[] args) throws IOException{
		File file = new File("D:/docs.txt");
		List<String> lines = FileUtils.readLines(file, "UTF-8");
		List<String> _lines = Lists.newArrayList();
		for( String line: lines ){
			Matcher m = p.matcher(line);
			if( m.find()) {
				String _line = writeDoc(m);
				_lines.add(_line);
			} else {
				System.out.println("不符合：" + line);
			}
		}
		FileUtils.writeLines(file, "UTF-8", _lines);
		
	}
}
