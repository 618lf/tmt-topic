package com.tmt.platform.qa.utils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.JdbcSqlExecutor;
import com.tmt.base.common.utils.Maps;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.Doc;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.TaxDoc;
import com.tmt.platform.qa.service.TaxDocService;

/**
 * 税法文件
 * @author lifeng
 *
 */
public class Docs {
	
	private static String PRE_CHARS = "根据《|按照《";//这几个词后面连接的是书名号,才是正确的一段法规的引用
	private static String END_CHARS = "。";
	private static String RES_CHARS = "。因此";
	private static String DOC_NAME_REX = "[根据|按照]*[\\s]*(《[^《》]*([纪要]{2}|[批复]{2}|[请示]{2}|[报告]{2}|[通告]{2}|[决议]{2}|[命令]{2}|[办法]{2}|[公告]{2}|[条例]{2}|[细则]{2}|[规定]{2}|[规则]{2}|[决定]{2}|[通知]{2}|[税务]{2}|[意见]{2}|法|税|令|函)[^《》]*》)";//文件名
	private static String DOC_CONTENT_REX = "^[一|“]*[、\\.]*";
	private static String DOC_REX = "《([^《》]*([纪要]{2}|[批复]{2}|[请示]{2}|[报告]{2}|[通告]{2}|[决议]{2}|[命令]{2}|[办法]{2}|[公告]{2}|[条例]{2}|[细则]{2}|[规定]{2}|[规则]{2}|[决定]{2}|[通知]{2}|[税务]{2}|[意见]{2}|法|税|令|函)[^《》]*)》[\\s]*[（\\(]{0,1}([^《》（\\(）\\)]+号){0,1}[）\\)]{0,1}[\\s]*[的]*[规定]*[，：,:]*(.*)";
	private static String DOC_HAS_NAME_REX = "[\\s]*[（\\(]{0,1}([^《》（\\(）\\)]+号){0,1}[）\\)]{0,1}[\\s]*[的]*[规定]*[，：,:]*(.*)"; //前面需要加上文件名有文件名的匹配路径
	private static Pattern DOC_P = Pattern.compile(DOC_REX);
	private static Pattern DOC_C_P = Pattern.compile(DOC_CONTENT_REX);
	private static TaxDocService taxDocService = SpringContextHolder.getBean(TaxDocService.class);
	
	/**
	 * 答疑的回答的内容匹配
	 * @param qa
	 * @return
	 */
	public static List<Doc> docMatches(Qa qa) {
		return Docs.docMatches(qa.getAnswer());
	}
	
	/**
	 * 税法文件匹配
	 * @param content
	 * @return
	 */
	public static List<Doc> docMatches( String content ) {
		List<String> contents = contentSplit(content);
		List<Doc> docs = Lists.newArrayList();
		for(String _content: contents) {
			Matcher m = DOC_P.matcher(_content);
			if(m.find()) {
				docs.add(Doc.newDoc(m.group(1), m.group(3), m.group(4)));
			}
		}
		List<String> _docs = loadDocs();
		//判断是否解析正确
		for(Doc doc: docs){
			//主要是内容是否少了,重新从整个文件获取
			if(StringUtil3.isBlank(doc.getLarDocContent()) || (DOC_C_P.matcher(doc.getLarDocContent()).find() && StringUtil3.length(doc.getLarDocContent()) < 10)
				|| StringUtil3.length(doc.getLarDocContent()) < 10 ) { //符合多条
				Pattern DOC_HC_P = Pattern.compile("《" +doc.getLarDocName()+ "》" + DOC_HAS_NAME_REX);
				Matcher m = DOC_HC_P.matcher(content);
				if(m.find()) {
					doc.setLarDocContent(m.group(2));
				}
			}
			//文件号没有的
			if(StringUtil3.isBlank(doc.getLarDocNum()) || StringUtil3.isBlank(StringUtil3.trimToEmpty(doc.getLarDocNum()))) {
				doc.setLarDocNum(findDocNum(_docs, doc));
			}
			//文件号还没有的 -- 去法规库中按照文件名查找文件号
			if(StringUtil3.isBlank(doc.getLarDocNum()) || StringUtil3.isBlank(StringUtil3.trimToEmpty(doc.getLarDocNum()))) {
				doc.setLarDocNum(findDocNum(doc));
			}
		}
		return docs;
	}
	
	public static List<String> contentSplit(String content){
		List<String> contents = Lists.newArrayList();
		content = content.replaceAll(DOC_NAME_REX, "按照$1");//文件名添加按照
		//分割出临时的法规
		Iterable<String> _contents = Splitter.onPattern(PRE_CHARS).split(content);
		for( String temp: _contents ){
			String _content = "《" + temp;
			if( DOC_P.matcher(_content).find() ) {//匹配的上法规文件
				contents.add(deleteSomethingFormContent(_content));
			}
		}
		return contents;
	}
	
	/**
	 * 从处理好的内容中删除部分无用的字符
	 * @param content
	 * @return
	 */
	public static String deleteSomethingFormContent(String content){
		String _content = StringUtil3.substringBeforeLast(content, RES_CHARS);
		if(!content.equals(_content)) {
			content = _content + END_CHARS;
		}
		return content;
	}
	
	/**
	 * 查找文件号 -- 可以绑定到当前线程
	 * @return
	 */
	public static String findDocNum(List<String> docs, Doc doc ) {
		if( doc != null && docs != null && docs.size() != 0 ) {
			String line = null;
			for( String _doc: docs) {
				if( _doc != null && StringUtil3.isNotBlank(_doc) && StringUtil3.isNotBlank(StringUtil3.trimToEmpty(_doc)) ) {
					if( StringUtil3.startsWith(_doc, doc.getLarDocName() + ":") 
							&& ((line != null && _doc.compareTo(line)>0 ) || line == null)) { //匹配上文件名
						line = _doc;//取最新的
					}
				}
			}
			if( line != null ){
				return line.split(":")[2];
			}
		}
		return doc.getLarDocNum();
	}
	
	/**
	 * 查找法规库
	 * @param doc
	 * @return
	 */
	public static String findDocNum(Doc doc ) {
		TaxDoc taxDoc = null;
		if( taxDocService != null) {
			taxDoc = taxDocService.queryChinaTaxByDocName(doc.getLarDocName());
		}
		return taxDoc != null?taxDoc.getDocNum():"";
	}
	
	public static List<String> loadDocs() {
		//当前路径 -- 需要重新,有可能是打成jar包
		List<String> _docs = null;
		try {
			_docs = FileUtils.readLines(new File(Docs.class.getClassLoader().getResource("com/tmt/platform/qa/utils/doc.txt").toURI()), Globals.DEFAULT_ENCODING);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		if( _docs != null && _docs.size() != 0) {
			List<String> copys = Lists.newArrayList();
			for( String _doc : _docs ) {
				String s1 = StringUtil3.removeBom(_doc.split(":")[0]);
				String s2 = _doc.split(":")[1];
				String s3 = _doc.split(":")[2];
				copys.add(StringUtil3.trimToEmpty(s1) + ":" + StringUtil3.trimToEmpty(s2) + ":" + StringUtil3.trimToEmpty(s3));
			}
			_docs = copys;
		}
		docNumError(_docs);
		return _docs;
	}
	
	//文件错误的
	public static void docNumError(List<String> docs){
		List<String> names = Lists.newArrayList();
		for( String doc: docs) {
			String name = doc.split(":")[0];
			Map<String,Object> param = Maps.newHashMap();
			param.put("NAME", name);
			
			Map<String,Object> res = JdbcSqlExecutor.queryForMap("SELECT COUNT(1) RES FROM tax_qa_rela a where a.LAR_DOC_NAME= :NAME", param);
			if( res != null && res.get("RES") != null && 
					!"0".equals(String.valueOf(res.get("RES")))) {
				names.add(name);
			}
		}
		System.out.println("需要修改的项：" + names.size());
		for(int i= 0,j = names.size(); i< j; i++ ) {
			System.out.println( i + ":" + names.get(i));
		}
	}
	public static void main(String[] args) {
		String content = "《注册税务师管理暂行办法》提供正式申报电子数据及下列资料：（1）出口货物报关单（其他联次或通过口岸电子执法系统打印的报关单信息页面）；（2）主管税务机关要求报送的其他资料。";
		List<Doc> docs = Docs.docMatches(content);
		for(Doc doc: docs){
			System.out.println(doc.getLarDocName() + ":" + doc.getLarDocNum() + ":" + doc.getLarDocContent());
		}
	}
}
