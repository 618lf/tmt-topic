package com.tmt.platform.qa.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.utils.JsonMapper;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.enums.QType;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.Topic;
import com.tmt.platform.qa.entity.TopicImp;
import com.tmt.platform.qa.entity.TopicOption;
import com.tmt.platform.qa.entity.TopicRela;

/**
 * 试题帮助类
 * @author lifeng
 */
public class Topics {
	
	private static String Q_FROM_PARAM = "fromObj.";
	private static String RELAS_PARAM = "relas.";

	/**
	 * 获取相关信息从request 中.
	 * @param topic
	 * @param request
	 * @return
	 */
	public static String fetchKeysFromRequest(final Topic topic, HttpServletRequest request) {
		String[] keys = request.getParameterValues("skey");
		if(keys == null || keys.length == 0) {
			return StringUtil3.format("%s【%s】", "修改试题失败", "请填写答案");
		} else if( QType.isChoice(topic.getQType()) ) {
			String[] options = request.getParameterValues("soption");
			String[] salisa = request.getParameterValues("salisa");
			List<TopicOption> tOptions = Lists.newArrayList();
			for(int i= 0,j= options.length; i <j; i++ ){
				tOptions.add(new TopicOption(salisa[i],options[i]));
			}
			topic.setOptions(tOptions);
		}
		topic.setSkey(Joiner.on("").appendTo(new StringBuilder(), keys).toString());
		return null;
	}
	
	/**
	 * 获取数据来源的信息，获取fromObj.XXX 的参数
	 * @param topic
	 * @param request
	 * @return
	 */
	public static String fetchFromFromRequest(final Topic topic, HttpServletRequest request) {
		Map<String,Object> params = Maps.newHashMap();
		for (Object param : request.getParameterMap().keySet()){ 
			 String key = String.valueOf(param);
			 if(key != null && StringUtil3.startsWith(key, Q_FROM_PARAM)) {
				 params.put(StringUtil3.substringAfter(key, Q_FROM_PARAM), request.getParameter(key));
			 }
		}
		if(topic.getQFrom() != null && !params.isEmpty()) {
			Qa fromObj = JsonMapper.fromJson(JsonMapper.toJson(params), Qa.class);
			topic.setFromObj(fromObj);
		}
		return null;
	}
	
	/**
	 * 获取税种关联的信息，获取relas.XXX 的参数  -- 数组
	 * @param topic
	 * @param request
	 * @return
	 */
	public static String fetchRelasFromRequest(final Topic topic, HttpServletRequest request) {
		Map<String,String[]> params = Maps.newHashMap();
		for (Object param : request.getParameterMap().keySet()){ 
			 String key = String.valueOf(param);
			 if(key != null && StringUtil3.startsWith(key, RELAS_PARAM)) {
				 params.put(StringUtil3.substringAfter(key, RELAS_PARAM), request.getParameterValues(key));
			 }
		}
		List<Map<String,String>> relas = Lists.newArrayList();
		if( params.containsKey("id") ) { //id属性
			String[] ids = params.get("id");
			for( int i= 0,j = ids.length; i<j;i ++ ){
				Map<String,String> rela = Maps.newHashMap();
				for (Object param : params.keySet()){ 
					String key = String.valueOf(param); 
					String[] values = params.get(key);
					if(values == null || values.length <= i) {
						continue;
					}
					rela.put(key, values[i]);
				}
				relas.add(rela);
			}
		}
		List<TopicRela> tRelas = JsonMapper.fromJsonToList(JsonMapper.toJson(relas), TopicRela.class);
		//转换是-->1
		if( tRelas != null && tRelas.size() != 0) {
			for( TopicRela rela: tRelas ) {
				 if(rela != null) {
					 rela.setApplyProvince("是".equals(rela.getApplyProvince())?Topic.YES:Topic.NO);
				 }
			}
		}
		
		topic.setRelas(tRelas);
		return null;
	}
	
	/**
	 * 导入转换
	 * @param items
	 * @return
	 */
	public static List<Topic> fromTopicImps(List<TopicImp> items){
		List<Topic> topics = Lists.newArrayList();
		for(TopicImp item: items) {
			Topic topic = item.toTopic();
			topics.add(topic);
		}
		return topics;
	}
	
	/**
	 * 格式化导入的试题
	 * @param topic
	 * @return
	 */
	public static Topic dealImpTopic(Topic topic) {
		//Pattern p = Pattern.compile("^[\\s]*([A-Z]).(.+)[\\s]*$");
		if( QType.isChoice(topic.getQType())) { //选择题
//			List<TopicOption> _options = topic.getOptions();
//			for( TopicOption option: _options ) {
//				if(StringUtil3.isNotBlank(option.getSoption())) {
//					Matcher m = p.matcher(option.getSoption());
//					if( m.find() ) {
//						option.setSoption(m.group(1));
//						option.setSalisa(m.group(2));
//					}
//				}
//			}
//			topic.setOptions(_options);
		} else if( QType.TFNG == topic.getQType())  { //判断题
			String _skey = topic.getSkey();
			if( StringUtil3.isNotBlank(_skey) && StringUtil3.startsWith(_skey, Topic.YES)) {
				topic.setSkey(Topic.YES);
			} else {
				topic.setSkey(Topic.NO);
			}
		} else if( QType.COUNTING == topic.getQType())  { //判断题
			String _skey = topic.getSkey();
			if( StringUtil3.isNotBlank(_skey) && StringUtil3.endsWith(_skey, ".0")) {
				topic.setSkey(StringUtil3.removeEnd(_skey, ".0"));
			}
		}
		return topic;
	}
	
	//不支持list
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		List<Object> options = new ArrayList<Object>();
		options.add("A.12");
		Map<String,Object> values = new HashMap<String,Object>();
		values.put("postMinute", 300);
		values.put("options[0].soption", "1");
		Topic topic = new Topic();
		BeanUtils.copyProperties(topic, values);
		System.out.println(topic.getOptions());
	}
}
