package com.tmt.platform.qa.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.FileUtils;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.TaxDoc;

/**
 * 法规文件帮助类,以后改成FTP的方式来存储文件
 * @author lifeng
 */
public class TaxDocs {

	//文件存储路径
	private static String TAX_DOC_FILE_PATH = "D:/taxdocs/";
	
	
	/**
	 * 格式化查询条件中的特殊字符: 圆括号、方括号、全角、半角、六角括号，全部替换为%
	 * @param query
	 * @return
	 */
	public static String formateLikeQuery(String query) {
		String _query = query;
		_query = StringUtil3.replaceP(_query, "%");
		_query = StringUtil3.replaceZ(_query, "%");
		_query = _query.replaceAll("[%]+", "%");
		if(StringUtil3.startsWith(_query, "%")) {
			_query = _query.substring(1);
		}
		if(StringUtil3.endsWith(_query, "%")) {
			_query = StringUtil3.substring(_query, 0, StringUtil3.length(_query) - 1);
		}
		if(StringUtil3.isNotBlank(_query)) {
			_query = StringUtil3.trim(_query);
		}
		return _query;
	}
	
	public static void main(String[] args) {
		System.out.println(formateLikeQuery(""));
	}
	
	/**
	 * 获取法规文件的内容
	 * @param docs
	 */
	public static void fetchContent(List<TaxDoc> docs){
		for( TaxDoc doc: docs ){
			try{
				String sourceUrl = doc.getSourceUrl();
				if(StringUtil3.isNotBlank(sourceUrl)) {
					String html = fetchContent(sourceUrl);
					if( html != null ) {
						Document _doc = Jsoup.parse(html);
						//获取效力和成文日期
						String enabled = _doc.getElementById("cwrq").text();
						if( enabled != null) {
							String[] ss = enabled.split("成文日期：");
							doc.setEnabled(ss[0].trim());
							doc.setDocDate(DateUtil3.getFormatDate(ss[1].trim(), "yyyy-MM-dd"));
						}
						//文件的内容
						String content = _doc.getElementById("Zoom2").html();
						if( content != null) {
							content = StringUtil3.remove(content, "<o:p>");
							content = StringUtil3.remove(content, "</o:p>");
							content = StringUtil3.remove(content, "<span style=\"font-size: medium\">&nbsp;</span>");
							doc.setDocContent(content);
						}
						//识别税种
						List<Category> categorys = Categorys.findCategorys(doc);
						if(categorys != null && categorys.size() != 0) {
							StringBuilder _categorys = new StringBuilder();
							for(Category category: categorys) {
								_categorys.append(category.getName()).append(",");
							}
							if(_categorys.length() != 0) {
							   _categorys.deleteCharAt(_categorys.length()-1);
							}
							doc.setCategorys(_categorys.toString());
						}
						//存储到文件系统
						saveToFile(doc);
					}
				}
			}catch(Exception e){}
		}
	}
	
	/**
	 * 存储到文件，返回文件的路径
	 * 存储的是文件的名字，路径使用统一配置的方式
	 * @return
	 */
	public static void saveToFile(TaxDoc doc){
		String path = TAX_DOC_FILE_PATH;
		File filePath = new File(path);
		if(!filePath.exists()) {
			filePath.mkdirs();
		}
		File docFile = new File(filePath, doc.getDocFileName());
		if( docFile.exists() ) {
			docFile.delete();
		}
		try {
			FileUtils.write(docFile, doc.getDocContent(), "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		doc.setDocContent(doc.getDocFileName());
	}
	
	public static String readContent(TaxDoc doc) {
		File docFile = new File(TAX_DOC_FILE_PATH, doc.getDocFileName());
		try {
			return FileUtils.readFileToString(docFile,"UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String readContentText(TaxDoc doc) {
		String html = readContent(doc);
		if( html != null) {
			html = StringUtil3.replaceHtml(html);
		}
		return html;
	}
	
	public static String fetchContent(String sourceUrl) {
		sourceUrl = sourceUrl.replaceAll("\\.\\./", "");
		sourceUrl = sourceUrl.replaceAll("\\./", "");
		URL xUrl = null;
		HttpURLConnection xHuc = null;
		BufferedReader in = null;
		try {
			xUrl = new URL(sourceUrl);
			if (xUrl != null) {
				xHuc = (HttpURLConnection) xUrl.openConnection();
			}
			xHuc.setRequestProperty("Connection", "keep-alive");
			xHuc.setRequestProperty("Cache-Control", "max-age=0");
			xHuc.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			xHuc.setRequestProperty(
					"User-Agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36");
			xHuc.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
			xHuc.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");
			xHuc.setRequestProperty("contentType", "GBK");
			xHuc.connect();
			int isOk = xHuc.getResponseCode();
			String html = null;
			if (isOk == 200) {
				in = new BufferedReader(new InputStreamReader(
						xHuc.getInputStream(), "UTF-8"));
				int chint = 0;
				StringBuffer sb = new StringBuffer();
				while ((chint = in.read()) != -1) {
					sb.append((char) chint);
				}
				html = sb.toString();
			}
			return html;
		} catch (Exception e) {
			System.out.println("error:" + sourceUrl);
			e.printStackTrace();
		} finally {
			if (xHuc != null) {
				xHuc.disconnect();
			}
			IOUtils.closeQuietly(in);
		}
		return null;
	}
	
	// -------------查找结构----------------
	/**
	 * 构建查询的对象
	 * 
	 * @param qas
	 */
	public static void build(List<TaxDoc> taxDocs) {
		TaxDocFinder.build(taxDocs);
	}

	/**
	 * 找到对象
	 * 
	 * @param office
	 * @param code
	 * @return
	 */
	public static TaxDoc find(String sourceUrl) {
		return TaxDocFinder.getInstance().find(sourceUrl);
	}

	/**
	 * 清除缓存
	 */
	public static void remove() {
		TaxDocFinder.remove();
	}

	private static class TaxDocFinder {
		public static ThreadLocal<Map<String, TaxDoc>> context = new ThreadLocal<Map<String, TaxDoc>>();
		private static TaxDocFinder finder = new TaxDocFinder();

		private TaxDocFinder() {}

		public static TaxDocFinder getInstance() {
			return finder;
		}

		public TaxDoc find(String sourceUrl) {
			if (context.get() != null) {
				return context.get().get(sourceUrl);
			}
			return null;
		}

		public static TaxDocFinder build(List<TaxDoc> taxDocs) {
			Map<String, TaxDoc> maps = new HashMap<String, TaxDoc>();
			for (TaxDoc taxDoc : taxDocs) {
				maps.put(taxDoc.getSourceUrl(), taxDoc);
			}
			context.remove();
			context.set(maps);
			return finder;
		}

		public static void remove() {
			context.remove();
		}
	}
}
