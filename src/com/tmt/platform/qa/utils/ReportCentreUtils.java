package com.tmt.platform.qa.utils;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.tmt.base.component.excel.imp.ColumnMapper;
import com.tmt.base.component.excel.util.ExcelExpUtils;

/**
 * 报表工具类
 * @author lifeng
 */
public class ReportCentreUtils {

	
	/**
	 * 按试题类型和试题难度统计试题 -- 导出参数
	 * @param datas
	 * @return
	 */
	public static Map<String,Object> buildQTypeAndQLevelExpParam(List<Map<String,Object>> vaules) {
		String fileName = "统计导出";
		String title = "统计导出";
		List<ColumnMapper> columns = getQTypeAndQLevelColumns();
		return ExcelExpUtils.buildExpParams(fileName, title, columns, vaules);
	}
	
	
	/**
	 * 返回按试题类型和试题难度统计试题 导出的列
	 * @return
	 */
	private static List<ColumnMapper> getQTypeAndQLevelColumns(){
		List<ColumnMapper> columns = Lists.newArrayList();
		columns.add(ColumnMapper.buildExpText("题型", "Q_TYPE"));
		columns.add(ColumnMapper.buildExpText("难度", "Q_LEVEL"));
		columns.add(ColumnMapper.buildExpText("试题数量", "TOPIC_TOTAL"));
		columns.add(ColumnMapper.buildExpText("答疑汇编", "QA_TOTAL"));
		columns.add(ColumnMapper.buildExpText("考试汇编", "EXAM_TOTAL"));
		columns.add(ColumnMapper.buildExpText("自定义", "CUSTOM_TOTAL"));
		return columns;
	}
}
