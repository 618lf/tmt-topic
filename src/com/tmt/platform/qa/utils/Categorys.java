package com.tmt.platform.qa.utils;

import java.util.List;

import com.google.common.collect.Lists;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.TaxDoc;
import com.tmt.platform.qa.service.CategoryService;
import com.tmt.platform.utils.TaxConstants;

public class Categorys {

	private static CategoryService categoryService = SpringContextHolder.getBean(CategoryService.class);
	
	public static List<Category> findCategorys(Qa qa) {
		List<Category> categorys = categoryService.queryCategorys();
		List<Category> _categorys = Lists.newArrayList();
		for( Category category: categorys) {
			if( !TaxConstants.CATEGORYS_OTHER_CODE.equals(category.getCode())
				 && ( category.nameMatches(qa.getContent()) || category.nameMatches(qa.getAnswer()) )) {
				_categorys.add(category);
			}
		}
		return _categorys;
	}
	
	public static Category findOtherCategorys() {
		return categoryService.findOtherCategory();
	}
	
	public static List<Category> findCategorys(TaxDoc doc){
		List<Category> categorys = categoryService.queryCategorys();
		List<Category> _categorys = Lists.newArrayList();
		for( Category category: categorys) {
			if( !TaxConstants.CATEGORYS_OTHER_CODE.equals(category.getCode())
				 && ( category.nameMatches(doc.getDocContent()) || category.nameMatches(doc.getDocName()) )) {
				_categorys.add(category);
			}
		}
		return _categorys;
	}
}
