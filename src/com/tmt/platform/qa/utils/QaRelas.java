package com.tmt.platform.qa.utils;

import java.util.List;

import com.google.common.collect.Lists;
import com.tmt.platform.qa.entity.Category;
import com.tmt.platform.qa.entity.Doc;
import com.tmt.platform.qa.entity.Qa;
import com.tmt.platform.qa.entity.QaRela;

/**
 * 答疑关联信息工具
 * @author lifeng
 *
 */
public class QaRelas {

	/**
	 * 创建关联信息
	 * @param qa
	 * @return
	 */
	public static List<QaRela> buildQaRelas(Qa qa) {
		List<QaRela> qaRelas = Lists.newArrayList();
		//匹配的税种
		List<Category> categorys = Categorys.findCategorys(qa);
		//匹配的文件和文件号
		List<Doc> docs = Docs.docMatches(qa);
		if( (categorys != null && categorys.size() !=0)
				|| (docs != null && docs.size() !=0) ) {
			if((categorys != null && categorys.size() !=0)
					&& (docs != null && docs.size() !=0)) {//笛卡尔集
				for( Category category: categorys) {
					for( Doc doc: docs) {
						QaRela qaRela = new QaRela();
						qaRela.setCategoryId(category.getId());
						qaRela.setLarDocNum(doc.getLarDocNum());
						qaRela.setLarDocName(doc.getLarDocName());
						qaRela.setLarDocContent(doc.getLarDocContent());
						qaRelas.add(qaRela);
					}
				}
			} else if((categorys != null && categorys.size() !=0)) {
				for( Category category: categorys) {
					QaRela qaRela = new QaRela();
					qaRela.setCategoryId(category.getId());
					qaRelas.add(qaRela);
				}
			}else if((docs != null && docs.size() !=0)) {
				for( Doc doc: docs) {
					QaRela qaRela = new QaRela();
					qaRela.setLarDocNum(doc.getLarDocNum());
					qaRela.setLarDocName(doc.getLarDocName());
					qaRela.setLarDocContent(doc.getLarDocContent());
					qaRelas.add(qaRela);
				}
			}
		}
		//添加其他
		if(qaRelas.size() == 0) {
			QaRela qaRela = new QaRela();
			qaRela.setCategoryId(Categorys.findOtherCategorys().getId());
			qaRelas.add(qaRela);
		}
		for(QaRela qaRela: qaRelas) {
			qaRela.setQaId(qa.getId());
			qaRela.setApplyProvince(Qa.YES);
		}
		return qaRelas;
	}
}
