package com.tmt.platform.qa.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.Encodes;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.Qa;

/**
 * Qa 工具类
 * @author lifeng
 *
 */
public final class Qas {
	
	/**
	 * 格式化答案
	 * @param qa
	 * @return
	 */
	public static Qa answerFormate(Qa qa) {
		String answer = qa.getAnswer();
		if (StringUtil3.isNotBlank(answer)) {
			answer = answer.replace("一、", "<br/>一、");
			answer = answer.replace("二、", "<br/>二、");
			answer = answer.replace("三、", "<br/>三、");
			answer = answer.replace("四、", "<br/>四、");
			answer = answer.replace("(一)", "<br/>(一)");
			answer = answer.replace("(二)", "<br/>(二)");
			answer = answer.replace("(三)", "<br/>(三)");
			answer = answer.replace("(四)", "<br/>(四)");
			answer = answer.replace("（一）", "<br/>（一）");
			answer = answer.replace("（二）", "<br/>（二）");
			answer = answer.replace("（三）", "<br/>（三）");
			answer = answer.replace("（四）", "<br/>（四）");
			answer = answer.replace("1、", "<br/>1、");
			answer = answer.replace("2、", "<br/>2、");
			answer = answer.replace("3、", "<br/>3、");
			answer = answer.replace("4、", "<br/>4、");
			answer = answer.replace("1.", "<br/>1、");
			answer = answer.replace("2.", "<br/>2、");
			answer = answer.replace("3.", "<br/>3、");
			answer = answer.replace("4.", "<br/>4、");
		}
		qa.setAnswer(answer);
		return qa;
	}
	
	/**
	 * 初始化code
	 * @param qa
	 * @return
	 * @throws IOException 
	 */
	public static Qa setCode(Qa qa) {
		String nextSeq = (String)IdGen.getInstance().generateId(qa.getQFrom().getQaCodeSeq());
		try {
			qa.setCode(StringUtil3.appendTo(new StringBuilder(qa.getQFrom().getQaCodePre()), StringUtil3.leftPad(nextSeq, 8, "0")).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return qa;
	}
	//-------------查找结构----------------
	/**
	 * 构建查询的对象
	 * 查找可以 按照 template中的唯一属性
	 * sourceUrl 或  title 优先 sourceUrl
	 * @param qas
	 */
	public static void build(List<Qa> qas, Qa template) {
		QaFinder.build(qas, template);
	}
	
	/**
	 * 找到对象
	 * @param office
	 * @param code
	 * @return
	 */
	public static Qa find(String sourceUrl) {
		return QaFinder.getInstance().find(sourceUrl);
	}
	
	/**
	 * 清除缓存
	 */
	public static void remove(){
		QaFinder.remove();
	}
	
	private static class QaFinder{
		public static ThreadLocal<Map<String,Qa>> context = new ThreadLocal<Map<String,Qa>>();
		private static QaFinder finder = new QaFinder();
		private QaFinder(){}
		public static QaFinder getInstance(){
			return finder;
		}
		public Qa find(String key){
			String _key = Encodes.encodeBase64(key.getBytes());
			if(context.get() != null) {
				return context.get().get(_key);
			}
			return null;
		}
		/**
		 * key 使用Base64编码
		 * @param qas
		 * @param template
		 * @return
		 */
		public static QaFinder build(List<Qa> qas, Qa template){
			Map<String,Qa> maps = new HashMap<String,Qa>();
			for( Qa qa: qas ) {
				String key = null;
				if(StringUtil3.isNotBlank(template.getSourceUrl())) {
					key = qa.getSourceUrl();
				} else if(StringUtil3.isNotBlank(template.getTitle())) {
					key = qa.getTitle() + qa.getPublishDate();
				}
				key = Encodes.encodeBase64(key.getBytes());
				maps.put(key, qa);
			}
			context.remove();
			context.set(maps);
			return finder;
		}
		public static void remove(){
			context.remove();
		}
	}
}
