package com.tmt.platform.qa.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tmt.base.common.utils.StringUtil3;
import com.tmt.platform.qa.entity.RelaEntity;

/**
 * qaRelas,TopicRelas 帮助类
 * @author lifeng
 */
public class Relas {

	
	/**
	 * 得到 税种s
	 * @param qaRelas
	 * @return
	 */
	public static String getCategorys(List<? extends RelaEntity> relas){
		Set<String> categorys = new HashSet<String>();
		for(RelaEntity qaRela: relas){
			categorys.add(qaRela.getCategoryName());
		}
		return StringUtil3.join(categorys, ",");
	}
	
	/**
	 * 得到 税种明细s
	 * @param qaRelas
	 * @return
	 */
	public static String getCategoryItems(List<? extends RelaEntity> relas){
		Set<String> categorys = new HashSet<String>();
		for(RelaEntity qaRela: relas){
			categorys.add(qaRela.getCategoryItemName());
		}
		return StringUtil3.join(categorys, ",");
	}
}
