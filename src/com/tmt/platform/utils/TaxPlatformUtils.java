package com.tmt.platform.utils;

import java.util.List;

import com.google.common.collect.Lists;
import com.tmt.base.common.utils.CacheUtils;
import com.tmt.base.system.entity.LabelVO;
import com.tmt.platform.enums.QFrom;
import com.tmt.platform.enums.QLevel;
import com.tmt.platform.enums.QType;

/**
 * 工具类
 * @author lifeng
 */
public class TaxPlatformUtils {

	//使用系统缓存
	private static String CACHE_PREFIX = "TP#";
	/**
	 * 试题类型
	 * @return
	 */
	public static List<LabelVO> getQTypes(){
		 List<LabelVO> labels = Lists.newArrayList();
		 for(QType type: QType.values()) {
			 labels.add(LabelVO.newLabel(type.name(), type.getName()));
		 }
		 return labels;
	}
	
	/**
	 * 试题类型
	 * @return
	 */
	public static List<LabelVO> getQLevels(){
		 List<LabelVO> labels = Lists.newArrayList();
		 for(QLevel type: QLevel.values()) {
			 labels.add(LabelVO.newLabel(type.name(), type.getName()));
		 }
		 return labels;
	}
	
	/**
	 * 试题类型
	 * @return
	 */
	public static List<LabelVO> getQFroms(){
		 List<LabelVO> labels = Lists.newArrayList();
		 for(QFrom type: QFrom.values()) {
			 labels.add(LabelVO.newLabel(type.name(), type.getName()));
		 }
		 return labels;
	}
	
	//---------------提供缓存的操作---------------
	public static <T> void put(String key, T value) {
		String _key = new StringBuilder(CACHE_PREFIX).append(key).toString();
		CacheUtils.put(_key, value);
	}
	public static <T> T get(String key) {
		String _key = new StringBuilder(CACHE_PREFIX).append(key).toString();
		return CacheUtils.get(_key);
	}
	public static void delete(String key) {
		String _key = new StringBuilder(CACHE_PREFIX).append(key).toString();
		CacheUtils.evict(_key);
	}
}
