package com.tmt.platform.utils;

/**
 * 税务平台系统常量设置
 * @author lifeng
 */
public class TaxConstants {

	//------------------自增长CODE------------------------------------
	/**
	 * 答疑 code自增长
	 */
	public static final String QA_CODE_SEQ = "QA_CODE_SEQ";
	/**
	 * 自定义 code自增长
	 */
	public static final String CUSTOM_CODE_SEQ = "CUSTOM_CODE_SEQ";
	/**
	 * 考试 code自增长
	 */
	public static final String EXAM_CODE_SEQ = "EXAM_CODE_SEQ";
	/**
	 * 试题 code自增长
	 */
	public static final String TOPIC_CODE_SEQ = "TOPIC_CODE_SEQ";
	
	//-------------------缓存----------------------------------
	public static final String PLATFORM_CATEGORY_CACHE_KEY = "PLATFORM_CATEGORY_CACHE_KEY";
	
	//-------------------权限--------------------------------------------
	//初审的权限
	public static final String TOPIC_AUDIT_INIT_VERITY = "TOPIC:AUDIT:INIT_VERITY";
	//终审的权限
	public static final String TOPIC_AUDIT_FINALLY_VERITY = "TOPIC:AUDIT:FINALLY_VERITY";
	
	//-------------------过滤数据权限(答疑) 区域类型---------------------
	public static final String QA_FILTER_AREA_TYPE = "PLATFORM_QA_FILTER_AREAS";
	//-------------------过滤数据权限(答疑编题) 税种类型---------------------
	public static final String QA_FILTER_TOPIC_CATEGORY_TYPE = "PLATFORM_QA_FILTER_TOPIC_CATEGORYS";
	//-------------------过滤数据权限(试题) 查看所有权限---------------------
	public static final String TOPIC_VIEW_ALL = "PLATFORM:TOPIC:VIEW:ALL";
	public static final String TOPIC_UPDATE_ALL = "PLATFORM:TOPIC:UPDATE:ALL";
	//-------------------过滤数据权限(答疑) 答疑汇编修改所有用户的数据---------------------
	public static final String QA_UPDATE_ALL = "PLATFORM:QA:UPDATE:ALL";
	//-------------------过滤数据权限(测试)---------------------
	public static final String QA_FILTER_2013_YEAR = "PLATFORM:QA:FILTER:2013Y";
	public static final String QA_FILTER_2012_YEAR = "PLATFORM:QA:FILTER:2012Y";
	public static final String QA_FILTER_2011_YEAR = "PLATFORM:QA:FILTER:2011Y";
	public static final String QA_FILTER_ALL_YEAR = "PLATFORM:QA:FILTER:ALLY";
	
	//----------------------答疑汇编抽取数据路径 本地路径, 网络路径-------------------
	/**
	 * 默认可直接处理的抽取数据任务
	 * DefaultProcessingFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_DEFAULT = "QA_FETCH_DATA_PATH_CODE_DEFAULT";
	
	/**
	 * 国家税务总局获取数据的路径
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_CT = "QA_FETCH_DATA_PATH_CODE_CT";
	
	/**
	 * 深圳地税抽取数据
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_SZDS = "QA_FETCH_DATA_PATH_CODE_SZDS";
	
	/**
	 * 深圳国税抽取数据
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_SZGS = "QA_FETCH_DATA_PATH_CODE_SZGS";
	
	/**
	 * 广东省国家税务局
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_GDGS = "QA_FETCH_DATA_PATH_CODE_GDGS";
	
	/**
	 * 华东地区，默认可直接处理的抽取数据任务
	 * HDDefaultFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_DEFAULT_HD = "QA_FETCH_DATA_PATH_CODE_DEFAULT_HD";
	
	/**
	 * 华北地区，默认可直接处理的抽取数据任务
	 * HDDefaultFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_DEFAULT_HB = "QA_FETCH_DATA_PATH_CODE_DEFAULT_HB";
	
	/**
	 * 华北地区，处理content修改为title的抽取数据任务
	 * HDDefaultFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_DEFAULT_HB_EDIT_CONTENT = "QA_FETCH_DATA_PATH_CODE_DEFAULT_HB_EDIT_CONTENT";
	
	/**
	 * 西南地区，默认可直接处理的抽取数据任务
	 * XNDefaultFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_DEFAULT_XN = "QA_FETCH_DATA_PATH_CODE_DEFAULT_XN";
	
	/**
	 * 西南地区，处理content修改为title的抽取数据任务
	 * XNDefaultFetchDataTask
	 */
	public static final String QA_FETCH_DATA_PATH_CODE_EDIT_XN= "QA_FETCH_DATA_PATH_CODE_EDIT_XN";
	
	
	/**
	 * 税收法规
	 */
	public static final String DOC_FETCH_DATA_PATH_CODE = "DOC_FETCH_DATA_PATH_CODE";
	
	//-------------------------通用行业CODE----------------------------
	public static final String INDUTRY_PUBLIC_CODE = "000";
	public static final String BUSINESS_PUBLIC_CODE = "003";
	//-------------------------其他税种CODE----------------------------
	public static final String CATEGORYS_OTHER_CODE = "000";
}
