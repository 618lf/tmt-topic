package com.tmt.taxsns.front.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tmt.base.common.utils.CookieUtils;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.utils.UserUtils;
import com.tmt.base.system.web.LoginController;

/**
 * 网站端登录 -- 会员登录
 * 逻辑基本上和后台登录一样,只是返回的页面不同，但继承会使用后台登录的url风格。
 * 支持form表单登录
 * 支持ajax登录   ---返回的错误码一致
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class MemLoginController extends BaseController{

	/**
	 * 用户登录请求
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model){
		String username = CookieUtils.getCookie(request, FormAuthenticationFilter.DEFAULT_USERNAME_PARAM);
		if(UserUtils.isRemembered()) {
			username = UserUtils.getUser().getLoginName();//默认使用登录名
		}
		if( username != null ) {
		   model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
		}
		return "/taxsns/front/MemLogin";
	}
	
	/**
	 * 用户登录验证
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@RequestParam(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String username, HttpServletRequest request, HttpServletResponse response, Model model){
		model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
		model.addAttribute("isValidateCodeLogin", LoginController.isValidateLoginCode(username, false));//只判断，不改变
		return "/taxsns/front/MemLogin";
	}
}
