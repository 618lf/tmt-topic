package com.tmt.taxsns.front.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Member;
import com.tmt.base.system.security.AuthenticationRealm;
import com.tmt.base.system.security.Principal;
import com.tmt.base.system.service.MemberService;
import com.tmt.base.system.service.SystemService;
import com.tmt.base.system.utils.Members;
import com.tmt.base.system.web.AuthLoginController;
import com.tmt.weixin.base.api.entity.SnsToken;
import com.tmt.weixin.base.utils.WechatClient;
import com.tmt.weixin.entity.PubAccount;
import com.tmt.weixin.utils.PubAccounts;

/**
 * 微信授权登录
 * @author lifeng
 *
 */
@Controller
@RequestMapping(value = "${frontPath}/wechat")
public class WxLoginController extends AuthLoginController {

	@Autowired
	private SystemService systemService;
	@Autowired
	private AuthenticationRealm realm;
	@Autowired
	private MemberService memberService;
	
	/**
	 * 微信认证  -- 如果成功则返回一个 如果客户端的IP地址不是微信的服务器，则返回登录界面
	 * 如果只需要用户的openid, 则不需要获取用户的信息
	 * @param code  -- 授权码
	 * @param state --菜单标识
	 * @return
	 */
	@RequestMapping(value="oAuth")
	public String wechatOAuth(String code, String state, HttpServletRequest request, HttpServletResponse response){
		Member member = null;
		if(StringUtil3.isNotBlank(code)) {//必须要有code
			PubAccount pubAccount = PubAccounts.getAuthPubAccount();
			SnsToken token = WechatClient.oauth2AccessToken(pubAccount.getAppId(),
					pubAccount.getAppSecret(), code);
			
			if (token != null && StringUtil3.isNotBlank(token.getOpenid())) {//必须要有openid
				member = this.memberService.findWeixinMember(token.getOpenid(), token.getUnionid());
				if (member == null) {
					com.tmt.weixin.base.api.entity.User user = WechatClient.userinfo(token.getAccess_token(),token.getOpenid());
					member = Members.registerWeixinMember(user);
				}
			}
		}
		if( member != null) {
			//用member去登录
			Principal principal = new Principal(member,realm.getName());
			this.loginShiro(request, response, principal);//登录成功
			return "system/SysIndex";
		}
		return "system/SysLogin";
	}
}
