package com.tmt.taxsns.front.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.web.BaseController;

/**
 * 网站Controller
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class TaxsnsController extends BaseController{

	/**
	 * 网站首页
	 */
	@RequestMapping
	public String index(Model model) {
		model.addAttribute("description", "税务知识学习课堂");
		model.addAttribute("keywords", "税务 知识");
		return "/taxsns/front/FrontIndex";
	}
}
