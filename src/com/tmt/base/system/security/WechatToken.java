package com.tmt.base.system.security;

import org.apache.shiro.authc.HostAuthenticationToken;

/**
 * 微信认证信息收集
 * 只能收集到用户的openId 但可以知道访问的主机是不是微信的服务器
 * @author lifeng
 */
public class WechatToken implements HostAuthenticationToken {

	private static final long serialVersionUID = -8022183274691079639L;
	
	private String openId;
	private String unionId;
	private String host;
	
	public WechatToken(){}
	
	public WechatToken(String openId) {
		this.openId = openId;
	}
	
	public WechatToken(String openId, String unionId) {
		this.openId = openId;
		this.unionId = unionId;
	}
	
	public WechatToken(String openId, String unionId, String host) {
		this.openId = openId;
		this.unionId = unionId;
		this.host = host;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public Object getPrincipal() {
		return this.getOpenId();
	}

	@Override
	public Object getCredentials() {
		return this.getOpenId();
	}

	@Override
	public String getHost() {
		return host;
	}
}
