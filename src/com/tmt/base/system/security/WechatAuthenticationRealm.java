package com.tmt.base.system.security;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

import com.tmt.base.system.entity.Member;
import com.tmt.base.system.security.exception.AccountLockException;
import com.tmt.base.system.service.MemberService;

/**
 * 微信授权认证
 * @author lifeng
 */
public class WechatAuthenticationRealm extends AbstractAuthorizingRealm{

	
	private MemberService memberService;
	
	/**
	 * 只处理微信登录
	 */
	@Override
	public boolean supports(AuthenticationToken token) {
		return token instanceof WechatToken;
	}

	/**
	 * 授权 -- 存储在 cache中
	 * 怎么管理缓存,需要提供一个统一的管理的地方 
	 * 存储的是菜单和按钮的授权
	 * 貌似是通用的：Subect在判断权限时，并不知道是哪一个: Realm
	 * 都会执行，现在使用的权限是同一套，所以可以只加载一个就够了
	 * bug ，如果没有的权限，则这个Realm 没有缓存，所有会一值执行加载
	 * 所以返回一个空的
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		return info;
	}
	
	/**
	 * 认证 
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo( AuthenticationToken authcToken) throws AuthenticationException {
		if( authcToken instanceof WechatToken ) {
			WechatToken token = (WechatToken)authcToken;
			//验证用户的服务器 --暂时不做
			Member user = this.memberService.findWeixinMember(token.getOpenId(), token.getUnionId());
			if( user != null) {
				if( user.isLocked() ) {
					throw new AccountLockException("账户锁定,请联系管理员.");
				}
				return new SimpleAuthenticationInfo(new Principal(user, getName()), //存储用户信息到缓存
						   user.getOpenid(), getName());
			}
		}
		return null;
	}
	
	/**
	 * 设置简单的验证器
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		//简单的密码匹配器
		SimpleCredentialsMatcher matcher = new SimpleCredentialsMatcher();
		setCredentialsMatcher(matcher);
	}
	
	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
}
