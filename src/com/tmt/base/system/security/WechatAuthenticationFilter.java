package com.tmt.base.system.security;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;

import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Member;
import com.tmt.base.system.service.MemberService;
import com.tmt.base.system.utils.Members;
import com.tmt.weixin.base.api.entity.SnsToken;
import com.tmt.weixin.base.api.entity.User;
import com.tmt.weixin.base.utils.WechatClient;
import com.tmt.weixin.entity.PubAccount;
import com.tmt.weixin.utils.PubAccounts;

/**
 * 
 * 微信授权过滤器
 * @author lifeng
 * 
 */
public class WechatAuthenticationFilter extends AuthenticatingFilter {

	public static final String DEFAULT_CODE_PARAM = "code";
	// 授权的code
	private String codeParam = DEFAULT_CODE_PARAM;
	private String loginUrl = DEFAULT_LOGIN_URL;
	private MemberService memberService;

	/**
	 * 收集授权信息 ,授权之后访问该访问的页面,连接地址不用改 有一个特殊需求：就是需要自动注册
	 */
	@Override
	protected AuthenticationToken createToken(ServletRequest request,
			ServletResponse response) throws Exception {
		String code = WebUtils.getCleanParam(request, this.getCodeParam());
		if(StringUtil3.isNotBlank(code)) {//必须要有code
			PubAccount pubAccount = PubAccounts.getAuthPubAccount();
			SnsToken token = WechatClient.oauth2AccessToken(pubAccount.getAppId(),
					pubAccount.getAppSecret(), code);
			if (token != null && StringUtil3.isNotBlank(token.getOpenid())) {//必须要有openid
				Member member = this.memberService.findWeixinMember(token.getOpenid(), token.getUnionid());
				if (member == null) {
					User user = WechatClient.userinfo(token.getAccess_token(),token.getOpenid());
					member = Members.registerWeixinMember(user);
				}
				WechatToken authToken = new WechatToken();
				authToken.setOpenId(member.getOpenid());
				authToken.setUnionId(member.getUnionid());
				return authToken;
			}
		}
		return null;
	}

	/**
	 * 失败，则执行跳转到登录页面的动作
	 */
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		if (isLoginRequest(request, response)) { // 是登录操作
			return executeLogin(request, response);
		}
		redirectToLogin(request, response);
		return false;
	}

	public String getCodeParam() {
		return codeParam;
	}

	public void setCodeParam(String codeParam) {
		this.codeParam = codeParam;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}
}
