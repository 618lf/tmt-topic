package com.tmt.base.system.service;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.Maps;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.dao.MemberDao;
import com.tmt.base.system.entity.Member;
import com.tmt.base.system.entity.User;

@Service
public class MemberService extends BaseService<Member, String>{

	@Autowired
	private MemberDao memberDao;
	@Autowired
	private UserService userService;
	
	@Override
	protected BaseIbatisDAO<Member, String> getEntityDao() {
		return memberDao;
	}
	
	/**
	 * 保存会员
	 * @param member
	 */
	@Transactional
	public void save(Member member) {
		if( IdGen.isInvalidId(member.getId()) ) { //添加用户
			User user = member.getSaveAbledUser();
			String userId = this.userService.save(user);
			//设置Id
			member.setId(userId);
			this.insert(member);
		} else {
			User user = member;
			this.userService.save(user);
			this.update(member);
		}
	}
	
	/**
	 * 通过openId 和  unionId 获取微信用户信息
	 * 必须要有一个查询条件
	 * @param openId
	 * @param uninonId
	 * @return
	 */
	public Member findWeixinMember(String openId, String unionId) {
		Map<String,Object> param = Maps.newHashMap();
		if(StringUtil3.isNotBlank(openId)) {
			param.put("OPENID", openId);
		}
		if(StringUtil3.isNotBlank(unionId)) {
			param.put("UNIONID", unionId);
		}
		if(param.isEmpty()) return null;
		return this.queryForObject("findWeixinMember", param);
	}
}
