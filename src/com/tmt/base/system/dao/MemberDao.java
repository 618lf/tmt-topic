package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Member;

/**
 * 会员管理
 * @author lifeng
 */
@Repository
public class MemberDao extends BaseIbatisDAO<Member, String>{

	@Override
	protected String getNamespace() {
		return "SYS_MEMBER";
	}
}
