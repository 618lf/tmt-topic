package com.tmt.base.system.utils;

import java.util.List;

import com.tmt.Constants;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.entity.Group;
import com.tmt.base.system.entity.Member;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.service.GroupService;
import com.tmt.base.system.service.MemberService;
import com.tmt.base.system.service.OfficeService;


/**
 * 会员管理
 * @author lifeng
 */
public class Members {

	private static MemberService memberService = SpringContextHolder.getBean(MemberService.class);
	private static OfficeService officeService = SpringContextHolder.getBean(OfficeService.class);
	private static GroupService  groupService  = SpringContextHolder.getBean(GroupService.class);
	
	/**
	 * 注册微信会员
	 * @param user
	 * @return
	 */
	public static Member registerWeixinMember(com.tmt.weixin.base.api.entity.User user){
		Member member = new Member();
		member.setLoginName(user.getOpenid());
		member.setName(user.getNickname());
		member.setNickName(user.getNickname());
		member.setSex(user.getSex().toString());
		member.setCity(user.getCity());
		member.setProvince(user.getProvince());
		member.setOpenid(user.getOpenid());
		member.setUnionid(user.getUnionid());
		
		//设置默认组织结构信息
		member.setOfficeId(getWeixinMemberOffice());
		//设置默认用户组信息
		member.setGroupIds(getWeixinMemberGroups());
		//注册微信会员
		memberService.save(member);
		return member;
	}
	
	/**
	 * 如果不区分，则统一
	 * 得到微信注册会员的默认组织结构
	 * @return
	 */
	public static String getWeixinMemberOffice(){
		Office office = officeService.getByCode(Constants.getValueFromDict(Constants.WX_R_OFFICE_CODE));
		if(office == null) {
			throw new BaseRuntimeException("用户注册异常：无默认的组织结构");
		}
		return office.getId();
	}
	
	/**
	 * 如果不区分，则统一
	 * 得到网站注册会员的默认组织结构
	 * @return
	 */
	public static String getSiteMemberOffice(){
		Office office = officeService.getByCode(Constants.getValueFromDict(Constants.SITE_R_OFFICE_CODE));
		if(office == null) {
			throw new BaseRuntimeException("用户注册异常：无默认的组织结构");
		}
		return office.getId();
	}
	
	/**
	 * 如果不区分，则统一
	 * 得到微信注册会员的默认用户组
	 * 可以有多个
	 * @return
	 */
	public static String getWeixinMemberGroups(){
		List<Group> groups = groupService.findByCodes(Constants.getValueFromDict(Constants.WX_R_GROUP_CODES));
		if(groups == null) {
			throw new BaseRuntimeException("用户注册异常：无默认的用户组");
		}
		StringBuilder groupIds = new StringBuilder();
		for(Group group: groups) {
			groupIds.append(group.getId()).append(",");
		}
		return groupIds.toString();
	}
	
	/**
	 * 如果不区分，则统一
	 * 得到网站注册会员的默认用户组
	 * 可以有多个
	 * @return
	 */
	public static String getSiteMemberGroups(){
		List<Group> groups = groupService.findByCodes(Constants.getValueFromDict(Constants.SITE_R_GROUP_CODES));
		if(groups == null) {
			throw new BaseRuntimeException("用户注册异常：无默认的用户组");
		}
		StringBuilder groupIds = new StringBuilder();
		for(Group group: groups) {
			groupIds.append(group.getId()).append(",");
		}
		return groupIds.toString();
	}
}
