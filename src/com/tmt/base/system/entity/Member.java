package com.tmt.base.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.tmt.Constants;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 会员管理 -- 用户扩展信息  -- 在原有用户表基础上扩展
 * 这两个表的Id一致 会员即使用户,统一使用User的ID
 * @author lifeng
 */
public class Member extends User{
	
	private static final long serialVersionUID = 277208029064703880L;

	//业务需要的字段
	private String professionalTitle;//职称
	private String professionalLevel;//职称等级
	private String professionalQualification;//职业资格
	private String company;
	private String department;
	
	//微信需要的字段
	private String openid;//openid
	private String unionid;//unionid
	private String subscribe;//是否订阅
	private Date subscribeDate;//关注日期
	private Date lastActiveDate;//最后一次交互(发信息,点菜单)
	private String idleTime;//空闲时长，按小时，超过48小时的按天
	
	public void setIdleTime(String idleTime) {
		this.idleTime = idleTime;
	}
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	public Date getLastActiveDate() {
		return lastActiveDate;
	}
	public void setLastActiveDate(Date lastActiveDate) {
		this.lastActiveDate = lastActiveDate;
	}
	public String getSubscribe() {
		return subscribe;
	}
	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	public Date getSubscribeDate() {
		return subscribeDate;
	}
	public void setSubscribeDate(Date subscribeDate) {
		this.subscribeDate = subscribeDate;
	}
	public String getProfessionalTitle() {
		return professionalTitle;
	}
	public void setProfessionalTitle(String professionalTitle) {
		this.professionalTitle = professionalTitle;
	}
	public String getProfessionalLevel() {
		return professionalLevel;
	}
	public void setProfessionalLevel(String professionalLevel) {
		this.professionalLevel = professionalLevel;
	}
	public String getProfessionalQualification() {
		return professionalQualification;
	}
	public void setProfessionalQualification(String professionalQualification) {
		this.professionalQualification = professionalQualification;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	
	/**
	 * 不会自动添加主键,请手动维护
	 */
	@Override
	public String prePersist(Serializable key) {
		return this.id;
	}
	
	public String getIdleTime() {
		Long lastActiveTime = this.getLastActiveDate() == null?null:this.getLastActiveDate().getTime();
		if( lastActiveTime == null) {
			this.setIdleTime("很久");
		} else {
			Long currTime = DateUtil3.getTimeStampNow().getTime();
			double idleTime = (currTime - lastActiveTime)*1.0 /1000/60/60;//小时
			String idleTimeStr = "小时";
			if( idleTime > Constants.WX_AGENT_API_VALID_TIME) {
				idleTime = idleTime/24;//天
				idleTimeStr = "天";
			}
			this.setIdleTime(StringUtil3.format("%s%s", idleTime,idleTimeStr));
		}
		
		return idleTime;
	}
	
	/**
	 * 由于 prePersist  重写了， 插入时不能使用上转型对象
	 * 一般情况不需要使用
	 * 
	 * 获得User的数据
	 * @return
	 */
	@JSONField(serialize=false)
	public User getSaveAbledUser(){
		User user = new User();
		user.setLoginName(this.getOpenid());
		user.setName(this.getNickName());
		user.setNickName(this.getNickName());
		user.setSex(this.getSex().toString());
		user.setCity(this.getCity());
		user.setProvince(this.getProvince());
		//设置默认组织结构信息
		user.setOfficeId(this.getOfficeId());
		//设置默认用户组信息
		user.setGroupIds(this.getGroupIds());
		return user;
	}
}
