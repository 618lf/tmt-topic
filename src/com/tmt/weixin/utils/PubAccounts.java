package com.tmt.weixin.utils;

import com.tmt.weixin.entity.PubAccount;

/**
 * 公众号帮助类
 * @author lifeng
 */
public class PubAccounts {

	/**
	 * 获取授权登录的服务号,放入缓存管理
	 * 现在使用测试号来测试
	 * @return
	 */
	public static PubAccount getAuthPubAccount(){
		PubAccount account = new PubAccount();
		account.setAppId("wxeff9811824f0f3b6");
		account.setAppSecret("547fcb9f399c1b8f6cf029993c8a337b");
		return account;
	}
}
