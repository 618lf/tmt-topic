package com.tmt.weixin.base.msg;

import com.tmt.weixin.base.msg.MsgType.RespType;
import com.tmt.weixin.base.resource.Media;

/**
 * 回复图片消息 
 * @author lifeng
 */
public class RespMsgImage extends RespMsg
{
	private Media image;
	
	public RespMsgImage(BaseMsg req, Media image)
	{
		super(req, RespType.image.name());
		this.image = image;
	}

	public Media getImage()
	{
		return image;
	}

	public void setImage(Media image)
	{
		this.image = image;
	}

	@Override
	public String subWrite() {
		StringBuilder xmls = new StringBuilder();
		xmls.append("<Image><MediaId>").append(this.wrapCdata(this.image)).append("</MediaId></Image>");
		return xmls.toString();
	}
}
