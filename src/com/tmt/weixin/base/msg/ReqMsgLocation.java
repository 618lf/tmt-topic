package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 地理位置消息请求
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class ReqMsgLocation extends ReqMsg {
	// 地理位置维度
	private String location_X;
	// 地理位置经度
	private String location_Y;
	// 地图缩放大小
	private String scale;
	// 地理位置信息
	private String label;

	public String getLocation_X() {
		return location_X;
	}

	public void setLocation_X(String locationX) {
		this.location_X = locationX;
	}

	public String getLocation_Y() {
		return location_Y;
	}

	public void setLocation_Y(String locationY) {
		this.location_Y = locationY;
	}

	public String getScale() {
		return scale;
	}

	public String getLabel() {
		return label;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public void read(Document document) {
		super.read(document);
		this.location_X = readFormDocument(document, "Location_X");
		this.location_Y = readFormDocument(document, "Location_Y");
		this.scale = readFormDocument(document, "Scale");
		this.label = readFormDocument(document, "Label");
	}
}
