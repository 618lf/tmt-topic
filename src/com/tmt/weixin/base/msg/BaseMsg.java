package com.tmt.weixin.base.msg;



/**
 * 消息基类
 * @author lifeng
 */
public class BaseMsg extends MsgHead {

	public void setHead(MsgHead head) {
		this.toUserName = head.getToUserName();  
		this.fromUserName = head.getFromUserName(); 
		this.createTime  = head.getCreateTime();
		this.msgType = head.getMsgType();
		this.event = head.getEvent();
	}
}
