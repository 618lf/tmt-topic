package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 视频请求消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class ReqMsgVideo extends ReqMsgMedia {
	
	// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	private String thumbMediaId;

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
	
	// 因为用户不能发送音乐消息给我们，因此没有实现
	@Override
	public void read(Document document) {
		super.read(document);
		this.thumbMediaId = readFormDocument(document, "thumbMediaId");
	}
}
