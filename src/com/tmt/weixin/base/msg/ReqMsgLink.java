package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 链接请求消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class ReqMsgLink extends ReqMsg {
	// 消息标题
	private String title;
	// 消息描述
	private String description;
	// 消息链接
	private String url;

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public void read(Document document) {
		super.read(document);
		this.title = readFormDocument(document, "Title");
		this.description = readFormDocument(document, "Description");
		this.url = readFormDocument(document, "Url");
	} 
}
