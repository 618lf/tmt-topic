package com.tmt.weixin.base.msg;

import com.tmt.weixin.base.msg.MsgType.RespType;

/**
 * 文本应答消息
 * 
 * @author lifeng
 * 
 */
public class RespMsgText extends RespMsg {
	// 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
	private String content;

	public RespMsgText(BaseMsg req, String content) {
		super(req, RespType.text.name());
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String subWrite() {
		return new StringBuilder("<Content>").append(this.wrapCdata(content)).append("</Content>").toString();
	}
}
