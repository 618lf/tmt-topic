package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 多媒体请求消息的公共类。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public abstract class ReqMsgMedia extends ReqMsg {

	// 消息媒体id，可以调用多媒体文件下载接口拉取数据。
	protected String mediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public void read(Document document) {
		super.read(document);
		this.mediaId = readFormDocument(document, "MediaId");
	}
}
