package com.tmt.weixin.base.msg;


/**
 * 请求、响应、事件 消息类型
 * 
 * @author lifeng
 */
public class MsgType {

	/**
	 * 用户请求消息类型
	 * 
	 * @author Administrator
	 * 
	 */
	public static enum ReqType {
		text, image, link, location, video, voice, event;
	}

	/**
	 * 事件消息类型包括，菜单上的的
	 * 
	 * @author Administrator
	 * 
	 */
	public static enum EventType {
		subscribe, unsubscribe, scan, LOCATION, CLICK, VIEW, scancode_waitmsg, scancode_push, location_select, pic_weixin, pic_photo_or_album, pic_sysphoto;
	}

	/**
	 * 应答消息类型定义。
	 * 
	 * @author cailx
	 * 
	 */
	public static enum RespType {
		image, music, news, text, video, voice
	}
}
