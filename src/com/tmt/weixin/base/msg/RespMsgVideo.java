package com.tmt.weixin.base.msg;

import com.tmt.weixin.base.msg.MsgType.RespType;
import com.tmt.weixin.base.resource.MediaEx;

/**
 * 回复视频消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class RespMsgVideo extends RespMsg {
	private MediaEx video;

	public RespMsgVideo(BaseMsg req, MediaEx video) {
		super(req, RespType.video.name());
		this.video = video;
	}

	public MediaEx getVideo() {
		return video;
	}

	public void setVideo(MediaEx video) {
		this.video = video;
	}

	@Override
	public String subWrite() {
		StringBuilder sb = new StringBuilder();
		sb.append("<Video>");
		sb.append("<MediaId>").append(this.wrapCdata(this.video.getMediaId())).append("</MediaId>");
		sb.append("<Title>").append(this.wrapCdata(this.video.getTitle())).append("</Title>");
		sb.append("<Description>").append(this.wrapCdata(this.video.getDescription())).append("</Description>");
		sb.append("</Video>");
		return sb.toString();
	}

}
