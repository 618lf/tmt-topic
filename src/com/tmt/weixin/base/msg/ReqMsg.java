package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 请求消息基类  -- 读取xml中的数据
 * @author lifeng
 */
public abstract class ReqMsg extends BaseMsg {

	// 消息id，64位整型
	protected String msgId;
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
	public void read(Document document) {
		this.msgId = readFormDocument(document, "MsgId");
	}
}
