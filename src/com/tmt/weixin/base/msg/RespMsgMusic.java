package com.tmt.weixin.base.msg;

import com.tmt.weixin.base.msg.MsgType.RespType;
import com.tmt.weixin.base.resource.Music;

/**
 * 回复音乐消息
 * @author lifeng
 *
 */
public class RespMsgMusic extends RespMsg
{
	private Music music;
    
    public RespMsgMusic(BaseMsg req, Music music)
    {
    	super(req, RespType.music.name());
    	this.music = music;
    }

	public Music getMusic()
	{
		return music;
	}

	public void setMusic(Music music)
	{
		this.music = music;
	}

	@Override
	public String subWrite() {
		StringBuilder sb = new StringBuilder();
		sb.append("<Music>");
		sb.append("<Title>").append(this.wrapCdata(this.music.getTitle())).append("</Title>");
		sb.append("<Description>").append(this.wrapCdata(this.music.getDescription())).append("</Description>");
		sb.append("<MusicUrl>").append(this.wrapCdata(this.music.getMusicUrl())).append("</MusicUrl>");
		sb.append("<HQMusicUrl>").append(this.wrapCdata(this.music.getHqmusicUrl())).append("</HQMusicUrl>");		
		sb.append("<ThumbMediaId>").append(this.wrapCdata(this.music.getThumbMediaId())).append("</ThumbMediaId>");
		sb.append("</Music>");
		return sb.toString();
	}
}
