package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 点击菜单跳转链接时的事件推送
 */
public class MenuEventMsgLocationSelect extends MenuEventMsg {

	// 地理位置维度
	private String location_X;
	// 地理位置经度
	private String location_Y;
	// 地图缩放大小
	private String scale;
	// 地理位置信息
	private String label;
	private String poiname;

	public String getLocation_X() {
		return location_X;
	}

	public void setLocation_X(String location_X) {
		this.location_X = location_X;
	}

	public String getLocation_Y() {
		return location_Y;
	}

	public void setLocation_Y(String location_Y) {
		this.location_Y = location_Y;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPoiname() {
		return poiname;
	}

	public void setPoiname(String poiname) {
		this.poiname = poiname;
	}

	@Override
	public void read(Document document) {
		super.read(document);
		this.location_X = readFormDocument(document, "Location_X");
		this.location_Y = readFormDocument(document, "Location_Y");
		this.scale = readFormDocument(document, "Scale");
		this.label = readFormDocument(document, "Label");
		this.poiname = readFormDocument(document, "Poiname");
	}
}
