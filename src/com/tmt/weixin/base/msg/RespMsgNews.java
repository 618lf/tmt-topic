package com.tmt.weixin.base.msg;

import java.util.Arrays;
import java.util.List;

import com.tmt.weixin.base.msg.MsgType.RespType;
import com.tmt.weixin.base.resource.Article;

/**
 * 回复图文消息
 * 
 * @author lifeng
 * 
 */
public class RespMsgNews extends RespMsg {
	
	private List<Article> articles;
	private int articleCount;

	public RespMsgNews(BaseMsg req, List<Article> articles) {
		super(req, RespType.news.name());
		setArticles(articles);
		this.articleCount = articles.size();
	}

	public RespMsgNews(BaseMsg req, Article... articles) {
		this(req, Arrays.asList(articles));
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
		this.articleCount = articles.size();
	}

	public int getArticleCount() {
		return articleCount;
	}

	@Override
	public String subWrite() {
		StringBuilder sb = new StringBuilder();
		sb.append("<ArticleCount>"+articles.size()+"</ArticleCount>");
		sb.append("<Articles>");
		for(Article a : articles){
			sb.append("<item>");
			sb.append("<Title>").append(this.wrapCdata(a.getTitle())).append("</Title>");
			sb.append("<Description>").append(this.wrapCdata(a.getDescription())).append("</Description>");
			sb.append("<PicUrl>").append(this.wrapCdata(a.getPicUrl())).append("</PicUrl>");
			sb.append("<Url>").append(this.wrapCdata(a.getUrl())).append("</Url>");
			sb.append("</item>");
		}
		sb.append("</Articles>");
		return sb.toString();
	}
}
