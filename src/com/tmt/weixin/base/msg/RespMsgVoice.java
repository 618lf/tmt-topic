package com.tmt.weixin.base.msg;

import com.tmt.weixin.base.msg.MsgType.RespType;
import com.tmt.weixin.base.resource.Media;

/**
 * 回复语音消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class RespMsgVoice extends RespMsg {
	private Media voice;

	public RespMsgVoice(BaseMsg req, Media voice) {
		super(req, RespType.voice.name());
		this.voice = voice;
	}

	public Media getVoice() {
		return voice;
	}

	public void setVoice(Media voice) {
		this.voice = voice;
	}

	@Override
	public String subWrite() {
		StringBuilder xmls = new StringBuilder();
		xmls.append("<Voice><MediaId>").append(this.wrapCdata(this.voice)).append("</MediaId></Voice>");
		return xmls.toString();
	}

}
