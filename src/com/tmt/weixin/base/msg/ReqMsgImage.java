package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 图片请求消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class ReqMsgImage extends ReqMsgMedia {
	// 图片链接
	private String picUrl;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	
	@Override
	public void read(Document document) {
		super.read(document);
		this.picUrl = readFormDocument(document, "PicUrl");
	}
}
