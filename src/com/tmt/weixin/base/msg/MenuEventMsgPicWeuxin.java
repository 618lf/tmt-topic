package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 弹出微信相册发图器的事件推送
 * pic_photo_or_album 或  pic_weixin 或 pic_sysphoto
 */
public class MenuEventMsgPicWeuxin extends MenuEventMsg {

	private String count;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public void read(Document document) {
		this.count = readFormDocument(document, "Count");
	}
}
