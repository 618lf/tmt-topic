package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 菜单上的事件: 1 点击菜单拉取消息时的事件推送 2 点击菜单跳转链接时的事件推送 3 scancode_push：扫码推事件的事件推送 4
 * scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送 5 pic_sysphoto：弹出系统拍照发图的事件推送 6
 * pic_photo_or_album：弹出拍照或者相册发图的事件推送 7 pic_weixin：弹出微信相册发图器的事件推送 8
 * location_select：弹出地理位置选择器的事件推送
 * 
 * @author lifeng
 */
public class MenuEventMsg extends BaseMsg{
	
	// 事件KEY值，与自定义菜单接口中KEY值对应
	protected String eventKey;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public void read(Document document) {
		this.eventKey = readFormDocument(document, "EventKey");
	}
}
