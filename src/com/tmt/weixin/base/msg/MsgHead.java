package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.tmt.base.common.utils.DateUtil3;

/**
 * 消息头
 * @author lifeng
 */
public class MsgHead {
	
	protected String toUserName;  
	protected String fromUserName;  
	protected String createTime;  
	protected String msgType;
	protected String event;
	
	public MsgHead(){
		this.createTime = String.valueOf(DateUtil3.getTimeStampNow().getTime());
	}
	
	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public void read(Document document) {
		this.toUserName = readFormDocument(document, "ToUserName");
		this.fromUserName = readFormDocument(document, "FromUserName");
		this.createTime =  readFormDocument(document, "CreateTime");
		this.msgType =  readFormDocument(document, "MsgType");
		this.event =  readFormDocument(document, "Event");
	}
	
	public String readFormDocument(Document document, String tagName) {
		NodeList elements = document.getElementsByTagName(tagName);
		if( elements != null && elements.getLength() != 0) {
			return elements.item(0).getTextContent();
		}
		return "";
	}
	
	/**
	 * 根据消息头构建具体的请求消息
	 * @return
	 */
	public BaseMsg buildMsg(Document document) {
		BaseMsg msg = null;
		MsgType.ReqType _msgType = MsgType.ReqType.valueOf(this.msgType);
		if(_msgType == MsgType.ReqType.text) {
			msg = new ReqMsgText();
		} else if( _msgType == MsgType.ReqType.image) {
			msg = new ReqMsgImage();
		} else if( _msgType == MsgType.ReqType.link) {
			msg = new ReqMsgLink();
		} else if( _msgType == MsgType.ReqType.location) {
			msg = new ReqMsgLocation();
		} else if( _msgType == MsgType.ReqType.video) {
			msg = new ReqMsgVideo();
		} else if( _msgType == MsgType.ReqType.voice) {
			msg = new ReqMsgVoice();
		} else if( _msgType == MsgType.ReqType.event) {//事件(基本只需要处理 CLICK事件)
			MsgType.EventType _eventType = MsgType.EventType.valueOf(this.event);
			if( _eventType == MsgType.EventType.CLICK) {
				msg = new MenuEventMsgClick();
			} else if( _eventType == MsgType.EventType.VIEW) {
				msg = new MenuEventMsgView();
			} else if( _eventType == MsgType.EventType.LOCATION) {//自动发地理位置事件
				msg = new EventMsgLocation();
			} else if( _eventType == MsgType.EventType.pic_weixin || _eventType == MsgType.EventType.pic_photo_or_album ||  _eventType == MsgType.EventType.pic_sysphoto ) {//打开相册发相片事件
				msg = new MenuEventMsgPicWeuxin();
			} else if( _eventType == MsgType.EventType.scancode_push ||  _eventType == MsgType.EventType.scancode_waitmsg) {//扫码(二维码，或条码)推事件,或等待
				msg = new MenuEventMsgScancodePush();
			} else if( _eventType == MsgType.EventType.location_select) {//弹出地理位置选择(建议不处理)
				msg = new MenuEventMsgLocationSelect();
			}  else if( _eventType == MsgType.EventType.scan || _eventType == MsgType.EventType.subscribe || _eventType == MsgType.EventType.unsubscribe ) {
				msg = new EventMsgUserAttention();
			}
		}
		msg.setHead(this);
		msg.read(document);
		return msg;
	}
}
