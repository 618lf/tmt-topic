package com.tmt.weixin.base.msg;

import org.w3c.dom.Document;

/**
 * 自定义菜单事件: 扫码推事件的事件推送
 * scancode_waitmsg 或  scancode_push
 * 
 */
public class MenuEventMsgScancodePush extends MenuEventMsg {
	// 事件KEY值，与自定义菜单接口中KEY值对应
	private String scanType;
	private String scanResult;
	
	public String getScanType() {
		return scanType;
	}
	public void setScanType(String scanType) {
		this.scanType = scanType;
	}
	public String getScanResult() {
		return scanResult;
	}
	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}
	public void read(Document document) {
		this.scanType = readFormDocument(document, "ScanType");
		this.scanResult = readFormDocument(document, "ScanResult");
	}
}
