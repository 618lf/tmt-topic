package com.tmt.weixin.base.api;

import java.nio.charset.Charset;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;

import com.tmt.base.common.utils.JsonMapper;
import com.tmt.weixin.base.api.entity.BaseResult;
import com.tmt.weixin.base.api.entity.Menu;
import com.tmt.weixin.base.api.entity.MenuButtons;
import com.tmt.weixin.base.utils.JsonResponseHandler;

/**
 * 微信菜单管理
 * @author lifeng
 *
 */
public class MenuAPI extends BaseAPI{

	private static String CREATE_MENU_URL = BASE_URI + "/cgi-bin/menu/create";
	private static String GET_MENU_URL = BASE_URI + "/cgi-bin/menu/get";
	
	/**
	 * 创建菜单
	 * @param access_token
	 * @param menuJson 菜单json 数据 例如{\"button\":[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"},{\"type\":\"click\",\"name\":\"歌手简介\",\"key\":\"V1001_TODAY_SINGER\"},{\"name\":\"菜单\",\"sub_button\":[{\"type\":\"view\",\"name\":\"搜索\",\"url\":\"http://www.soso.com/\"},{\"type\":\"view\",\"name\":\"视频\",\"url\":\"http://v.qq.com/\"},{\"type\":\"click\",\"name\":\"赞一下我们\",\"key\":\"V1001_GOOD\"}]}]}
	 * @return
	 */
	public BaseResult createMenu(String accessToken, String menuJson){
		HttpUriRequest httpUriRequest = RequestBuilder.post()
						.setHeader(jsonHeader)
						.setUri(CREATE_MENU_URL)
						.addParameter("access_token", accessToken)
						.setEntity(new StringEntity(menuJson,Charset.forName("utf-8")))
						.build();
        return localHttpClient.execute(httpUriRequest,JsonResponseHandler.createResponseHandler(BaseResult.class));
	}
	
	/**
	 * 创建菜单
	 * @param access_token
	 * @param menuButtons
	 * @return
	 */
	public BaseResult createMenu(String access_token, MenuButtons menuButtons){
		return createMenu(access_token, JsonMapper.toJson(menuButtons));
	}
	
	/**
	 * 获取菜单
	 * @param access_token
	 * @return
	 */
	public Menu getMenu(String accessToken){
		HttpUriRequest httpUriRequest = RequestBuilder.post()
					.setUri(GET_MENU_URL)
					.addParameter("access_token", accessToken)
					.build();
		return localHttpClient.execute(httpUriRequest, JsonResponseHandler.createResponseHandler(Menu.class));
	}
	
	/**
	 * 删除菜单
	 * @param access_token
	 * @return
	 */
	public BaseResult deleteMenu(String access_token){
		HttpUriRequest httpUriRequest = RequestBuilder.post()
				.setUri(BASE_URI+"/cgi-bin/menu/delete")
				.addParameter("access_token", access_token)
				.build();
		return localHttpClient.execute(httpUriRequest, JsonResponseHandler.createResponseHandler(BaseResult.class));
	}
}
