package com.tmt.weixin.base.api.templatemessage;

import com.tmt.weixin.base.api.entity.BaseResult;

public class TemplateMessageResult extends BaseResult{

	private Long msgid;

	public Long getMsgid() {
		return msgid;
	}

	public void setMsgid(Long msgid) {
		this.msgid = msgid;
	}


}
