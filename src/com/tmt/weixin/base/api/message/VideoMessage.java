package com.tmt.weixin.base.api.message;

import com.tmt.weixin.base.resource.MediaEx;

/**
 * 视频
 * 
 * @author LiYi
 * 
 */
public class VideoMessage extends Message {

	public MediaEx video;

	public VideoMessage(String touser, MediaEx video) {
		super(touser, "video");
		this.video = video;
	}

	public MediaEx getVideo() {
		return video;
	}

	public void setVideo(MediaEx video) {
		this.video = video;
	}
}
