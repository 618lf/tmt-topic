package com.tmt.weixin.base.api.message;

import com.tmt.weixin.base.resource.Media;

/**
 * 语音
 *
 * @author LiYi
 *
 */
public class VoiceMessage extends Message {

	public VoiceMessage(String touser,String mediaId) {
		super(touser,"voice");
		this.voice = new Media(mediaId);
	}

	public Media voice;

	public Media getMedia() {
		return voice;
	}

	public void setMedia(Media voice) {
		this.voice = voice;
	}
}
