package com.tmt.weixin.base.api.message;

import com.tmt.weixin.base.resource.Music;

public class MusicMessage extends Message {

	private Music music;

	public MusicMessage(String touser, Music music) {
		super(touser, "music");
		this.music = music;
	}

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}
}
