package com.tmt.weixin.base.api.message;

import java.util.List;

import com.tmt.weixin.base.resource.Article;
import com.tmt.weixin.base.resource.News;

/**
 * 图文消息 图文消息条数限制在10条以内，注意，如果图文数超过10，则将会无响应。
 * 
 * @author LiYi
 * 
 */
public class NewsMessage extends Message {

	public NewsMessage(String touser, List<Article> articles) {
		super(touser, "news");
		this.news = new News();
		this.news.setArticles(articles);
	}

	private News news;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}
}
