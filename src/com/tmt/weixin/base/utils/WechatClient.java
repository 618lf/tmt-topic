package com.tmt.weixin.base.utils;

import com.tmt.weixin.base.api.SnsAPI;
import com.tmt.weixin.base.api.entity.SnsToken;
import com.tmt.weixin.base.api.entity.User;

/**
 * 微信接口调用客户端
 * @author lifeng
 */
public class WechatClient {

	/**
	 * 获得授权Token  SnsToken
	 * @param appid
	 * @param secret
	 * @param code
	 * @return
	 */
	public static SnsToken oauth2AccessToken(String appid, String secret, String code) {
		return SnsAPI.oauth2AccessToken(appid, secret, code);
	}
	
	/**
	 * 获取授权认证的用户
	 * @param accessToken
	 * @param openId
	 * @return
	 */
	public static User userinfo(String accessToken, String openId){
		return SnsAPI.userinfo(accessToken, openId);
	}
}
