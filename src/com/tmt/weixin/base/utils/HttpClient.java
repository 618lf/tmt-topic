package com.tmt.weixin.base.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;

/**
 * Http 请求
 * @author lifeng
 *
 */
public class HttpClient {

	/**
	 * 返回string
	 */
	public static String post(String url, String param) {
		String result = null;
		URL xUrl = null;
		HttpURLConnection xHuc = null;
		BufferedReader in = null;
		try{
			xUrl = new URL(url);
			if (xUrl != null) {
				xHuc = (HttpURLConnection) xUrl.openConnection();
			}
			xHuc.setRequestMethod("POST");
			xHuc.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
			xHuc.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
			xHuc.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6,ja;q=0.4,zh-TW;q=0.2");
			xHuc.setRequestProperty("Connection", "keep-alive");
			xHuc.setRequestProperty("Content-Length", "92");
			xHuc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			xHuc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36");
			xHuc.setDoOutput(true);
			xHuc.setDoInput(true);
			xHuc.connect();
			if( param != null) {
				xHuc.getOutputStream().write(param.getBytes());//输入参数
			}
			int isOk = xHuc.getResponseCode();
			if (isOk == 200) {
				in = new BufferedReader(new InputStreamReader(
						xHuc.getInputStream(), "UTF-8"));
				int chint = 0;
				StringBuffer sb = new StringBuffer();
				while ((chint = in.read()) != -1) {
					sb.append((char) chint);
				}
				return sb.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (xHuc != null) {
				xHuc.disconnect();
			}
			IOUtils.closeQuietly(in);
		}
		return result;
	}
	
	/**
	 * 返回具体的类
	 * @param url
	 * @param param
	 * @param responseHandler
	 * @return
	 */
	public static <T> T post(String url, String param ,ResponseHandler<T> responseHandler) {
		String response = post(url, param);
		return responseHandler.doHandle(response);
	}
	
	/**
	 * 请求结果处理器
	 * @author lifeng
	 *
	 * @param <T>
	 */
	public static interface ResponseHandler<T>{
		
		public T doHandle(String response);
	}
}
