package com.tmt.weixin.base.service;

import com.tmt.weixin.base.msg.BaseMsg;
import com.tmt.weixin.base.msg.RespMsg;

/**
 * 业务模块基类
 * @author lifeng
 */
public interface AppService {

	/**
	 * 执行消息
	 * @param msg
	 * @return
	 */
	public RespMsg onMsg(BaseMsg msg);
}
