package com.tmt.weixin.base.resource;

/**
 * 多媒体消息，对应Image消息,这种多媒体需要上传到微信服务器。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class Media {
	// 发送的媒体ID
	public String mediaId;

	public Media(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return mediaId;
	}
}
