package com.tmt.weixin.base.resource;

/**
 * 多媒体扩展消息，包括标题及描述,可对应微消息中的video与voice
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class MediaEx extends Media {
	// 多媒体标题 ，可选
	public String title;
	// 多媒体描述 可选
	public String description;

	public MediaEx(String media_id, String title, String description) {
		super(media_id);
		this.title = title;
		this.description = description;
	}

	public String getTitle() {
		return title == null?"":title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description == null?"":description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
