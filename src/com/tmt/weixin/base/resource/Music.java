package com.tmt.weixin.base.resource;

/**
 * 音乐消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public class Music extends FreeMedia {

	public String musicUrl;
	// 高质量音乐链接，WIFI环境优先使用该链接播放音乐
	public String hqmusicUrl;
	// 缩略图的媒体id，通过上传多媒体文件，得到的id
	public String thumbMediaId;

	public Music(String title, String description, String musicurl,
			String hqmusicurl, String thumb_media_id) {
		super(title, description);
		this.musicUrl = musicurl;
		this.hqmusicUrl = hqmusicurl;
		this.thumbMediaId = thumb_media_id;
	}

	public String getMusicUrl() {
		return musicUrl==null?"":musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public String getHqmusicUrl() {
		return hqmusicUrl == null?"":hqmusicUrl;
	}

	public void setHqmusicUrl(String hqmusicUrl) {
		this.hqmusicUrl = hqmusicUrl;
	}

	public String getThumbMediaId() {
		return thumbMediaId==null?"":thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
}
