package com.tmt.weixin.base.resource;

/**
 * 引用外部资源的多媒体消息。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 * 
 */
public abstract class FreeMedia {
	// 多媒体标题 ，可选
	public String title;
	// 多媒体描述 可选
	public String description;

	public FreeMedia(String title, String description) {
		this.title = title;
		this.description = description;
	}

	public String getTitle() {
		return title == null?"":title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description == null?"":description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
