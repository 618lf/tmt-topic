package com.tmt.weixin.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.Member;
import com.tmt.base.system.service.MemberService;

/**
 * 微信会员管理  -- 粉丝管理
 * @author lifeng
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/weixin/fans")
public class FansController extends BaseController{

	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = {"initList"})
	public String intiList(Member member, Model model){
		return "/weixin/FansList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Member member, Model model, Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(member != null && StringUtil3.isNotBlank(member.getNickName())) {
			c.andLike("U.NICK_NAME", "nickName", member.getNickName());
		}
		c.andConditionSql("M.OPENID IS NOT NULL");
		page = this.memberService.queryForPage(qc, param);
		return page;
	}
}
