package com.tmt.weixin.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.web.BaseController;
import com.tmt.weixin.entity.PubAccount;
import com.tmt.weixin.service.PubAccountService;

/**
 * 微信公众帐号管理
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/weixin/pubAccount")
public class PubAccountController extends BaseController {

	@Autowired
	private PubAccountService pubAccountService;
	
	@RequestMapping(value="list")
	public String list(Model model){
		QueryCondition qc = new QueryCondition();
		List<PubAccount> pubAccounts = pubAccountService.queryByCondition(qc);
		model.addAttribute("accounts", pubAccounts);
		return "/weixin/PubAccountList";
	}
}
