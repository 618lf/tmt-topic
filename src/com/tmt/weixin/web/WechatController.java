package com.tmt.weixin.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.config.Globals;
import com.tmt.base.common.exception.BaseAppException;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.weixin.base.msg.BaseMsg;
import com.tmt.weixin.base.msg.RespMsg;
import com.tmt.weixin.base.service.AppService;
import com.tmt.weixin.service.WebchatService;
import com.tmt.weixin.utils.SignUtil;

/**
 * 微信接入
 * @author lifeng
 */
@Controller
@RequestMapping(value = "${adminPath}/wechat")
public class WechatController extends BaseController implements InitializingBean{

	private String token = "douguazhijia";
	private WebchatService webchatService;
	
	//初始化并设置属性之后，设置消息处理监听器
	@Override
	public void afterPropertiesSet() throws Exception {
		if( webchatService == null ) {
			webchatService = SpringContextHolder.getBean(WebchatService.class);
		}
		if( webchatService == null ) {
			throw new BaseAppException(" message handle service can not be null");
		}
		//注册默认的消息处理
		if( webchatService.getAppService() == null) {
			AppService appService = SpringContextHolder.getBean(AppService.class);
			if( appService == null) {
				appService = new AppService(){
					//不处理任何消息,使用默认的消息回复
					@Override
					public RespMsg onMsg(BaseMsg msg) {
						return null;
					}
				};
			}
			webchatService.setAppService(appService);
		}
	}
	
	/**
	 * 用于测试是否可以和微信接入
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String wechatGet(String signature, String timestamp, String nonce, String echostr, HttpServletRequest request) {
		if(StringUtil3.isNotBlank(signature) && StringUtil3.isNotBlank(nonce) && StringUtil3.isNotBlank(echostr)
				&& SignUtil.checkSignature(token, signature, timestamp, nonce) ) {
			return echostr;
		}
		return "ERROR";
	}
	
	/**
	 * 微信端调用的本地接口
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String wechatPost(HttpServletRequest request, HttpServletResponse response) {
		String reqMsg = this.getMsgFromRequest(request);
		logger.debug("收到消息：" + reqMsg);
		String respMsg = webchatService.onMsg(reqMsg);
		logger.debug("回复消息：" + respMsg);
		return respMsg;
	}
	
	/**
	 * 从request 中获取消息
	 * @param request
	 * @return
	 */
	private String getMsgFromRequest(HttpServletRequest request){
		String charset = request.getCharacterEncoding();
		if (charset == null) {
			charset = Globals.DEFAULT_ENCODING;
		}
		try {
			List<String> lines = IOUtils.readLines(request.getInputStream(), charset);
			StringBuilder _lines = new StringBuilder();
			for(String line: lines) {
				_lines.append(line);
			}
			return _lines.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 微信认证  -- 如果成功则返回一个 如果客户端的IP地址不是微信的服务器，则返回登录界面
	 * 如果只需要用户的openid, 则不需要获取用户的信息
	 * @param code  -- 授权码
	 * @param state --菜单标识
	 * @return
	 */
	@RequestMapping(value="oAuth")
	public String wechatOAuth(String code, String state){
		if(StringUtil3.isNotBlank(state)) { //微信授权认证:根据state 返回不同的页面
			return "system/SysIndex";
		}
		return "system/SysLogin";
	}
}
