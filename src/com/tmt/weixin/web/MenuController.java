package com.tmt.weixin.web;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.tmt.base.common.utils.JsonMapper;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.component.cache.redis.RedisUtils;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.weixin.base.api.MenuAPI;
import com.tmt.weixin.base.api.SnsAPI;
import com.tmt.weixin.base.api.TokenAPI;
import com.tmt.weixin.base.api.entity.BaseResult;
import com.tmt.weixin.base.api.entity.MenuButtons;
import com.tmt.weixin.base.api.entity.MenuButtons.Button;
import com.tmt.weixin.base.api.entity.SnsToken;
import com.tmt.weixin.base.api.entity.Token;
import com.tmt.weixin.base.api.entity.User;

/**
 * 微信菜单管理
 * @author lifeng
 */
@Controller("wxMenuController")
@RequestMapping(value = "${adminPath}/wechat/menu")
public class MenuController extends BaseController {

	public static String accessToken = "accessToken";
	
	//订阅号（测试号）
	public static String appid = "wxeff9811824f0f3b6";
	public static String secret = "547fcb9f399c1b8f6cf029993c8a337b";
	
	//服务号（现在使用服务号 -- 没有用）
	public static String sAppid = "wx047b7c74d64a3389";
	public static String sSecret = "01b1524a2a75d776ab192c660bba6797";
	
	@RequestMapping(value="list")
	public String list(){
		return "/weixin/MenuList";
	}
	
	@ResponseBody
	@RequestMapping(value="testMenu")
	public AjaxResult testMenu(){
		//从缓存获取token
		Token token = JsonMapper.fromJson(RedisUtils.getString(accessToken), Token.class);
		if( token == null) {
			//获取token
			TokenAPI tokenAPI = new TokenAPI();
			token = tokenAPI.token(appid, secret);
			//放入缓存
			//RedisUtils.set(accessToken, token, 120);
			RedisUtils.set(accessToken, JsonMapper.toJson(token), 120);
		}
		
		//创建菜单
		MenuAPI menuAPI = new MenuAPI();
		
		Button b1 = new Button();
		b1.setKey("001");
		b1.setName("发图");
		
		Button b11 = new Button();
		b11.setKey("001001");
		b11.setName("系统拍照发图");
		b11.setType("pic_sysphoto");
		
		Button b12 = new Button();
		b12.setKey("001002");
		b12.setName("拍照或者相册发图");
		b12.setType("pic_photo_or_album");
		
		Button b13 = new Button();
		b13.setKey("001003");
		b13.setName("微信相册发图");
		b13.setType("pic_weixin");
		
		List<Button> s1 = Lists.newArrayList();
		s1.add(b11);s1.add(b12);s1.add(b13);
		b1.setSub_button(s1);
		
		Button b2 = new Button();
		b2.setKey("002");
		b2.setName("菜单");
		
		Button b21 = new Button();
		b21.setKey("002001");
		b21.setName("发送位置");
		b21.setType("location_select");
		
		Button b22 = new Button();
		b22.setKey("002002");
		b22.setName("税务公社");
		b22.setType("view");
		b22.setUrl("http://e3ded8e.ngrok.com/pro-tax-platform");
		
		Button b23 = new Button();
		b23.setKey("002003");
		b23.setName("每日签到");
		b23.setType("click");
		
		List<Button> s2 = Lists.newArrayList();
		s2.add(b21);s2.add(b22);s2.add(b23);
		b2.setSub_button(s2);
		
		Button b3 = new Button();
		b3.setKey("003");
		b3.setName("扫码");
		
		Button b31 = new Button();
		b31.setKey("003001");
		b31.setName("扫码带提示");
		b31.setType("scancode_waitmsg");
		
		Button b32 = new Button();
		b32.setKey("003002");
		b32.setName("扫码推事件");
		b32.setType("scancode_push");
		
		List<Button> s3 = Lists.newArrayList();
		s3.add(b31);s3.add(b32);
		b3.setSub_button(s3);
		
		//按钮
		MenuButtons menuItem = new MenuButtons();
		menuItem.setButton(new Button[]{b1,b2,b3});
		
		System.out.println(JsonMapper.toJson(menuItem));
		
		BaseResult result = menuAPI.createMenu(token.getAccess_token(), menuItem);
		System.out.println(result.getErrmsg());
		
		return AjaxResult.success();
	}
	
	/**
	 * 授权(不知道已认证的订阅号能不能获取  --> SnsToken)
	 * 如果这个获取不到,则比较麻烦
	 * @return
	 */
	@RequestMapping(value="oAuth")
	public String oAuth(String code){
		String userInfo = "";
		if( StringUtil3.isNotBlank(code) ) { //微信授权认证
			SnsToken token = SnsAPI.oauth2AccessToken(appid, secret, code);
			
			System.out.println(JsonMapper.toJson(token));
			if( token != null) {
				User user = SnsAPI.userinfo(token.getAccess_token(), token.getOpenid());
			    userInfo = JsonMapper.toJson(user);
			}
		}
		return "system/SysLogin";
	}
}
