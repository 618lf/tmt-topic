package com.tmt.weixin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.weixin.dao.ReceiveMessageDao;
import com.tmt.weixin.entity.ReceiveMessage;

@Service
public class ReceiveMessageService extends BaseService<ReceiveMessage, String>{

	@Autowired
	private ReceiveMessageDao receiveMessageDao;
	
	@Override
	protected BaseIbatisDAO<ReceiveMessage, String> getEntityDao() {
		return receiveMessageDao;
	}
	
	/**
	 * 保存消息
	 * @param message
	 */
	@Transactional
	public void save(ReceiveMessage message){
		if(IdGen.isInvalidId(message.getId())) {
			this.insert(message);
		} else {
			this.update(message);
		}
	}
}
