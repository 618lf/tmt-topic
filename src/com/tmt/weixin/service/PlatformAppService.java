package com.tmt.weixin.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.tmt.weixin.base.msg.BaseMsg;
import com.tmt.weixin.base.msg.RespMsg;
import com.tmt.weixin.base.msg.RespMsgText;
import com.tmt.weixin.base.service.AppService;

/**
 * 平台处理消息服务
 * @author lifeng
 */
public class PlatformAppService implements AppService{

	@Override
	public RespMsg onMsg(BaseMsg msg) {
		String redirectUri = "http://3729941.ngrok.com/pro-tax-platform/admin/wechat/oAuth";
		try {
			redirectUri = URLEncoder.encode(redirectUri, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		StringBuilder _content = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize?");
		_content.append("appid=").append("wxeff9811824f0f3b6")
                .append("&redirect_uri=").append(redirectUri)
                .append("&response_type=code")
                .append("&scope=").append("snsapi_userinfo")
                .append("&state=ABC").append("#wechat_redirect");
		
		StringBuilder contnet = new StringBuilder();
		contnet.append("谢谢关注,").append("<a href='").append(_content)
			   .append("'>").append("请绑定帐号").append("</a>");
		System.out.println("授权连接："+ contnet);
		RespMsg _msg = new RespMsgText(msg, contnet.toString());
		return _msg;
	}
}
