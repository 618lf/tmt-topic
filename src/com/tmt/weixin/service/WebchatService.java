package com.tmt.weixin.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.weixin.base.msg.BaseMsg;
import com.tmt.weixin.base.msg.MsgHead;
import com.tmt.weixin.base.msg.RespMsg;
import com.tmt.weixin.base.msg.RespMsgText;
import com.tmt.weixin.base.service.AppService;

/**
 * 微信基础服务
 * @author lifeng
 */
@Service
public class WebchatService {
	
	protected static Logger logger = LoggerFactory.getLogger(WebchatService.class);
	protected static String DEFAULT_MESSGAE_TEXT = "谢谢关注!";
	private static DocumentBuilder builder;
	static{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	//支持多个业务service
	private AppService appService;
	
	/**
	 * 执行消息处理
	 * @param xml
	 * @return
	 */
	public String onMsg(String xml){
		String result = null;
		StringReader reader = null;
		try{
			reader = new StringReader(xml);
			Document doc = builder.parse(new InputSource(reader));
			//读取消息头信息
			MsgHead head = new MsgHead();
			head.read(doc);
			
			//根据消息头构建具体的请求消息
			BaseMsg msg = head.buildMsg(doc);
			
			//执行消息得到返回信息
			RespMsg resp = appService.onMsg(msg);
			
			//对于事件,应不回复（即使回复也收不到信息,微信不会处理除了 CLICK 和 scancode_waitmsg 之外的事件）
			if( resp == null) {
				resp = new RespMsgText(msg, DEFAULT_MESSGAE_TEXT);
			}
			//得到返回消息的Xml
			result = resp.write();
		}catch(Exception e){
			logger.error("call app service failed.", e);
			throw new BaseRuntimeException("error while process msg", e);
		}finally{
			IOUtils.closeQuietly(reader);
		}
		return result;
	}
	
	public AppService getAppService() {
		return appService;
	}

	public void setAppService(AppService appService) {
		this.appService = appService;
	}
}
