package com.tmt.weixin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.weixin.dao.PubAccountDao;
import com.tmt.weixin.entity.PubAccount;

/**
 * 微信服务管理
 * @author lifeng
 *
 */
@Service
public class PubAccountService extends BaseService<PubAccount, String>{

	@Autowired
	private PubAccountDao pubAccountDao;
	
	@Override
	protected BaseIbatisDAO<PubAccount, String> getEntityDao() {
		return pubAccountDao;
	}
}
