package com.tmt.weixin.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.weixin.entity.ReceiveMessage;

@Repository
public class ReceiveMessageDao extends BaseIbatisDAO<ReceiveMessage, String>{

	@Override
	protected String getNamespace() {
		return "WX_RECEIVE_MESSAGE";
	}

}
