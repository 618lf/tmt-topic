package com.tmt.weixin.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.weixin.entity.PubAccount;

/**
 * 公众号管理
 * @author lifeng
 *
 */
@Repository
public class PubAccountDao extends BaseIbatisDAO<PubAccount, String>{

	@Override
	protected String getNamespace() {
		return "WX_PUB_ACCOUNT";
	}

}
