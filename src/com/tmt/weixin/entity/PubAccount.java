package com.tmt.weixin.entity;

import com.tmt.base.system.entity.BaseEntity;

/**
 * 微信公众号
 * @author lifeng
 */
public class PubAccount extends BaseEntity<String>{

	private static final long serialVersionUID = -5484213386867819550L;
	
	private String name;
    private String type;
    private String pic;
    private String email;
    private String srcId;
    private String wxNum;
    private String authenState;
    private String authenDesc;
    private String principal;
    private String address;
    private String qrCode;
    private String appId;
    private String appSecret;
    private String token;
    private String encodingAesKey;
    private String openWxId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSrcId() {
		return srcId;
	}
	public void setSrcId(String srcId) {
		this.srcId = srcId;
	}
	public String getWxNum() {
		return wxNum;
	}
	public void setWxNum(String wxNum) {
		this.wxNum = wxNum;
	}
	public String getAuthenState() {
		return authenState;
	}
	public void setAuthenState(String authenState) {
		this.authenState = authenState;
	}
	public String getAuthenDesc() {
		return authenDesc;
	}
	public void setAuthenDesc(String authenDesc) {
		this.authenDesc = authenDesc;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getEncodingAesKey() {
		return encodingAesKey;
	}
	public void setEncodingAesKey(String encodingAesKey) {
		this.encodingAesKey = encodingAesKey;
	}
	public String getOpenWxId() {
		return openWxId;
	}
	public void setOpenWxId(String openWxId) {
		this.openWxId = openWxId;
	}
}