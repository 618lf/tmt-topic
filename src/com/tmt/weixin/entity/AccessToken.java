package com.tmt.weixin.entity;

import java.util.Date;

import com.tmt.base.system.entity.BaseEntity;

/**
 * 微信访问控制的token
 * @author liFeng
 * 2014年7月12日
 */
public class AccessToken extends BaseEntity<String>{
	
	private static final long serialVersionUID = 2046170333326420281L;
	
	private String accessToken;
    private Date addTime;
    private Integer expiresIb;
    
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Integer getExpiresIb() {
		return expiresIb;
	}
	public void setExpiresIb(Integer expiresIb) {
		this.expiresIb = expiresIb;
	}
}