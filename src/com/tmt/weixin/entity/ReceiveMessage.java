package com.tmt.weixin.entity;

import com.tmt.base.system.entity.BaseEntity;

/**
 * 接收的消息  -- 回复的消息
 * @author liFeng
 * 2014年7月12日
 */
public class ReceiveMessage extends BaseEntity<String>{
   
	private static final long serialVersionUID = 6457532675799376128L;

	private String msgId;//消息ID
    private String msgType;//消息类型
    private String formUserName;//消息发送者
    private String content;//收到的消息
    private String resContent;//回复的消息
    private String toUserName;//消息接收者
    private String createTime;//发送者发送消息的时间
    
    public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFormUserName() {
		return formUserName;
	}
	public void setFormUserName(String formUserName) {
		this.formUserName = formUserName;
	}
	public String getResContent() {
		return resContent;
	}
	public void setResContent(String resContent) {
		this.resContent = resContent;
	}
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}