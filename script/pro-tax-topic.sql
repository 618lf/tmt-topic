/*
Navicat MySQL Data Transfer

Source Server         : 127_ptms
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : pro-tax-topic

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2014-12-25 20:04:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `PARENT_ID` varchar(64) NOT NULL COMMENT '父级编号',
  `PARENT_IDS` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `CODE` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `NAME` varchar(100) NOT NULL COMMENT '区域名称',
  `LEVEL` tinyint(4) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '区域类型',
  `PATH` varchar(2000) DEFAULT NULL,
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('0', '-1', '-1,', '001', '广东', '1', '1', '/广东', null, null, null, '0', '超级管理员', '2014-12-16 22:33:37', null, '0');
INSERT INTO `sys_area` VALUES ('21', '0', '-1,0,', '001001', '深圳', '2', '2', '/广东/深圳', '0', '超级管理员', '2014-12-16 22:43:26', null, null, null, null, '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `LABEL` varchar(200) NOT NULL COMMENT '标签名',
  `CODE` varchar(200) NOT NULL COMMENT '字典编号 --KEY',
  `VALUE` varchar(1000) NOT NULL COMMENT '数据值',
  `TYPE` varchar(100) NOT NULL COMMENT '类型',
  `LEVEL` tinyint(4) DEFAULT NULL,
  `ENCRYPT` char(1) DEFAULT NULL COMMENT '是否MD5加密',
  `PARENT_ID` varchar(64) NOT NULL COMMENT '编号',
  `PARENT_IDS` varchar(2000) NOT NULL COMMENT '编号',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('0', '系统配置', 'SYS_CONFIG', 'SYS_CONFIG', 'SYS_CONFIG', '1', null, '-1', '-1,', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_dict` VALUES ('21', '会员注册参数', 'USER_R_CONFIG', 'USER_R_CONFIG', 'USER_R_CONFIG', '2', null, '0', '-1,0,', '0', '超级管理员', '2014-12-16 22:55:02', null, null, null, null, '0');
INSERT INTO `sys_dict` VALUES ('22', '微信用户注册默认组织编码', 'WX_R_OFFICE_CODE', '001002', 'USER_R_CONFIGS', '3', null, '21', '-1,0,21,', '0', '超级管理员', '2014-12-16 22:56:26', null, null, null, '只能填写一个', '0');
INSERT INTO `sys_dict` VALUES ('23', '微信用户注册默认用户组', 'WX_R_GROUP_CODES', '001', 'USER_R_CONFIGS', '3', null, '21', '-1,0,21,', '0', '超级管理员', '2014-12-16 22:57:20', null, null, null, '可以填写多个，用 ‘,’ 分割', '0');

-- ----------------------------
-- Table structure for sys_dual
-- ----------------------------
DROP TABLE IF EXISTS `sys_dual`;
CREATE TABLE `sys_dual` (
  `X` char(1) NOT NULL DEFAULT 'X' COMMENT '编号',
  PRIMARY KEY (`X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统表';

-- ----------------------------
-- Records of sys_dual
-- ----------------------------
INSERT INTO `sys_dual` VALUES ('X');

-- ----------------------------
-- Table structure for sys_excel_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_excel_item`;
CREATE TABLE `sys_excel_item` (
  `ID` varchar(64) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `TEMPLATE_ID` varchar(64) DEFAULT NULL,
  `COLUMN_NAME` varchar(32) DEFAULT NULL,
  `PROPERTY` varchar(64) DEFAULT NULL,
  `DATA_TYPE` varchar(32) DEFAULT NULL,
  `DATA_FORMAT` varchar(255) DEFAULT NULL,
  `VERIFY_TYPE` varchar(32) DEFAULT NULL,
  `VERIFY_FORMAT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel 模版项';

-- ----------------------------
-- Records of sys_excel_item
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excel_template
-- ----------------------------
DROP TABLE IF EXISTS `sys_excel_template`;
CREATE TABLE `sys_excel_template` (
  `ID` varchar(64) NOT NULL,
  `NAME` varchar(200) DEFAULT NULL,
  `TYPE` varchar(200) DEFAULT NULL,
  `TARGET_CLASS` varchar(128) DEFAULT NULL,
  `START_ROW` tinyint(4) DEFAULT NULL,
  `CREATE_ID` varchar(64) DEFAULT NULL,
  `CREATE_NAME` varchar(100) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `EXTEND_ATTR` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel 模版';

-- ----------------------------
-- Records of sys_excel_template
-- ----------------------------

-- ----------------------------
-- Table structure for sys_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_group`;
CREATE TABLE `sys_group` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `NAME` varchar(100) NOT NULL COMMENT '组名',
  `CODE` varchar(100) DEFAULT NULL COMMENT '组编码',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_group
-- ----------------------------
INSERT INTO `sys_group` VALUES ('22', '普通会员', '001', '0', '超级管理员', '2014-12-16 22:50:52', '0', '超级管理员', '2014-12-16 22:51:21', '普通的用户注册，或微信用户', '0');

-- ----------------------------
-- Table structure for sys_group_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_group_role`;
CREATE TABLE `sys_group_role` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `GROUP_ID` varchar(64) NOT NULL COMMENT '用户编号',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组-角色';

-- ----------------------------
-- Records of sys_group_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_group_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_group_user`;
CREATE TABLE `sys_group_user` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `GROUP_ID` varchar(64) NOT NULL COMMENT '用户组编号',
  `USER_ID` varchar(64) NOT NULL COMMENT '用户编号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组-用户';

-- ----------------------------
-- Records of sys_group_user
-- ----------------------------
INSERT INTO `sys_group_user` VALUES ('21', '22', '21');
INSERT INTO `sys_group_user` VALUES ('41', '22', '41');

-- ----------------------------
-- Table structure for sys_id_store
-- ----------------------------
DROP TABLE IF EXISTS `sys_id_store`;
CREATE TABLE `sys_id_store` (
  `TABLE_NAME` varchar(100) NOT NULL,
  `MIN_VALUE` decimal(20,0) NOT NULL,
  `CURRENT_VALUE` decimal(20,0) NOT NULL,
  `MAX_VALUE` decimal(20,0) NOT NULL,
  `STEP` decimal(4,0) NOT NULL,
  `REMARK` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统主键表';

-- ----------------------------
-- Records of sys_id_store
-- ----------------------------
INSERT INTO `sys_id_store` VALUES ('SYS_AREA', '1', '21', '99999999999', '20', 'TABLE SYS_AREA ID');
INSERT INTO `sys_id_store` VALUES ('SYS_DICT', '1', '21', '99999999999', '20', 'TABLE SYS_DICT ID');
INSERT INTO `sys_id_store` VALUES ('SYS_GROUP', '1', '21', '99999999999', '20', 'TABLE SYS_GROUP ID');
INSERT INTO `sys_id_store` VALUES ('SYS_GROUP_USER', '1', '41', '99999999999', '20', 'TABLE SYS_GROUP_USER ID');
INSERT INTO `sys_id_store` VALUES ('SYS_OFFICE', '1', '21', '99999999999', '20', 'TABLE SYS_OFFICE ID');
INSERT INTO `sys_id_store` VALUES ('SYS_USER', '1', '41', '99999999999', '20', 'TABLE SYS_USER ID');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` varchar(64) NOT NULL COMMENT '编号（默认是UUID）',
  `TYPE` char(1) DEFAULT '1' COMMENT '日志类型',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `REMOTE_ADDR` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `USER_AGENT` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `REQUEST_URI` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `METHOD` varchar(5) DEFAULT NULL COMMENT '操作方式',
  `PARAMS` text COMMENT '操作提交的数据',
  `EXCEPTION` text COMMENT '异常信息',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_member
-- ----------------------------
DROP TABLE IF EXISTS `sys_member`;
CREATE TABLE `sys_member` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `PROFESSIONAL_TITLE` varchar(200) DEFAULT NULL COMMENT '职称',
  `PROFESSIONAL_LEVEL` varchar(20) DEFAULT NULL COMMENT '职称等级',
  `PROFESSIONAL_QUALIFICATION` varchar(300) DEFAULT NULL COMMENT '职业资格',
  `COMPANY` varchar(300) DEFAULT NULL COMMENT '所属公司',
  `DEPARTMENT` varchar(300) DEFAULT NULL COMMENT '所属部门',
  `OPENID` varchar(100) DEFAULT NULL COMMENT '微信openId',
  `UNIONID` varchar(100) DEFAULT NULL COMMENT '微信unionId',
  `SUBSCRIBE` char(1) DEFAULT '0' COMMENT '是否关注',
  `SUBSCRIBE_DATE` datetime DEFAULT NULL COMMENT '关注日期',
  `LAST_ACTIVE_DATE` datetime DEFAULT NULL COMMENT '最后一次活动日期',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_member
-- ----------------------------
INSERT INTO `sys_member` VALUES ('21', null, null, null, null, null, 'oabu2swPGUS8s5hHfPgbnZ4RaxiM', null, null, null, null);
INSERT INTO `sys_member` VALUES ('41', null, null, null, null, null, 'oabu2syy4xHuMvHy1pAq9mKfiljU', null, null, null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `PARENT_ID` varchar(64) NOT NULL COMMENT '父级编号',
  `PARENT_IDS` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `NAME` varchar(100) NOT NULL COMMENT '菜单名称',
  `HREF` varchar(255) DEFAULT NULL COMMENT '链接',
  `TARGET` varchar(20) DEFAULT NULL COMMENT '目标',
  `ICON_CLASS` varchar(100) DEFAULT NULL COMMENT '图标',
  `SORT` int(11) NOT NULL COMMENT '排序（升序）',
  `LEVEL` int(11) NOT NULL COMMENT '层级从0开始',
  `TYPE` int(11) NOT NULL COMMENT '菜单类型',
  `QUICK_MENU` varchar(100) DEFAULT NULL,
  `TOP_MENU` varchar(100) DEFAULT NULL,
  `IS_SHOW` char(1) NOT NULL COMMENT '是否在菜单中显示',
  `IS_ACTIVITI` char(1) DEFAULT NULL COMMENT '是否同步工作流',
  `PERMISSION` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0', '-1', '-1,', '系统菜单', null, null, null, '0', '0', '1', null, null, '0', '0', null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('1', '0', '-1,0,', '系统管理', null, null, 'icon-th-large', '1', '1', '1', 'icon-star', null, '1', null, null, null, null, null, '0', '超级管理员', '2014-12-15 14:40:22', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '1', '-1,0,1,', '模板配置', '/admin/system/excel/initList', null, 'icon-qrcode', '9', '2', '2', 'configuration', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('11', '0', '-1,0,', '个人信息', '/system/user/selfInfo', null, null, '99', '1', '2', null, null, '0', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('12', '11', '-1,0,11,', '修改密码', null, null, null, '1', '2', '3', null, null, '1', null, 'selfinfo:md:password', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('13', '11', '-1,0,11,', '修改角色', null, null, null, '2', '2', '3', null, null, '1', null, 'selfinfo:md:role', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('14', '0', '-1,0,', '系统维护', null, null, 'icon-qrcode', '2', '1', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('15', '14', '-1,0,14,', '系统日志', '/admin/system/log/initList', null, 'icon-qrcode', '1', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('16', '14', '-1,0,14,', '在线用户', '/admin/system/online/initList', null, 'icon-qrcode', '2', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('17', '14', '-1,0,14,', '统计分析', null, null, 'icon-qrcode', '99', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('18', '14', '-1,0,14,', '缓存监控', '/admin/system/cache/monitor', null, 'icon-qrcode', '3', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('19', '14', '-1,0,14,', '数据库监控', null, null, 'icon-qrcode', '4', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '-1,0,1,', '菜单管理', '/admin/system/menu/initList', null, 'icon-qrcode', '1', '2', '2', 'addressbook', null, '1', null, null, null, null, null, '0', '超级管理员', '2014-12-15 14:36:45', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '0', '-1,0,', '微信平台', null, null, 'icon-th-large', '4', '1', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('21', '20', '-1,0,20,', '自定义菜单', '/admin/wechat/menu/list', null, 'icon-qrcode', '4', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('22', '20', '-1,0,20,', '公众账号', '/admin/weixin/pubAccount/list', null, 'icon-qrcode', '1', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('23', '20', '-1,0,20,', '粉丝管理', '/admin/weixin/fans/initList', null, null, '2', '2', '2', null, null, '1', null, null, null, null, null, '0', '超级管理员', '2014-12-17 07:06:36', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '20', '-1,0,20,', '素材管理', null, null, null, '99', '2', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('25', '24', '-1,0,20,24,', '默认回复设置', null, null, null, '1', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('26', '24', '-1,0,20,24,', '文本回复', null, null, null, '2', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('27', '24', '-1,0,20,24,', '图文回复', null, null, null, '3', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('28', '24', '-1,0,20,24,', 'LBS回复', null, null, null, '4', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('29', '20', '-1,0,20,', '群发消息', null, null, null, '3', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('3', '1', '-1,0,1,', '区域管理', '/admin/system/area/initList', null, 'icon-qrcode', '2', '2', '2', 'world', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('4', '1', '-1,0,1,', '组织管理', '/admin/system/office/initList', null, 'icon-qrcode', '3', '2', '2', 'multi-agents', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('5', '1', '-1,0,1,', '权限管理', '/admin/system/role/initList', null, 'icon-qrcode', '4', '2', '2', 'shield', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('6', '1', '-1,0,1,', '用户组管理', '/admin/system/group/initList', null, 'icon-qrcode', '5', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('7', '1', '-1,0,1,', '用户管理', '/admin/system/user/initList', null, 'icon-qrcode', '5', '2', '2', 'agent', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('8', '1', '-1,0,1,', '参数配置', '/admin/system/dict/initList', null, 'icon-qrcode', '8', '2', '2', 'processing', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('9', '1', '-1,0,1,', '任务管理', '/admin/system/task/initList', null, 'icon-qrcode', '7', '2', '2', 'edit', null, '1', null, null, null, null, null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `PARENT_ID` varchar(64) NOT NULL COMMENT '父级编号',
  `PARENT_IDS` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `AREA_ID` varchar(64) NOT NULL COMMENT '归属区域',
  `CODE` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `NAME` varchar(100) NOT NULL COMMENT '机构名称',
  `TYPE` char(1) NOT NULL COMMENT '机构类型',
  `ADDRESS` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `ZIP_CODE` varchar(100) DEFAULT NULL COMMENT '邮政编码',
  `MASTER` varchar(100) DEFAULT NULL COMMENT '负责人',
  `PHONE` varchar(200) DEFAULT NULL COMMENT '电话',
  `FAX` varchar(200) DEFAULT NULL COMMENT '传真',
  `EMAIL` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `LEVEL` tinyint(4) DEFAULT NULL,
  `PATH` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构表';

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES ('0', '-1', '-1,', '21', '001', '深圳市智信移科技有限公司', '1', null, null, null, null, null, null, null, null, null, '0', '超级管理员', '2014-12-16 22:45:58', null, '0', '1', '/深圳市智信移科技有限公司');
INSERT INTO `sys_office` VALUES ('21', '0', '-1,0,', '21', '001002', 'VIP会员部', '2', null, null, null, null, null, null, '0', '超级管理员', '2014-12-16 22:46:44', '0', '超级管理员', '2014-12-16 22:49:14', null, '0', '2', '/深圳市智信移科技有限公司/VIP会员部');
INSERT INTO `sys_office` VALUES ('22', '0', '-1,0,', '21', '001001', '软件实施部', '2', null, null, null, null, null, null, '0', '超级管理员', '2014-12-16 22:47:16', '0', '超级管理员', '2014-12-16 22:47:25', null, '0', '2', '/深圳市智信移科技有限公司/软件实施部');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `OFFICE_ID` varchar(64) DEFAULT NULL COMMENT '归属机构',
  `NAME` varchar(100) NOT NULL COMMENT '角色名称',
  `DATA_SCOPE` char(1) DEFAULT NULL COMMENT '数据范围',
  `CODE` varchar(30) DEFAULT NULL,
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色编号',
  `MENU_ID` varchar(64) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `USER_ID` varchar(64) NOT NULL COMMENT '用户编号',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `NAME` varchar(64) NOT NULL COMMENT '任务名称',
  `TYPE` varchar(15) NOT NULL COMMENT '任务类型',
  `CRON_EXPRESSION` varchar(100) DEFAULT NULL COMMENT '调度表达式',
  `ALLOW_EXECUTE_COUNT` int(11) DEFAULT NULL COMMENT '允许执行的次数',
  `YET_EXECUTE_COUNT` int(11) DEFAULT '0' COMMENT '已经执行的次数',
  `FAIL_EXECUTE_COUNT` int(11) DEFAULT '0' COMMENT '执行失败的次数',
  `PRE_EXECUTE_TIME` datetime DEFAULT NULL COMMENT '上一次执行时间',
  `NEXT_EXECUTE_TIME` datetime DEFAULT NULL COMMENT '预计下一次执行时间',
  `BUSINESS_OBJECT` char(200) DEFAULT NULL COMMENT '业务对象',
  `BUSINESS_OBJECT_NAME` varchar(200) DEFAULT NULL COMMENT '业务对象名称',
  `TASK_STATUS` char(10) DEFAULT '0' COMMENT '任务状态',
  `MANUAL_OPERATION` int(11) DEFAULT NULL COMMENT '手动执行次数',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表';

-- ----------------------------
-- Records of sys_task
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` varchar(64) NOT NULL COMMENT '编号',
  `OFFICE_ID` varchar(64) NOT NULL COMMENT '归属部门',
  `LOGIN_NAME` varchar(100) NOT NULL COMMENT '登录名',
  `PASSWORD` varchar(200) DEFAULT NULL COMMENT '密码',
  `NO` varchar(100) DEFAULT NULL COMMENT '工号',
  `NAME` varchar(100) NOT NULL COMMENT '姓名',
  `EMAIL` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(200) DEFAULT NULL COMMENT '电话',
  `MOBILE` varchar(200) DEFAULT NULL COMMENT '手机',
  `BIRTHDAY` date DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `DUTY` varchar(255) DEFAULT NULL,
  `LOGIN_IP` varchar(100) DEFAULT NULL COMMENT '最后登陆IP：待定',
  `LOGIN_DATE` datetime DEFAULT NULL COMMENT '最后登陆时间：待定',
  `STATUS` tinyint(4) DEFAULT '1' COMMENT '用户状态',
  `ZIP_CODE` varchar(20) DEFAULT NULL COMMENT '邮编',
  `NICK_NAME` varchar(100) DEFAULT NULL COMMENT '昵称',
  `ID_CARD_NO` varchar(80) DEFAULT NULL COMMENT '身份证号',
  `SEX` char(1) DEFAULT NULL COMMENT '性别',
  `USER_TYPE` varchar(20) DEFAULT NULL COMMENT '用户类型',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `PROVINCE` varchar(100) DEFAULT NULL COMMENT '省',
  `CITY` varchar(100) DEFAULT NULL COMMENT '市',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0', '0', 'SuperAdmin', '940e2c8569a39cc8c798e4df18a32384fa365e412f6c2c95a782e140', '000', '超级管理员', '618lf@163.com', null, null, null, null, null, null, null, '4', null, null, null, null, null, null, null, null, '0', '超级管理员', '2014-12-15 17:58:52', null, '0', null, null);
INSERT INTO `sys_user` VALUES ('21', '21', 'oabu2swPGUS8s5hHfPgbnZ4RaxiM', '6b136e89ef39c01a5fac0048dc050a5527d175e4f4c61d87a32acc98', null, '李锋', null, null, null, null, null, null, null, null, '1', null, '李锋', null, '1', null, null, null, '2014-12-17 14:17:06', null, null, null, null, '0', '广东', '深圳');
INSERT INTO `sys_user` VALUES ('41', '21', 'oabu2syy4xHuMvHy1pAq9mKfiljU', '7503e5633ef1dab01fabf1aedfc49ec3e9465ad806e798d6ebfd96ba', null, 'joyce', null, null, null, null, null, null, null, null, '1', null, 'joyce', null, '2', null, null, null, '2014-12-17 15:42:35', null, null, null, null, '0', '广东', '深圳');

-- ----------------------------
-- Table structure for tax_business_type
-- ----------------------------
DROP TABLE IF EXISTS `tax_business_type`;
CREATE TABLE `tax_business_type` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `NAME` varchar(100) NOT NULL COMMENT '名称',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_business_type
-- ----------------------------

-- ----------------------------
-- Table structure for tax_category
-- ----------------------------
DROP TABLE IF EXISTS `tax_category`;
CREATE TABLE `tax_category` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `NAME` varchar(64) NOT NULL COMMENT '名称',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_category
-- ----------------------------

-- ----------------------------
-- Table structure for tax_category_item
-- ----------------------------
DROP TABLE IF EXISTS `tax_category_item`;
CREATE TABLE `tax_category_item` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `NAME` varchar(64) NOT NULL COMMENT '名称',
  `CATEGORY_ID` varchar(64) NOT NULL COMMENT '税种',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_category_item
-- ----------------------------

-- ----------------------------
-- Table structure for tax_doc
-- ----------------------------
DROP TABLE IF EXISTS `tax_doc`;
CREATE TABLE `tax_doc` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `DOC_NAME` varchar(300) NOT NULL COMMENT '文件名',
  `DOC_NUM` varchar(200) NOT NULL COMMENT '文件号',
  `DOC_TYPE` varchar(500) DEFAULT NULL COMMENT '文件类型',
  `DOC_DATE` datetime DEFAULT NULL COMMENT '成文日期',
  `DOC_CONTENT` varchar(300) DEFAULT NULL COMMENT '文件内容地址',
  `ENABLED` varchar(20) DEFAULT NULL COMMENT '是否有效',
  `PUB_DATE` datetime DEFAULT NULL COMMENT '发布日期',
  `PUB_OFFICE` varchar(200) DEFAULT NULL COMMENT '发布机关',
  `CATEGORYS` varchar(400) DEFAULT NULL COMMENT '税种s',
  `SOURCE_URL` varchar(250) DEFAULT NULL COMMENT '数据来源',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_doc
-- ----------------------------

-- ----------------------------
-- Table structure for tax_indutry
-- ----------------------------
DROP TABLE IF EXISTS `tax_indutry`;
CREATE TABLE `tax_indutry` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `NAME` varchar(100) NOT NULL COMMENT '名称',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_indutry
-- ----------------------------

-- ----------------------------
-- Table structure for tax_pub_office
-- ----------------------------
DROP TABLE IF EXISTS `tax_pub_office`;
CREATE TABLE `tax_pub_office` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `NAME` varchar(200) NOT NULL COMMENT '发布机关',
  `AREA` char(2) NOT NULL COMMENT '发布机关区域',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_pub_office
-- ----------------------------

-- ----------------------------
-- Table structure for tax_qa
-- ----------------------------
DROP TABLE IF EXISTS `tax_qa`;
CREATE TABLE `tax_qa` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `TITLE` varchar(500) DEFAULT NULL COMMENT '标题',
  `CONTENT` varchar(2000) DEFAULT NULL COMMENT '标题',
  `ANSWER` varchar(5000) DEFAULT NULL COMMENT '标题',
  `BUSINESS_TYPE_ID` varchar(64) DEFAULT NULL COMMENT '业务类型ID',
  `INDUTRY_ID` varchar(64) DEFAULT NULL COMMENT '所属行业',
  `BUSINESS_TYPE_NAME` varchar(100) DEFAULT NULL COMMENT '业务类型',
  `INDUTRY_NAME` varchar(100) DEFAULT NULL COMMENT '所属行业',
  `PUBLISH_DATE` varchar(20) DEFAULT NULL COMMENT '发布日期:YYYY-MM-DD',
  `PUBLISH_OFFICE` varchar(200) DEFAULT NULL COMMENT '发布机关',
  `PUBLISH_CODE` varchar(200) DEFAULT NULL COMMENT '发布编码',
  `SOURCE_URL` varchar(250) DEFAULT NULL COMMENT '数据来源',
  `REMARKS2` varchar(255) DEFAULT NULL COMMENT '备注2',
  `REMARKS3` varchar(255) DEFAULT NULL COMMENT '备注3',
  `KEY_WORDS` varchar(255) DEFAULT NULL COMMENT '自定义关键字',
  `STORAGE_FLAG` char(1) DEFAULT '0' COMMENT '是否入库',
  `MAKE_Q_FLAG` char(1) DEFAULT '0' COMMENT '是否编题',
  `MAKE_ID` varchar(64) DEFAULT NULL COMMENT '编制试题创建者',
  `MAKE_NAME` varchar(100) DEFAULT NULL COMMENT '编制试题创建者',
  `MAKE_DATE` datetime DEFAULT NULL COMMENT '编制试题时间',
  `MAKE_STATUS` varchar(10) DEFAULT NULL COMMENT '试题编制进度',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  `VERSION` int(10) DEFAULT NULL COMMENT '版本',
  `Q_FROM` varchar(10) DEFAULT NULL COMMENT '类型：答疑、考试 、自定义',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_qa
-- ----------------------------

-- ----------------------------
-- Table structure for tax_qa_rela
-- ----------------------------
DROP TABLE IF EXISTS `tax_qa_rela`;
CREATE TABLE `tax_qa_rela` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `QA_ID` varchar(64) NOT NULL COMMENT 'QA主键',
  `CATEGORY_ID` varchar(64) DEFAULT NULL COMMENT '税种',
  `CATEGORY_ITEM_ID` varchar(64) DEFAULT NULL COMMENT '税种明细',
  `LAR_DOC_NUM` varchar(200) DEFAULT NULL COMMENT '法规文号',
  `LAR_DOC_NAME` varchar(200) DEFAULT NULL COMMENT '法规文件名',
  `LAR_DOC_CONTENT` varchar(2000) DEFAULT NULL COMMENT '法规文件内容',
  `APPLY_PROVINCE` varchar(50) DEFAULT NULL COMMENT '是否全国通用',
  `APPLY_CITY` varchar(100) DEFAULT NULL COMMENT '适用省/市',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_qa_rela
-- ----------------------------

-- ----------------------------
-- Table structure for tax_topic
-- ----------------------------
DROP TABLE IF EXISTS `tax_topic`;
CREATE TABLE `tax_topic` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `CODE` varchar(64) NOT NULL COMMENT '编码',
  `NAME` varchar(64) DEFAULT NULL COMMENT '名称',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '试题状态,公开，不公开',
  `Q_ID` varchar(64) DEFAULT NULL COMMENT '来源ID',
  `Q_FROM` varchar(10) DEFAULT NULL COMMENT '来源：答疑、考试 、自己添加',
  `Q_TYPE` varchar(20) DEFAULT NULL COMMENT '试题类型',
  `Q_LEVEL` varchar(10) DEFAULT NULL COMMENT '试题难度',
  `POST_MINUTE` int(10) DEFAULT NULL COMMENT '答题时长',
  `CONTENT` varchar(4000) DEFAULT NULL COMMENT '题干',
  `SKEY` varchar(200) DEFAULT NULL COMMENT '答案',
  `KEY_DESC` varchar(4000) DEFAULT NULL COMMENT '答案解析',
  `TYPE` varchar(20) DEFAULT NULL COMMENT '进度：初始,初审,终审',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建人',
  `CREATE_NAME` varchar(64) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  `VERSION` int(10) DEFAULT NULL COMMENT '版本',
  `BUSINESS_TYPE_ID` varchar(64) DEFAULT NULL COMMENT '业务类型ID',
  `INDUTRY_ID` varchar(64) DEFAULT NULL COMMENT '所属行业',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_topic
-- ----------------------------

-- ----------------------------
-- Table structure for tax_topic_content
-- ----------------------------
DROP TABLE IF EXISTS `tax_topic_content`;
CREATE TABLE `tax_topic_content` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `TOPIC_ID` varchar(64) NOT NULL COMMENT '题目ID',
  `Q_TYPE` varchar(20) DEFAULT NULL COMMENT '试题类型',
  `Q_LEVEL` varchar(10) DEFAULT NULL COMMENT '试题难度',
  `POST_MINUTE` int(10) DEFAULT NULL COMMENT '答题时长',
  `CONTENT` varchar(4000) DEFAULT NULL COMMENT '题干',
  `SKEY` varchar(200) DEFAULT NULL COMMENT '答案',
  `KEY_DESC` varchar(4000) DEFAULT NULL COMMENT '答案解析',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建人',
  `CREATE_NAME` varchar(64) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  `TYPE` varchar(20) DEFAULT NULL COMMENT '类型：初始,初审,终审',
  `BUSINESS_TYPE_ID` varchar(64) DEFAULT NULL COMMENT '业务类型ID',
  `INDUTRY_ID` varchar(64) DEFAULT NULL COMMENT '所属行业',
  `VERSION` int(10) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_topic_content
-- ----------------------------

-- ----------------------------
-- Table structure for tax_topic_options
-- ----------------------------
DROP TABLE IF EXISTS `tax_topic_options`;
CREATE TABLE `tax_topic_options` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `TOPIC_ID` varchar(64) NOT NULL COMMENT '题目ID',
  `CONTENT_ID` varchar(64) NOT NULL COMMENT '内容ID',
  `SOPTION` varchar(10) DEFAULT NULL COMMENT '选择项',
  `SALISA` varchar(500) DEFAULT NULL COMMENT '选择项描述',
  `TYPE` varchar(20) DEFAULT NULL COMMENT '类型：初始,初审,终审',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_topic_options
-- ----------------------------

-- ----------------------------
-- Table structure for tax_topic_rela
-- ----------------------------
DROP TABLE IF EXISTS `tax_topic_rela`;
CREATE TABLE `tax_topic_rela` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `TOPIC_ID` varchar(64) NOT NULL COMMENT '试题主键',
  `CATEGORY_ID` varchar(64) NOT NULL COMMENT '税种',
  `CATEGORY_ITEM_ID` varchar(64) NOT NULL COMMENT '税种明细',
  `LAR_DOC_NUM` varchar(200) NOT NULL COMMENT '法规文号',
  `LAR_DOC_NAME` varchar(200) NOT NULL COMMENT '法规文件名',
  `LAR_DOC_CONTENT` varchar(2000) NOT NULL COMMENT '法规文件内容',
  `APPLY_PROVINCE` varchar(50) DEFAULT NULL COMMENT '是否全国通用',
  `APPLY_CITY` varchar(100) DEFAULT NULL COMMENT '适用省/市',
  `TYPE` varchar(20) DEFAULT NULL COMMENT '类型：初始,初审,终审',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tax_topic_rela
-- ----------------------------

-- ----------------------------
-- Table structure for wx_pub_account
-- ----------------------------
DROP TABLE IF EXISTS `wx_pub_account`;
CREATE TABLE `wx_pub_account` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `NAME` varchar(200) DEFAULT NULL COMMENT '公众账户名称',
  `TYPE` varchar(20) DEFAULT NULL COMMENT '公众账户类型',
  `PIC` varchar(200) DEFAULT NULL COMMENT '公众账户图片',
  `EMAIL` varchar(100) DEFAULT NULL COMMENT '登录邮箱',
  `SRC_ID` varchar(100) DEFAULT NULL COMMENT '原始id',
  `WX_NUM` varchar(100) DEFAULT NULL COMMENT '微信号',
  `AUTHEN_STATE` varchar(20) DEFAULT NULL COMMENT '微信认证状态',
  `AUTHEN_DESC` varchar(500) DEFAULT NULL COMMENT '微信认证描述',
  `PRINCIPAL` varchar(500) DEFAULT NULL COMMENT '主体信息',
  `ADDRESS` varchar(500) DEFAULT NULL COMMENT '地址',
  `QR_CODE` varchar(200) DEFAULT NULL COMMENT '二维码',
  `APP_ID` varchar(100) DEFAULT NULL COMMENT 'APPID',
  `APP_SECRET` varchar(100) DEFAULT NULL COMMENT 'APP_SECRET',
  `TOKEN` varchar(100) DEFAULT NULL COMMENT '公众帐号TOKEN,信息验证',
  `ENCODING_AES_KEY` varchar(100) DEFAULT NULL COMMENT '消息加解密密钥',
  `OPEN_WX_ID` varchar(200) DEFAULT NULL COMMENT '绑定的开发平台用户Id',
  `CREATE_ID` varchar(64) DEFAULT NULL COMMENT '创建者',
  `CREATE_NAME` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_ID` varchar(64) DEFAULT NULL COMMENT '更新者',
  `UPDATE_NAME` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '更新时间',
  `REMARKS` varchar(255) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_pub_account
-- ----------------------------
INSERT INTO `wx_pub_account` VALUES ('1', '税务公社', '服务号', null, null, null, null, '', null, null, null, null, 'wx047b7c74d64a3389', '01b1524a2a75d776ab192c660bba6797', 'taxsns', null, null, null, null, null, null, null, null, null);
INSERT INTO `wx_pub_account` VALUES ('2', '税务公社', '订阅号', null, null, null, null, null, null, null, null, null, 'wxeff9811824f0f3b6', '547fcb9f399c1b8f6cf029993c8a337b', 'taxsns', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for wx_receive_message
-- ----------------------------
DROP TABLE IF EXISTS `wx_receive_message`;
CREATE TABLE `wx_receive_message` (
  `ID` varchar(64) NOT NULL COMMENT '主键',
  `MSG_ID` varchar(64) DEFAULT NULL COMMENT '消息ID',
  `MSG_TYPE` varchar(20) NOT NULL COMMENT '消息类型',
  `FROM_USER_NAME` varchar(100) DEFAULT NULL COMMENT '消息发送者OPENID',
  `TO_USER_NAME` varchar(100) DEFAULT NULL COMMENT '消息接收者',
  `CONTENT` varchar(1000) DEFAULT NULL COMMENT '收到的消息',
  `RES_CONTENT` varchar(1000) DEFAULT NULL COMMENT '回复的消息',
  `CREATE_TIME` varchar(64) DEFAULT NULL COMMENT '收到消息的时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_receive_message
-- ----------------------------

-- ----------------------------
-- View structure for v_tax_q_from
-- ----------------------------
DROP VIEW IF EXISTS `v_tax_q_from`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tax_q_from` AS select 'QA' AS `Q_FROM` from `sys_dual` union all select 'EXAM' AS `Q_FROM` from `sys_dual` union all select 'CUSTOM' AS `Q_FROM` from `sys_dual` ;

-- ----------------------------
-- View structure for v_tax_q_level
-- ----------------------------
DROP VIEW IF EXISTS `v_tax_q_level`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tax_q_level` AS select 'I' AS `Q_LEVEL` from `sys_dual` union all select 'II' AS `Q_LEVEL` from `sys_dual` union all select 'III' AS `Q_LEVEL` from `sys_dual` ;

-- ----------------------------
-- View structure for v_tax_q_type
-- ----------------------------
DROP VIEW IF EXISTS `v_tax_q_type`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tax_q_type` AS select 'ONE_CHOICE' AS `Q_TYPE` from `sys_dual` union all select 'MULT_CHOICE' AS `Q_TYPE` from `sys_dual` union all select 'ONE_MORE_CHOICE' AS `Q_TYPE` from `sys_dual` union all select 'TFNG' AS `Q_TYPE` from `sys_dual` union all select 'COUNTING' AS `Q_TYPE` from `sys_dual` ;

-- ----------------------------
-- View structure for v_tax_type_level
-- ----------------------------
DROP VIEW IF EXISTS `v_tax_type_level`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tax_type_level` AS select `v_tax_q_type`.`Q_TYPE` AS `Q_TYPE`,`v_tax_q_level`.`Q_LEVEL` AS `Q_LEVEL` from (`v_tax_q_type` join `v_tax_q_level`) ;

-- ----------------------------
-- Function structure for F_NEXT_VALUE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_NEXT_VALUE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `F_NEXT_VALUE`(`TABLENAME` VARCHAR(50)) RETURNS varchar(50) CHARSET utf8
BEGIN
 DECLARE V_SEQ_STEP VARCHAR (25);
 UPDATE SYS_ID_STORE SET CURRENT_VALUE = LAST_INSERT_ID(GREATEST(CURRENT_VALUE,MIN_VALUE) + STEP) WHERE TABLE_NAME = TABLENAME;
    SET V_SEQ_STEP = CONCAT(LAST_INSERT_ID(),'-'); 
    SET V_SEQ_STEP = CONCAT(V_SEQ_STEP,(SELECT STEP FROM SYS_ID_STORE WHERE TABLE_NAME = TABLENAME));
 RETURN V_SEQ_STEP;
END
;;
DELIMITER ;
