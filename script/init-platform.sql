SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- 业务类型
-- ----------------------------
DROP TABLE IF EXISTS  TAX_BUSINESS_TYPE;
CREATE TABLE TAX_BUSINESS_TYPE(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   NAME  VARCHAR(100) NOT NULL COMMENT '名称',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 行业
-- ----------------------------
DROP TABLE IF EXISTS  TAX_INDUTRY;
CREATE TABLE TAX_INDUTRY(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   NAME  VARCHAR(100) NOT NULL COMMENT '名称',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 税种
-- ----------------------------
DROP TABLE IF EXISTS TAX_CATEGORY;
CREATE TABLE TAX_CATEGORY(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   NAME  VARCHAR(64) NOT NULL COMMENT '名称',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 税种 - 税种明细
-- ----------------------------
DROP TABLE IF EXISTS TAX_CATEGORY_ITEM;
CREATE TABLE TAX_CATEGORY_ITEM(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   NAME  VARCHAR(64) NOT NULL COMMENT '名称',
   CATEGORY_ID VARCHAR(64) NOT NULL COMMENT '税种',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库  - 发布机关
-- ----------------------------
DROP TABLE IF EXISTS  TAX_PUB_OFFICE;
CREATE TABLE TAX_PUB_OFFICE(
  ID       VARCHAR(64) NOT NULL COMMENT '主键',
  NAME VARCHAR(200) NOT NULL COMMENT '发布机关',
  AREA CHAR(2) NOT NULL COMMENT '发布机关区域',
  PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 汇编（答疑、考试）
-- ----------------------------
DROP TABLE IF EXISTS  TAX_QA;
CREATE TABLE TAX_QA(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   TITLE VARCHAR(500) COMMENT '标题',
   CONTENT VARCHAR(2000) COMMENT '标题',
   ANSWER  VARCHAR(5000) COMMENT '标题',
   BUSINESS_TYPE_ID VARCHAR(64) COMMENT '业务类型ID',
   INDUTRY_ID VARCHAR(64) COMMENT '所属行业',
   BUSINESS_TYPE_NAME VARCHAR(100) COMMENT '业务类型',
   INDUTRY_NAME VARCHAR(100) COMMENT '所属行业',
   PUBLISH_DATE VARCHAR(20) COMMENT '发布日期:YYYY-MM-DD',
   PUBLISH_OFFICE VARCHAR(200) COMMENT '发布机关',
   PUBLISH_CODE VARCHAR(200) COMMENT '发布编码',
   SOURCE_URL VARCHAR(250) COMMENT '数据来源',
   REMARKS2 VARCHAR(255) COMMENT '备注2',
   REMARKS3 VARCHAR(255) COMMENT '备注3',
   KEY_WORDS VARCHAR(255) COMMENT '自定义关键字',
   STORAGE_FLAG CHAR(1) DEFAULT '0' COMMENT '是否入库',
   MAKE_Q_FLAG  CHAR(1) DEFAULT '0' COMMENT '是否编题',
   MAKE_ID  VARCHAR(64) DEFAULT NULL COMMENT '编制试题创建者',
   MAKE_NAME  VARCHAR(100) DEFAULT NULL COMMENT '编制试题创建者',
   MAKE_DATE  DATETIME DEFAULT NULL COMMENT '编制试题时间',
   MAKE_STATUS VARCHAR(10) COMMENT '试题编制进度',
   CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
   CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
   CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
   UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
   UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
   UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
   REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   VERSION  INTEGER(10) COMMENT '版本',
   Q_FROM  VARCHAR(10) COMMENT '类型：答疑、考试 、自定义',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 汇编  - 税种关联表
-- ----------------------------
DROP TABLE IF EXISTS  TAX_QA_RELA;
CREATE TABLE TAX_QA_RELA(
  ID    VARCHAR(64) NOT NULL COMMENT '主键',
  QA_ID VARCHAR(64) NOT NULL COMMENT 'QA主键',
  CATEGORY_ID VARCHAR(64) COMMENT '税种',
  CATEGORY_ITEM_ID VARCHAR(64) COMMENT '税种明细',
  LAR_DOC_NUM VARCHAR(200) COMMENT '法规文号',
  LAR_DOC_NAME VARCHAR(200) COMMENT '法规文件名',
  LAR_DOC_CONTENT VARCHAR(2000) COMMENT '法规文件内容',
  APPLY_PROVINCE VARCHAR(50) COMMENT '是否全国通用',
  APPLY_CITY VARCHAR(100) COMMENT '适用省/市',
  PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库 -- 题目(审核完成之后将内容等信息传送到相应字段)
-- ----------------------------
DROP TABLE IF EXISTS TAX_TOPIC;
CREATE TABLE TAX_TOPIC(
   ID    VARCHAR(64) NOT NULL COMMENT '主键',
   CODE  VARCHAR(64) NOT NULL COMMENT '编码',
   NAME  VARCHAR(64) COMMENT '名称',
   STATUS VARCHAR(10) COMMENT '试题状态,公开，不公开',
   Q_ID VARCHAR(64)  COMMENT '来源ID',
   Q_FROM  VARCHAR(10) COMMENT '来源：答疑、考试 、自己添加',
   Q_TYPE      VARCHAR(20) COMMENT '试题类型',
   Q_LEVEL     VARCHAR(10) COMMENT '试题难度',
   POST_MINUTE INTEGER(10) COMMENT '答题时长',
   CONTENT     VARCHAR(4000) COMMENT '题干',
   SKEY       VARCHAR(200) COMMENT '答案',
   KEY_DESC    VARCHAR(4000) COMMENT '答案解析',
   TYPE VARCHAR(20) COMMENT '进度：初始,初审,终审',
   CREATE_ID   VARCHAR(64) COMMENT '创建人',
   CREATE_NAME VARCHAR(64) COMMENT '创建人',
   CREATE_DATE DATETIME COMMENT '创建时间',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   VERSION  INTEGER(10) COMMENT '版本',
   BUSINESS_TYPE_ID VARCHAR(64) COMMENT '业务类型ID',
   INDUTRY_ID VARCHAR(64) COMMENT '所属行业',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库 -- 题目审核(审核可以修改)
-- ----------------------------
DROP TABLE IF EXISTS TAX_TOPIC_CONTENT;
CREATE TABLE TAX_TOPIC_CONTENT(
   ID          VARCHAR(64) NOT NULL COMMENT '主键',
   TOPIC_ID    VARCHAR(64) NOT NULL COMMENT '题目ID',
   Q_TYPE      VARCHAR(20) COMMENT '试题类型',
   Q_LEVEL     VARCHAR(10) COMMENT '试题难度',
   POST_MINUTE INTEGER(10) COMMENT '答题时长',
   CONTENT     VARCHAR(4000) COMMENT '题干',
   SKEY       VARCHAR(200) COMMENT '答案',
   KEY_DESC    VARCHAR(4000) COMMENT '答案解析',
   CREATE_ID   VARCHAR(64) COMMENT '创建人',
   CREATE_NAME VARCHAR(64) COMMENT '创建人',
   CREATE_DATE DATETIME COMMENT '创建时间',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   TYPE VARCHAR(20) COMMENT '类型：初始,初审,终审',
   BUSINESS_TYPE_ID VARCHAR(64) COMMENT '业务类型ID',
   INDUTRY_ID VARCHAR(64) COMMENT '所属行业',
   VERSION  INTEGER(10) COMMENT '版本',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库 -- 题目选项
-- ----------------------------
DROP TABLE IF EXISTS TAX_TOPIC_OPTIONS;
CREATE TABLE TAX_TOPIC_OPTIONS(
   ID          VARCHAR(64) NOT NULL COMMENT '主键',
   TOPIC_ID    VARCHAR(64) NOT NULL COMMENT '题目ID',
   CONTENT_ID  VARCHAR(64) NOT NULL COMMENT '内容ID',
   SOPTION VARCHAR(10) COMMENT '选择项',
   SALISA   VARCHAR(500) COMMENT '选择项描述',
   TYPE VARCHAR(20) COMMENT '类型：初始,初审,终审',
   DEL_FLAG CHAR(1) DEFAULT '0' COMMENT '删除标记',
   PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库  - 税种关联表
-- ----------------------------
DROP TABLE IF EXISTS  TAX_TOPIC_RELA;
CREATE TABLE TAX_TOPIC_RELA(
  ID    VARCHAR(64) NOT NULL COMMENT '主键',
  TOPIC_ID VARCHAR(64) NOT NULL COMMENT '试题主键',
  CATEGORY_ID VARCHAR(64) NOT NULL COMMENT '税种',
  CATEGORY_ITEM_ID VARCHAR(64) NOT NULL COMMENT '税种明细',
  LAR_DOC_NUM VARCHAR(200) NOT NULL COMMENT '法规文号',
  LAR_DOC_NAME VARCHAR(200) NOT NULL COMMENT '法规文件名',
  LAR_DOC_CONTENT VARCHAR(2000) NOT NULL COMMENT '法规文件内容',
  APPLY_PROVINCE VARCHAR(50) COMMENT '是否全国通用',
  APPLY_CITY VARCHAR(100) COMMENT '适用省/市',
  TYPE VARCHAR(20) COMMENT '类型：初始,初审,终审',
  PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 题库  - 法规文件
-- ----------------------------
DROP TABLE IF EXISTS  TAX_DOC;
CREATE TABLE TAX_DOC(
  ID       VARCHAR(64) NOT NULL COMMENT '主键',
  DOC_NAME VARCHAR(300) NOT NULL COMMENT '文件名',
  DOC_NUM  VARCHAR(200) NOT NULL COMMENT '文件号',
  DOC_TYPE VARCHAR(500) COMMENT '文件类型',
  DOC_DATE DATETIME COMMENT '成文日期',
  DOC_CONTENT VARCHAR(300) COMMENT '文件内容地址',
  ENABLED  VARCHAR(20) COMMENT '是否有效',
  PUB_DATE DATETIME COMMENT '发布日期',
  PUB_OFFICE VARCHAR(200) COMMENT '发布机关',
  CATEGORYS  VARCHAR(400) COMMENT '税种s',
  SOURCE_URL VARCHAR(250) COMMENT '数据来源',
  PRIMARY KEY (ID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 创建相应的视图
-- ----------------------------
CREATE OR REPLACE VIEW V_TAX_Q_TYPE
AS       SELECT 'ONE_CHOICE' Q_TYPE FROM SYS_DUAL UNION ALL
	     SELECT 'MULT_CHOICE' Q_TYPE FROM SYS_DUAL UNION ALL
	     SELECT 'ONE_MORE_CHOICE' Q_TYPE FROM SYS_DUAL UNION ALL
	     SELECT 'TFNG' Q_TYPE FROM SYS_DUAL UNION ALL
	     SELECT 'COUNTING' Q_TYPE FROM SYS_DUAL;

CREATE OR REPLACE VIEW V_TAX_Q_LEVEL
AS       SELECT 'I' Q_LEVEL FROM SYS_DUAL UNION ALL
	     SELECT 'II' Q_LEVEL FROM SYS_DUAL UNION ALL
	     SELECT 'III' Q_LEVEL FROM SYS_DUAL;

CREATE OR REPLACE VIEW V_TAX_TYPE_LEVEL
AS SELECT * FROM V_TAX_Q_TYPE, V_TAX_Q_LEVEL;

CREATE OR REPLACE VIEW V_TAX_Q_FROM
AS       SELECT 'QA' Q_FROM FROM SYS_DUAL UNION ALL
	     SELECT 'EXAM' Q_FROM FROM SYS_DUAL UNION ALL
	     SELECT 'CUSTOM' Q_FROM FROM SYS_DUAL;
	     
-- ----------------------------
-- 初始化数据(基础数据)
-- ----------------------------
INSERT INTO SYS_MENU VALUES ('30', '0', '-1,0,', '税务平台', null, null, 'icon-th-large', '3', '1', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('31', '30', '-1,0,30,', '业务类型', '/platform/businessType/initList', null, 'icon-qrcode', '1', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('32', '30', '-1,0,30,', '行业管理', '/platform/indutry/initList', null, 'icon-qrcode', '2', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('33', '30', '-1,0,30,', '税种管理', '/platform/category/initList', null, 'icon-qrcode', '3', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('34', '30', '-1,0,30,', '税种明细', '/platform/categoryItem/initList', null, 'icon-qrcode', '4', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('35', '30', '-1,0,30,', '税收法规', '/admin/platform/taxdoc/initList', null, 'icon-qrcode', '5', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');

INSERT INTO SYS_MENU VALUES ('36', '30', '-1,0,30,', '答疑汇编', '/platform/qa/initList', null, 'icon-qrcode', '6', '2', '2', 'lookup', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('37', '36', '-1,0,30,36,', '答疑筛选:发布机关:华东地区', null, null, null, '1', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:HD', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('38', '36', '-1,0,30,36,', '答疑筛选:发布机关:华南地区', null, null, null, '2', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:HN', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('39', '36', '-1,0,30,36,', '答疑筛选:发布机关:华中地区', null, null, null, '3', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:HZ', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('40', '36', '-1,0,30,36,', '答疑筛选:发布机关:华北地区', null, null, null, '4', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:HB', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('41', '36', '-1,0,30,36,', '答疑筛选:发布机关:西北地区', null, null, null, '5', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:XB', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('42', '36', '-1,0,30,36,', '答疑筛选:发布机关:西南地区', null, null, null, '6', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:XN', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('43', '36', '-1,0,30,36,', '答疑筛选:发布机关:东北地区', null, null, null, '7', '3', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:AREA:DB', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('44', '36', '-1,0,30,36,', '答疑汇编:试题筛选:修改所有数据', null, null, null, '99', '3', '3', null, null, '1', null, 'PLATFORM:QA:UPDATE:ALL', null, null, null, null, null, null, null, '0');

INSERT INTO SYS_MENU VALUES ('45', '30', '-1,0,30,', '题库管理', null, null, 'icon-th', '7', '2', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('46', '45', '-1,0,30,45,', '试题编制', '/admin/platform/topic/initList', null, 'icon-qrcode', '1', '3', '2', 'add-item', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('47', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:个人所得税', null, null, null, '1', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:PERSONTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('48', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:企业所得税', null, null, null, '2', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:COMPANYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('49', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:增值税', null, null, null, '3', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:ZZTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('50', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:消费税', null, null, null, '4', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:XFTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('51', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:土地增值税', null, null, null, '5', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:TDZZTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('52', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:印花税', null, null, null, '7', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:YHTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('53', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:城市维护建设税', null, null, null, '8', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:CSWHJSTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('54', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:文化事业建设费', null, null, null, '9', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:WHSYJSTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('55', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:契税', null, null, null, '10', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:QTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('56', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:耕地占用税', null, null, null, '11', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:GDZYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('57', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:资源税', null, null, null, '6', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:ZYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('58', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:车船税', null, null, null, '12', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:CCTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('59', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:车辆购置税', null, null, null, '13', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:CLGZTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('60', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:城镇土地使用税', null, null, null, '14', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:CZTDSYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('61', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:烟叶税', null, null, null, '15', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:YYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('62', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:教育费附加', null, null, null, '16', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:JYFFJTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('63', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:房产税', null, null, null, '17', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:FCTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('64', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:营业税', null, null, null, '18', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:YINGYTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('65', '46', '-1,0,30,45,46,', '试题编制:答疑汇编:税种筛选:其他', null, null, null, '19', '4', '3', null, null, '1', null, 'PLATFORM:QA:FILTER:TOPIC:CATEGORY:OTHERTAX', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('66', '46', '-1,0,30,45,46,', '查看所有试题', null, null, null, '99', '4', '3', null, null, '1', null, 'PLATFORM:TOPIC:VIEW:ALL', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('67', '46', '-1,0,30,45,46,', '修改所有试题', null, null, null, '100', '4', '3', null, null, '1', null, 'PLATFORM:TOPIC:UPDATE:ALL', null, null, null, null, null, null, null, '0');

INSERT INTO SYS_MENU VALUES ('68', '45', '-1,0,30,45,', '试题初审', '/admin/platform/topic/initVList', null, 'icon-qrcode', '2', '3', '2', 'configuration02', null, '1', null, 'TOPIC:AUDIT:INIT_VERITY', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('69', '45', '-1,0,30,45,', '试题终审', '/admin/platform/topic/initFvList', null, 'icon-qrcode', '3', '3', '2', 'configuration', null, '1', null, 'TOPIC:AUDIT:FINALLY_VERITY', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('70', '45', '-1,0,30,45,', '试题发布', '/admin/platform/topic/initFList', null, 'icon-qrcode', '4', '3', '2', 'database', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('71', '30', '-1,0,30,', '报表中心', null, null, 'icon-qrcode', '99', '2', '1', 'bar-chart', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('72', '71', '-1,0,30,71,', '按题型统计', '/admin/platform/report/listByQTypeAndQLevel', null, 'icon-qrcode', '1', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('73', '71', '-1,0,30,71,', '按行业统计', '/admin/platform/report/listByIndutry', null, 'icon-qrcode', '3', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('74', '71', '-1,0,30,71,', '按税种统计', '/admin/platform/report/listByCategory', null, 'icon-qrcode', '2', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('75', '71', '-1,0,30,71,', '按业务类型统计', '/admin/platform/report/listByBusinessType', null, 'icon-qrcode', '4', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('76', '71', '-1,0,30,71,', '按试题来源统计', '/admin/platform/report/listByFrom', null, 'icon-qrcode', '5', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('77', '71', '-1,0,30,71,', '按税种明细统计', '/admin/platform/report/listByCategoryItem', null, 'icon-qrcode', '6', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
