SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- 系统菜单表
-- ----------------------------
DROP TABLE IF EXISTS SYS_MENU;
CREATE TABLE SYS_MENU (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  PARENT_ID VARCHAR(64) NOT NULL COMMENT '父级编号',
  PARENT_IDS VARCHAR(2000) NOT NULL COMMENT '所有父级编号',
  NAME VARCHAR(100) NOT NULL COMMENT '菜单名称',
  HREF VARCHAR(255) DEFAULT NULL COMMENT '链接',
  TARGET VARCHAR(20) DEFAULT NULL COMMENT '目标',
  ICON_CLASS VARCHAR(100) DEFAULT NULL COMMENT '图标',
  SORT INT(11) NOT NULL COMMENT '排序（升序）',
  LEVEL INT(11) NOT NULL COMMENT '层级从0开始',
  TYPE INT(11) NOT NULL COMMENT '菜单类型',
  QUICK_MENU VARCHAR(100) DEFAULT NULL,
  TOP_MENU VARCHAR(100) DEFAULT NULL,
  IS_SHOW CHAR(1) NOT NULL COMMENT '是否在菜单中显示',
  IS_ACTIVITI CHAR(1) DEFAULT NULL COMMENT '是否同步工作流',
  PERMISSION VARCHAR(200) DEFAULT NULL COMMENT '权限标识',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='菜单表';

-- ----------------------------
-- 区域
-- ----------------------------
DROP TABLE IF EXISTS SYS_AREA;
CREATE TABLE SYS_AREA (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  PARENT_ID VARCHAR(64) NOT NULL COMMENT '父级编号',
  PARENT_IDS VARCHAR(2000) NOT NULL COMMENT '所有父级编号',
  CODE VARCHAR(100) DEFAULT NULL COMMENT '区域编码',
  NAME VARCHAR(100) NOT NULL COMMENT '区域名称',
  LEVEL TINYINT(4) DEFAULT NULL,
  TYPE CHAR(1) DEFAULT NULL COMMENT '区域类型',
  PATH VARCHAR(2000) DEFAULT NULL,
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='区域表';

-- ----------------------------
-- 组织结构
-- ----------------------------
DROP TABLE IF EXISTS SYS_OFFICE;
CREATE TABLE SYS_OFFICE (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  PARENT_ID VARCHAR(64) NOT NULL COMMENT '父级编号',
  PARENT_IDS VARCHAR(2000) NOT NULL COMMENT '所有父级编号',
  AREA_ID VARCHAR(64) NOT NULL COMMENT '归属区域',
  CODE VARCHAR(100) DEFAULT NULL COMMENT '区域编码',
  NAME VARCHAR(100) NOT NULL COMMENT '机构名称',
  TYPE CHAR(1) NOT NULL COMMENT '机构类型',
  ADDRESS VARCHAR(255) DEFAULT NULL COMMENT '联系地址',
  ZIP_CODE VARCHAR(100) DEFAULT NULL COMMENT '邮政编码',
  MASTER VARCHAR(100) DEFAULT NULL COMMENT '负责人',
  PHONE VARCHAR(200) DEFAULT NULL COMMENT '电话',
  FAX VARCHAR(200) DEFAULT NULL COMMENT '传真',
  EMAIL VARCHAR(200) DEFAULT NULL COMMENT '邮箱',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  LEVEL TINYINT(4) DEFAULT NULL,
  PATH VARCHAR(2000) DEFAULT NULL,
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='机构表';

-- ----------------------------
-- 系统角色
-- ----------------------------
DROP TABLE IF EXISTS SYS_ROLE;
CREATE TABLE SYS_ROLE (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  OFFICE_ID VARCHAR(64) DEFAULT NULL COMMENT '归属机构',
  NAME VARCHAR(100) NOT NULL COMMENT '角色名称',
  DATA_SCOPE CHAR(1) DEFAULT NULL COMMENT '数据范围',
  CODE VARCHAR(30) DEFAULT NULL,
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='角色表';

-- ----------------------------
-- 角色-菜单
-- ----------------------------
DROP TABLE IF EXISTS SYS_ROLE_MENU;
CREATE TABLE SYS_ROLE_MENU (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  ROLE_ID VARCHAR(64) NOT NULL COMMENT '角色编号',
  MENU_ID VARCHAR(64) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='角色-菜单';

-- ----------------------------
-- 系统用户角色
-- ----------------------------
DROP TABLE IF EXISTS SYS_ROLE_USER;
CREATE TABLE SYS_ROLE_USER (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  USER_ID VARCHAR(64) NOT NULL COMMENT '用户编号',
  ROLE_ID VARCHAR(64) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='用户-角色';

-- ----------------------------
-- 系统用户组
-- ----------------------------
DROP TABLE IF EXISTS SYS_GROUP;
CREATE TABLE SYS_GROUP (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  NAME VARCHAR(100) NOT NULL COMMENT '组名',
  CODE VARCHAR(100) DEFAULT NULL COMMENT '组编码',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='用户表';

-- ----------------------------
-- 系统用户组角色
-- ----------------------------
DROP TABLE IF EXISTS SYS_GROUP_ROLE;
CREATE TABLE SYS_GROUP_ROLE (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  GROUP_ID VARCHAR(64) NOT NULL COMMENT '用户编号',
  ROLE_ID VARCHAR(64) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='用户组-角色';

-- ----------------------------
-- 系统用户组用户
-- ----------------------------
DROP TABLE IF EXISTS SYS_GROUP_USER;
CREATE TABLE SYS_GROUP_USER (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  GROUP_ID VARCHAR(64) NOT NULL COMMENT '用户组编号',
  USER_ID VARCHAR(64) NOT NULL COMMENT '用户编号',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='用户组-用户';

-- ----------------------------
-- 系统用户
-- ----------------------------
DROP TABLE IF EXISTS SYS_USER;
CREATE TABLE SYS_USER (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  OFFICE_ID VARCHAR(64) NOT NULL COMMENT '归属部门',
  LOGIN_NAME VARCHAR(100) NOT NULL COMMENT '登录名',
  PASSWORD VARCHAR(200) DEFAULT NULL COMMENT '密码',
  NO VARCHAR(100) DEFAULT NULL COMMENT '工号',
  NAME VARCHAR(100) NOT NULL COMMENT '姓名',
  EMAIL VARCHAR(200) DEFAULT NULL COMMENT '邮箱',
  PHONE VARCHAR(200) DEFAULT NULL COMMENT '电话',
  MOBILE VARCHAR(200) DEFAULT NULL COMMENT '手机',
  BIRTHDAY DATE DEFAULT NULL,
  ADDRESS VARCHAR(255) DEFAULT NULL,
  DUTY VARCHAR(255) DEFAULT NULL,
  LOGIN_IP VARCHAR(100) DEFAULT NULL COMMENT '最后登陆IP：待定',
  LOGIN_DATE DATETIME DEFAULT NULL COMMENT '最后登陆时间：待定',
  STATUS TINYINT(4) DEFAULT '1' COMMENT '用户状态',
  ZIP_CODE VARCHAR(20) DEFAULT NULL COMMENT '邮编',
  NICK_NAME VARCHAR(100) DEFAULT NULL COMMENT '昵称',
  ID_CARD_NO VARCHAR(80) DEFAULT NULL COMMENT '身份证号',
  SEX CHAR(1) DEFAULT NULL COMMENT '性别',
  PROVINCE VARCHAR(100) DEFAULT NULL COMMENT '省',
  CITY VARCHAR(100) DEFAULT NULL COMMENT '市',
  USER_TYPE VARCHAR(20) DEFAULT NULL COMMENT '用户类型',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='用户表';

-- ----------------------------
-- 系统任务
-- ----------------------------
DROP TABLE IF EXISTS SYS_TASK;
CREATE TABLE SYS_TASK (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  NAME VARCHAR(64) NOT NULL COMMENT '任务名称',
  TYPE VARCHAR(15) NOT NULL COMMENT '任务类型',
  CRON_EXPRESSION VARCHAR(100) DEFAULT NULL COMMENT '调度表达式',
  ALLOW_EXECUTE_COUNT INT(11) DEFAULT NULL COMMENT '允许执行的次数',
  YET_EXECUTE_COUNT INT(11) DEFAULT '0' COMMENT '已经执行的次数',
  FAIL_EXECUTE_COUNT INT(11) DEFAULT '0' COMMENT '执行失败的次数',
  PRE_EXECUTE_TIME DATETIME DEFAULT NULL COMMENT '上一次执行时间',
  NEXT_EXECUTE_TIME DATETIME DEFAULT NULL COMMENT '预计下一次执行时间',
  BUSINESS_OBJECT CHAR(200) DEFAULT NULL COMMENT '业务对象',
  BUSINESS_OBJECT_NAME VARCHAR(200) DEFAULT NULL COMMENT '业务对象名称',
  TASK_STATUS CHAR(10) DEFAULT '0' COMMENT '任务状态',
  MANUAL_OPERATION INT(11) DEFAULT NULL COMMENT '手动执行次数',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='任务表';


-- ----------------------------
-- 数据字典
-- ----------------------------
DROP TABLE IF EXISTS SYS_DICT;
CREATE TABLE SYS_DICT (
  ID VARCHAR(64) NOT NULL COMMENT '编号',
  LABEL VARCHAR(200) NOT NULL COMMENT '标签名',
  CODE VARCHAR(200) NOT NULL COMMENT '字典编号 --KEY',
  VALUE VARCHAR(1000) NOT NULL COMMENT '数据值',
  TYPE VARCHAR(100) NOT NULL COMMENT '类型',
  LEVEL TINYINT(4) DEFAULT NULL,
  ENCRYPT CHAR(1) DEFAULT NULL COMMENT '是否MD5加密',
  PARENT_ID VARCHAR(64) NOT NULL COMMENT '编号',
  PARENT_IDS VARCHAR(2000) NOT NULL COMMENT '编号',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  UPDATE_ID VARCHAR(64) DEFAULT NULL COMMENT '更新者',
  UPDATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '更新者',
  UPDATE_DATE DATETIME DEFAULT NULL COMMENT '更新时间',
  REMARKS VARCHAR(255) DEFAULT NULL COMMENT '备注信息',
  DEL_FLAG CHAR(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='字典表';

-- ----------------------------
-- Excel 模版
-- ----------------------------
DROP TABLE IF EXISTS SYS_EXCEL_TEMPLATE;
CREATE TABLE SYS_EXCEL_TEMPLATE (
  ID VARCHAR(64) NOT NULL,
  NAME VARCHAR(200) DEFAULT NULL,
  TYPE VARCHAR(200) DEFAULT NULL,
  TARGET_CLASS VARCHAR(128) DEFAULT NULL,
  START_ROW TINYINT(4) DEFAULT NULL,
  CREATE_ID VARCHAR(64) DEFAULT NULL,
  CREATE_NAME VARCHAR(100) DEFAULT NULL,
  CREATE_DATE DATETIME DEFAULT NULL,
  EXTEND_ATTR VARCHAR(200) DEFAULT NULL,
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='Excel 模版';

-- ----------------------------
-- Excel 模版项
-- ----------------------------
DROP TABLE IF EXISTS SYS_EXCEL_ITEM;
CREATE TABLE SYS_EXCEL_ITEM (
  ID VARCHAR(64) NOT NULL,
  NAME VARCHAR(100) DEFAULT NULL,
  TEMPLATE_ID VARCHAR(64) DEFAULT NULL,
  COLUMN_NAME VARCHAR(32) DEFAULT NULL,
  PROPERTY VARCHAR(64) DEFAULT NULL,
  DATA_TYPE VARCHAR(32) DEFAULT NULL,
  DATA_FORMAT VARCHAR(255) DEFAULT NULL,
  VERIFY_TYPE VARCHAR(32) DEFAULT NULL,
  VERIFY_FORMAT VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='Excel 模版项';

-- ----------------------------
-- 系统日志表
-- ----------------------------
DROP TABLE IF EXISTS SYS_LOG;
CREATE TABLE SYS_LOG (
  ID VARCHAR(64) NOT NULL COMMENT '编号（默认是UUID）',
  TYPE CHAR(1) DEFAULT '1' COMMENT '日志类型',
  CREATE_ID VARCHAR(64) DEFAULT NULL COMMENT '创建者',
  CREATE_NAME VARCHAR(100) DEFAULT NULL COMMENT '创建者',
  CREATE_DATE DATETIME DEFAULT NULL COMMENT '创建时间',
  REMOTE_ADDR VARCHAR(255) DEFAULT NULL COMMENT '操作IP地址',
  USER_AGENT VARCHAR(255) DEFAULT NULL COMMENT '用户代理',
  REQUEST_URI VARCHAR(255) DEFAULT NULL COMMENT '请求URI',
  METHOD VARCHAR(5) DEFAULT NULL COMMENT '操作方式',
  PARAMS TEXT COMMENT '操作提交的数据',
  EXCEPTION TEXT COMMENT '异常信息',
  PRIMARY KEY (ID)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='日志表';


-- ----------------------------
-- 系统主键表（适用单机版,或全局唯一码）
-- ----------------------------
DROP TABLE IF EXISTS SYS_ID_STORE;
CREATE TABLE SYS_ID_STORE (
  TABLE_NAME VARCHAR(100) NOT NULL,
  MIN_VALUE DECIMAL(20,0) NOT NULL,
  CURRENT_VALUE DECIMAL(20,0) NOT NULL,
  MAX_VALUE DECIMAL(20,0) NOT NULL,
  STEP DECIMAL(4,0) NOT NULL,
  REMARK VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (TABLE_NAME)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='系统主键表';

-- ----------------------------
-- 工具表
-- ----------------------------
DROP TABLE IF EXISTS SYS_DUAL;
CREATE TABLE SYS_DUAL (
  X CHAR(1) NOT NULL DEFAULT 'X' COMMENT '编号',
  PRIMARY KEY (X)
) ENGINE=INNODB DEFAULT CHARSET=UTF8 COMMENT='系统表';

-- ----------------------------
-- 函数 F_NEXT_VALUE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_NEXT_VALUE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `F_NEXT_VALUE`(`TABLENAME` VARCHAR(50)) RETURNS varchar(50) CHARSET utf8
BEGIN
 DECLARE V_SEQ_STEP VARCHAR (25);
 UPDATE SYS_ID_STORE SET CURRENT_VALUE = LAST_INSERT_ID(GREATEST(CURRENT_VALUE,MIN_VALUE) + STEP) WHERE TABLE_NAME = TABLENAME;
    SET V_SEQ_STEP = CONCAT(LAST_INSERT_ID(),'-'); 
    SET V_SEQ_STEP = CONCAT(V_SEQ_STEP,(SELECT STEP FROM SYS_ID_STORE WHERE TABLE_NAME = TABLENAME));
 RETURN V_SEQ_STEP;
END
;;
DELIMITER ;

-- ----------------------------
-- 初始化数据(基础数据)
-- ----------------------------
INSERT INTO SYS_MENU VALUES ('0', '-1', '-1,', '系统菜单', null, null, null, '0', '0', '1', null, null, '0', '0', null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_AREA VALUES ('0', '-1', '-1,', '000', '中国', '1', '1', '/中国', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_OFFICE VALUES ('0', '-1', '-1,', '0', '000', '组织结构', '1', null, null, null,null, null, null, null, null, null, null, null, null, null, '0', '1', '/组织结构');
INSERT INTO SYS_USER VALUES ('0', '0', 'SuperAdmin', '940e2c8569a39cc8c798e4df18a32384fa365e412f6c2c95a782e140', null, '超级管理员', null, null, null, null, null, null, null, null, '4', null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_DICT VALUES ('0', '系统配置', 'SYS_CONFIG', 'SYS_CONFIG', 'SYS_CONFIG', '1', null, '-1', '-1,', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_DUAL VALUES ('X');

-- ----------------------------
-- 初始化数据(基础数据)
-- ----------------------------
INSERT INTO SYS_MENU VALUES ('1', '0', '-1,0,', '系统管理', null, null, 'icon-th-large', '1', '1', '1', 'icon-star', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('2', '1', '-1,0,1,', '菜单管理', '/admin/system/menu/initList', null, 'icon-qrcode', '1', '2', '2', 'addressbook', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('3', '1', '-1,0,1,', '区域管理', '/admin/system/area/initList', null, 'icon-qrcode', '2', '2', '2', 'world', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('4', '1', '-1,0,1,', '组织管理', '/admin/system/office/initList', null, 'icon-qrcode', '3', '2', '2', 'multi-agents', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('5', '1', '-1,0,1,', '权限管理', '/admin/system/role/initList', null, 'icon-qrcode', '4', '2', '2', 'shield', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('6', '1', '-1,0,1,', '用户组管理', '/admin/system/group/initList', null, 'icon-qrcode', '5', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('7', '1', '-1,0,1,', '用户管理', '/admin/system/user/initList', null, 'icon-qrcode', '5', '2', '2', 'agent', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('8', '1', '-1,0,1,', '参数配置', '/admin/system/dict/initList', null, 'icon-qrcode', '8', '2', '2', 'processing', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('9', '1', '-1,0,1,', '任务管理', '/admin/system/task/initList', null, 'icon-qrcode', '7', '2', '2', 'edit', null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('10', '1', '-1,0,1,', '模板配置', '/admin/system/excel/initList', null, 'icon-qrcode', '9', '2', '2', 'configuration', null, '1', null, null, null, null, null, null, null, null, null, '0');

INSERT INTO SYS_MENU VALUES ('11', '0', '-1,0,', '个人信息', '/system/user/selfInfo', null, null, '99', '1', '2', null, null, '0', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('12', '11', '-1,0,11,', '修改密码', null, null, null, '1', '2', '3', null, null, '1', null, 'selfinfo:md:password', null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('13', '11', '-1,0,11,', '修改角色', null, null, null, '2', '2', '3', null, null, '1', null, 'selfinfo:md:role', null, null, null, null, null, null, null, '0');

INSERT INTO SYS_MENU VALUES ('14', '0', '-1,0,', '系统维护', null, null, 'icon-qrcode', '2', '1', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('15', '14', '-1,0,14,', '系统日志', '/admin/system/log/initList', null, 'icon-qrcode', '1', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('16', '14', '-1,0,14,', '在线用户', '/admin/system/online/initList', null, 'icon-qrcode', '2', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('17', '14', '-1,0,14,', '统计分析', null, null, 'icon-qrcode', '99', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('18', '14', '-1,0,14,', '缓存监控', '/admin/system/cache/monitor', null, 'icon-qrcode', '3', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('19', '14', '-1,0,14,', '数据库监控', null, null, 'icon-qrcode', '4', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');


INSERT INTO SYS_MENU VALUES ('20', '0', '-1,0,', '微信平台', null, null, 'icon-th-large', '4', '1', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('21', '20', '-1,0,20,', '自定义菜单', '/admin/wechat/menu/list', null, 'icon-qrcode', '4', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('22', '20', '-1,0,20,', '公众账号', '/admin/weixin/pubAccount/list', null, 'icon-qrcode', '1', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('23', '20', '-1,0,20,', '粉丝管理', null, null, null, '2', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('24', '20', '-1,0,20,', '素材管理', null, null, null, '99', '2', '1', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('25', '24', '-1,0,20,24,', '默认回复设置', null, null, null, '1', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('26', '24', '-1,0,20,24,', '文本回复', null, null, null, '2', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('27', '24', '-1,0,20,24,', '图文回复', null, null, null, '3', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('28', '24', '-1,0,20,24,', 'LBS回复', null, null, null, '4', '3', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');
INSERT INTO SYS_MENU VALUES ('29', '20', '-1,0,20,', '群发消息', null, null, null, '3', '2', '2', null, null, '1', null, null, null, null, null, null, null, null, null, '0');

